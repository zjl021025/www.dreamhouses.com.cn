<?php

namespace app\command;

use app\appapi\controller\Autorun;
use think\console\Command;
use think\console\Input;
use think\console\Output;

class ZxjzAutoRun extends Command
{
    protected function configure()
    {
        // 指令配置
        $this->setName('ZxjzAutoRun')
            ->setDescription('the ZxjzAutoRun command');
    }

    protected function execute(Input $input, Output $output)
    {
    	// 指令输出
        $autorun = new Autorun();
        $autorun->zxjznotify();
    }
}
