<?php

namespace app\command;

use Qiniu\Auth;
use Qiniu\Storage\UploadManager;

class uploadQiniu
{
    //file 文件
    //filename 存储在七牛云的目录
    public static function upload($file){
        $accessKey = config('qiniu.accessKey');
        $secretKey = config('qiniu.secretKey');
        $auth = new Auth($accessKey,$secretKey);
        $bucket = config('qiniu.bucket');
        $uploadManger = new UploadManager();
        $key = 'image';
        $token = $auth->uploadToken($bucket);
        $fileUrl = $uploadManger->putFile($token,$key,$file);
        if($fileUrl != null){
            return '上传失败';
        }else{
            return $fileUrl;
        }
    }
    /**
     * 获取外链
     */
    public static function geturl($fileUrl){
        $accessKey = config('qiniu.accessKey');
        $secretKey = config('qiniu.secretKey');
        $auth = new Auth($accessKey,$secretKey);
        $bucket = config('qiniu.bucket');
        $domain = config('qiniu.domain');
        $baseUrl = 'http://'.$domain.'/'.$fileUrl;
        $signedUrl = $auth->privateDownloadUrl($baseUrl);
        return $signedUrl;
    }
}