<?php
//系统菜单
return array(
    'system'=>array('name'=>'首页','child'=>array(
        array('name' => '首页','icon'=>1,'child' => array(
            array('name' => '系统首页', 'act'=>'welcome', 'op'=>'Index'),
        )),
        array('name' => '用户管理','icon'=>2,'child' => array(
            array('name' => '用户列表', 'act'=>'userList', 'op'=>'User'),
            array('name' => '认证审核', 'act'=>'applyList', 'op'=>'User'),
            array('name' => '房屋信息', 'act'=>'houseList', 'op'=>'User'),
        )),
        array('name' => '房屋管理','icon'=>3,'child' => array(
            array('name' => '楼盘列表', 'act'=>'estateList', 'op'=>'Estate'),
            array('name' => '用户作品', 'act'=>'userWork', 'op'=>'Estate'),
            array('name' => '用户家', 'act'=>'userHome', 'op'=>'Estate'),
            array('name' => '用户上传楼盘', 'act'=>'estateUpload', 'op'=>'Estate'),
        )),
        array('name' => '案例管理','icon'=>4,'child' => array(
            array('name' => '平台案例', 'act'=>'platformCase', 'op'=>'Dycase'),
            array('name' => '用户发布案例', 'act'=>'userPublishing', 'op'=>'Dycase'),
            array('name' => '评论管理', 'act'=>'evaluateList', 'op'=>'Evaluate'),
            array('name' => '风格管理', 'act'=>'styleManagement', 'op'=>'Dycase'),
            array('name' => '布局管理', 'act'=>'layoutManager', 'op'=>'Dycase'),
        )),
        array('name' => '装修贴士','icon'=>5,'child' => array(
            array('name'=>'贴士列表','act'=>'tipsList','op'=>'Tips'),
        )),
        array('name' => '智能装修','icon'=>6,'child' => array(
            array('name'=>'智能全屋装修','act'=>'intelligentDecoration','op'=>'Intelligent'),
        )),
        array('name' => '家居管理','icon'=>7,'child' => array(
            array('name'=>'软装分类','act'=>'softClassify','op'=>'Furniture'),
            array('name'=>'硬装分类','act'=>'hardmountClassify','op'=>'Furniture'),
            array('name'=>'硬装管理','act'=>'frockList','op'=>'Furniture'),
            array('name'=>'软装管理','act'=>'softList','op'=>'Furniture'),
        )),
        array('name' => '渲染','icon'=>8,'child' => array(
            array('name'=>'渲染设置','act'=>'renderSet','op'=>'Scene'),
            array('name'=>'图片渲染','act'=>'pictureRender','op'=>'Scene'),
            array('name'=>'视频渲染','act'=>'mediaRender','op'=>'Scene'),
            array('name'=>'360渲染','act'=>'threeRender','op'=>'Scene'),
        )),
        array('name' => '内容管理','icon'=>9,'child'=>array(
            array('name' => '方案','act' => 'schemeList','op' => 'Dynamic'),
            array('name' => '视频','act' => 'videoList','op' => 'Dynamic'),
            array('name' => '图文','act' => 'imagetextList','op' => 'Dynamic'),
            array('name' => '设计师','act' => 'designerList','op' => 'User'),
        )),
        array('name' => '推荐管理','icon'=>10,'child'=>array(
            array('name' => '内容推荐','act' => 'homeList','op' => 'Dynamic'),
        )),
        array('name' => '榜单管理','icon'=>11,'child'=>array(
            array('name' => '方案榜单','act' => 'billboardList','op' => 'Dynamic'),
            array('name' => '家居榜单','act' => 'billboardList','op' => 'Furniture'),
            array('name' => '设计师榜单','act' => 'billboardList','op' => 'User'),
        )),
        array('name' => '评论管理','icon'=>1,'child' => array(
            array('name'=>'评论列表','act'=>'evaluateList','op'=>'Evaluate'),
        )),
        array('name' => '选项配置','icon'=>3,'child' => array(
            array('name'=>'设计经验','act'=>'expeList','op'=>'Label'),
            array('name'=>'设计风格','act'=>'styleList','op'=>'Label'),
            array('name'=>'设计费用','act'=>'priceList','op'=>'Label'),
            array('name'=>'户型类型','act'=>'housetypeList','op'=>'Label'),
            array('name'=>'房间类型','act'=>'roomList','op'=>'Label'),
            array('name'=>'预算类型','act'=>'budgetList','op'=>'Label'),
            array('name'=>'面积类型','act'=>'areaList','op'=>'Label'),
        )),
        array('name' => '其他信息','icon'=>4,'child' => array(
            array('name'=>'协议','act'=>'agreement','op'=>'Article'),
            array('name'=>'系统设置','act'=>'index','op'=>'System'),
            array('name'=>'APP引导','act'=>'index','op'=>'Guide'),
        )),
        array('name' => '消息管理','icon'=>5,'child' => array(
            array('name'=>'消息列表','act'=>'messageList','op'=>'Message'),
        )),
        array('name' => '权限设置','icon'=>6,'child'=>array(
            array('name' => '管理员列表', 'act'=>'index', 'op'=>'Admin'),
            array('name' => '角色管理', 'act'=>'role', 'op'=>'Admin'),
            array('name' => '管理员日志', 'act'=>'log', 'op'=>'Admin'),
        )),
    )),
);
