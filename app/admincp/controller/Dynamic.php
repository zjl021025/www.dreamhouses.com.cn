<?php

namespace app\admincp\controller;

use app\admincp\model\UserDynamic;
use think\facade\View;
use think\facade\Db;
use think\Page;

class Dynamic extends Base
{
    //列表
    public function dynamicList($typeid)
    {
        $UserDynamic = new UserDynamic();
        $info = $UserDynamic->getList($typeid);
        $list = $info['list'];
        $pager = $info['pager'];
        $show = $info['show'];
        $condition = $info['condition'];
        View::assign('list', $list);// 赋值数据集
        View::assign('pager', $pager);// 赋值分页输出
        View::assign('show', $show);// 赋值分页输出
        View::assign('condition', $condition);
        return View::fetch();
    }

    //详情
    public function dynamic($typeid)
    {
        $info = [];
        $dynamic_id = input('get.dynamic_id/d');
        if ($dynamic_id > 0) {
            $info = Db::name('user_dynamic')->where(['dynamic_id'=>$dynamic_id])->find();
            $info['add_time'] = get_date($info['add_time'],'Y-m-d H:i:s');
            if($typeid!=2){
                $info['images'] = explode(',',$info['images']);
            }
            if($typeid==3){
                $over_images = explode(',',$info['over_images']);
                $room = [];
                $room[] = [
                    'images'=>$over_images,
                    'content'=>$info['over_content'],
                    'room_name'=>"总览",
                ];
                $dynamic_room = Db::name('user_dynamic_room')->where(['scheme_id'=>$info['dynamic_id']])
                    ->field('room_id,content,images')
                    ->order('id asc')
                    ->select()->toArray();
                foreach ($dynamic_room as $value){
                    $images = explode(',',$value['images']);
                    $room[] = [
                        'images'=>$images,
                        'content'=>$value['content'],
                        'room_name'=>get_label_name($value['room_id']),
                    ];
                }
                $info['room'] = $room;
            }
        }
        View::assign('info', $info);
        return View::fetch();
    }

    //方案列表
    public function schemeList()
    {
        return $this->dynamicList(3);
    }

    //方案详情
    public function scheme()
    {
        return $this->dynamic(3);
    }

    //视频列表
    public function videoList()
    {
        return $this->dynamicList(2);
    }

    //视频详情
    public function video()
    {
        return $this->dynamic(2);
    }

    //图文列表
    public function imagetextList()
    {
        return $this->dynamicList(1);
    }

    //图文详情
    public function imagetext()
    {
        return $this->dynamic(1);
    }

    public function delimagetext()
    {
        return $this->del(1);
    }

    public function delvideo()
    {
        return $this->del(2);
    }

    public function delscheme()
    {
        return $this->del(3);
    }

    protected function del($typeid)
    {
        $ids = input('ids');
        $res = Db::name('user_dynamic')->whereIn('dynamic_id',$ids)->delete();
        if(!$res){
            return dyajaxReturn(0, '操作失败');
        }
        if($typeid==3){
            Db::name('user_dynamic')->whereIn('scheme_id',$ids)->save(['scheme_id'=>0]);
        }
        switch ($typeid){
            case '1':
                $log_info = "删除图文:". $ids;
                $dyurl = url('Dynamic/imagetextList');
                break;
            case '2':
                $log_info = "删除视频:". $ids;
                $dyurl = url('Dynamic/videoList');
                break;
            default:
                $log_info = "删除方案:". $ids;
                $dyurl = url('Dynamic/schemeList');
                break;
        }
        adminLog($log_info);
        return dyajaxReturn(1, '操作成功',[],$dyurl);
    }

    public function homeList()
    {
        $keywords = trim(input('keywords'));
        $condition['keywords'] = $keywords;
        $field = 'dynamic_id,title,content,add_time,is_home,typeid,home_sort';
        $where['is_home'] = 1;
        $where['is_show'] = 1;
        if($keywords){
            $where['title|content'] = ['like','%'.$keywords.'%'];
        }
        $count = Db::name('UserDynamic')->where($where)->count();// 查询满足要求的总记录数
        $pager = new Page($count, $this->page_size);// 实例化分页类 传入总记录数和每页显示的记录数
        $list = Db::name('UserDynamic')->where($where)
            ->field($field)
            ->order('home_sort asc,dynamic_id desc')
            ->limit($pager->firstRow, $pager->listRows)
            ->select()->toArray();
        $type = [
            '1'=>'图文',
            '2'=>'视频',
            '3'=>'方案',
        ];
        foreach ($list as &$value){
            $value['add_time'] = get_date($value['add_time']);
            $value['type_name'] = $type[$value['typeid']];
            if($value['typeid']==2){
                $value['title'] = $value['content'];
            }
        }
        $show = $pager->show();// 分页显示输出
        View::assign('list', $list);// 赋值数据集
        View::assign('pager', $pager);// 赋值分页输出
        View::assign('show', $show);// 赋值分页输出
        View::assign('condition', $condition);
        return View::fetch();
    }

    public function billboardList()
    {
        $keywords = trim(input('keywords'));
        $condition['keywords'] = $keywords;
        $field = 'dynamic_id,title,add_time,browse,virtually_browse';
        $where['typeid'] = 3;
        $where['is_show'] = 1;
        if($keywords){
            $where['title|content'] = ['like','%'.$keywords.'%'];
        }
        $count = Db::name('UserDynamic')->where($where)->count();// 查询满足要求的总记录数
        $pager = new Page($count, $this->page_size);// 实例化分页类 传入总记录数和每页显示的记录数
        $list = Db::name('UserDynamic')->where($where)
            ->field($field)
            ->orderRaw('virtually_browse+browse desc,dynamic_id desc')
            ->limit($pager->firstRow, $pager->listRows)
            ->select()->toArray();
        foreach ($list as &$value){
            $value['add_time'] = get_date($value['add_time']);
        }
        $show = $pager->show();// 分页显示输出
        View::assign('list', $list);// 赋值数据集
        View::assign('pager', $pager);// 赋值分页输出
        View::assign('show', $show);// 赋值分页输出
        View::assign('condition', $condition);
        return View::fetch();
    }
}