<?php

namespace app\admincp\controller;

use app\admincp\model\FurnitureCategory;
use Overtrue\Pinyin\Pinyin;
use think\facade\Db;
use think\facade\Request;
use think\facade\View;
use think\Page;
use function adminLog;
use function dyajaxReturn;
use function get_date;
use function input;
use function url;
use function validate;

//家居
class Furniture extends Base
{
    //列表
    public function categoryList()
    {
        $keywords = trim(input('keywords'));
        $condition['keywords'] = $keywords;
        if(!empty($keywords)){
            $list = Db::name('furniture_category')
                ->where(['pid'=>0,'is_deleted'=>0,['name','like',"%$keywords%"]])
                ->select()->toArray();
        }else{
            $list = Db::name('furniture_category')
                ->where(['pid'=>0,'is_deleted'=>0])
                ->select()->toArray();
        }
        View::assign('list', $list);// 赋值数据集
        View::assign('condition', $condition);
        return View::fetch();
    }

    /* 详情信息 */
    public function category()
    {
        $act = "add";
        $info = [];
        $id = input('get.id/d');
        if ($id > 0) {
            $info = Db::name('furniture_category')->where(['id'=>$id])->find();
            $act = 'edit';
        }
        $category = Db::name('furniture_category')->where(['is_deleted'=>0,'pid'=>0])->field('id,name')->select()->toArray();
        View::assign('act', $act);
        View::assign('info', $info);
        View::assign('category', $category);
        return View::fetch();
    }

    /* 更新信息 */
    public function categoryHandleSoft()
    {
        $param = input('post.');
        $validate = validate('FurnitureCategory.'.$param['act']);
        if (!$validate->batch(true)->check($param)) {
            // 验证失败 输出错误信息
            $error = $validate->getError();
            $error_msg = array_values($error);
            return dyajaxReturn(0, $error_msg[0], $error);
        }
        $act = $param['act'];
        $typeid = 2;
        if(in_array($act,['add','edit'])){
            if(!$param['pid']>0){
                if(!$param['icon']){
                    return dyajaxReturn(0, '请上传图标');
                }
                if(!$param['image']){
                    return dyajaxReturn(0, '请上传封面图');
                }
            }else{
                $typeid = Db::name('furniture_category')->where('id', $param['pid'])->value('typeid');
            }
            $data = [
                'name'=>$param['name'],
                'pid'=>$param['pid'],
                'folder'=>$param['folder'],
                'image'=>$param['image']?$param['image']:'',
                'icon'=>$param['icon']?$param['icon']:'',
                'typeid'=>$typeid,
                'is_show'=>$param['is_show'],
                'sort'=>$param['sort']!=''?$param['sort']:50,
            ];
            if ($act == 'add') {
                $r = Db::name('furniture_category')->insertGetId($data);
                $log_info = "新增家居分类:".$r;
            } elseif ($act == 'edit') {
                $r = Db::name('furniture_category')->where('id', $param['id'])->update($data);
                $log_info = "更新家居分类:". $param['id'];
            }
        }else{
            if ($act == 'del') {
                $r = Db::name('furniture_category')->where(['id' => $param['id']])->update(['is_deleted'=>1]);
                $log_info = "删除家居分类:". $param['id'];
            }
        }
        if (!$r) {
            return dyajaxReturn(0, '操作失败或未做修改');
        }
        adminLog($log_info);
        return dyajaxReturn(1, '操作成功');
    }

    //列表
    public function twocategoryList()
    {
        $pid = input('pid/d');
        $FurnitureCategory = new FurnitureCategory();
        $info = $FurnitureCategory->getList($pid);
        $list = $info['list'];
//        $pager = $info['pager'];
//        $show = $info['show'];
        $condition = $info['condition'];
        View::assign('list', $list);// 赋值数据集
//        View::assign('pager', $pager);// 赋值分页输出
//        View::assign('show', $show);// 赋值分页输出
        View::assign('condition', $condition);
        View::assign('pid', $pid);
        return View::fetch();
    }

    /* 详情信息 */
    public function twocategory()
    {
        $pid = input('pid/d');
        $act = "add";
        $info = [];
        $id = input('get.id/d');
        if ($id > 0) {
            $info = Db::name('furniture_category')->where(['id'=>$id])->find();
            $act = 'edit';
            $pid = $info['pid'];
        }
        $category = Db::name('furniture_category')->where(['is_deleted'=>0,'pid'=>0])->field('id,name')->select()->toArray();
        View::assign('act', $act);
        View::assign('info', $info);
        View::assign('category', $category);
        View::assign('pid', $pid);
        return View::fetch();
    }

    //工装
    public function frockList()
    {
        $Furniture = new \app\admincp\model\Furniture();
        $info = $Furniture->getList(1);
        $list = $info['list'];
        $pager = $info['pager'];
        $show = $info['show'];
        $condition = $info['condition'];
        $category = Db::name('furniture_category')->where(['is_deleted'=>0,'typeid'=>1,'pid'=>0])->field('id,name')->order('sort asc,id desc')->select();
        View::assign('category', $category);
        $twocategory = [];
        $threecategory = [];
        $cate_id = input('cate_id/d');
        if($cate_id){
            $twocategory = Db::name('furniture_category')->where(['is_deleted'=>0,'pid'=>$cate_id])->field('id,name')->order('sort asc,id desc')->select()->toArray();
            $threecategory = Db::name('furniture_category')->where(['is_deleted'=>0,'pid'=>$twocategory['id']])->field('id,name')->order('sort asc,id desc')->select()->toArray();
        }
        View::assign('twocategory', $twocategory);
        View::assign('threecategory', $threecategory);
        View::assign('list', $list);// 赋值数据集
        View::assign('pager', $pager);// 赋值分页输出
        View::assign('show', $show);// 赋值分页输出
        View::assign('condition', $condition);
        return View::fetch();
    }

    /* 工装详情信息 */
    public function frock()
    {
        $act = "add";
        $info = [];
        $id = input('get.id/d');
        $twocategory = [];
        if ($id > 0) {
            $info = Db::name('furniture')->where(['id'=>$id])->find();
            $act = 'edit';
            $info['size'] = json_decode($info['size'],true);
            $info['material'] = json_decode($info['material'],true);
            $twocategory = Db::name('furniture_category')->where(['is_deleted'=>0,'pid'=>$info['cate_id']])->field('id,name')->order('sort asc,id desc')->select();
            $threecategory = Db::name('furniture_category')->where(['is_deleted'=>0,'pid'=>$info['two_cate_id']])->field('id,name')->order('sort asc,id desc')->select();
            $info['images'] = $info['images']?explode(',',$info['images']):[];
            $attr =  Db::name('furniture_attr')->where('furniture_id', $id)->field('name,value')->order('id asc')->select()->toArray();
            $info['attr'] = $attr?$attr:[];
        }
        $category = Db::name('furniture_category')->where(['is_deleted'=>0,'typeid'=>1,'pid'=>0])->field('id,name')->order('sort asc,id desc')->select();
        View::assign('category', $category);
        View::assign('twocategory', $twocategory);
        View::assign('threecategory', $threecategory);
        View::assign('act', $act);
        View::assign('info', $info);
        return View::fetch();
    }

    /* 工装更新信息 */
    public function frockHandle()
    {
        $param = input('post.');
        $validate = validate('Furniture.frock:'.$param['act']);
        if (!$validate->batch(true)->check($param)) {
            // 验证失败 输出错误信息
            $error = $validate->getError();
            $error_msg = array_values($error);
            return dyajaxReturn(0, $error_msg[0], $error);
        }
        $act = $param['act'];
        if(in_array($act,['add','edit'])){
            foreach ($param['size'] as $k=>$v){
                $param['size'][$k]['size_name'] = $v['length'].'x'.$v['width'].'x'.$v['height'].'厘米';
            }
            $data = [
                'name'=>$param['name'],
                'brand_name'=>$param['brand_name'],
                'filename'=>$param['filename'],
                'image'=>$param['images'][0],
                'images'=>implode(',',$param['images']),
                'cate_id'=>$param['cate_id'],
                'two_cate_id'=>$param['two_cate_id'],
                'furniture_no'=>$param['furniture_no'],
                'price'=>$param['price'],
                'size'=>json_encode($param['size']),
                'material'=>json_encode($param['material']),
                'is_show'=>$param['is_show'],
                'sort'=>$param['sort']!=''?$param['sort']:50,
                'typeid'=>$param['typeid'],
                'ios_url' => $param['ios_url'],
                'Android_url' => $param['Android_url'],
                'pc_url'=>$param['pc_url'],
                'three_cate_id' => $param['three_cate_id']
            ];
            if ($act == 'add') {
                $r = Db::name('furniture')->insertGetId($data);
                $log_info = "新增工装:".$r;
                $furniture_id = $r;
            } elseif ($act == 'edit') {
                $r = Db::name('furniture')->where('id', $param['id'])->update($data);
                $log_info = "更新工装:". $param['id'];
                $furniture_id = $param['id'];
            }
            if($furniture_id && isset($param['attr'])){
                $attr = [];
                foreach ($param['attr'] as $v){
                    $attr[] = [
                        'name'=>$v['name'],
                        'value'=>$v['value'],
                        'furniture_id'=>$furniture_id,
                        'add_time'=>time(),
                    ];
                }
                Db::name('furniture_attr')->where('furniture_id', $furniture_id)->delete();
                Db::name('furniture_attr')->insertAll($attr);
                $r = true;
            }elseif($furniture_id){
                Db::name('furniture_attr')->where('furniture_id', $furniture_id)->delete();
                $r = true;
            }
        }else{
            if ($act == 'del') {
                $r = Db::name('furniture')->whereIn('id',$param['ids'])->update(['is_deleted'=>1]);
                Db::name('furniture_attr')->whereIn('furniture_id', $param['ids'])->delete();
                $log_info = "删除工装:". $param['ids'];
            }
        }
        if (!$r) {
            return dyajaxReturn(0, '操作失败或未做修改');
        }
        $dyurl = url('Furniture/frockList');
        adminLog($log_info);
        return dyajaxReturn(1, '操作成功',[],$dyurl);
    }

    //软装列表
    public function softList()
    {
        $Furniture = new \app\admincp\model\Furniture();
        $info = $Furniture->getList(2);
        $list = $info['list'];
        $pager = $info['pager'];
        $show = $info['show'];
        $condition = $info['condition'];
        $category = Db::name('furniture_category')->where(['is_deleted'=>0,'typeid'=>2,'pid'=>0])->field('id,name')->order('sort asc,id desc')->select();
        View::assign('category', $category);
        $twocategory = [];
        $threecategory = [];
        $cate_id = input('cate_id/d');
        if($cate_id){
            $twocategory = Db::name('furniture_category')->where(['is_deleted'=>0,'pid'=>$cate_id])->field('id,name')->order('sort asc,id desc')->select()->toArray();
            $threecategory = Db::name('furniture_category')->where(['is_deleted'=>0,'pid'=>$twocategory['id']])->field('id,name')->order('sort asc,id desc')->select()->toArray();
        }
        View::assign('twocategory', $twocategory);
        View::assign('threecategory', $threecategory);
        View::assign('list', $list);// 赋值数据集
        View::assign('pager', $pager);// 赋值分页输出
        View::assign('show', $show);// 赋值分页输出
        View::assign('condition', $condition);
        return View::fetch();
    }

    /* 软装详情信息 */
    public function soft()
    {
        $act = "add";
        $info = [];
        $id = input('get.id/d');
        $twocategory = [];
        if ($id > 0) {
            $info = Db::name('furniture')->where(['id'=>$id])->find();
            $act = 'edit';
            $info['size'] = json_decode($info['size'],true);
            $info['material'] = json_decode($info['material'],true);
            $twocategory = Db::name('furniture_category')->where(['is_deleted'=>0,'pid'=>$info['cate_id']])->field('id,name')->order('sort asc,id desc')->select();
            $threecategory = Db::name('furniture_category')->where(['is_deleted'=>0,'pid'=>$info['two_cate_id']])->field('id,name')->order('sort asc,id desc')->select();
            $info['images'] = $info['images']?explode(',',$info['images']):[];
            $attr =  Db::name('furniture_attr')->where('furniture_id', $id)->field('name,value')->order('id asc')->select()->toArray();
            $info['attr'] = $attr?$attr:[];
        }
        $category = Db::name('furniture_category')->where(['is_deleted'=>0,'typeid'=>2,'pid'=>0])->field('id,name')->order('sort asc,id desc')->select();
        View::assign('category', $category);
        View::assign('twocategory', $twocategory);
        View::assign('threecategory', $threecategory);
        View::assign('act', $act);
        View::assign('info', $info);
        return View::fetch();
    }

    /* 工装更新信息 */
    public function softHandle()
    {
        $param = input('post.');
        $validate = validate('Furniture.soft:'.$param['act']);
        if (!$validate->batch(true)->check($param)) {
            // 验证失败 输出错误信息
            $error = $validate->getError();
            $error_msg = array_values($error);
            return dyajaxReturn(0, $error_msg[0], $error);
        }
        $act = $param['act'];
        if(in_array($act,['add','edit'])){
            foreach ($param['size'] as $k=>$v){
                $param['size'][$k]['size_name'] = $v['length'].'x'.$v['width'].'x'.$v['height'].'厘米';
            }
            $data = [
                'name'=>$param['name'],
                'brand_name'=>$param['brand_name'],
                'filename'=>$param['filename'],
                'image'=>$param['images'][0],
                'images'=>implode(',',$param['images']),
                'cate_id'=>$param['cate_id'],
                'two_cate_id'=>$param['two_cate_id'],
                'furniture_no'=>$param['furniture_no'],
                'price'=>$param['price'],
                'size'=>json_encode($param['size']),
                'material'=>json_encode($param['material']),
                'is_show'=>$param['is_show'],
                'sort'=>$param['sort']!=''?$param['sort']:50,
                'buy_url'=>$param['buy_url']?$param['buy_url']:'',
                'typeid'=>$param['typeid'],
                'ios_url' => $param['ios_url'],
                'Android_url' => $param['Android_url'],
                'pc_url'=>$param['pc_url'],
                'three_cate_id' => $param['three_cate_id']
            ];
            $furniture_id = 0;
            if ($act == 'add') {
                $r = Db::name('furniture')->insertGetId($data);
                $log_info = "新增软装:".$r;
                $furniture_id = $r;
            } elseif ($act == 'edit') {
                $r = Db::name('furniture')->where('id', $param['id'])->update($data);
                $log_info = "更新软装:". $param['id'];
                $furniture_id = $param['id'];
            }
            if($furniture_id && isset($param['attr'])){
                $attr = [];
                foreach ($param['attr'] as $v){
                    $attr[] = [
                        'name'=>$v['name'],
                        'value'=>$v['value'],
                        'furniture_id'=>$furniture_id,
                        'add_time'=>time(),
                    ];
                }
                Db::name('furniture_attr')->where('furniture_id', $furniture_id)->delete();
                Db::name('furniture_attr')->insertAll($attr);
                $r = true;
            }elseif($furniture_id){
                Db::name('furniture_attr')->where('furniture_id', $furniture_id)->delete();
                $r = true;
            }
        }else{
            if ($act == 'del') {
                $r = Db::name('furniture')->whereIn('id',$param['ids'])->update(['is_deleted'=>1]);
                Db::name('furniture_attr')->whereIn('furniture_id', $param['ids'])->delete();
                $log_info = "删除软装:". $param['ids'];
            }
        }
        if (!$r) {
            return dyajaxReturn(0, '操作失败或未做修改');
        }
        adminLog($log_info);
        $dyurl = url('Furniture/softList');
        return dyajaxReturn(1, '操作成功',[],$dyurl);
    }

    public function getTwoCate()
    {
        $id = input('id/d',-1);
        $id = $id?$id:-1;
        $twocategory = Db::name('furniture_category')->where(['is_deleted'=>0,'pid'=>$id])->field('id,name')->order('sort asc,id desc')->select()->toArray();
        return dyajaxReturn(1, '获取成功',$twocategory);
    }
    public function getThreeCate()
    {
        $id = input('id/d',-1);
        $id = $id?$id:-1;
        $threecategory = Db::name('furniture_category')->where(['is_deleted'=>0,'pid'=>$id])->field('id,name')->order('sort asc,id desc')->select()->toArray();
        return dyajaxReturn(1, '获取成功',$threecategory);
    }

    public function getFilename()
    {
        $filename = "";
        $cate_id = input('cate_id/d');
        $two_cate_id = input('two_cate_id/d');
        $brand_name = input('brand_name/s');
        $pinyin = new Pinyin(); // 默认
        $cate_name = Db::name('furniture_category')->where('id',$cate_id)->value('name');
        $filename .= $pinyin->abbr($cate_name);
        $two_cate_name = Db::name('furniture_category')->where('id',$two_cate_id)->value('name');
        $filename .= '-'.$pinyin->abbr($two_cate_name);
        if($brand_name){
            $brand_name = str_replace(' ','',$brand_name);
            if(preg_match("/[\x7f-\xff]/",$brand_name)){
                $filename .= '-'.$pinyin->abbr($brand_name);
            }else{
                $filename .= '-'.strtolower($brand_name);
            }
        }
        return dyajaxReturn(1, '获取成功',['filename'=>$filename]);
    }

    public function billboardList()
    {
        $keywords = trim(input('keywords'));
        $furniture_no = trim(input('furniture_no'));
        $condition['keywords'] = $keywords;
        $field = 'id,name,add_time,browse,virtually_browse,furniture_no';
        $where['is_show'] = 1;
        if($keywords){
            $where['name'] = ['like','%'.$keywords.'%'];
        }
        if($furniture_no){
            $where['furniture_no'] = ['like','%'.$furniture_no.'%'];
        }
        $condition['furniture_no'] = $furniture_no;
        $count = Db::name('furniture')->where($where)->count();// 查询满足要求的总记录数
        $pager = new Page($count, $this->page_size);// 实例化分页类 传入总记录数和每页显示的记录数
        $list = Db::name('furniture')->where($where)
            ->field($field)
            ->orderRaw('virtually_browse+browse desc,sort asc,id desc')
            ->limit($pager->firstRow, $pager->listRows)
            ->select()->toArray();
        foreach ($list as &$value){
            $value['add_time'] = get_date($value['add_time']);
        }
        $show = $pager->show();// 分页显示输出
        View::assign('list', $list);// 赋值数据集
        View::assign('pager', $pager);// 赋值分页输出
        View::assign('show', $show);// 赋值分页输出
        View::assign('condition', $condition);
        return View::fetch();
    }
    //软装分类
    public function softClassify(){
        $name = Request::post('name');
        if(!empty($name)){
            $count = Db::name('furniture_category')->where(['typeid'=>2,'pid'=>0,'is_deleted'=>0,['name','like',"%$name%"]])->count();
            $pager = new Page($count,$this->page_size);
            $show = $pager->show();
            $furnitureData = Db::name('furniture_category')
                ->where(['typeid'=>2,'pid'=>0,'is_deleted'=>0,['name','like',"%$name%"]])
                ->order('sort','desc')
                ->limit($pager->firstRow, $pager->listRows)
                ->select()->toArray();
        }else{
            $count = Db::name('furniture_category')->where(['typeid'=>2,'pid'=>0,'is_deleted'=>0,])->count();
            $pager = new Page($count,$this->page_size);
            $show = $pager->show();
            $furnitureData = Db::name('furniture_category')
                ->where(['typeid'=>2,'pid'=>0,'is_deleted'=>0])
                ->order('sort','desc')
                ->limit($pager->firstRow, $pager->listRows)
                ->select()->toArray();
        }
        View::assign('furnitureData',$furnitureData);
        View::assign('pager',$pager);
        View::assign('show',$show);
        return View::fetch();
    }
    //添加软装一级分类
    public function softClassifyInsert(){
        return View::fetch();
    }
    //修改软装一级分类
    public function softClassifyUpdate(){
        $param = Request::param();
        $cateData = Db::name('furniture_category')->where('id',$param['id'])->find();
        View::assign('cateData',$cateData);
        return View::fetch();
    }

    /**
     * 修改软装一级分类
     * @return void
     */
    public function updateFurnitureCate(){
        $param = Request::param();
        Db::startTrans();
        try {
            $data = [
                'name' => $param['name'],
                'folder' => $param['folder'],
                'image' => $param['imageUrl'],
                'icon' => $param['iconUrl'],
                'sort' => $param['sort'],
                'is_show' => $param['is_show'],
            ];
            Db::name('furniture_category')->where('id',$param['id'])->update($data);
            Db::commit();
            return dyajaxReturn(1,'修改成功');
        }catch (\Exception $exception){
            Db::rollback();
            return dyajaxReturn(0,'修改失败');
        }
    }
    /**
     * 添加软装一级家具分类
     * @return void
     */
    public function insertFurnitureCate(){
        $param = Request::param();
        Db::startTrans();
        try {
            $data = [
                'name' => $param['name'],
                'folder' => $param['folder'],
                'image' => $param['imageUrl'],
                'icon' => $param['iconUrl'],
                'typeid' => 2,
                'add_time' => time(),
                'sort' => $param['sort'],
                'is_show' => $param['is_show'],
                'pid' => 0
            ];
            Db::name('furniture_category')->insert($data);
            Db::commit();
            return dyajaxReturn(1,'添加成功');
        }catch (\Exception $exception){
            Db::rollback();
            return dyajaxReturn(0,'添加失败');
        }
    }

    /**
     * 上传图片
     * @return void
     */
    public function uploadImage(){
        $file = \request()->file('file');
        $accesskey = config('qiniu.accessKey');
        $secretKey = config('qiniu.secretKey');
        $domain = config('qiniu.domain');
        $auth = new \Qiniu\Auth($accesskey,$secretKey);

        $qiniu_file = \think\facade\Filesystem::disk('qiniu')->putFile('image',$file);
        $baseUrl = 'http://'.$domain.'/'.$qiniu_file;
        $signUrl = $auth->privateDownloadUrl($baseUrl);
        return dyajaxReturn(1,'成功',$signUrl);
    }
    /**
     * 上传图片
     * @return void
     */
    public function uploadIcon(){
        $file = \request()->file('file');
        $accesskey = config('qiniu.accessKey');
        $secretKey = config('qiniu.secretKey');
        $domain = config('qiniu.domain');
        $auth = new \Qiniu\Auth($accesskey,$secretKey);

        $qiniu_file = \think\facade\Filesystem::disk('qiniu')->putFile('image',$file);
        $baseUrl = 'http://'.$domain.'/'.$qiniu_file;
        $signUrl = $auth->privateDownloadUrl($baseUrl);
        return dyajaxReturn(1,'成功',$signUrl);
    }
    //软装二级分类
    public function softClassifyTwo(){
        $pid = input('pid');
        $name = input('name');
        if(!empty($name)){
            $count = Db::name('furniture_category')->where(['typeid'=>2,'pid'=>$pid,'is_deleted'=>0,['name','like',"%$name%"]])->count();
            $pager = new Page($count,$this->page_size);
            $show = $pager->show();
            $furnitureData = Db::name('furniture_category')
                ->where(['typeid'=>2,'pid'=>$pid,'is_deleted'=>0,['name','like',"%$name%"]])
                ->order('sort','desc')
                ->limit($pager->firstRow, $pager->listRows)
                ->select()->toArray();
        }else{
            $count = Db::name('furniture_category')->where(['typeid'=>2,'pid'=>$pid,'is_deleted'=>0,])->count();
            $pager = new Page($count,$this->page_size);
            $show = $pager->show();
            $furnitureData = Db::name('furniture_category')
                ->where(['typeid'=>2,'pid'=>$pid,'is_deleted'=>0])
                ->order('sort','desc')
                ->limit($pager->firstRow, $pager->listRows)
                ->select()->toArray();
        }
        View::assign('furnitureData',$furnitureData);
        View::assign('pager',$pager);
        View::assign('show',$show);
        View::assign('pid',$pid);
        return View::fetch();
    }
    //添加软装二级分类
    public function softClassifyTwoInsert(){
        $param = Request::param();
        $twoCate = Db::name('furniture_category')->where('id',$param['pid'])->find();
        View::assign('twoCate',$twoCate);
        return View::fetch();
    }
    /**
     * 添加软装二级家具分类
     * @return void
     */
    public function insertTwoFurnitureCate(){
        $param = Request::param();
        Db::startTrans();
        try {
            $data = [
                'name' => $param['name'],
                'folder' => $param['folder'],
                'image' => $param['imageUrl'],
                'icon' => $param['iconUrl'],
                'typeid' => 2,
                'add_time' => time(),
                'sort' => $param['sort'],
                'is_show' => $param['is_show'],
                'pid' => $param['pid']
            ];
            Db::name('furniture_category')->insert($data);
            Db::commit();
            return dyajaxReturn(1,'添加成功');
        }catch (\Exception $exception){
            Db::rollback();
            return dyajaxReturn(0,'添加失败');
        }
    }
    //修改软装二级分类
    public function softClassifyTwoUpdate(){
        $param = Request::param();
        $twoCateData = Db::name('furniture_category')->where('id',$param['id'])->find();
        $twoCateData['pidName'] = Db::name('furniture_category')->where('id',$twoCateData['pid'])->value('name');
        View::assign('twoCateData',$twoCateData);
        return View::fetch();
    }
    /**
     * 修改软装二级分类
     * @return void
     */
    public function updateTwoFurnitureCate(){
        $param = Request::param();
        Db::startTrans();
        try {
            $data = [
                'name' => $param['name'],
                'folder' => $param['folder'],
                'image' => $param['imageUrl'],
                'icon' => $param['iconUrl'],
                'sort' => $param['sort'],
                'is_show' => $param['is_show'],
            ];
            Db::name('furniture_category')->where('id',$param['id'])->update($data);
            Db::commit();
            return dyajaxReturn(1,'修改成功');
        }catch (\Exception $exception){
            Db::rollback();
            return dyajaxReturn(0,'修改失败');
        }
    }
    //软装三级分类
    public function softClassifyThree(){
        $pid = input('pid');
        $name = input('name');
        if(!empty($name)){
            $count = Db::name('furniture_category')->where(['typeid'=>2,'pid'=>$pid,'is_deleted'=>0,['name','like',"%$name%"]])->count();
            $pager = new Page($count,$this->page_size);
            $show = $pager->show();
            $furnitureData = Db::name('furniture_category')
                ->where(['typeid'=>2,'pid'=>$pid,'is_deleted'=>0,['name','like',"%$name%"]])
                ->order('sort','desc')
                ->limit($pager->firstRow, $pager->listRows)
                ->select()->toArray();
        }else{
            $count = Db::name('furniture_category')->where(['typeid'=>2,'pid'=>$pid,'is_deleted'=>0,])->count();
            $pager = new Page($count,$this->page_size);
            $show = $pager->show();
            $furnitureData = Db::name('furniture_category')
                ->where(['typeid'=>2,'pid'=>$pid,'is_deleted'=>0])
                ->order('sort','desc')
                ->limit($pager->firstRow, $pager->listRows)
                ->select()->toArray();
        }
        View::assign('furnitureData',$furnitureData);
        View::assign('pager',$pager);
        View::assign('show',$show);
        View::assign('pid',$pid);
        return View::fetch();
    }
    //添加软装三级分类
    public function softClassifyThreeInsert(){
        $param = Request::param();
        $twoCate = Db::name('furniture_category')->where('id',$param['pid'])->find();
        $oneCate = Db::name('furniture_category')->where('id',$twoCate['pid'])->find();
        View::assign('twoCate',$twoCate);
        View::assign('oneCate',$oneCate);
        return View::fetch();
    }
    /**
     * 添加软装三级家具分类
     * @return void
     */
    public function insertThreeFurnitureCate(){
        $param = Request::param();
        Db::startTrans();
        try {
            $data = [
                'name' => $param['name'],
                'folder' => $param['folder'],
                'image' => $param['imageUrl'],
                'icon' => $param['iconUrl'],
                'typeid' => 2,
                'add_time' => time(),
                'sort' => $param['sort'],
                'is_show' => $param['is_show'],
                'pid' => $param['pid']
            ];
            Db::name('furniture_category')->insert($data);
            Db::commit();
            return dyajaxReturn(1,'添加成功');
        }catch (\Exception $exception){
            Db::rollback();
            return dyajaxReturn(0,'添加失败');
        }
    }
    //修改软装三级分类
    public function softClassifyThreeUpdate(){
        $param = Request::param();
        $threeCateData = Db::name('furniture_category')->where('id',$param['id'])->find();
        $lastData = Db::name('furniture_category')->where('id',$threeCateData['pid'])->find();
        $threeCateData['twoName'] = $lastData['name'];
        $threeCateData['oneName'] = Db::name('furniture_category')->where('id',$lastData['pid'])->value('name');
        View::assign('threeCateData',$threeCateData);
        return View::fetch();
    }

    /**
     * 修改三级软装分类
     * @return void
     */
    public function updateThreeFurnitureCate(){
        $param = Request::param();
        Db::startTrans();
        try {
            $data = [
                'name' => $param['name'],
                'folder' => $param['folder'],
                'image' => $param['imageUrl'],
                'icon' => $param['iconUrl'],
                'sort' => $param['sort'],
                'is_show' => $param['is_show'],
            ];
            Db::name('furniture_category')->where('id',$param['id'])->update($data);
            Db::commit();
            return dyajaxReturn(1,'修改成功');
        }catch (\Exception $exception){
            Db::rollback();
            return dyajaxReturn(0,'修改失败');
        }
    }
    //硬装分类
    public function hardmountClassify(){
        $name = Request::post('name');
        if(!empty($name)){
            $count = Db::name('furniture_category')->where(['typeid'=>1,'pid'=>0,'is_deleted'=>0,['name','like',"%$name%"]])->count();
            $pager = new Page($count,$this->page_size);
            $show = $pager->show();
            $furnitureData = Db::name('furniture_category')
                ->where(['typeid'=>1,'pid'=>0,'is_deleted'=>0,['name','like',"%$name%"]])
                ->order('sort','desc')
                ->limit($pager->firstRow, $pager->listRows)
                ->select()->toArray();
        }else{
            $count = Db::name('furniture_category')->where(['typeid'=>1,'pid'=>0,'is_deleted'=>0,])->count();
            $pager = new Page($count,$this->page_size);
            $show = $pager->show();
            $furnitureData = Db::name('furniture_category')
                ->where(['typeid'=>1,'pid'=>0,'is_deleted'=>0])
                ->order('sort','desc')
                ->limit($pager->firstRow, $pager->listRows)
                ->select()->toArray();
        }
        View::assign('furnitureData',$furnitureData);
        View::assign('pager',$pager);
        View::assign('show',$show);
        return View::fetch();
    }
    //添加硬装一级分类
    public function hardmountClassifyInsert(){
        return View::fetch();
    }

    /**
     * 添加一级硬装分类
     * @return void
     */
    public function insertHardmountFurnitureCate(){
        $param = Request::param();
        Db::startTrans();
        try {
            $data = [
                'name' => $param['name'],
                'folder' => $param['folder'],
                'image' => $param['imageUrl'],
                'icon' => $param['iconUrl'],
                'typeid' => 1,
                'add_time' => time(),
                'sort' => $param['sort'],
                'is_show' => $param['is_show'],
                'pid' => 0
            ];
            Db::name('furniture_category')->insert($data);
            Db::commit();
            return dyajaxReturn(1,'添加成功');
        }catch (\Exception $exception){
            Db::rollback();
            return dyajaxReturn(0,'添加失败');
        }
    }
    //修改硬装一级分类
    public function hardmountClassifyUpdate(){
        $param = Request::param();
        $cateData = Db::name('furniture_category')->where('id',$param['id'])->find();
        View::assign('cateData',$cateData);
        return View::fetch();
    }

    /**
     * 修改硬装一级分类
     * @return \think\response\Json
     */
    public function updateHardmountFurnitureCate(){
        $param = Request::param();
        Db::startTrans();
        try {
            $data = [
                'name' => $param['name'],
                'folder' => $param['folder'],
                'image' => $param['imageUrl'],
                'icon' => $param['iconUrl'],
                'sort' => $param['sort'],
                'is_show' => $param['is_show'],
            ];
            Db::name('furniture_category')->where('id',$param['id'])->update($data);
            Db::commit();
            return dyajaxReturn(1,'修改成功');
        }catch (\Exception $exception){
            Db::rollback();
            return dyajaxReturn(0,'修改失败');
        }
    }
    //硬装二级分类
    public function hardmountClassifyTwo(){
        $pid = input('pid');
        $name = input('name');
        if(!empty($name)){
            $count = Db::name('furniture_category')->where(['typeid'=>1,'pid'=>$pid,'is_deleted'=>0,['name','like',"%$name%"]])->count();
            $pager = new Page($count,$this->page_size);
            $show = $pager->show();
            $furnitureData = Db::name('furniture_category')
                ->where(['typeid'=>1,'pid'=>$pid,'is_deleted'=>0,['name','like',"%$name%"]])
                ->order('sort','desc')
                ->limit($pager->firstRow, $pager->listRows)
                ->select()->toArray();
        }else{
            $count = Db::name('furniture_category')->where(['typeid'=>1,'pid'=>$pid,'is_deleted'=>0,])->count();
            $pager = new Page($count,$this->page_size);
            $show = $pager->show();
            $furnitureData = Db::name('furniture_category')
                ->where(['typeid'=>1,'pid'=>$pid,'is_deleted'=>0])
                ->order('sort','desc')
                ->limit($pager->firstRow, $pager->listRows)
                ->select()->toArray();
        }
        View::assign('furnitureData',$furnitureData);
        View::assign('pager',$pager);
        View::assign('show',$show);
        View::assign('pid',$pid);
        return View::fetch();
    }
    //添加硬装二级分类
    public function hardmountClassifyTwoInsert(){
        $param = Request::param();
        $twoCate = Db::name('furniture_category')->where('id',$param['pid'])->find();
        View::assign('twoCate',$twoCate);
        return View::fetch();
    }

    /**
     * 添加硬装二级分类
     * @return void
     */
    public function insertTwoHardmountFurnitureCate(){
        $param = Request::param();
        Db::startTrans();
        try {
            $data = [
                'name' => $param['name'],
                'folder' => $param['folder'],
                'image' => $param['imageUrl'],
                'icon' => $param['iconUrl'],
                'typeid' => 1,
                'add_time' => time(),
                'sort' => $param['sort'],
                'is_show' => $param['is_show'],
                'pid' => $param['pid']
            ];
            Db::name('furniture_category')->insert($data);
            Db::commit();
            return dyajaxReturn(1,'添加成功');
        }catch (\Exception $exception){
            Db::rollback();
            return dyajaxReturn(0,'添加失败');
        }
    }
    //修改硬装二级分类
    public function hardmountClassifyTwoUpdate(){
        $param = Request::param();
        $twoCateData = Db::name('furniture_category')->where('id',$param['id'])->find();
        $twoCateData['pidName'] = Db::name('furniture_category')->where('id',$twoCateData['pid'])->value('name');
        View::assign('twoCateData',$twoCateData);
        return View::fetch();
    }

    /**
     * 修改硬装二级分类
     * @return void
     */
    public function updateTwoHardmountFurnitureCate(){
        $param = Request::param();
        Db::startTrans();
        try {
            $data = [
                'name' => $param['name'],
                'folder' => $param['folder'],
                'image' => $param['imageUrl'],
                'icon' => $param['iconUrl'],
                'sort' => $param['sort'],
                'is_show' => $param['is_show'],
            ];
            Db::name('furniture_category')->where('id',$param['id'])->update($data);
            Db::commit();
            return dyajaxReturn(1,'修改成功');
        }catch (\Exception $exception){
            Db::rollback();
            return dyajaxReturn(0,'修改失败');
        }
    }
    //硬装三级分类
    public function hardmountClassifyThree(){
        $pid = input('pid');
        $name = input('name');
        if(!empty($name)){
            $count = Db::name('furniture_category')->where(['typeid'=>1,'pid'=>$pid,'is_deleted'=>0,['name','like',"%$name%"]])->count();
            $pager = new Page($count,$this->page_size);
            $show = $pager->show();
            $furnitureData = Db::name('furniture_category')
                ->where(['typeid'=>1,'pid'=>$pid,'is_deleted'=>0,['name','like',"%$name%"]])
                ->order('sort','desc')
                ->limit($pager->firstRow, $pager->listRows)
                ->select()->toArray();
        }else{
            $count = Db::name('furniture_category')->where(['typeid'=>1,'pid'=>$pid,'is_deleted'=>0,])->count();
            $pager = new Page($count,$this->page_size);
            $show = $pager->show();
            $furnitureData = Db::name('furniture_category')
                ->where(['typeid'=>1,'pid'=>$pid,'is_deleted'=>0])
                ->order('sort','desc')
                ->limit($pager->firstRow, $pager->listRows)
                ->select()->toArray();
        }
        View::assign('furnitureData',$furnitureData);
        View::assign('pager',$pager);
        View::assign('show',$show);
        View::assign('pid',$pid);
        return View::fetch();
    }
    //添加硬装三级分类
    public function hardmountClassifyThreeInsert(){
        $param = Request::param();
        $twoCate = Db::name('furniture_category')->where('id',$param['pid'])->find();
        $oneCate = Db::name('furniture_category')->where('id',$twoCate['pid'])->find();
        View::assign('twoCate',$twoCate);
        View::assign('oneCate',$oneCate);
        return View::fetch();
    }

    /**
     *添加硬装三级分类
     * @return void
     */
    public function insertThreeHardmountFurnitureCate(){
        $param = Request::param();
        Db::startTrans();
        try {
            $data = [
                'name' => $param['name'],
                'folder' => $param['folder'],
                'image' => $param['imageUrl'],
                'icon' => $param['iconUrl'],
                'typeid' => 1,
                'add_time' => time(),
                'sort' => $param['sort'],
                'is_show' => $param['is_show'],
                'pid' => $param['pid']
            ];
            Db::name('furniture_category')->insert($data);
            Db::commit();
            return dyajaxReturn(1,'添加成功');
        }catch (\Exception $exception){
            Db::rollback();
            return dyajaxReturn(0,'添加失败');
        }
    }
    //修改硬装三级分类
    public function hardmountClassifyThreeUpdate(){
        $param = Request::param();
        $threeCateData = Db::name('furniture_category')->where('id',$param['id'])->find();
        $lastData = Db::name('furniture_category')->where('id',$threeCateData['pid'])->find();
        $threeCateData['twoName'] = $lastData['name'];
        $threeCateData['oneName'] = Db::name('furniture_category')->where('id',$lastData['pid'])->value('name');
        View::assign('threeCateData',$threeCateData);
        return View::fetch();
    }

    /**
     * 修改硬装三级分类
     * @return void
     */
    public function updateThreeHardmountFurnitureCate(){
        $param = Request::param();
        Db::startTrans();
        try {
            $data = [
                'name' => $param['name'],
                'folder' => $param['folder'],
                'image' => $param['imageUrl'],
                'icon' => $param['iconUrl'],
                'sort' => $param['sort'],
                'is_show' => $param['is_show'],
            ];
            Db::name('furniture_category')->where('id',$param['id'])->update($data);
            Db::commit();
            return dyajaxReturn(1,'修改成功');
        }catch (\Exception $exception){
            Db::rollback();
            return dyajaxReturn(0,'修改失败');
        }
    }

    /**
     * 删除家装分类
     * @return void
     */
    public function delFurnitureCate(){
        $param = Request::param();
        $data = Db::name('furniture_category')->where('pid',$param['id'])->select()->toArray();
        if(!empty($data)){
            return dyajaxReturn(0,'该分类下有其他分类数据');
        }
        $del = Db::name('furniture_category')->where('id',$param['id'])->update(['is_deleted'=>1,'is_show'=>0]);
        if(!$del){
            return dyajaxReturn(0,'删除失败');
        }
        return dyajaxReturn(1,'删除成功');
    }
}
