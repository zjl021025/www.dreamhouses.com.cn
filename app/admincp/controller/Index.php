<?php


namespace app\admincp\controller;
use think\facade\View;
use think\facade\Db;
class Index extends Base {

    public function index(){
        $admin_info = getAdminInfo(session('admin_id'));
        View::assign('admin_info',$admin_info);             
        View::assign('menu',getMenuArr());
        return View::fetch();
    }
   
    public function welcome(){
        $day = date("Y-m-d");
        $today = strtotime($day);
        $Furniture = new \app\admincp\model\Furniture();
        $info = $Furniture->getList(1);
        $count['frock_count'] = $info['count'];
        $info = $Furniture->getList(2);
        $count['soft_count'] = $info['count'];
        View::assign('count',$count);
        View::assign('url',[]);
        return View::fetch();
    }
    
    public function get_sys_info(){
		$sys_info['os']             = PHP_OS;
		$sys_info['zlib']           = function_exists('gzclose') ? 'YES' : 'NO';//zlib
		$sys_info['safe_mode']      = (boolean) ini_get('safe_mode') ? 'YES' : 'NO';//safe_mode = Off		
		$sys_info['timezone']       = function_exists("date_default_timezone_get") ? date_default_timezone_get() : "no_timezone";
		$sys_info['curl']			= function_exists('curl_init') ? 'YES' : 'NO';	
		$sys_info['web_server']     = $_SERVER['SERVER_SOFTWARE'];
		$sys_info['phpv']           = phpversion();
		$sys_info['ip'] 			= GetHostByName($_SERVER['SERVER_NAME']);
		$sys_info['fileupload']     = @ini_get('file_uploads') ? ini_get('upload_max_filesize') :'unknown';
		$sys_info['max_ex_time'] 	= @ini_get("max_execution_time").'s'; //脚本最大执行时间
		$sys_info['set_time_limit'] = function_exists("set_time_limit") ? true : false;
		$sys_info['domain'] 		= $_SERVER['HTTP_HOST'];
		$sys_info['memory_limit']   = ini_get('memory_limit');	                                
        $sys_info['version']   	    = file_get_contents(APP_PATH.'admincp/config/version.html');
		$mysqlinfo = Db::query("SELECT VERSION() as version");
		$sys_info['mysql_version']  = $mysqlinfo[0]['version'];
		if(function_exists("gd_info")){
			$gd = gd_info();
			$sys_info['gdinfo'] 	= $gd['GD Version'];
		}else {
			$sys_info['gdinfo'] 	= "未知";
		}
		return $sys_info;
    }
    
    
    /**
     * ajax 修改指定表数据字段  一般修改状态 比如 是否推荐 是否开启 等 图标切换的
     * table,id_name,id_value,field,value
     */
    public function changeTableVal(){  
        $table = input('table'); // 表名
        $id_name = input('id_name'); // 表主键id名
        $id_value = input('id_value'); // 表主键id值
        $field  = input('field'); // 修改哪个字段
        $value  = input('value'); // 修改字段值
        $level  = input('level/d',0); // 等级
        
        if ($table == "region" && $field=="is_show") {
            
            $parent_id_path = Db::name($table)->where($id_name,$id_value)->value('parent_id_path');
            $pidarr = explode(',',$parent_id_path);
            $province_id = $pidarr[2];
            switch($level){
                case 1:
                    //省份
                    
                break;
                case 2:
                    //城市
                    $city_id = $pidarr[3];
                    if($value == 1){
                        Db::name($table)->where(['id'=>['in',[$province_id,$city_id]]])->save(['is_show'=>1]);
                        Db::name($table)->where(['pid'=>$city_id])->save(['is_show'=>1]);
                    }else{
                        Db::name($table)->where(['pid'=>$city_id])->save(['is_show'=>0]);
                    }
                break;
                case 3:
                   //地区 
                    $city_id = $pidarr[3];
                    $area_id = $pidarr[4];
                    if($value == 1){
                        //获取
                        Db::name($table)->where(['id'=>['in',[$province_id,$city_id,$area_id]]])->save(['is_show'=>1]);
                    }
                break;
            }

            $update = [
                $field => $value,
            ];
            Db::name($table)->where([$id_name => $id_value])->save($update);
        } else if ($table == "users" && $field=="is_platform") {

            $user = Db::name($table)->where($id_name,$id_value)->find();
            $update = [
                $field => $value,
            ];
            if($user['is_authen']!=1){
                if($value==1){
                    $style_id = Db::name('label')->where('typeid',2)->value('id');
                    $expe_id = Db::name('label')->where('typeid',1)->value('id');
                    $price_id = Db::name('label')->where('typeid',3)->value('id');
                    $update['style_ids'] = $style_id;
                    $update['expe_id'] = $expe_id;
                    $update['price_id'] = $price_id;
                }else{
                    $update['style_ids'] = '';
                    $update['expe_id'] = '';
                    $update['price_id'] = '';
                }
            }
            Db::name($table)->where([$id_name => $id_value])->save($update);
        } else if ($table == "user_dynamic" && $field=="is_home") {

            Db::name($table)->where([$id_name => $id_value])->save(array($field=>$value)); // 根据条件保存修改的数据
            if($value==0){
                Db::name($table)->where([$id_name => $id_value])->save(array('home_sort'=>0)); // 根据条件保存修改的数据
            }
        } else {
            if($table=='message_notice' && $field=="is_show" && $value==1){
                //判断是否首次发送
                $message_notice = Db::name($table)->where([$id_name => $id_value])->find();
                $is_send = $message_notice['is_send'];
                if($is_send==0){
                    Db::name($table)->where([$id_name => $id_value])->save(array('is_send'=>1));
                    switch ($message_notice['user_type']){
                        case 0:
                            Db::name('users')->where(['is_deleted'=>0])->update(['sys_new_msg'=>1]);
                            break;
                        case 1:
                        case 2:
                            Db::name('users')->where(['is_deleted'=>0,'designer_type'=>$message_notice['user_type']])->update(['sys_new_msg'=>1]);
                            break;
                        case 3:
                            Db::name('users')->where(['is_deleted'=>0,'user_id'=>$message_notice['user_id']])->update(['sys_new_msg'=>1]);
                            break;
                    }
                }
            }
            Db::name($table)->where([$id_name => $id_value])->save(array($field=>$value)); // 根据条件保存修改的数据
        }
    }

    public function about(){

    	return View::fetch();
    }


    /* 
     * 图片上传
     * @param $filenames  文件域 
     * @param $position  存放目录
    */
    public function dyupload(){
        return dyuploadimg('dypic',input('imgpath/s'));
    }

    //视频上传
    public function dyuploadVideo()
    {
        return dyuploadVideo('dyvideo',input('imgpath/s'));
    }

    /* 判断文件是否存在 */
    public function FileExist(){
        $filename = input('filename/s','');
        $typeid = input('typeid/d',0);
        if(!in_array($typeid,[1,2])) return dyajaxReturn(0, '类型错误');
        $str = $typeid == 1? "图片" : "视频" ;
        if(empty($filename)) return dyajaxReturn(0, $str.'不存在');
        if(!file_exists(ltrim($filename,'/'))) return dyajaxReturn(0, $str.'不存在');
        return dyajaxReturn(1, $str.'存在');
    }

    public function maplnglat()
    {
        $city_id = input('city_id/d', '');
        $region = Db::name('region')->where('id',$city_id)->field('name,lng,lat')->find();
        $name = $region['name'];
        $web_key = config('amap_web_key');
        View::assign('name', $name);
        View::assign('web_key', $web_key);
        View::assign('lng', $region['lng']);
        View::assign('lat', $region['lat']);
        return View::fetch();
    }
}