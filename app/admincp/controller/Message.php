<?php

namespace app\admincp\controller;

use app\admincp\model\MessageNotice;
use think\facade\View;
use think\Page;
use think\facade\Db;

//消息
class Message extends Base
{
    public $user_type = [];
    public function __construct()
    {
        parent::__construct();
        $this->user_type = [
            '全体',
            '独立设计师',
            '机构设计师',
            '个人会员',
        ];
        View::assign('user_type',$this->user_type);
    }

    //列表
    public function messageList()
    {
        $MessageNotice = new MessageNotice();
        $info = $MessageNotice->getList();
        $list = $info['list'];
        $pager = $info['pager'];
        $show = $info['show'];
        $condition = $info['condition'];
        View::assign('list', $list);// 赋值数据集
        View::assign('pager', $pager);// 赋值分页输出
        View::assign('show', $show);// 赋值分页输出
        View::assign('condition', $condition);
        return View::fetch();
    }
    /* 详情信息 */
    public function message()
    {
        $act = "add";
        $info = [];
        $message_id = input('get.message_id/d');
        if ($message_id > 0) {
            $info = Db::name('message_notice')->where(['message_id'=>$message_id])->find();
            $act = 'edit';
        }
        View::assign('act', $act);
        View::assign('info', $info);
        $user = Db::name('users')->where(['is_deleted'=>0])->field('user_id,mobile')->select()->toArray();
        View::assign('user',$user);
        return View::fetch();
    }
    /* 更新信息 */
    public function messageHandle()
    {
        $param = input('post.');
        $validate = validate('message.'.$param['act']);
        if (!$validate->batch(true)->check($param)) {
            // 验证失败 输出错误信息
            $error = $validate->getError();
            $error_msg = array_values($error);
            return dyajaxReturn(0, $error_msg[0], $error);
        }
        $act = $param['act'];
        if(in_array($act,['add','edit'])){
            $data = [
                'user_type'=>$param['user_type'],
                'content'=>$param['content'],
                'user_id'=>$param['user_id']?$param['user_id']:0,
                'type'=>1,
                'is_show'=>$param['is_show'],
            ];
            if($param['user_type']!=3){
                $data['user_id'] = 0;
            }
            if ($act == 'add') {
                $data['send_time'] = time();
                $data['is_send'] = $param['is_show'];
                $r = Db::name('message_notice')->insertGetId($data);
                if($r){
                    if($param['is_show']==1){
                        switch ($param['user_type']){
                            case 0:
                                Db::name('users')->where(['is_deleted'=>0])->update(['sys_new_msg'=>1]);
                                break;
                            case 1:
                            case 2:
                                Db::name('users')->where(['is_deleted'=>0,'designer_type'=>$param['user_type']])->update(['sys_new_msg'=>1]);
                                break;
                            case 3:
                                Db::name('users')->where(['is_deleted'=>0,'user_id'=>$param['user_id']])->update(['sys_new_msg'=>1]);
                                break;
                        }
                    }
                }
                $log_info = "新增消息:".$r;
            } elseif ($act == 'edit') {
                $r = Db::name('message_notice')->where('message_id', $param['message_id'])->update($data);
                $log_info = "更新消息:". $param['message_id'];
            }
        }else{
            if ($act == 'del') {
                $r = Db::name('message_notice')->where(['message_id' => $param['message_id']])->delete();
                $log_info = "删除消息:". $param['message_id'];
            }
        }
        if (!$r) {
            return dyajaxReturn(0, '操作失败或未做修改');
        }
        adminLog($log_info);
        return dyajaxReturn(1, '操作成功');
    }

}
