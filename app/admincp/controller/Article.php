<?php

namespace app\admincp\controller;

use think\facade\View;
use think\Page;
use think\facade\Db;

class Article extends Base
{
    public function __construct()
    {
        parent::__construct();
        //户型列表
        $estate = Db::name('estate')->where(['is_deleted'=>0])->order('sort asc,estate_id desc')->field('estate_id,name')->select()->toArray();
        View::assign('estate', $estate);
    }

    //资讯列表
    public function articleList()
    {
        $Article = new \app\admincp\model\Article();
        $info = $Article->getList(0);
        $list = $info['list'];
        $pager = $info['pager'];
        $show = $info['show'];
        $condition = $info['condition'];
        View::assign('list', $list);// 赋值数据集
        View::assign('pager', $pager);// 赋值分页输出
        View::assign('show', $show);// 赋值分页输出
        View::assign('condition', $condition);
        return View::fetch();
    }
    /* 获取资讯信息 */
    public function article()
    {
        $act = "add";
        $info = [];
        $article_id = input('get.article_id/d');
        if ($article_id > 0) {
            $info = Db::name('article')->where(['article_id'=>$article_id])->find();
            $act = 'edit';
        }
        View::assign('act', $act);
        View::assign('info', $info);
        return View::fetch();
    }
    /* 更新资讯信息 */
    public function aticleHandle()
    {
        $data = input('post.');
        $validate = validate('Article.'.$data['act']);
        if (!$validate->batch(true)->check($data)) {
            // 验证失败 输出错误信息
            $error = $validate->getError();
            $error_msg = array_values($error);
            return dyajaxReturn(0, $error_msg[0], $error);
        }
        $act = $data['act'];
        unset($data['act']);
        $data['article_type'] = 0;
        if ($act == 'add') {            
            $data['add_time'] = time();
            $r = Db::name('article')->insertGetId($data);
            $log_info = "新增资讯:".$data['title'];
        } elseif ($act == 'edit') {
            $name = Db::name('article')->where('article_id', $data['article_id'])->value('title');
            $r = Db::name('article')->update($data);
            $log_info = "更新资讯:".$name;
        } elseif ($act == 'del') {
            $name = Db::name('article')->where('article_id', $data['article_id'])->value('title');
            $r = Db::name('article')->where(['article_id' => $data['article_id']])->delete();
            $log_info = "删除资讯:".$name;
        }
        if (!$r) {
            return dyajaxReturn(0, '操作失败或未做修改');
        }
        adminLog($log_info);
        return dyajaxReturn(1, '操作成功');
    }
    //删除文章列表
    public function delList()
    {
        $ids = input('post.ids', '');
        if ($ids == '') return dyajaxReturn(0, '非法操作');
        $listIds = rtrim($ids);
        $log_info = '批量删除资讯:';
        $name_arr = Db::name('article')->whereIn('article_id', $listIds)->column('title');
        $name_str = implode(',',$name_arr);
        Db::name('article')->whereIn('article_id', $listIds)->delete();
        adminLog($log_info.$name_str);
        return dyajaxReturn(1, '操作成功',[]);
    }

    //协议
    public function agreement()
    {
        $article_id = input('article_id/d',1);
        $agreement = config('agreement');
        $title = $agreement[$article_id];
        $article = Db::name('article')->where('article_id',$article_id)->field('intro,content')->find();
        $content = $article['content'];
        $intro = $article['intro'];
        View::assign('title',$title);
        View::assign('article_id',$article_id);
        View::assign('intro',$intro);
        View::assign('content',$content);
        View::assign('agreement',$agreement);
        return View::fetch();
    }

    //协议修改
    public function agreementHandle()
    {
        $article_id = input('article_id/d');
        $content = input('content/s');
        $intro = input('intro/s');
        $res = Db::name('article')->where('article_id',$article_id)->update(['content'=>$content,'intro'=>$intro]);
        if (!$res) {
            return dyajaxReturn(0, '操作失败或未做修改');
        }
        if($article_id==4){
            //更新安卓版本信息
            $str = file_get_contents("./public/updateapk/version.xml");
            $pattern_code = "/(<code>[0-9]+<\/code>)/i";
            $pattern_version = "/(<version>[0-9\.]+<\/version>)/i";
            $pattern_url = "/(<url>[\s\S]+<\/url>)/i";
            $pattern_content = "/(<content>[\s\S]+<\/content>)/i";
            $subject = $str;
            preg_match_all($pattern_code,$subject,$code_out);
            preg_match_all($pattern_version,$subject,$version_out);
            preg_match_all($pattern_url,$subject,$url_out);
            preg_match_all($pattern_content,$subject,$content_out);
            $subject = str_ireplace($code_out[0][0], "<code>".config('android_code')."</code>", $subject);
            $subject = str_ireplace($version_out[0][0], "<version>".config('android_version')."</version>", $subject);
            $subject = str_ireplace($url_out[0][0], "<url>".SITE_URL."/public/updateapk/zxjzapp.apk</url>", $subject);
            $subject = str_ireplace($content_out[0][0], "<content>".$content."</content>", $subject);
            file_put_contents("./public/updateapk/version.xml",$subject);
        }
        $agreement = config('agreement');
        $title = $agreement[$article_id];
        $log_info = "更新协议:".$title;
        adminLog($log_info);
        return dyajaxReturn(1, '操作成功');
    }
}
