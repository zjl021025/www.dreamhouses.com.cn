<?php

namespace app\admincp\controller;

use app\admincp\model\Evaluate as EvaluateModel;
use think\facade\Request;
use think\facade\View;
use think\facade\Db;
use think\Page;

//评价
class Evaluate extends Base
{
    //列表
    public function evaluateList()
    {
        $EvaluateModel = new EvaluateModel();
        $info = $EvaluateModel->getList();
        $list = $info['list'];
        $pager = $info['pager'];
        $show = $info['show'];
        $condition = $info['condition'];
        View::assign('list', $list);// 赋值数据集
        View::assign('pager', $pager);// 赋值分页输出
        View::assign('show', $show);// 赋值分页输出
        View::assign('condition', $condition);
        return View::fetch();
    }
    /* 详情信息 */
    public function evaluate()
    {
        $info = [];
        $evaluate_id = input('get.evaluate_id/d');
        if ($evaluate_id > 0) {
            $info = Db::name('evaluate')->where(['evaluate_id'=>$evaluate_id])->find();
            $info['add_time'] = get_date($info['add_time'],'Y-m-d H:i:s');
        }
        View::assign('info', $info);
        return View::fetch();
    }
    /* 更新信息 */
    public function evaluateHandle()
    {
        $param = input('post.');
        if(!$param['evaluate_id']){
            return dyajaxReturn(0, "信息不存在");
        }
        $act = $param['act'];
        if ($act == 'del') {
            $r = Db::name('evaluate')->where(['evaluate_id' => $param['evaluate_id']])->delete();
            $log_info = "删除评价:". $param['evaluate_id'];
        }
        if (!$r) {
            return dyajaxReturn(0, '操作失败或未做修改');
        }
        adminLog($log_info);
        return dyajaxReturn(1, '操作成功');
    }

    /**
     * 作品评论列表
     * @return void
     */
    public function workEvaluateList(){
        $keywords = trim(input('keywords'));
        $userData = trim(input('userData'));
        $condition['keywords'] = $keywords;
        $condition['userData'] = $userData;
        if($keywords){
            $where['eva.content'] = ['like','%'.$keywords.'%'];
        }
        if($userData){
            $where['users.nickname|users.mobile'] = ['like',"%$userData%"];
        }
        $field = 'eva.evaluate_id,eva.content,eva.is_show,eva.add_time,users.nickname,users.avatar,users.last_ip,user_works.title';
        $count = Db::name('evaluate_work')
            ->alias('eva')
            ->leftjoin('users','eva.user_id = users.user_id')
            ->leftjoin('user_works','eva.works_id = user_works.works_id')
            ->where($where)->count();// 查询满足要求的总记录数
        $pager = new Page($count, $this->page_size);// 实例化分页类 传入总记录数和每页显示的记录数
        $list = Db::name('evaluate_work')
            ->alias('eva')
            ->field($field)
            ->leftjoin('users','eva.user_id = users.user_id')
            ->leftjoin('user_works','eva.works_id = user_works.works_id')
            ->where($where)
            ->order('eva.evaluate_id desc')
            ->limit($pager->firstRow, $pager->listRows)
            ->select()->toArray();
        if ($list) {
            foreach ($list as &$val) {
                $val['add_time'] = get_date($val['add_time']);
            }
        }
        $show = $pager->show();// 分页显示输出
        View::assign('list', $list);// 赋值数据集
        View::assign('pager', $pager);// 赋值分页输出
        View::assign('show', $show);// 赋值分页输出
        View::assign('condition', $condition);
        return View::fetch();
    }

}
