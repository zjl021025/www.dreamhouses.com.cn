<?php

namespace app\admincp\controller;

use think\facade\Db;
use think\facade\Request;
use think\facade\View;
use think\Page;
use function dyajaxReturn;
use function input;

class Intelligent extends Base
{
    //智能装修
    public function intelligentDecoration(){
        $param = Request::param();

        //选择房间
        $roomData = Db::name('label')->where(['typeid'=>5,'is_show'=>1])->select()->toArray();
        //选择风格
        $styleData = Db::name('label')->where(['typeid'=>2,'is_show'=>1])->select()->toArray();
        if(!empty($param['title']) || !empty($param['style_id']) || !empty($param['room_id'])){
            $array = array_filter($param, function ($val) {
                return $val !== "";
            });
            $title = $array['title'];
            if(!array_key_exists('title',$array)){
                if(!array_key_exists('style_id',$array)){
                    $count = Db::name('intelligent')
                        ->alias('i')
                        ->leftJoin('label','i.style_id = label.id')
                        ->leftJoin('color','i.color_id = color.id')
                        ->where(['i.is_deleted'=>0,'room_id'=>$array['room_id']])
                        ->count();
                    $pager = new Page($count,$this->page_size);
                    $show = $pager->show();
                    $intelligentData = Db::name('intelligent')
                        ->alias('i')
                        ->leftJoin('label','i.style_id = label.id')
                        ->leftJoin('color','i.color_id = color.id')
                        ->field('i.id,i.title,i.room_id,label.name,color.color_name,i.add_time,i.content,i.image,i.is_show')
                        ->where(['i.is_deleted'=>0,'room_id'=>$array['room_id']])
                        ->limit($pager->firstRow,$pager->listRows)
                        ->select()->toArray();
                }elseif (!array_key_exists('room_id',$array)){
                    $count = Db::name('intelligent')
                        ->alias('i')
                        ->leftJoin('label','i.style_id = label.id')
                        ->leftJoin('color','i.color_id = color.id')
                        ->where(['i.is_deleted'=>0,'style_id'=>$array['style_id']])
                        ->count();
                    $pager = new Page($count,$this->page_size);
                    $show = $pager->show();
                    $intelligentData = Db::name('intelligent')
                        ->alias('i')
                        ->leftJoin('label','i.style_id = label.id')
                        ->leftJoin('color','i.color_id = color.id')
                        ->field('i.id,i.title,i.room_id,label.name,color.color_name,i.add_time,i.content,i.image,i.is_show')
                        ->where(['i.is_deleted'=>0,'style_id'=>$array['style_id']])
                        ->limit($pager->firstRow,$pager->listRows)
                        ->select()->toArray();
                }else{
                    $count = Db::name('intelligent')
                        ->alias('i')
                        ->leftJoin('label','i.style_id = label.id')
                        ->leftJoin('color','i.color_id = color.id')
                        ->where(['i.is_deleted'=>0,'style_id'=>$array['style_id'],'room_id'=>$array['room_id']])
                        ->count();
                    $pager = new Page($count,$this->page_size);
                    $show = $pager->show();
                    $intelligentData = Db::name('intelligent')
                        ->alias('i')
                        ->leftJoin('label','i.style_id = label.id')
                        ->leftJoin('color','i.color_id = color.id')
                        ->field('i.id,i.title,i.room_id,label.name,color.color_name,i.add_time,i.content,i.image,i.is_show')
                        ->where(['i.is_deleted'=>0,'style_id'=>$array['style_id'],'room_id'=>$array['room_id']])
                        ->limit($pager->firstRow,$pager->listRows)
                        ->select()->toArray();
                }
            }elseif (!array_key_exists('style_id',$array)){
                if(!array_key_exists('title',$array)){
                    $count = Db::name('intelligent')
                        ->alias('i')
                        ->leftJoin('label','i.style_id = label.id')
                        ->leftJoin('color','i.color_id = color.id')
                        ->where(['i.is_deleted'=>0,'room_id'=>$array['room_id']])
                        ->count();
                    $pager = new Page($count,$this->page_size);
                    $show = $pager->show();
                    $intelligentData = Db::name('intelligent')
                        ->alias('i')
                        ->leftJoin('label','i.style_id = label.id')
                        ->leftJoin('color','i.color_id = color.id')
                        ->field('i.id,i.title,i.room_id,label.name,color.color_name,i.add_time,i.content,i.image,i.is_show')
                        ->where(['i.is_deleted'=>0,'room_id'=>$array['room_id']])
                        ->limit($pager->firstRow,$pager->listRows)
                        ->select()->toArray();
                }elseif (!array_key_exists('room_id',$array)){
                    $count = Db::name('intelligent')
                        ->alias('i')
                        ->leftJoin('label','i.style_id = label.id')
                        ->leftJoin('color','i.color_id = color.id')
                        ->where(['i.is_deleted'=>0,['title','like',"%$title%"]])
                        ->count();
                    $pager = new Page($count,$this->page_size);
                    $show = $pager->show();
                    $intelligentData = Db::name('intelligent')
                        ->alias('i')
                        ->leftJoin('label','i.style_id = label.id')
                        ->leftJoin('color','i.color_id = color.id')
                        ->field('i.id,i.title,i.room_id,label.name,color.color_name,i.add_time,i.content,i.image,i.is_show')
                        ->where(['i.is_deleted'=>0,['title','like',"%$title%"]])
                        ->limit($pager->firstRow,$pager->listRows)
                        ->select()->toArray();
                }else{
                    $count = Db::name('intelligent')
                        ->alias('i')
                        ->leftJoin('label','i.style_id = label.id')
                        ->leftJoin('color','i.color_id = color.id')
                        ->where(['i.is_deleted'=>0,['title','like',"%$title%"],'room_id'=>$array['room_id']])
                        ->count();
                    $pager = new Page($count,$this->page_size);
                    $show = $pager->show();
                    $intelligentData = Db::name('intelligent')
                        ->alias('i')
                        ->leftJoin('label','i.style_id = label.id')
                        ->leftJoin('color','i.color_id = color.id')
                        ->field('i.id,i.title,i.room_id,label.name,color.color_name,i.add_time,i.content,i.image,i.is_show')
                        ->where(['i.is_deleted'=>0,['title','like',"%$title%"],'room_id'=>$array['room_id']])
                        ->limit($pager->firstRow,$pager->listRows)
                        ->select()->toArray();
                }
            }elseif (!array_key_exists('room_id',$array)){
                if(!array_key_exists('style_id',$array)){
                    $count = Db::name('intelligent')
                        ->alias('i')
                        ->leftJoin('label','i.style_id = label.id')
                        ->leftJoin('color','i.color_id = color.id')
                        ->where(['i.is_deleted'=>0,['title','like',"%$title%"]])
                        ->count();
                    $pager = new Page($count,$this->page_size);
                    $show = $pager->show();
                    $intelligentData = Db::name('intelligent')
                        ->alias('i')
                        ->leftJoin('label','i.style_id = label.id')
                        ->leftJoin('color','i.color_id = color.id')
                        ->field('i.id,i.title,i.room_id,label.name,color.color_name,i.add_time,i.content,i.image,i.is_show')
                        ->where(['i.is_deleted'=>0,['title','like',"%$title%"]])
                        ->limit($pager->firstRow,$pager->listRows)
                        ->select()->toArray();
                }elseif (!array_key_exists('title',$array)){
                    $count = Db::name('intelligent')
                        ->alias('i')
                        ->leftJoin('label','i.style_id = label.id')
                        ->leftJoin('color','i.color_id = color.id')
                        ->where(['i.is_deleted'=>0,'style_id'=>$array['style_id']])
                        ->count();
                    $pager = new Page($count,$this->page_size);
                    $show = $pager->show();
                    $intelligentData = Db::name('intelligent')
                        ->alias('i')
                        ->leftJoin('label','i.style_id = label.id')
                        ->leftJoin('color','i.color_id = color.id')
                        ->field('i.id,i.title,i.room_id,label.name,color.color_name,i.add_time,i.content,i.image,i.is_show')
                        ->where(['i.is_deleted'=>0,'style_id'=>$array['style_id']])
                        ->limit($pager->firstRow,$pager->listRows)
                        ->select()->toArray();
                }else{
                    $count = Db::name('intelligent')
                        ->alias('i')
                        ->leftJoin('label','i.style_id = label.id')
                        ->leftJoin('color','i.color_id = color.id')
                        ->where(['i.is_deleted'=>0,'style_id'=>$array['style_id'],['title','like',"%$title%"]])
                        ->count();
                    $pager = new Page($count,$this->page_size);
                    $show = $pager->show();
                    $intelligentData = Db::name('intelligent')
                        ->alias('i')
                        ->leftJoin('label','i.style_id = label.id')
                        ->leftJoin('color','i.color_id = color.id')
                        ->field('i.id,i.title,i.room_id,label.name,color.color_name,i.add_time,i.content,i.image,i.is_show')
                        ->where(['i.is_deleted'=>0,'style_id'=>$array['style_id'],['title','like',"%$title%"]])
                        ->limit($pager->firstRow,$pager->listRows)
                        ->select()->toArray();
                }
            }else{
                $count = Db::name('intelligent')
                    ->alias('i')
                    ->leftJoin('label','i.style_id = label.id')
                    ->leftJoin('color','i.color_id = color.id')
                    ->where(['i.is_deleted'=>0,'style_id'=>$array['style_id'],['title','like',"%$title%"],'room_id'=>$array['room_id']])
                    ->count();
                $pager = new Page($count,$this->page_size);
                $show = $pager->show();
                $intelligentData = Db::name('intelligent')
                    ->alias('i')
                    ->leftJoin('label','i.style_id = label.id')
                    ->leftJoin('color','i.color_id = color.id')
                    ->field('i.id,i.title,i.room_id,label.name,color.color_name,i.add_time,i.content,i.image,i.is_show')
                    ->where(['i.is_deleted'=>0,'style_id'=>$array['style_id'],['title','like',"%$title%"],'room_id'=>$array['room_id']])
                    ->limit($pager->firstRow,$pager->listRows)
                    ->select()->toArray();
            }
        }else{
            $count = Db::name('intelligent')
                ->alias('i')
                ->leftJoin('label','i.style_id = label.id')
                ->leftJoin('color','i.color_id = color.id')
                ->where('i.is_deleted',0)
                ->count();
            $pager = new Page($count,$this->page_size);
            $show = $pager->show();
            $intelligentData = Db::name('intelligent')
                ->alias('i')
                ->leftJoin('label','i.style_id = label.id')
                ->leftJoin('color','i.color_id = color.id')
                ->field('i.id,i.title,i.room_id,label.name,color.color_name,i.add_time,i.content,i.image,i.is_show')
                ->where('i.is_deleted',0)
                ->limit($pager->firstRow,$pager->listRows)
                ->select()->toArray();
        }
        $label = Db::name('label')->where(['typeid'=>5,'is_show'=>1])->select()->toArray();
        foreach ($intelligentData as $key=>$val){
            foreach ($label as $k=>$v){
                if($v['id']==$val['room_id']){
                    $intelligentData[$key]['room'] = $v['name'];
                }
            }
            $intelligentData[$key]['add_time'] = date('Y-m-d H:i:s', $val['add_time']);
        }
        View::assign('roomData',$roomData);
        View::assign('styleData',$styleData);
        View::assign('intelligentData',$intelligentData);
        View::assign('pager',$pager);
        View::assign('show',$show);
        return View::fetch();
    }
    //智能装修添加
    public function intelligentDecorationInsert(){
        //选择房间
        $roomData = Db::name('label')->where(['typeid'=>5,'is_show'=>1])->select()->toArray();
        //选择风格
        $styleData = Db::name('label')->where(['typeid'=>2,'is_show'=>1])->select()->toArray();
        //选择色系
        $colorData = Db::name('color')->where('is_show',1)->select()->toArray();
        //选择家具
        $furnitureData = Db::name('furniture')->where(['typeid'=>2,'is_show'=>1,'is_deleted'=>0])->order('add_time','desc')->select()->toArray();
        foreach ($furnitureData as $key => $val){
            $furnitureData[$key]['add_time'] = date('Y-m-d H:i:s', $val['add_time']);
            $furnitureData[$key]['typeid'] = $val['typeid'] == 1?'工装':'软装';
            $furnitureData[$key]['cate_id'] = Db::name('furniture_category')->where('id',$val['cate_id'])->value('name');
            $furnitureData[$key]['two_cate_id'] = Db::name('furniture_category')->where('id',$val['two_cate_id'])->value('name');
        }
        View::assign('roomData',$roomData);
        View::assign('styleData',$styleData);
        View::assign('colorData',$colorData);
        View::assign('furnitureData',$furnitureData);
        return View::fetch();
    }

    /**
     * 添加智能设计数据
     * @return void
     */
    public function insertIntelligent(){
        $param = Request::param();
        //将家具ID转为字符串
        $param['selectedCheckbox'] = implode(',',$param['selectedCheckbox']);
        $param['selectedCheckboxInput'] = implode(',',$param['selectedCheckboxInput']);
        //添加入库
        Db::startTrans();
        try {
            $data = [
                'title' => $param['title'],
                'room_id' => $param['room_id'],
                'style_id' => $param['style_id'],
                'color_id' => $param['color_id'],
                'add_time' => time(),
                'content' => $param['content'],
                'image' => $param['image'],
                'video' => $param['video'],
                'furniture_ids' => $param['selectedCheckbox'],
                'video_size' => $param['filesize'],
                'furniture_tags' => $param['selectedCheckboxInput']
            ];
            $insert = Db::name('intelligent')->insert($data);
            if(!$insert){
                Db::rollback();
                return dyajaxReturn(0,'上传失败');
            }
            Db::commit();
            return dyajaxReturn(1,'上传成功',$param);
        }catch (\Exception $exception){
            Db::rollback();
            return dyajaxReturn(0,'上传失败');
        }
    }

    /**
     * 上传图片
     * @return void
     */
    public function uploadImage(){
        $file = \request()->file('file');
        $domain = config('qiniu.domain');
        $qiniu_file = \think\facade\Filesystem::disk('qiniu')->putFile('image',$file);
        $baseUrl = 'http://'.$domain.'/'.$qiniu_file;
        return dyajaxReturn(1,'成功',$baseUrl);
    }
    /**
     * 上传视频
     * @return void
     */
    public function uploadVideo(){
        $file = \request()->file('file');
        $domain = config('qiniu.domain');
        $qiniu_file = \think\facade\Filesystem::disk('qiniu')->putFile('video',$file);
        $baseUrl = 'http://'.$domain.'/'.$qiniu_file;
        return dyajaxReturn(1,'成功',$baseUrl);
    }
    //智能装修修改
    public function intelligentDecorationUpdate(){
        $param = Request::param();
        $data = Db::name('intelligent')->where('id',$param['id'])->find();
        //选择房间
        $roomData = Db::name('label')->where(['typeid'=>5,'is_show'=>1])->select()->toArray();
        //选择风格
        $styleData = Db::name('label')->where(['typeid'=>2,'is_show'=>1])->select()->toArray();
        //选择色系
        $colorData = Db::name('color')->where('is_show',1)->select()->toArray();
        $furnitureIds = explode(',',$data['furniture_ids']);
        $furnitureTags = explode(',',$data['furniture_tags']);
        $array = [];
        foreach ($furnitureIds as $key=>$val){
            foreach ($furnitureTags as $k=>$v){
                if($key == $k){
                    $array[] = ['id'=>$val,'tag'=>$v];
                }
            }
        }
        $array = array_column($array,null,'id');
        //选择家具
        $furnitureData = Db::name('furniture')->where(['typeid'=>2,'is_show'=>1,'is_deleted'=>0])->order('add_time','desc')->select()->toArray();
        foreach ($furnitureData as $key => $val){
            $furnitureData[$key]['add_time'] = date('Y-m-d H:i:s', $val['add_time']);
            $furnitureData[$key]['cate_id'] = Db::name('furniture_category')->where('id',$val['cate_id'])->value('name');
            $furnitureData[$key]['two_cate_id'] = Db::name('furniture_category')->where('id',$val['two_cate_id'])->value('name');
        }
        View::assign('array',$array);
        View::assign('data',$data);
        View::assign('roomData',$roomData);
        View::assign('styleData',$styleData);
        View::assign('colorData',$colorData);
        View::assign('furnitureData',$furnitureData);
        return View::fetch();
    }

    /**
     * 修改数据
     * @return void
     */
    public function updateIntelligent(){
        $param = Request::param();
        //将字节换成mb
        $param['selectedCheckbox'] = implode(',',$param['selectedCheckbox']);
        $param['selectedCheckboxInput'] = implode(',',$param['selectedCheckboxInput']);
        Db::startTrans();
        try {
            $data = [
                'title' => $param['title'],
                'room_id' => $param['room_id'],
                'style_id' => $param['style_id'],
                'color_id' => $param['color_id'],
                'add_time' => time(),
                'content' => $param['content'],
                'image' => $param['image'],
                'video' => $param['video'],
                'furniture_ids' => $param['selectedCheckbox'],
                'video_size' => $param['video_size'],
                'furniture_tags' => $param['selectedCheckboxInput']
            ];
            $update = Db::name('intelligent')->where('id',$param['intelligent_id'])->update($data);
            if(!$update){
                Db::rollback();
                return dyajaxReturn(0,'修改失败');
            }
            Db::commit();
            return dyajaxReturn(1,'修改成功');
        }catch (\Exception $exception){
            Db::rollback();
            return dyajaxReturn(0,'修改失败');
        }
    }
    //查看智能全屋装修
    public function intelligentDecorationLook(){
        $param = Request::param();
        $data = Db::name('intelligent')->where('id',$param['id'])->find();
        $data['room'] = Db::name('label')->where('id',$data['room_id'])->value('name');
        $data['style'] = Db::name('label')->where('id',$data['style_id'])->value('name');
        $data['color'] = Db::name('color')->where('id',$data['color_id'])->value('color_name');
        $data['add_time'] = date('Y-m-d H:i:s', $data['add_time']);
        //查找家具
        $furniture = explode(',',$data['furniture_ids']);
        foreach ($furniture as $value){
            $furnitureData[] = Db::name('furniture')->where('id',$value)->find();
        }
        foreach ($furnitureData as $key => $val){
            $furnitureData[$key]['two_cate'] = Db::name('furniture_category')->where('id',$val['two_cate_id'])->value('name');
        }
        View::assign('data',$data);
        View::assign('furnitureData',$furnitureData);
        return View::fetch();
    }
    //删除智能全屋装修
    public function intelligentDel(){
        $param = Request::param();
        $del = Db::name('intelligent')->where('id',$param['id'])->update(['is_deleted'=>1]);
        if(!$del){
            return dyajaxReturn(0,'删除失败');
        }
        return dyajaxReturn(1,'删除成功');
    }

}