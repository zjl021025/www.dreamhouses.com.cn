<?php

namespace app\admincp\controller;

use app\admincp\model\SceneHouse;
use app\admincp\model\UserWorksResource;
use Qiniu\Auth;
use think\facade\View;
use think\facade\Db;

//场景
class Scene extends Base
{
    //户型列表
    public function housetypeList()
    {
        $SceneHouse = new SceneHouse();
        $info = $SceneHouse->getList();
        $list = $info['list'];
        $pager = $info['pager'];
        $show = $info['show'];
        $condition = $info['condition'];
        View::assign('list', $list);// 赋值数据集
        View::assign('pager', $pager);// 赋值分页输出
        View::assign('show', $show);// 赋值分页输出
        View::assign('condition', $condition);
        return View::fetch();
    }
    /* 户型详情信息 */
    public function housetype()
    {
        $act = "add";
        $info = [];
        $house_id = input('get.house_id/d');
        if ($house_id > 0) {
            $info = Db::name('scene_house')->where(['house_id'=>$house_id])->find();
            $act = 'edit';
            $info['images'] = $info['images']?explode(',',$info['images']):[];
            $info['over_images'] = $info['over_images']?explode(',',$info['over_images']):[];
            $info['room'] = Db::name('scene_house_room')->where(['house_id'=>$house_id])->field('content,images,room_id')->select()->toArray();
            foreach ($info['room'] as $k=>$v){
                $info['room'][$k]['images'] = $v['images']?explode(',',$v['images']):[];
                $info['room'][$k]['name'] = get_label_name($v['room_id']);;
            }
        }
        View::assign('act', $act);
        View::assign('info', $info);
        $estate = Db::name('estate')->where(['is_deleted'=>0])->field('estate_id,name')->order('sort asc,estate_id desc')->select();
        View::assign('estate',$estate);
        //房间列表
        $room = Db::name('label')->where(['is_show'=>1,'typeid'=>5])->order('sort asc,id desc')->field('id,name')->select()->toArray();
        $style = Db::name('label')->where(['is_show'=>1,'typeid'=>9])->order('sort asc,id desc')->field('id,name')->select()->toArray();
        View::assign('room', $room);
        View::assign('style', $style);
        return View::fetch();
    }
    /* 户型更新信息 */
    public function housetypeHandle()
    {
        $param = input('post.');
        $validate = validate('housetype.'.$param['act']);
        if (!$validate->batch(true)->check($param)) {
            // 验证失败 输出错误信息
            $error = $validate->getError();
            $error_msg = array_values($error);
            return dyajaxReturn(0, $error_msg[0], $error);
        }
        $act = $param['act'];
        if(in_array($act,['add','edit'])){
            $room = $param['room'];
            if(!empty($room)){
                $room_ids = array_column($room,'room_id');
            }else{
                $room_ids = '';
            }
            $data = [
                'image'=>$param['image']?$param['image']:'',
                'images'=>$param['images']?implode(',',$param['images']):'',
                'house_image'=>$param['house_image'],
                'name'=>$param['name'],
                'ios_url'=>$param['ios_url'],
                'Android_url'=>$param['Android_url'],
                'pc_url'=>$param['pc_url'],
                'estate_id'=>$param['estate_id'],
                'house_type_id'=>$param['house_type_id'],
                'style_id'=>$param['style_id'],
                'room_ids'=>$room_ids?implode(',',$room_ids):'',
                'over_images'=>$param['over_images']?implode(',',$param['over_images']):'',
                'over_content'=>$param['over_content']?$param['over_content']:'',
            ];

            $data['area'] = get_estate_area($param['estate_id'],$param['house_type_id']);
            if ($act == 'add') {
                $data['add_time'] = time();
                $r = Db::name('scene_house')->insertGetId($data);
                $log_info = "新增户型:".$param['name'];
                //保存房间信息
                $room_insert = [];
                if(!empty($room)){
                    foreach ($room as $v){
                        $room_insert[] = [
                            'content'=>$v['content']?$v['content']:'',
                            'images'=>$v['images']?implode(',',$v['images']):'',
                            'house_id'=>$r,
                            'room_id'=>$v['room_id'],
                            'add_time'=>time(),
                        ];
                    }
                }else{
                    $room_insert = [];
                }
                if($room_insert){
                    Db::name('scene_house_room')->insertAll($room_insert);
                }
            } elseif ($act == 'edit') {
                $scene_house = Db::name('scene_house')->where('house_id', $param['house_id'])->field('name,room_ids')->find();
                $name = $scene_house['name'];
                $old_room_ids = $scene_house['room_ids']?explode(',',$scene_house['room_ids']):[];
                $r = Db::name('scene_house')->where('house_id', $param['house_id'])->update($data);
                $log_info = "更新户型:".$name;
                //保存房间信息
                $room_insert = [];
                if(!empty($room)){
                    foreach ($room as $v){
                        if(in_array($v['room_id'],$old_room_ids)){
                            $room_data = [
                                'content'=>$v['content']?$v['content']:'',
                                'images'=>$v['images']?implode(',',$v['images']):'',
                            ];
                            Db::name('scene_house_room')->where(['house_id'=>$param['house_id'],'room_id'=>$v['room_id']])->save($room_data);
                        }else{
                            $room_insert[] = [
                                'content'=>$v['content']?$v['content']:'',
                                'images'=>$v['images']?implode(',',$v['images']):'',
                                'house_id'=>$param['house_id'],
                                'room_id'=>$v['room_id'],
                                'add_time'=>time(),
                            ];
                        }
                    }
                }else{
                    $room_insert = [];
                }
                if($room_insert){
                    Db::name('scene_house_room')->insertAll($room_insert);
                }
                Db::name('scene_house_room')->where(['house_id'=>$param['house_id'],'room_id'=>['notin',$room_ids]])->delete();
                $r = true;
            }
        }else{
            if ($act == 'del') {
                $name = Db::name('scene_house')->where('house_id', $param['house_id'])->value('name');
                $r = Db::name('scene_house')->where(['house_id' => $param['house_id']])->update(['is_deleted'=>1,'is_show'=>0]);
                $log_info = "删除户型:".$name;
            }
        }
        if (!$r) {
            return dyajaxReturn(0, '操作失败或未做修改');
        }
        adminLog($log_info);
        return dyajaxReturn(1, '操作成功');
    }

    /**
     * 获取七牛云token
     * @return string
     */
    public function getToken(){
        $accesskey = config('qiniu.accessKey');
        $secretKey = config('qiniu.secretKey');
        $bucket = config('qiniu.bucket');
        $expires = 7200;
        //构建鉴权对象
        $auth = new Auth($accesskey,$secretKey);
        $token = $auth->uploadToken($bucket,null,$expires,null,true);
        return $token;
    }

    //列表
    public function renderList()
    {
        $Resource = new UserWorksResource();
        $info = $Resource->getList();
        $list = $info['list'];
        $pager = $info['pager'];
        $show = $info['show'];
        $condition = $info['condition'];
        View::assign('list', $list);// 赋值数据集
        View::assign('pager', $pager);// 赋值分页输出
        View::assign('show', $show);// 赋值分页输出
        View::assign('condition', $condition);
        return View::fetch();
    }

    public function furnitureList()
    {
        $Furniture = new \app\admincp\model\Furniture();
        $info = $Furniture->getList();
        $list = $info['list'];
        $pager = $info['pager'];
        $show = $info['show'];
        $condition = $info['condition'];
        View::assign('list', $list);// 赋值数据集
        View::assign('pager', $pager);// 赋值分页输出
        View::assign('show', $show);// 赋值分页输出
        View::assign('condition', $condition);
        return View::fetch();
    }

    //渲染设置
    public function renderSet(){
        return View::fetch();
    }
    //图片渲染
    public function pictureRender(){
        return View::fetch();
    }
    //图片渲染查看
    public function pictureRenderLook(){
        return View::fetch();
    }
    //视频渲染
    public function mediaRender(){
        return View::fetch();
    }
    //视频渲染查看
    public function mediaRenderLook(){
        return View::fetch();
    }
    //360渲染
    public function threeRender(){
        return View::fetch();
    }
    //360渲染查看
    public function threeRenderLook(){
        return View::fetch();
    }
}
