<?php


namespace app\admincp\controller;

use think\facade\Cache;
use think\facade\Db;

//平台后台的execl导出功能
class Excels extends Base
{
    public $excel_num = 10000;//每张excel表的数据存储量
    /**
     * 初始化操作
     */
    public function __construct() {
        parent::__construct();
    }
    //用户导出
    public function userExport()
    {
        $where['u.is_deleted'] = 0;
        $where['u.is_authen'] = 1;//审核通过的
        $add_time = input('add_time/s',false, 'trim');
        $start_time = '';
        $end_time = '';
        if($add_time){
            [$start_time,$end_time] = get_start_endtime_by_range_time($add_time);
        }
        $keywords = input('keywords/s', false, 'trim');
        if ($keywords) {
            $where['ua.name|u.mobile|u.nickname'] = array('like','%'.$keywords.'%');
        }
        if (input('type') == 1) {
            //统计数据
            $d['num'] = Db::name('users')->alias('u')
                ->leftjoin('user_authen ua','ua.user_id=u.user_id')
                ->where($where)->count();
            Cache::set('admincp_user_all_num', $d['num'], 1800);//将要导出的数据总量放在缓存中
            Cache::set('admincp_user_leading_num', '0', 600);//已导出的数据量（初始为0条）
            return dyajaxReturn(1, '获取数据成功', $d);
        }
        $field = 'u.user_id,u.mobile,u.add_time,ua.name,ua.province,ua.city,ua.district,ua.address,u.tool_ids,u.category_ids,u.integral';
        $user = Db::name('users')->alias('u')
            ->leftjoin('user_authen ua','ua.user_id=u.user_id')
            ->where($where)
            ->field($field)
            ->order('u.user_id desc')
            ->select()
            ->toArray();
        $userList = [];
        if ($user) {
            foreach ($user as $k=>$v) {
                $userList[$k]['name'] = $v['name'];
                $userList[$k]['mobile'] = $v['mobile'];
                $userList[$k]['address'] = get_meraddress($v,' ');
                $userList[$k]['tool_name'] = get_category_name($v['tool_ids']);
                $userList[$k]['category_name'] = get_category_name($v['category_ids'],1);
                $userList[$k]['integral'] = $v['integral'];
                $userList[$k]['install_num'] = get_user_order_count($v['user_id'],$start_time,$end_time,0);//安装
                $userList[$k]['repair_num'] = get_user_order_count($v['user_id'],$start_time,$end_time,1);//维修
            }
        }
        $ext = ['姓名','手机号','地址','提供工具','擅长产品品类','积分','安装订单总数量','维修订单总数量'];
        $path_url = "public/upload/excel/user";//zip,excel放的位置
        $name = '安装师傅信息汇总表'.date('Y-m-d');
        csv_export($name,$userList, $ext, $path_url, $this->excel_num, 'admincp_user_leading_num', 'admincp_user_zip_url');
    }

    //订单列表导出
    public function userOrderExport()
    {
        $where = [];
        $time_type = input('time_type/d',1);//1=发布时间 2=发货时间 3=完成时间 4=取消时间 5=中止时间
        $time = urldecode(input('time/s'));//1=发布时间 2=发货时间 3=完成时间 4=取消时间 5=中止时间
        $start_time = '';
        $end_time = '';
        $user_id = input('user_id/d');
        if($user_id){
            $where['o.user_id'] = $user_id;
        }
        if($time){
            [$start_time,$end_time] = get_start_endtime_by_range_time($time);
        }
        if($start_time && $end_time){
            switch ($time_type){
                case 1:
                    $where['o.add_time'] = ['between',[$start_time,$end_time]];
                    break;
                case 2:
                    $where['o.shipping_time'] = ['between',[$start_time,$end_time]];
                    break;
                case 3:
                    $where['o.confirm_time'] = ['between',[$start_time,$end_time]];
                    break;
                case 4:
                    $where['o.cancel_time'] = ['between',[$start_time,$end_time]];
                    break;
                case 5:
                    $where['o.suspend_time'] = ['between',[$start_time,$end_time]];
                    break;
            }
        }
        $order_status = input('order_status/d', -999);//订单状态
        if($order_status!=-999){
            $where['o.order_status'] = $order_status;
            if(!in_array($order_status,[6,7])){
                $where['o.order_status'] = -1;
            }
        }else{
            $where['o.order_status'] = ['in',[6,7]];
        }
        $order_type = input('order_type/d', -999);//订单状态
        if($order_type!=-999){
            $where['o.order_type'] = $order_type;
        }
        //关键词
        $keywords = input('keywords/s', '','trim');
        if($keywords){
            $where['o.order_name|o.order_sn|o.consignee'] = ['like',"%$keywords%"];
        }
        if (input('type') == 1) {
            //统计数据
            $d['num'] = Db::name('order')->alias('o')
                ->join('users u','u.user_id=o.user_id')
                ->leftjoin('user_authen ua','ua.user_id=u.user_id')
                ->leftjoin('order_quantity oq','oq.order_id=o.order_id')
                ->where($where)->count();
            Cache::set('admincp_userorder_all_num', $d['num'], 1800);//将要导出的数据总量放在缓存中
            Cache::set('admincp_userorder_leading_num', '0', 600);//已导出的数据量（初始为0条）
            return dyajaxReturn(1, '获取数据成功', $d);
        }
        $field = 'ua.name,o.consignee,o.mobile,o.meraddress,o.order_status,o.order_type,oq.spec_name,oq.size_width,o.order_id';
        $field .= ',oq.size_height,oq.material_name,o.confirm_time,o.suspend_time';
        $order = Db::name('order')->alias('o')
            ->join('users u','u.user_id=o.user_id')
            ->leftjoin('user_authen ua','ua.user_id=u.user_id')
            ->leftjoin('order_quantity oq','oq.order_id=o.order_id')
            ->where($where)
            ->order('order_id desc')
            ->field($field)
            ->select()->toArray();
        $orderList = [];
        if ($order) {
            foreach ($order as $k=>$v) {
                $orderList[$k]['name'] = $v['name'];
                $orderList[$k]['consignee'] = $v['consignee'];
                $orderList[$k]['mobile'] = $v['mobile'];
                $orderList[$k]['meraddress'] = implode(' ',explode(',',$v['meraddress']));
                $orderList[$k]['spec_name'] = $v['spec_name']?$v['spec_name']:'';
                $orderList[$k]['size_name'] = $v['size_width']?$v['size_width'].' X '.$v['size_height'].' mm':'';
                $orderList[$k]['material_name'] = $v['material_name']?$v['material_name']:'';
                $orderList[$k]['num'] = 1;
                if($v['order_status']==6){
                    if($v['order_type']==0){
                        $orderList[$k]['confirm_time'] = $v['confirm_time']?date('Y-m-d H:i:s',$v['confirm_time']):'';//安装完成时间
                        $orderList[$k]['repair_time'] = '';//维修完成时间
                    }else{
                        $orderList[$k]['confirm_time'] = '';//安装完成时间
                        $orderList[$k]['repair_time'] = $v['confirm_time']?date('Y-m-d H:i:s',$v['confirm_time']):'';//维修完成时间
                    }
                }else{
                    $orderList[$k]['confirm_time'] = $v['suspend_time']?date('Y-m-d H:i:s',$v['suspend_time']):'';//中止安装完成时间
                    $orderList[$k]['repair_time'] = '';//维修完成时间
                }
                $orderList[$k]['order_status'] = get_order_status_name($v);//订单状态
            }
        }
        $ext = ['师傅姓名','用户姓名','用户电话','用户地址','产品型号','产品尺寸','面料型号','产品数量/套','安装时间','维修时间','订单状态'];
        $path_url = "public/upload/excel/userorder";//zip,excel放的位置
        $name = '安装师傅订单信息汇总表'.date('Y-m-d');
        csv_export($name,$orderList, $ext, $path_url, $this->excel_num, 'admincp_userorder_leading_num', 'admincp_userorder_zip_url');
    }

    /**
     * excel导出/导入百分比
     *
     * @return void
     */
    public function getProcess()
    {
        $name1 = input('all_num');
        $name2 = input('leading_num');
        $n1 = Cache::get($name1);//获取总数量
        $n2 = Cache::get($name2);//获取已插入的数量
        if ($n1 > 0) {
            $d['num'] = round(($n2/$n1)*100, 2); //bcdiv($n2, $n1, 4)*100;//bc函数计算保留4位小数（计算导出百分比）
        } else {
            $d['num'] = 0;
        }
        if ($d['num'] == 100) {
            Cache::set($name1, $n1, 1);
            Cache::set($name2, $n2, 1);
        }
        return dyajaxReturn(1, '获取数据成功', $d);
    }
    /**
     * 根据文件地址判断文件是否存在
     *
     * @return void
     */
    public function getExcelUrl()
    {
        $url = input('url');
        $url1 = Cache::get($url);//获取导出的excel文件地址
        if (file_exists($url1)) {
            Cache::set($url, $url1, 5);
            switch ($url){
                case 'admincp_user_zip_url':
                    $log_info = "导出用户成功";
                    adminLog($log_info);
                    break;
            }
            return dyajaxReturn(1, '导出成功', [], "/".$url1);
        } else {
            return dyajaxReturn(0, '地址不存在');
        }
    }
}