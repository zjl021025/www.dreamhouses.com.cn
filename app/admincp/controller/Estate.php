<?php

namespace app\admincp\controller;

use think\facade\Request;
use think\facade\View;
use think\facade\Db;
use think\Page;

//楼盘
class Estate extends Base
{
    //列表
    public function estateList()
    {
        $param = Request::param();
        if (!empty($param['estateName'] || !empty($param['createTime']))){
            $array = array_filter($param, function ($val) {
                return $val !== "";
            });
            $estateName = $array['estateName'];
            if ($array['estateName'] == "" && $array['createTime'] == "") {
                return dyajaxReturn(0, '请输入搜索内容');
            }
            if (array_key_exists('createTime', $array)) {
                $array['createTime'] = strtotime($array['createTime']);
            }
            //TODO: 楼盘列表多条件查询
            if (!array_key_exists('estateName', $array)) {
                $count = Db::name('estate')->where('add_time', 'between', [$array['createTime'], time()])->where('is_deleted',0)->count();
                $pager = new Page($count,$this->page_size);
                $show = $pager->show();
                $estateData = Db::name('estate')->where('add_time', 'between', [$array['createTime'], time()])->where('is_deleted',0)->limit($pager->firstRow, $pager->listRows)->select()->toArray();
            } else if (!array_key_exists('createTime', $array)) {
                $count = Db::name('estate')->where('name', 'like', "%$estateName%")->where('is_deleted',0)->count();
                $pager = new Page($count,$this->page_size);
                $show = $pager->show();
                $estateData = Db::name('estate')->where('name', 'like', "%$estateName%")->where('is_deleted',0)->limit($pager->firstRow, $pager->listRows)->select()->toArray();
            } else {
                $count = Db::name('estate')->where('name', 'like', "%$estateName%")->where('is_deleted',0)->where('add_time', 'between', [$array['createTime'], time()])->count();
                $pager = new Page($count,$this->page_size);
                $show = $pager->show();
                $estateData = Db::name('estate')->where('name', 'like', "%$estateName%")->where('is_deleted',0)->where('add_time', 'between', [$array['createTime'], time()])->limit($pager->firstRow, $pager->listRows)->select()->toArray();
            }
        }else{
            $count = Db::name('estate')->where('is_deleted',0)->count();
            $pager = new Page($count,$this->page_size);
            $show = $pager->show();
            $estateData = Db::name('estate')->where('is_deleted',0)->limit($pager->firstRow, $pager->listRows)->select()->toArray();
        }
        foreach ($estateData as $key=>$val){
            $estateData[$key]['add_time'] = date('Y-m-d H:i:s', $val['add_time']);
        }
        View::assign('estateData',$estateData);
        View::assign('pager', $pager);// 赋值分页输出
        View::assign('show', $show);// 赋值分页输出
        return View::fetch();
    }

    //用户上传数据
    public function estateUpload()
    {
        $count = Db::name('house')
            ->alias('h')
            ->leftJoin('users','h.user_id = users.user_id')
            ->leftJoin('label','h.house_type_id = label.id')
            ->leftJoin('estate','h.estate_id = estate.estate_id')
            ->where('h.is_deleted',0)
            ->count();
        $pager = new Page($count,$this->page_size);
        $show = $pager->show();
        $houseData = Db::name('house')
            ->alias('h')
            ->leftJoin('users','h.user_id = users.user_id')
            ->leftJoin('label','h.house_type_id = label.id')
            ->leftJoin('estate','h.estate_id = estate.estate_id')
            ->field('h.house_id,users.nickname,users.avatar,users.mobile,h.name,h.address,h.area,h.add_time,h.is_enter')
            ->where('h.is_deleted',0)
            ->limit($pager->firstRow, $pager->listRows)
            ->select()->toArray();
        foreach ($houseData as $key=>$val){
            $houseData[$key]['add_time'] = date('Y-m-d H:i:s', $val['add_time']);
            $houseData[$key]['is_enter'] = $val['is_enter'] == 1 ? '是' : '否';
        }
        View::assign('houseData',$houseData);
        View::assign('pager',$pager);
        View::assign('show',$show);
        return View::fetch();
    }
    //用户上传楼盘详情
    public function estateUploadLook(){
        $param = Request::param();
        $LookData = Db::name('house')->where('house_id',$param['house_id'])->find();
        $LookData['estate'] = Db::name('estate')->where('estate_id',$LookData['estate_id'])->value('name');
        $LookData['house_type'] = Db::name('label')->where('id',$LookData['house_type_id'])->value('name');
        $LookData['is_enter'] = $LookData['is_enter'] == 1 ? '是' : '否';
        $roomData = Db::name('house_room')->where('house_id',$LookData['house_id'])->select()->toArray();
        foreach ($roomData as $key=>$val){
            if($val['images'] == null){
                $videoData[] = $val;
            }elseif ($val['videos'] == null){
                $imageData[] = $val;
            }
        }
        View::assign('LookData',$LookData);
        View::assign('videoData',$videoData);
        View::assign('imageData',$imageData);
        return View::fetch();
    }

    /**
     * 修改户型面积
     * @return void
     */
    public function updateArea(){
        $area = input('area');
        $house_id = input('house_id');
        if(empty($area)){
            return dyajaxReturn(0,'面积不能为空');
        }
        //修改
        $update = Db::name('scene_house')->where(['house_id'=>$house_id])->update(['area'=>$area]);
        if(!$update){
            return dyajaxReturn(0,'修改失败');
        }
        return dyajaxReturn(1,'修改成功',$area);
    }
    //删除用户上传楼盘
    public function estateUploadDel(){
        $param = Request::param();
        $del = Db::name('house')->where('house_id',$param['house_id'])->update(['is_deleted'=>1]);
        if(!$del){
            return dyajaxReturn(0,'删除失败');
        }
        return dyajaxReturn(1,'删除成功');
    }

    //下拉列表
    public function pullEstate($pullList)
    {
        switch ($pullList){
            case 'all':
                $estateData = Db::name('estate')->where('is_deleted',0)->select()->toArray();
                break;
            case 'show':
                $estateData = Db::name('estate')->where(['is_show'=>1,'is_deleted'=>0])->select()->toArray();
                break;
            case 'notShow':
                $estateData = Db::name('estate')->where(['is_show'=>0,'is_deleted'=>0])->select()->toArray();
                break;
            default:
                $estateData = Db::name('estate')->where('is_deleted',0)->select()->toArray();
        }
        View::assign('estateData',$estateData);
        View::fetch();
    }

    //楼盘户型
    public function estateTypeList(){
        $param = Request::param();
        $count = Db::name('scene_house')->where(['estate_id'=>$param['estate_id'],'is_deleted'=>0])->count();
        $pager = new Page($count,$this->page_size);
        $show = $pager->show();
        $sceneData = Db::name('scene_house')->where(['estate_id'=>$param['estate_id'],'is_deleted'=>0])->limit($pager->firstRow, $pager->listRows)->select()->toArray();
        foreach ($sceneData as $key=>$val){
            $sceneData[$key]['style'] = Db::name('label')->where('id',$val['style_id'])->value('name');
            $sceneData[$key]['house_type'] = Db::name('label')->where('id',$val['house_type_id'])->value('name');
            $sceneData[$key]['add_time'] = date('Y-m-d H:i:s', $val['add_time']);
        }
        View::assign('sceneData',$sceneData);
        View::assign('pager',$pager);
        View::assign('show',$show);
        return View::fetch();
    }
    //用户作品
    public function userWork()
    {
        $param = Request::param();
        $where['work.is_deleted'] = 0;
        $field = ['work.works_id,users.avatar,users.nickname,work.title,label.name,work.image,work.is_issue,work.add_time'];
       if(!empty($param['userData'])){
           $userData = $param['userData'];
            $where['users.nickname|users.mobile'] = ['like',"%$userData%"];
       }
       if(!empty($param['workName'])){
            $workName = $param['workName'];
            $where['work.title'] = ['like',"%$workName%"];
       }
       if(!empty($param['create_time'])){
            $create_time = strtotime($param['create_time']);
            $where['work.add_time'] = ['>',$create_time];
       }
        $count = Db::name('user_works')
            ->alias('work')
            ->leftJoin('users','work.user_id = users.user_id')
            ->leftJoin('label','work.house_type_id = label.id')
            ->where($where)
            ->count();
        $pager = new Page($count,$this->page_size);
        $show = $pager->show();
        //展示所有用户作品
        $workData = Db::name('user_works')
            ->alias('work')
            ->leftJoin('users','work.user_id = users.user_id')
            ->leftJoin('label','work.house_type_id = label.id')
            ->field($field)
            ->where($where)
            ->limit($pager->firstRow, $pager->listRows)
            ->select()->toArray();
        foreach ($workData as $key=>$val){
            $workData[$key]['add_time'] = date('Y-m-d', $val['add_time']);
            $workData[$key]['is_issue'] = $val['is_issue'] == 1 ? '是' : '否';
        }
        View::assign('workData',$workData);
        View::assign('pager',$pager);
        View::assign('show',$show);
        return View::fetch();
    }
    //查看用户作品
    public function userWorkLook()
    {
        $param = Request::param();
        $workData = Db::name('user_works')->where('works_id',$param['work_id'])->find();
        $workData['room'] = implode(',',array_column(Db::name('user_works_room')->field('content')->where('works_id',$param['work_id'])->select()->toArray(),'content'));
        $labelData = Db::name('label')->field('name')->where('id',$workData['house_type_id'])->find();
        $workData['house_type'] = $labelData['name'];
        $areaData = Db::name('scene_house')->field('area')->where('house_id',$workData['house_id'])->find();
        $workData['area'] = $areaData['area'];
        $styleData = Db::name('label')->field('name')->where('id',$workData['style_id'])->find();
        $workData['style'] = $styleData['name'];
        $estateData = Db::name('estate')->field('name')->where('estate_id',$workData['estate_id'])->find();
        $workData['estate'] = $estateData['name'];
        $workData['ios_url'] = $workData['ios_url'] != ''? $workData['ios_url'] : '空';
        $workData['Android_url'] = $workData['Android_url'] != ''? $workData['Android_url'] : '空';
        $workData['pc_url'] = $workData['pc_url'] != ''? $workData['pc_url'] : '空';
        $workData['add_time'] = date('Y-m-d H:i:s', $workData['add_time']);
        View::assign('workData',$workData);
        return View::fetch();
    }
    //删除用户作品
    public function userWorkDel(){
        $param = Request::param();
        $del = Db::name('user_works')->where('works_id',$param['works_id'])->update(['is_deleted'=>1]);
        if(!$del){
            return dyajaxReturn(0,'删除失败');
        }
        return dyajaxReturn(1,'删除成功');
    }

    //用户家
    public function userHome()
    {
        $nickname = input('nickname');
        $name = input('name');
        $add_time = strtotime(input('add_time'));
        $field = ['house.house_id,users.avatar,users.nickname,house.name,house.image,house.is_show,house.add_time,house.house_type_id'];
        $where['house.is_deleted'] = 0;
        if(!empty($nickname)){
            $where['users.nickname'] = ['like',"%$nickname%"];
        }
        if(!empty($name)){
            $where['house.name'] = ['like',"%$name%"];
        }
        if(!empty($add_time)){
            $where['house.add_time'] = ['>',$add_time];
        }
        $count = Db::name('user_house')
            ->alias('house')
            ->leftJoin('users','house.user_id = users.user_id')
            ->where($where)
            ->count();
        $pager = new Page($count,$this->page_size);
        $show = $pager->show();
        //展示所有用户作品
        $houseData = Db::name('user_house')
            ->alias('house')
            ->leftJoin('users','house.user_id = users.user_id')
            ->field($field)
            ->where($where)
            ->limit($pager->firstRow, $pager->listRows)
            ->select()->toArray();
        foreach ($houseData as $key=>$val){
            $houseData[$key]['add_time'] = date('Y-m-d', $val['add_time']);
            $houseData[$key]['house_type'] = Db::name('label')->where(['id'=>$val['house_type_id']])->value('name');
        }
        View::assign('houseData',$houseData);
        View::assign('pager',$pager);
        View::assign('show',$show);
        return View::fetch();
    }

    //查看用户家详情
    public function userHomeLook()
    {
        return View::fetch();
    }
    //删除我的家
    public function userHomeDel(){
        $house_id = input('house_id');
        $del = Db::name('user_house')->where(['house_id'=>$house_id])->update(['is_deleted'=>1]);
        if(!$del){
            return dyajaxReturn(0,'删除失败');
        }
        return dyajaxReturn(1,'删除成功');
    }

    /* 详情信息 */
    public function estate()
    {
        $act = "add";
        $info = [];
        $estate_id = input('get.estate_id/d');
        if ($estate_id > 0) {
            $info = Db::name('estate')->where(['estate_id' => $estate_id])->find();
            $info['exact'] = date('Y-m-d',$info['exact']);
            $act = 'edit';
        }
        //获取楼盘年限信息
        $vague = Db::name('label')->where(['typeid'=>12,'is_show'=>1,'is_deleted'=>0])->order('sort','ace')->select()->toArray();
        View::assign('vague',$vague);
        View::assign('act', $act);
        View::assign('info', $info);
        $this->getprovince();
        return View::fetch();
    }

    /* 更新信息 */
    public function estateHandle()
    {
        $param = input('post.');
        $validate = validate('estate.' . $param['act']);
        if (!$validate->batch(true)->check($param)) {
            // 验证失败 输出错误信息
            $error = $validate->getError();
            $error_msg = array_values($error);
            return dyajaxReturn(0, $error_msg[0], $error);
        }
        $act = $param['act'];
        if (in_array($act, ['add', 'edit'])) {
            $data = [
                'name' => $param['name'],
                'province_id' => $param['province_id'],
                'city_id' => $param['city_id'],
                'district_id' => $param['district_id'],
                'address' => $param['address'],
                'house_type_ids' => implode(',', $param['house_type']),
                'area' => json_encode($param['area']),
                'is_show' => $param['is_show'],
                'exact' => strtotime($param['exact']),
                'vague_id' => $param['vague_id']
            ];
            if ($act == 'add') {
                $data['add_time'] = time();
                $r = Db::name('estate')->insertGetId($data);
                if ($r) {
                    //楼盘同步
                    Db::name('house')->where(['estate_id' => 0, 'name' => $param['name'], 'province_id' => $param['province_id'], 'city_id' => $param['city_id'], 'district_id' => $param['district_id']])->update(['estate_id' => $r]);
                }
                $log_info = "新增楼盘:" . $param['name'];
            } elseif ($act == 'edit') {
                $name = Db::name('estate')->where('estate_id', $param['estate_id'])->value('name');
                $r = Db::name('estate')->where('estate_id', $param['estate_id'])->update($data);
                if ($r) {
                    Db::name('house')->where(['estate_id' => 0, 'name' => $param['name'], 'province_id' => $param['province_id'], 'city_id' => $param['city_id'], 'district_id' => $param['district_id']])->update(['estate_id' => $param['estate_id']]);
                }
                $log_info = "更新楼盘:" . $name;
            }
        } else {
            if ($act == 'del') {
                $count = Db::name('house')->where(['estate_id' => $param['estate_id'], 'is_deleted' => 0])->value('house_id');
                if ($count) {
                    return dyajaxReturn(0, '楼盘下有用户房屋不可删除');
                }
                $count = Db::name('article')->where(['estate_id' => $param['estate_id'], 'article_type' => 0])->value('article_id');
                if ($count) {
                    return dyajaxReturn(0, '请先删除楼盘下资讯');
                }
                $name = Db::name('estate')->where('estate_id', $param['estate_id'])->value('name');
                $r = Db::name('estate')->where(['estate_id' => $param['estate_id']])->update(['is_deleted' => 1]);
                $log_info = "删除楼盘:" . $name;
            }
        }
        if (!$r) {
            return dyajaxReturn(0, '操作失败或未做修改');
        }
        adminLog($log_info);
        return dyajaxReturn(1, '操作成功');
    }

    private function getprovince()
    {
        $province = Db::name('region')->where(['level' => 1])->field('id,name')->cache(true)->select()->toArray();//获取省
        View::assign('province', $province);
    }

    /**
     * 七牛云上传ios文件
     * @return void
     */
    public function uploadQiniuIos(){
        $ios_file = \request()->file('file');
        $accesskey = config('qiniu.accessKey');
        $secretKey = config('qiniu.secretKey');
        $domain = config('qiniu.domain');
        $auth = new \Qiniu\Auth($accesskey,$secretKey);
        $qiniu_file = \think\facade\Filesystem::disk('qiniu')->putFile('model/ios',$ios_file);
        $baseUrl = 'http://'.$domain.'/'.$qiniu_file;
        $signUrl = $auth->privateDownloadUrl($baseUrl);
        return dyajaxReturn(1,'成功',$signUrl);
    }

    /**
     * 七牛云上传Android文件
     * @return void
     */
    public function uploadQiniuAndroid(){
        $Android_file = \request()->file('file');
        $accesskey = config('qiniu.accessKey');
        $secretKey = config('qiniu.secretKey');
        $domain = config('qiniu.domain');
        $auth = new \Qiniu\Auth($accesskey,$secretKey);
        $qiniu_file = \think\facade\Filesystem::disk('qiniu')->putFile('model/Android',$Android_file);
        $baseUrl = 'http://'.$domain.'/'.$qiniu_file;
        $signUrl = $auth->privateDownloadUrl($baseUrl);
        return dyajaxReturn(1,'成功',$signUrl);
    }

    /**
     * 七牛云上传pc文件
     * @return void
     */
    public function uploadQiniuPc(){
        $pc_file = \request()->file('file');
        $accesskey = config('qiniu.accessKey');
        $secretKey = config('qiniu.secretKey');
        $domain = config('qiniu.domain');
        $auth = new \Qiniu\Auth($accesskey,$secretKey);
        $qiniu_file = \think\facade\Filesystem::disk('qiniu')->putFile('model/pc',$pc_file);
        $baseUrl = 'http://'.$domain.'/'.$qiniu_file;
        $signUrl = $auth->privateDownloadUrl($baseUrl);
        return dyajaxReturn(1,'成功',$signUrl);
    }
}
