<?php




namespace app\admincp\controller;
use think\facade\View;

use app\common\logic\AdminLogic;
use app\common\logic\ModuleLogic;
use think\Page;
use think\Verify;
use think\Loader;
use think\facade\Db;
use think\facade\Session;

class Admin extends Base {

    public function index(){
    	$list = array();
    	$keywords = input('keywords/s');
        $condition['keywords'] = $keywords;
    	if(empty($keywords)){
    		$res = Db::name('admin')->where('admin_id','>',1)->select();
    	}else{
			$res = DB::name('admin')->where('user_name','like','%'.$keywords.'%')->where('admin_id','>',1)->order('admin_id')->select();
    	}
    	$role = Db::name('admin_role')->where('role_id','>',1)->column('role_name','role_id');
    	if($res && $role){
    		foreach ($res as $val){
    			$val['role'] =  $role[$val['role_id']];
    			$val['add_time'] = date('Y-m-d H:i:s',$val['add_time']);
    			$list[] = $val;
    		}
    	}
    	View::assign('list',$list);
    	View::assign('condition',$condition);
        return View::fetch();
    }
    
    /**
     * 修改管理员密码
     * @return \think\mixed
     */
    public function modify_pwd(){
        $admin_id = input('admin_id/d',0);
        $oldPwd = input('old_pw/s');
        $newPwd = input('new_pw/s');
        $new2Pwd = input('new_pw2/s');
       
        if($admin_id){
            $info = Db::name('admin')->where("admin_id", $admin_id)->find();
            $info['password'] =  "";
            View::assign('info',$info);
        }
         if(IS_POST){
            //修改密码
            $enOldPwd = encrypt($oldPwd);
            $enNewPwd = encrypt($newPwd);
            $admin = Db::name('admin')->where('admin_id' , $admin_id)->find();
            if(!$admin || $admin['password'] != $enOldPwd){
                return dyajaxReturn(-1,'旧密码不正确');
            }else if($newPwd != $new2Pwd){
                return dyajaxReturn(-1,'两次密码不一致');
            }else{
                $row = Db::name('admin')->where('admin_id' , $admin_id)->save(array('password' => $enNewPwd));
                if($row){
                    adminLog('更新密码');
                    return dyajaxReturn(1,'修改成功');
                }else{
                    return dyajaxReturn(1,'修改失败');
                }
            }
        }
        return View::fetch();
    }
    
    public function admin_info(){
    	$admin_id = input('get.admin_id/d',0);
		$city = array();
    	if($admin_id){
    		$info = Db::name('admin')->where("admin_id", $admin_id)->find();
			$info['password'] =  "";
    		View::assign('info',$info);
			if($info['province_id'] > 0){
				$city = Db::name('region')->where(['level'=>2,'pid'=>$info['province_id'],'is_show'=>1])->field('id,name')->select()->toArray();//获取市
			}
			
    	}
    	$act = empty($admin_id) ? 'add' : 'edit';
    	View::assign('act',$act);
    	View::assign('city',$city);
    	$role = Db::name('admin_role')->where('role_id','>',1)->select();
    	View::assign('role',$role);

		$this->getprovince();
    	return View::fetch();
    }
    
    public function adminHandle(){
    	$data = input('post.');
		$act = $data['act'];
		$adminValidate = validate(\app\admincp\validate\Admin::class);

		if(!$adminValidate->scene($data['act'])->batch(true)->check($data)){
			return dyajaxReturn(0, '操作失败',$adminValidate->getError());
		}
		
		if(empty($data['password'])){
			unset($data['password']);
		}else{
			$data['password'] = encrypt($data['password']);
		}
		unset($data['act'],$data['auth_code']);
		if($data['province_id'] > 0){
			if(empty($data['city_id'])) return dyajaxReturn(0, '请选择所在城市');
		}
    	if($act == 'add'){ 		
    		unset($data['admin_id']);
    		$data['add_time'] = time();
			$r = Db::name('admin')->insertGetId($data);
            $log_info = "新增管理员：".$data['user_name'];
    	}
    	
    	if($act == 'edit'){
    		$r = Db::name('admin')->where('admin_id', $data['admin_id'])->save($data);
            $log_info = "更新管理员：".$data['user_name'];
    	}
        if($act == 'del' && $data['admin_id']>1){
            $user_name = Db::name('admin')->where('admin_id', $data['admin_id'])->value('user_name');
    		$r = Db::name('admin')->where('admin_id', $data['admin_id'])->delete();
            $log_info = "删除管理员：".$user_name;
    	}
    	
    	if($r){
            adminLog($log_info);
			return dyajaxReturn(1, '操作成功','',url('Admin/index'));
		}else{
			return dyajaxReturn(0, '操作失败或是数据未修改');
    	}
    }

	private function getprovince(){
		$province = Db::name('region')->where(['level'=>1,'is_show'=>1])->field('id,name')->cache(true)->select()->toArray();//获取省
      	View::assign('province', $province);
	}


	public function delAdmins(){
		$ids = input('post.ids','');
        if(empty($ids)){
            return dyajaxReturn(-1,'非法操作');
        }
		$shippingIds = rtrim($ids);
		Db::name('shipping')->whereIn('shipping_id',$shippingIds)->delete();
        return dyajaxReturn(1,'操作成功',['url'=>url("Shipping/index")]);
	}
    
    
    /**
     * 管理员登陆
     */
    public function login(){

        if (IS_POST) {
            $code = input('post.vertify');
            $username = input('post.username/s');
            $password = input('post.password/s');
            $verify = new Verify();
            if (!$verify->check($code, "admin_login")) {
                return dyajaxReturn(0,'验证码错误');
            }
            $adminLogic = new AdminLogic;
            $return = $adminLogic->login($username, $password);
            return dyajaxReturn($return['errcode'],$return['message'],[],$return['dyurl']);
        }

        if (session('?admin_id') && session('admin_id') > 0) {
            $this->error("您已登录", url('Index/index'));
        }

        return View::fetch();
    }
    
    /**
     * 退出登陆
     */
    public function logout(){
        $adminLogic = new AdminLogic;
        $adminLogic->logout(session('admin_id'));

        $this->success("退出成功", url('Admin/login'));
    }
    
    /**
     * 验证码获取
     */
    public function vertify()
    {
        $config = array(
            'fontSize' => 30,
            'length' => 4,
            'useCurve' => false,
            'useNoise' => true,
            'fontttf' => '2.ttf',
        	'reset' => false
        );    
        $Verify = new Verify($config);
        $Verify->entry("admin_login");
        exit();
    }
    
    public function role(){
    	$list = Db::name('admin_role')->order('role_id desc')->select();
    	View::assign('list', $list);
    	return View::fetch();
    }
    
    public function role_info(){
    	$role_id = input('get.role_id/d');
    	$detail = array();
    	if($role_id){
    		$detail = Db::name('admin_role')->where("role_id",$role_id)->find();
    		$detail['act_list'] = explode(',', $detail['act_list']);
    		View::assign('detail', $detail);
    	}
        $moduleLogic = new ModuleLogic;
        $modules = $moduleLogic->getModules();
        $order_fields = array_keys($modules[0]['privilege']);
		$right = Db::name('system_menu')->select();
		foreach ($right as $val){
			if(!empty($detail)){
				$val['enable'] = in_array($val['id'], $detail['act_list']);
			}
            $modules[$val['group']][] = $val;
		}
        $new_modules = [];
        foreach ($order_fields as $v){
            if(isset($modules[$v])){
                $new_modules[$v] = $modules[$v];
            }
        }
		//admin权限组
        $group = (new ModuleLogic)->getPrivilege(0);
		View::assign('group',$group);
		View::assign('modules',$new_modules);
    	return View::fetch();
    }
    
    public function roleSave(){
    	$data = input('post.');
    	$res = $data['data'];
    	$res['act_list'] = is_array($data['right']) ? implode(',', $data['right']) : '';
        if(empty($res['act_list']))
            $this->error("请选择权限!");        
    	if(empty($data['role_id'])){
			$admin_role = Db::name('admin_role')->where(['role_name'=>$res['role_name']])->find();
			if($admin_role){
				$this->error("已存在相同的角色名称!");
			}else{
				$r = Db::name('admin_role')->insertGetId($res);
                $log_info = "新增角色：".$res['role_name'];
			}
    	}else{
			$admin_role = Db::name('admin_role')->where(['role_name'=>$res['role_name'],'role_id'=>['<>',$data['role_id']]])->find();
			if($admin_role){
				$this->error("已存在相同的角色名称!");
			}else{
				$r = Db::name('admin_role')->where('role_id', $data['role_id'])->save($res);
                $log_info = "更新角色：".$res['role_name'];
			}
    	}
		if($r){
			adminLog($log_info);
			$this->success("操作成功!",url('Admin/role_info',array('role_id'=>$data['role_id'])));
		}else{
			$this->error("操作失败!",url('Admin/role'));
		}
    }
    
    public function roleDel(){
    	$role_id = input('post.role_id/d');
    	$admin = Db::name('admin')->where('role_id',$role_id)->find();
    	if($admin){
            return dyajaxReturn(0,'请先清空所属该角色的管理员');
    	}else{
    	    $role_name = Db::name('admin_role')->where("role_id", $role_id)->value('role_name');
    		$d = Db::name('admin_role')->where("role_id", $role_id)->delete();
    		if($d){
                adminLog("删除角色：".$role_name);
                return dyajaxReturn(1,'操作成功',['url'=>url("Admin/role")]);
    		}else{
                return dyajaxReturn(0,'删除失败');
    		}
    	}
    }

	public function delRoles(){
		$ids = input('post.ids','');
		empty($ids) && $this->ajaxReturn(['status' => -1,'msg' =>"非法操作！",'data'  =>'']);
		$rolesIds = rtrim($ids);
		$admin = Db::name('admin')->whereIn('role_id',$rolesIds)->find();
		if ($admin) {
			$this->ajaxReturn(['status' => -1,'msg' => '请先清空所属该角色的管理员','url'=>url("Admin/role")]);
		} else {
            $role_name_arr = Db::name('admin_role')->whereIn('role_id', $rolesIds)->column('role_name');
            $role_name_str = implode(',',$role_name_arr);
            $log_info = '批量删除角色：'.$role_name_str;
			Db::name('admin_role')->whereIn('role_id',$rolesIds)->delete();
            adminLog($log_info);
			$this->ajaxReturn(['status' => 1,'msg' => '操作成功','url'=>url("Admin/role")]);
		}
	}
    
    public function log(){
		$p = input('p/d',1);
		$where['l.admin_id'] = ['>',1];
    	$logs = DB::name('admin_log')->alias('l')->join('admin a','a.admin_id =l.admin_id')->where($where)->order('log_time DESC')->page($p.',20')->select();
    	View::assign('list',$logs);
    	$count = DB::name('admin_log')->alias('l')->where($where)->count();
    	$Page = new Page($count,20);
    	$show = $Page->show();
		View::assign('pager',$Page);
		View::assign('page',$show);
    	return View::fetch();
    }
}