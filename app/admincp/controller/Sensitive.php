<?php

namespace app\admincp\controller;

use think\facade\View;
use think\Page;
use think\facade\Db;

class Sensitive extends Base
{
    //敏感词列表
    public function sensitiveList()
    {
        $keywords = trim(input('keywords'));
        $keywords && $where['name'] = ['like','%'.$keywords.'%'];
        $condition['keywords'] = $keywords;
        $count = Db::name('sensitive')->where($where)->count();// 查询满足要求的总记录数
        $Page = new Page($count, $this->page_size);// 实例化分页类 传入总记录数和每页显示的记录数
        $list = Db::name('sensitive')->where($where)
            ->order('id desc')->limit($Page->firstRow, $Page->listRows)
            ->select()->toArray();
        if ($list) {
            foreach ($list as &$val) {
                $val['add_time'] = date('Y-m-d H:i:s', $val['add_time']);
            }
        }
        $show = $Page->show();// 分页显示输出
        View::assign('list', $list);// 赋值数据集
        View::assign('pager', $Page);// 赋值分页输出
        View::assign('page', $show);// 赋值分页输出
        View::assign('condition', $condition);
        return View::fetch();
    }

    /* 获取敏感词信息 */
    public function sensitive()
    {
        $act = "add";
        $info = array();
        $id = input('get.id/d');
        if ($id > 0) {
            $info = Db::name('sensitive')->where(['id'=>$id])->find();
            $act = 'edit';
        }
        View::assign('act', $act);
        View::assign('info', $info);
        return View::fetch();
    }

    /* 更新敏感词信息 */
    public function sensitiveHandle()
    {
        $data = input('post.');
        $validate = validate('Sensitive.'.$data['act']);
        if (!$validate->batch(true)->check($data)) {
            // 验证失败 输出错误信息
            $error = $validate->getError();
            $error_msg = array_values($error);
            return dyajaxReturn(0, $error_msg[0], $error);
        }
        $act = $data['act'];
        unset($data['act']);
        if ($act == 'add') {            
            $data['add_time'] = time();
            $r = Db::name('sensitive')->insertGetId($data);
            $log_info = "新增敏感词";
        } elseif ($act == 'edit') {
            $r = Db::name('sensitive')->update($data);
            $log_info = "更新敏感词";
        } elseif ($act == 'del') {
            $r = Db::name('sensitive')->where(['id' => $data['id']])->delete();
            $log_info = "删除敏感词";
        }
        if (!$r) {
            return dyajaxReturn(0, '操作失败或未做修改');
        }
        adminLog($log_info);
        return dyajaxReturn(1, '操作成功');
    }

    //删除
    public function delList()
    {
        $ids = input('post.ids', '');
        if ($ids == '') return dyajaxReturn(0, '非法操作');
        $listIds = rtrim($ids);
        $log_info = '批量删除敏感词';
        Db::name('sensitive')->whereIn('id', $listIds)->delete();
        adminLog($log_info);
        return dyajaxReturn(1, '操作成功',[]);
    }
}
