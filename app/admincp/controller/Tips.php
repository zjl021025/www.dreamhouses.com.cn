<?php

namespace app\admincp\controller;

use app\admincp\validate\NodeProblem;
use think\facade\View;
use think\facade\Db;
use think\Page;

//装修贴士
class Tips extends Base
{
    //列表
    public function tipsList()
    {
        $keywords = input('keywords');
        if(empty($keywords)){
            $count = Db::name('fitment')->where(['fid'=>0,'delete_time'=>0])->count();
            $pager = new Page($count,$this->page_size);
            $show = $pager->show();// 分页显示输出
            $list = Db::name('fitment')->where(['fid'=>0,'delete_time'=>0])->order('level')->select()->toArray();
            foreach ($list as $key=>$val){
                $list[$key]['nodeNum'] = Db::name('fitment')->where(['fid'=>$val['fitment_id'],'delete_time'=>0])->count();
                $list[$key]['create_time'] = date('Y-m-d',$val['create_time']);
            }
        }else{
            $count = Db::name('fitment')->where(['fid'=>0,'delete_time'=>0,['name','like',"%$keywords%"]])->count();
            $pager = new Page($count,$this->page_size);
            $show = $pager->show();// 分页显示输出
            $list = Db::name('fitment')->where(['fid'=>0,'delete_time'=>0,['name','like',"%$keywords%"]])->order('level')->select()->toArray();
            foreach ($list as $key=>$val){
                $list[$key]['nodeNum'] = Db::name('fitment')->where(['fid'=>$val['fitment_id'],'delete_time'=>0,'is_show'=>1])->count();
                $list[$key]['create_time'] = date('Y-m-d',$val['create_time']);
            }
        }
        $condition['keywords'] = $keywords;
        View::assign('list', $list);// 赋值数据集
        View::assign('pager', $pager);// 赋值分页输出
        View::assign('show', $show);// 赋值分页输出
        View::assign('condition', $condition);
        return View::fetch();
    }
    /* 详情信息 */
    public function tips()
    {
        $act = "add";
        $info = [];
        $fitment_id = input('get.fitment_id/d');
        if ($fitment_id > 0) {
            $info = Db::name('fitment')->where(['fitment_id'=>$fitment_id])->find();
            $act = 'edit';
        }
        View::assign('act', $act);
        View::assign('info', $info);
        return View::fetch();
    }
    /* 更新信息 */
    public function tipsHandle()
    {
        $data = input('post.');
        $validate = validate('tips.'.$data['act']);
        if (!$validate->batch(true)->check($data)) {
            // 验证失败 输出错误信息
            $error = $validate->getError();
            $error_msg = array_values($error);
            return dyajaxReturn(0, $error_msg[0], $error);
        }
        $act = $data['act'];
        unset($data['act']);
        if ($act == 'add') {            
            $data['create_time'] = time();
            $r = Db::name('fitment')->insertGetId($data);
            $log_info = "新增装修贴士:".$data['name'];
        } elseif ($act == 'edit') {
            $name = Db::name('fitment')->where('fitment_id', $data['fitment_id'])->value('name');
            $r = Db::name('fitment')->update($data);
            $log_info = "更新装修贴士:".$name;
        } elseif ($act == 'del') {
            $name = Db::name('fitment')->where('fitment_id', $data['fitment_id'])->value('name');
            $delData = Db::name('fitment')->where(['fid'=>$data['fitment_id'],'delete_time'=>0])->select()->toArray();
            if(empty($delData)){
                $r = Db::name('fitment')->where(['fitment_id' => $data['fitment_id']])->update(['is_show'=>0,'delete_time'=>time()]);
                $log_info = "删除装修贴士:".$name;
            }else{
                $log_info = "该记录下有数据";
            }
        }
        if (!$r) {
            return dyajaxReturn(0, $log_info);
        }
        adminLog($log_info);
        return dyajaxReturn(1, '操作成功');
    }
    //删除文章列表
    public function delList()
    {
        $ids = input('post.ids', '');
        if ($ids == '') return dyajaxReturn(0, '非法操作');
        $listIds = rtrim($ids);
        $log_info = '批量删除装修贴士:';
        $name_arr = Db::name('fitment')->whereIn('fitment_id', $listIds)->column('name');
        $name_str = implode(',',$name_arr);
        $delData = [];
        $listIds = explode(',',$listIds);
        foreach ($listIds as $val){
            $delData[] = Db::name('fitment')->where('fid', $val)->select()->toArray();
        }
        if(empty($delData)){
            Db::name('fitment')->whereIn('fitment_id', $listIds)->update(['delete_time'=>time(),'is_show'=>0]);
            adminLog($log_info.$name_str);
        }else{
            return dyajaxReturn(0, $delData);
        }
        return dyajaxReturn(1, '操作成功',[]);
    }
    //节点列表
    public function nodeList(){
        $fitment_id = input('fitment_id');
        $keywords = input('keywords');
        if(empty($keywords)){
            $count = Db::name('fitment')->where(['fid'=>$fitment_id,'delete_time'=>0])->count();
            $pager = new Page($count,$this->page_size);
            $show = $pager->show();// 分页显示输出
            $list = Db::name('fitment')->where(['fid'=>$fitment_id,'delete_time'=>0])->order('level')->select()->toArray();
            foreach ($list as $key=>$val){
                $list[$key]['create_time'] = date('Y-m-d',$val['create_time']);
            }
        }else{
            $count = Db::name('fitment')->where(['fid'=>$fitment_id,'delete_time'=>0,['name','like',"%$keywords%"]])->count();
            $pager = new Page($count,$this->page_size);
            $show = $pager->show();// 分页显示输出
            $list = Db::name('fitment')->where(['fid'=>$fitment_id,'delete_time'=>0,['name','like',"%$keywords%"]])->order('level')->select()->toArray();
            foreach ($list as $key=>$val){
                $list[$key]['create_time'] = date('Y-m-d',$val['create_time']);
            }
        }
        $condition['keywords'] = $keywords;
        View::assign('list', $list);// 赋值数据集
        View::assign('fid',$fitment_id);
        View::assign('pager', $pager);// 赋值分页输出
        View::assign('show', $show);// 赋值分页输出
        View::assign('condition', $condition);
        return View::fetch();
    }
    /* 更新信息 */
    public function tipsNextHandle()
    {
        $data = input('post.');
        $validate = validate('tips.'.$data['act']);
        if (!$validate->batch(true)->check($data)) {
            // 验证失败 输出错误信息
            $error = $validate->getError();
            $error_msg = array_values($error);
            return dyajaxReturn(0, $error_msg[0], $error);
        }
        $act = $data['act'];
        unset($data['act']);
        if ($act == 'add') {
            $data['create_time'] = time();
            $r = Db::name('fitment')->insertGetId($data);
            $log_info = "新增装修贴士:".$data['name'];
        } elseif ($act == 'edit') {
            $name = Db::name('fitment')->where('fitment_id', $data['fitment_id'])->value('name');
            $r = Db::name('fitment')->update($data);
            $log_info = "更新装修贴士:".$name;
        } elseif ($act == 'del') {
            $name = Db::name('fitment')->where('fitment_id', $data['fitment_id'])->value('name');
            $r = Db::name('fitment')->where(['fitment_id' => $data['fitment_id']])->update(['is_show'=>0,'delete_time'=>time()]);
            $log_info = "删除装修贴士:".$name;
        }
        if (!$r) {
            return dyajaxReturn(0, '操作失败或未做修改');
        }
        adminLog($log_info);
        return dyajaxReturn(1, '操作成功');
    }
    /* 详情信息 */
    public function insertNode()
    {
        $fitment_id = input('get.fitment_id/d');
        $fid = input('fid');
        $act = "add";
        $info = [];
        if ($fitment_id > 0) {
            $info = Db::name('fitment')->where(['fitment_id'=>$fitment_id])->find();
            $act = 'edit';
        }
        $info['lastName'] = Db::name('fitment')->where(['fitment_id'=>$fid])->value('name');
        View::assign('act', $act);
        View::assign('info', $info);
        View::assign('fid',$fid);
        return View::fetch();
    }

    /**
     * 添加注意事项
     * @return void
     */
    public function nodeProblem(){
        $fitment_id = input('fitment_id');
        //获取当前注意事项所属的节点
        $node = Db::name('fitment')->where(['fitment_id'=>$fitment_id])->find();
        View::assign('node',$node);
        return View::fetch();
    }

    /**
     * 添加注意事项
     * @return void
     */
    public function insertNodeProblem(){
        $param = input('post.');
        $param['act'] = 'edit';
        $validate = new NodeProblem();
        if (!$validate->check($param)) {
            // 验证失败 输出错误信息
            $error = $validate->getError();
            $error_msg = array_values($error);
            return dyajaxReturn(0, $error_msg[0], $error);
        }
        $data = [
            'fitment_id'=>$param['fitment_id'],
            'title'=>$param['title'],
            'sort'=>$param['level'],
            'content'=>$param['content'],
            'create_time' => time(),
            'images' => $param['images_url'],
            'title_remind' => $param['title_remind']
        ];
        $insert = Db::name('notes')->insertGetId($data);
        $log_info = "新增装修贴士:".$param['title'];
        if(!$insert){
            return dyajaxReturn(0,'添加失败');
        }
        adminLog($log_info);
        return dyajaxReturn(1,'添加成功',);
    }

    /**
     * 修改注意事项
     * @return void
     */
    public function nodeProblemUpdate(){
        $id = input('id');
        //获取当前注意事项
        $data = Db::name('notes')->where(['id'=>$id])->find();
        //查看上级名称
        $node = Db::name('fitment')->where(['fitment_id'=>$data['fitment_id']])->find();
        View::assign('data',$data);
        View::assign('node',$node);
        return View::fetch();
    }

    /**
     * 修改
     * @return void
     */
    public function updateNodeProblem(){
        $param = input('post.');
        $validate = new NodeProblem();
        if (!$validate->check($param)) {
            // 验证失败 输出错误信息
            $error = $validate->getError();
            $error_msg = array_values($error);
            return dyajaxReturn(0, $error_msg[0], $error);
        }
        $update = Db::name('notes')
            ->where(['id'=>$param['id']])
            ->update(['title'=>$param['title'],'sort'=>$param['sort'],'content'=>$param['content'],'update_time'=>time(),'images'=>$param['images_url'],'title_remind'=>$param['title_remind'],'is_show'=>$param['is_show']]);
        $log_info = "修改装修贴士:".$param['title'];
        if(!$update){
            return dyajaxReturn(0,'修改失败');
        }
        adminLog($log_info);
        return dyajaxReturn(1,'修改成功',);
    }

    /**
     * 删除注意事项
     * @return void
     */
    public function delNodeProblem(){
        $id = input('id');
        $del = Db::name('notes')->where(['id'=>$id])->update(['delete_time'=>time(),'is_show'=>0]);
        if(!$del){
            return dyajaxReturn(0,'删除失败');
        }
        return dyajaxReturn(1,'删除成功');
    }
    /**
     * 注意事项列表
     * @return void
     */
    public function nodeProblemList(){
        $fitment_id = input('fitment_id');
        $keyword = input('keywords');
        $condition['keywords'] = $keyword;
        if(empty($keyword)){
            $count = Db::name('notes')->where(['fitment_id'=>$fitment_id,'delete_time'=>0])->count();
            $pager = new Page($count,$this->page_size);
            $show = $pager->show();// 分页显示输出
            $list = Db::name('notes')->where(['fitment_id'=>$fitment_id,'delete_time'=>0])->select()->toArray();
            foreach ($list as $key=>$val){
                $list[$key]['create_time'] = date('Y-m-d',$val['create_time']);
            }
        }else{
            $count = Db::name('notes')->where(['fitment_id'=>$fitment_id,'delete_time'=>0,['title','like',"%$keyword%"]])->count();
            $pager = new Page($count,$this->page_size);
            $show = $pager->show();// 分页显示输出
            $list = Db::name('notes')->where(['fitment_id'=>$fitment_id,'delete_time'=>0,['title','like',"%$keyword%"]])->select()->toArray();
            foreach ($list as $key=>$val){
                $list[$key]['create_time'] = date('Y-m-d',$val['create_time']);
            }
        }
        View::assign('list', $list);// 赋值数据集
        View::assign('fitment_id',$fitment_id);
        View::assign('pager', $pager);// 赋值分页输出
        View::assign('show', $show);// 赋值分页输出
        View::assign('condition', $condition);
        return View::fetch();
    }
    /**
     * 注意事项详情列表
     * @return void
     */
    public function nodeItemList(){
        $node_id = input('node_id');
        $keyword = input('keywords');
        $condition['keywords'] = $keyword;
        if(empty($keyword)){
            $count = Db::name('notes_item')->where(['notes_id'=>$node_id,'is_deleted'=>0])->count();
            $pager = new Page($count,$this->page_size);
            $show = $pager->show();// 分页显示输出
            $list = Db::name('notes_item')->where(['notes_id'=>$node_id,'is_deleted'=>0])->select()->toArray();
        }else{
            $count = Db::name('notes_item')->where(['notes_id'=>$node_id,'is_deleted'=>0,['content','like',"%$keyword%"]])->count();
            $pager = new Page($count,$this->page_size);
            $show = $pager->show();// 分页显示输出
            $list = Db::name('notes_item')->where(['notes_id'=>$node_id,'is_deleted'=>0,['content','like',"%$keyword%"]])->select()->toArray();
        }
        View::assign('list', $list);// 赋值数据集
        View::assign('node_id',$node_id);
        View::assign('pager', $pager);// 赋值分页输出
        View::assign('show', $show);// 赋值分页输出
        View::assign('condition', $condition);
        return View::fetch();
    }

    /**
     * 删除详情
     * @return void
     */
    public function delNodeItem(){
        $id = input('id');
        $del = Db::name('notes_item')->where(['id'=>$id])->update(['is_deleted'=>1,'is_show'=>0]);
        if(!$del){
            return dyajaxReturn(0,'删除失败');
        }
        return dyajaxReturn(1,'删除成功');
    }

    /**
     * 添加详情
     * @return void
     */
    public function nodeItem(){
        $node_id = input('node_id');
        //获取信息
        $node = Db::name('notes')->where(['id'=>$node_id])->find();
        View::assign('node',$node);
        return View::fetch();
    }

    /**
     * 添加详情
     * @return void
     */
    public function insertNodeItem(){
        $param = input('post.');
        if(empty($param['is_show'])){
            return dyajaxReturn(0,'数据传输错误');
        }
        $data = [
            'contents' => $param['content'],
            'image' => $param['image_url'],
            'video' => $param['video_url'],
            'notes_id' => $param['notes_id'],
            'is_show' => $param['is_show']
        ];
        $insert = Db::name('notes_item')->insert($data);
        if(!$insert){
            return dyajaxReturn(0,'添加失败');
        }
        return dyajaxReturn(1,'添加成功');
    }

    /**
     * 修改详情页面
     * @return void
     */
    public function nodeItemUpdate(){
        $id = input('id');
        $data = Db::name('notes_item')->where(['id'=>$id])->find();
        $node = Db::name('notes')->where(['id'=>$data['notes_id']])->find();
        View::assign('node',$node);
        View::assign('data',$data);
        return View::fetch();
    }

    /**
     * 修改详情
     * @return void
     */
    public function updateNodeItem(){
        $param = input('post.');
        $update = Db::name('notes_item')->where(['id'=>$param['id']])->update(['contents'=>$param['content'],'image'=>$param['image_url'],'video'=>$param['video_url'],'is_show'=>$param['is_show'],'notes_id'=>$param['notes_id']]);
        if(!$update){
            return dyajaxReturn(0,'修改失败');
        }
        return dyajaxReturn(1,'修改成功');
    }
    /**
     * 上传图片
     * @return void
     */
    public function uploadImage(){
        $file = \request()->file('file');
        $domain = config('qiniu.domain');
        $qiniu_file = \think\facade\Filesystem::disk('qiniu')->putFile('image',$file);
        $baseUrl = 'http://'.$domain.'/'.$qiniu_file;
        return dyajaxReturn(1,'成功',$baseUrl);
    }
    /**
     * 上传视频
     * @return void
     */
    public function uploadVideo(){
        $file = \request()->file('file');
        $domain = config('qiniu.domain');
        $qiniu_file = \think\facade\Filesystem::disk('qiniu')->putFile('video',$file);
        $baseUrl = 'http://'.$domain.'/'.$qiniu_file;
        return dyajaxReturn(1,'成功',$baseUrl);
    }
}
