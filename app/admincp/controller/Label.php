<?php

namespace app\admincp\controller;

use app\admincp\model\Label as LabelNotice;
use think\facade\View;
use think\facade\Db;

//标签
class Label extends Base
{
    //列表
    public function labelList($typeid)
    {
        $LabelNotice = new LabelNotice();
        $info = $LabelNotice->getList($typeid);
        $list = $info['list'];
        $pager = $info['pager'];
        $show = $info['show'];
        $condition = $info['condition'];
        View::assign('list', $list);// 赋值数据集
        View::assign('pager', $pager);// 赋值分页输出
        View::assign('show', $show);// 赋值分页输出
        View::assign('condition', $condition);
        return View::fetch();
    }
    /* 详情信息 */
    public function label($typeid)
    {
        $act = "add";
        $info = [];
        $id = input('get.id/d');
        if ($id > 0) {
            $info = Db::name('label')->where(['id'=>$id])->find();
            $act = 'edit';
        }
        View::assign('act', $act);
        View::assign('info', $info);
        View::assign('typeid',$typeid);
        return View::fetch();
    }

    /* 更新信息 */
    public function labelHandle($typeid)
    {
        $param = input('post.');
        $validate = validate('label.'.$param['act']);
        if (!$validate->batch(true)->check($param)) {
            // 验证失败 输出错误信息
            $error = $validate->getError();
            $error_msg = array_values($error);
            return dyajaxReturn(0, $error_msg[0], $error);
        }
        $act = $param['act'];
        $typeid_list = [
            1=>'设计经验',
            2=>'设计风格',
            3=>'设计费用',
            4=>'户型类型',
            5=>'房间类型',
            6=>'预算类型',
            7=>'面积类型',
        ];
        if(in_array($act,['add','edit'])){
            $data = [
                'name'=>$param['name'],
                'typeid'=>$typeid,
                'is_show'=>$param['is_show'],
                'min'=>$param['min']?$param['min']:0,
                'max'=>$param['max']?$param['max']:0,
            ];
            if ($act == 'add') {
                $r = Db::name('label')->insertGetId($data);
                $log_info = "新增".$typeid_list[$typeid].":".$r;
            } elseif ($act == 'edit') {
                $r = Db::name('label')->where('id', $param['id'])->update($data);
                $log_info = "更新".$typeid_list[$typeid].":". $param['id'];
            }
        }else{
            if ($act == 'del') {
                $r = Db::name('label')->where(['id' => $param['id']])->delete();
                $log_info = "删除".$typeid_list[$typeid].":". $param['id'];
            }
        }
        if (!$r) {
            return dyajaxReturn(0, '操作失败或未做修改');
        }
        adminLog($log_info);
        return dyajaxReturn(1, '操作成功');
    }

    //设计经验列表
    public function expeList()
    {
        return $this->labelList(1);
    }

    //设计经验详情
    public function expe()
    {
        return $this->label(1);
    }

    //设计经验操作
    public function expeHandle()
    {
        return $this->labelHandle(1);
    }

    //设计风格列表
    public function styleList()
    {
        return $this->labelList(2);
    }

    //设计风格详情
    public function style()
    {
        return $this->label(2);
    }

    //设计风格操作
    public function styleHandle()
    {
        return $this->labelHandle(2);
    }

    //设计费用列表
    public function priceList()
    {
        return $this->labelList(3);
    }

    //设计费用详情
    public function price()
    {
        return $this->label(3);
    }

    //设计费用操作
    public function priceHandle()
    {
        return $this->labelHandle(3);
    }

    //户型类型列表
    public function housetypeList()
    {
        return $this->labelList(4);
    }

    //户型类型详情
    public function housetype()
    {
        return $this->label(4);
    }

    //户型类型操作
    public function housetypeHandle()
    {
        return $this->labelHandle(4);
    }

    //房间类型列表
    public function roomList()
    {
        return $this->labelList(5);
    }

    //房间类型详情
    public function room()
    {
        return $this->label(5);
    }

    //房间类型操作
    public function roomHandle()
    {
        return $this->labelHandle(5);
    }

    //预算类型列表
    public function budgetList()
    {
        return $this->labelList(6);
    }

    //预算类型详情
    public function budget()
    {
        return $this->label(6);
    }

    //预算类型操作
    public function budgetHandle()
    {
        return $this->labelHandle(6);
    }

    //面积类型列表
    public function areaList()
    {
        return $this->labelList(7);
    }

    //面积类型详情
    public function area()
    {
        return $this->label(7);
    }

    //面积类型操作
    public function areaHandle()
    {
        return $this->labelHandle(7);
    }
}
