<?php

namespace app\admincp\controller;

use think\facade\Db;
use think\facade\Request;
use think\facade\View;
use think\Page;

class Dycase extends Base
{
    //平台案例
    public function platformCase(){
        $title = input('title');
        $nickname = input('nickname');
        $create_times = input('create_time');
        $create_time = strtotime($create_times);
        $where = ['dy.is_expert' => 1,'dy.is_deleted' => 0];
        if(!empty($title)){
            $where = ['dy.title','like',"%$title%"];
        }
        if(!empty($nickname)){
            $where = ['users.nickname','like',"%$nickname%"];
        }
        if(!empty($create_time)){
            $where = ['dy.add_time','like',"%$create_time%"];
        }
        $count = Db::name('user_dynamic')->alias('dy')->leftJoin('users','dy.user_id = users.user_id')->where($where)->count();
        $pager = new Page($count,$this->page_size);
        $show = $pager->show();
        $dynamicData = Db::name('user_dynamic')->alias('dy')->leftJoin('users','dy.user_id = users.user_id')->where($where)->limit($pager->firstRow,$pager->listRows)->select()->toArray();
        foreach ($dynamicData as $key=>$val){
            $dynamicData[$key]['style'] = Db::name('label')->where('id',$val['style_id'])->value('name');
            $dynamicData[$key]['house_type'] = Db::name('label')->where('id',$val['house_type_id'])->value('name');
            $dynamicData[$key]['add_time'] = date('Y-m-d',$val['add_time']);
        }
        View::assign('dynamicData',$dynamicData);
        View::assign('pager',$pager);
        View::assign('show',$show);
        return View::fetch();
    }
    //平台案例添加
    public function platformCaseInsert(){
        return View::fetch();
    }
    //案例编辑
    public function platformCaseUpdate(){
        return View::fetch();
    }
    //案例查看
    public function platformCaseLook(){
        return View::fetch();
    }

    /**
     * 删除平台案例
     * @return void
     */
    public function platformCaseDel(){
        $dynamic_id = input('dynamic_id');
        $del = Db::name('user_dynamic')->where(['dynamic_id'=>$dynamic_id])->update(['is_deleted'=>1,'is_show'=>0]);
        if(!$del){
            return dyajaxReturn(0,'删除失败');
        }
        return dyajaxReturn(1,'删除成功');
    }
    //被收藏
    public function platformCaseCollect(){
        return View::fetch();
    }
    //被点赞
    public function platformCaseLike(){
        return View::fetch();
    }
    //用户发布案例
    public function userPublishing(){
        $title = input('title');
        $nickname = input('nickname');
        $create_times = input('create_time');
        $create_time = strtotime($create_times);
        $where = ['dy.is_expert' => 0,'dy.is_deleted' => 0];
        if(!empty($title)){
            $where = ['dy.title','like',"%$title%"];
        }
        if(!empty($nickname)){
            $where = ['users.nickname','like',"%$nickname%"];
        }
        if(!empty($create_time)){
            $where = ['dy.add_time','like',"%$create_time%"];
        }
        $count = Db::name('user_dynamic')->alias('dy')->leftJoin('users','dy.user_id = users.user_id')->where($where)->count();
        $pager = new Page($count,$this->page_size);
        $show = $pager->show();
        $dynamicData = Db::name('user_dynamic')->alias('dy')->leftJoin('users','dy.user_id = users.user_id')->where($where)->limit($pager->firstRow,$pager->listRows)->select()->toArray();
        foreach ($dynamicData as $key=>$val){
            $dynamicData[$key]['style'] = Db::name('label')->where('id',$val['style_id'])->value('name');
            $dynamicData[$key]['house_type'] = Db::name('label')->where('id',$val['house_type_id'])->value('name');
            $dynamicData[$key]['add_time'] = date('Y-m-d',$val['add_time']);
        }
        View::assign('dynamicData',$dynamicData);
        View::assign('pager',$pager);
        View::assign('show',$show);
        return View::fetch();
    }
    //风格管理
    public function styleManagement(){
        $count = Db::name('label')->where(['typeid'=>2,'is_deleted'=>0])->count();
        $pager = new Page($count,$this->page_size);
        $show = $pager->show();
        $styleData = Db::name('label')->where(['typeid'=>2,'is_deleted'=>0])->order('sort')->select()->toArray();
        View::assign('styleData',$styleData);
        View::assign('pager',$pager);
        View::assign('show',$show);
        return View::fetch();
    }
    //风格修改
    public function styleManagementUpdate(){
        $id = input('id');
        $styleData = Db::name('label')->where(['id'=>$id])->find();
        View::assign('styleData',$styleData);
        return View::fetch();
    }
    public function updateStyle(){
        $param = Request::param();
        if(empty($param['name'])||empty($param['sort'])||empty($param['image'])){
            return dyajaxReturn(0,'数据不可以为空');
        }
        $update = Db::name('label')->where(['id'=>$param['id']])->update(['name'=>$param['name'],'sort'=>$param['sort'],'image'=>$param['image']]);
        if(!$update){
            return dyajaxReturn(0,'修改失败');
        }
        return dyajaxReturn(1,'修改成功');
    }
    //风格添加页面
    public function styleManagementInsert(){
        return View::fetch();
    }
    //风格添加
    public function insertStyle(){
        $param = Request::param();
        if(empty($param['name'])||empty($param['sort'])||empty($param['image'])){
            return dyajaxReturn(0,'数据不可以为空');
        }
        $insert = Db::name('label')->insert(['name'=>$param['name'],'sort'=>$param['sort'],'typeid'=>2,'image'=>$param['image']]);
        if(!$insert){
            return dyajaxReturn(0,'添加失败');
        }
        return dyajaxReturn(1,'添加成功');
    }

    /**
     * 删除风格
     * @return void
     */
    public function delStyle(){
        $id = input('id');
        $del = Db::name('label')->where(['id'=>$id])->update(['is_deleted'=>1,'is_show'=>0]);
        if(!$del){
            return dyajaxReturn(0,'删除失败');
        }
        return dyajaxReturn(1,'删除成功');
    }
    //布局管理
    public function layoutManager(){
        $houseTypeData = Db::name('label')->where(['typeid'=>4,'is_deleted'=>0])->order('sort')->select()->toArray();
        View::assign('housetypeData',$houseTypeData);
        return View::fetch();
    }
    //添加布局页面
    public function layoutManagerInsert(){
        return View::fetch();
    }

    /**
     * 添加布局
     * @return void
     */
    public function insertHouseType(){
        $param = Request::param();
        if(empty($param['name'])||empty($param['sort'])){
            return dyajaxReturn(0,'数据不能为空');
        }
        $insert = Db::name('label')->insert(['name'=>$param['name'],'sort'=>$param['sort'],'typeid'=>4]);
        if(!$insert){
            return dyajaxReturn(0,'添加失败');
        }
        return dyajaxReturn(1,'添加成功');
    }
    //编辑布局页面
    public function layoutManagerUpdate(){
        $id = input('id');
        $houseTypeData = Db::name('label')->where(['id'=>$id])->find();
        View::assign('houseTypeData',$houseTypeData);
        return View::fetch();
    }

    /**
     * 编辑布局
     * @return void
     */
    public function updateHouseType(){
        $param = Request::param();
        if(empty($param['name'])||empty($param['sort'])){
            return dyajaxReturn(0,'数据不能为空');
        }
        $update = Db::name('label')->where(['id'=>$param['id']])->update(['name'=>$param['name'],'sort'=>$param['sort']]);
        if(!$update){
            return dyajaxReturn(0,'修改失败');
        }
        return dyajaxReturn(1,'修改成功');
    }

    /**
     * 删除布局
     * @return void
     */
    public function delHouseType(){
        $id = input('id');
        $del = Db::name('label')->where(['id'=>$id])->update(['is_deleted'=>1,'is_show'=>0]);
        if(!$del){
            return dyajaxReturn(0,'删除失败');
        }
        return dyajaxReturn(1,'删除成功');
    }
}