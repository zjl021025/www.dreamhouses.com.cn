<?php


namespace app\admincp\controller;
use think\facade\View;

use app\common\logic\ModuleLogic;
use think\facade\Db;
use think\facade\Cache;

class System extends Base
{
	/*
	 * 配置入口
	 */
	public function index(){
		/*配置列表*/
		$group_list = [
//            'basic' => '通用设置',
            'water'    => '水印设置',
            'about'    => '关于我们',
            'resources'    => '资源上传',
        ];
		View::assign('group_list',$group_list);
		$inc_type =  input('get.inc_type','water');
		View::assign('inc_type',$inc_type);
		$config = dyCache($inc_type);
		View::assign('config',$config);//当前配置项
        View::assign('app', '');
		return View::fetch($inc_type);
	}

    public function handle(){
        $param = input('post.');
        $inc_type = $param['inc_type'];
        unset($param['inc_type']);
        if(strpos($param['domain_name'],'http://') !== false){
            $param['domain_name'] = substr($param['domain_name'],7);
        }
        if($inc_type!='resources'){
            dyCache($inc_type,$param);
        }else{
            //file
            $file = input('file_url');
            dy_zip('.'.$file);
        }
        switch ($inc_type){
            case 'distribut':
                return dyajaxReturn(1, '操作成功', $file,url('System/distribut'));
                break;
            default:
                return dyajaxReturn(1, '操作成功', $file,url('System/index',array('inc_type'=>$inc_type)));
                break;
        }
    }

    /**
     * 文件切片上传.
     * 简述流程: 以下用A文件举例说明
     * 1.前端传输A文件的唯一标识(这里A的唯一标识举例为:uniqueA) 且切好的片(这里切好的片举例为:①②③④,共四片)
     * 切断传输文件时要传输: 当前片的归属文件的唯一标识、当前是第几片、共几片。
     * 2.后端生成 uniqueA文件夹,用来存放①②③④片， 当前端传输完成之后,   进行①②③④片的合并处理.
     * 3.合并完成之后->删除文件标识生成的对应文件 并且做相对应的业务逻辑处理.
     * @author wh
     * @date 2020/06/03
     * @return \think\response\Json
     * @throws JsonException
     * @throws \OSS\Core\OssException
     */
    public function chunkUpload()
    {
        //禁止使用的后缀:
        $extBlackList = [
            'php',
            'js',
        ];


        $uniqueTag = request()->post('unique_tag','','string');//当前文件的唯一标识
        $nowNum = request()->post('now_num',0,'int');//当前是第几片
        $totalNum = request()->post('total_num',0,'int');//共切了几片
        $ext = request()->post('ext','','string');//文件的后缀,合并要用.
        $file = request()->file('file');//上传的文件

        if (in_array($ext,$extBlackList)){
            return dyajaxReturn(0, '危险后缀！');
        }


        if (empty($file) || empty($nowNum) || empty($totalNum) || empty($uniqueTag) || empty($ext)){
            return dyajaxReturn(0, '数据缺少！');
        }

        $saveSliceRootPath = UPLOAD_PATH.'zip/'.date('Y/m-d').'/';; //文件切片的存储目录(待合并的)
        $uniqueTagDir = $saveSliceRootPath.'/'.$uniqueTag; //当前文件的文件夹
        if (!(file_exists($uniqueTagDir))) @mkdir($uniqueTagDir,0777,true);
        $savename = \think\facade\Filesystem::disk('public')->putFileAs('zip/'.date('Y/m-d').'/'.$uniqueTag,$file,$nowNum); //return false or savename

        if($savename === false){
            return dyajaxReturn(0, '当前片上传失败！');
        }

        //如果当前第几片和共几片相同 代表该文件已经分片上传完成 进行合并处理.
        if($nowNum == $totalNum){
            $mergeResult = $this->mergeSlice($uniqueTagDir,$totalNum,$ext);
            if ($mergeResult === false) {
                return dyajaxReturn(0, '上传失败！');
            }else{
                return dyajaxReturn(1, '上传成功了！',$mergeResult);
            }
        }
        return dyajaxReturn(1, '上传成功了！');

    }

    /**
     * 合并存储的片
     * @author wh
     * @date 2020/06/03
     * @param $uniqueTagDir 当前要上传文件的文件夹(里面存放的是片)
     * @param $totalNum 总片数
     * @param $ext 后缀
     * @return bool or saveUrl
     * @throws JsonException
     * @throws \OSS\Core\OssException
     */
    private function mergeSlice($uniqueTagDir,$totalNum,$ext)
    {
        //文件名字
        $mergeFile = $uniqueTagDir.'.'.$ext;
        //打开文件
        $myfile = fopen($mergeFile, 'a');

        //进行合并操作.
        for ($i = 1; $i <= $totalNum; $i++) {
            // 单文件路径
            $filePart = $uniqueTagDir . DIRECTORY_SEPARATOR . $i;
            if(file_exists($filePart)){
                $chunk = file_get_contents($filePart);
                // 写入chunk
                fwrite($myfile, $chunk);
                @unlink($filePart);
            } else{
                return dyajaxReturn(0, "缺少第{$i}片文件，请重新上传");
            }
        }

        fclose($myfile);
        return $mergeFile;
    }

    /**
     * 清空系统缓存
     */
    public function cleanCache(){
        clearCache();
        $quick = input('quick',0);
		if($quick == 1){
		    if (input('ajax') == 'ajax') {
                $this->ajaxReturn(['status' => 1,'msg' => '操作成功','url'=>$quick]);
            }
			$script = "<script>parent.layer.msg('缓存清除成功', {time:3000,icon: 1});window.parent.location.reload();</script>";
		}else{
			$script = "<script>parent.layer.msg('缓存清除成功', {time:3000,icon: 1});window.location='/index.php?m=Admin&c=Index&a=welcome';</script>";
		}
       	exit($script);
    }
	    
    /**
     * 清空静态商品页面缓存
     */
      public function ClearGoodsHtml(){
            $goods_id = input('goods_id');            
            if(unlink("./Application/Runtime/Html/Home_Goods_goodsInfo_{$goods_id}.html")){
                // 删除静态文件                
                $html_arr = glob("./Application/Runtime/Html/Home_Goods*.html");
                foreach ($html_arr as $key => $val){            
                    strstr($val,"Home_Goods_ajax_consult_{$goods_id}") && unlink($val); // 商品咨询缓存
                    strstr($val,"Home_Goods_ajaxComment_{$goods_id}") && unlink($val); // 商品评论缓存
                }
                $json_arr = array('status'=>1,'msg'=>'清除成功','result'=>'');
            }else{
                $json_arr = array('status'=>-1,'msg'=>'未能清除缓存','result'=>'' );
            }                                                    
            $json_str = json_encode($json_arr);            
            exit($json_str);            
      } 
    /**
     * 商品静态页面缓存清理
     */
    public function ClearGoodsThumb(){
        $goods_id = input('goods_id');
        delFile(UPLOAD_PATH . "goods/thumb/" . $goods_id); // 删除缩略图
        Cache::clear('original_img_cache');
        $json_arr = array('status' => 1, 'msg' => '清除成功,请清除对应的静态页面', 'result' => '');
        $json_str = json_encode($json_arr);
        exit($json_str);
    }
    /**
     * 清空 文章静态页面缓存
     */
      public function ClearAritcleHtml(){
            $article_id = input('article_id');            
            unlink("./Application/Runtime/Html/Index_Article_detail_{$article_id}.html"); // 清除文章静态缓存
            unlink("./Application/Runtime/Html/Doc_Index_article_{$article_id}_api.html"); // 清除文章静态缓存
            unlink("./Application/Runtime/Html/Doc_Index_article_{$article_id}_phper.html"); // 清除文章静态缓存
            unlink("./Application/Runtime/Html/Doc_Index_article_{$article_id}_android.html"); // 清除文章静态缓存
            unlink("./Application/Runtime/Html/Doc_Index_article_{$article_id}_ios.html"); // 清除文章静态缓存
            $json_arr = array('status'=>1,'msg'=>'操作完成','result'=>'' );                                                          
            $json_str = json_encode($json_arr);            
            exit($json_str);            
      }
 
    
     
     function ajax_get_action(){
         $control = input('controller');
         $type = input('type',0);
         $module = (new ModuleLogic)->getModule($type);
         if (!$module) {
             exit('模块不存在或不可见');
         }

         $selectControl = [];
         $className = "app\\".$module['name']."\\controller\\".$control;
         $methods = (new \ReflectionClass($className))->getMethods(\ReflectionMethod::IS_PUBLIC);
         foreach ($methods as $method) {
             if ($method->class == $className) {
                 if ($method->name != '__construct' && $method->name != '_initialize') {
                     $selectControl[] = $method->name;
                 }
             }
         }

         $html = '';
         foreach ($selectControl as $val){
             $html .= "<li><label><input class='checkbox' name='act_list' value=".$val." type='checkbox'>".$val."</label></li>";
             if($val && strlen($val)> 18){
                 $html .= "<li></li>";
             }
         }
         exit($html);
     }
     
    function right_list(){
        $type = input('type',0);
        $moduleLogic = new ModuleLogic;
        if (!$moduleLogic->isModuleExist($type)) {
            $this->error('权限类型不存在');
        }
        $modules = $moduleLogic->getModules();
        $group = $moduleLogic->getPrivilege($type);

        $condition['type'] = $type;
        $name = input('name');
        if(!empty($name)){
            $condition['name|right'] = array('like',"%$name%");
        }
        $right_list = Db::name('system_menu')->where($condition)->order('group asc,id desc')->select();
        View::assign('right_list',$right_list);
        View::assign('group',$group);
        View::assign('modules',$modules);
        return View::fetch();
    }

    public function edit_right(){
        $type = input('type',0);
        $moduleLogic = new ModuleLogic;
        
        if (!$moduleLogic->isModuleExist($type)) {
            $this->error('模块不存在或不可见');
        }
        
        if(IS_POST){
            $data = input('post.');
            if(!$data['right']){
                $this->ajaxReturn(['status' => -1,'msg' => '请添加权限码']);
            }
            //去空格
            $data['name'] = trim($data['name']);
            
            $data['right'] = implode(',',$data['right']);
            unset($data['act_list']);
            if(!empty($data['id'])){
                Db::name('system_menu')->where(array('id'=>$data['id']))->save($data);
            }else{
                if(Db::name('system_menu')->where(array('type'=>$data['type'],'name'=>$data['name']))->count()>0){
                    $this->ajaxReturn(['status' => -1,'msg' => '该权限名称已添加，请检查']);
                }
                unset($data['id']);
                Db::name('system_menu')->insert($data);
            }
            $this->ajaxReturn(['status' => 1,'msg' => '操作成功']);
            exit;
        }
        $id = input('id');
        if($id){
            $info = Db::name('system_menu')->where(array('id'=>$id))->find();
            $info['right'] = explode(',', $info['right']);
            View::assign('info',$info);
        }

        $modules = $moduleLogic->getModules();
        $group = $moduleLogic->getPrivilege($type);
        $planPath = APP_PATH.$modules[$type]['name'].'/controller';
        $planList = array();
        $dirRes   = opendir($planPath);
        while($dir = readdir($dirRes))
        {
            if(!in_array($dir,array('.','..','.svn')))
            {
                $planList[] = basename($dir,'.php');
            }
        }
        sort($planList);//排序
        View::assign('modules', $modules);
        View::assign('planList',$planList);
        View::assign('group',$group);
        return View::fetch();
    }
     
     public function right_del(){
     	$id = input('del_id');
     	if(is_array($id)){
     		$id = implode(',', $id); 
     	}
     	if(!empty($id)){
     		$r = Db::name('system_menu')->where("id in ($id)")->delete();
     		if($r){
     			respose(1);
     		}else{
     			respose('删除失败');
     		}
     	}else{
     		respose('参数有误');
     	}
     }

    public function delList(){
        $ids = input('post.ids','');
        empty($ids) && $this->ajaxReturn(['status' => -1,'msg' =>"非法操作！",'data'  =>'']);
        $listIds = rtrim($ids);
        Db::name('system_menu')->whereIn('id',$listIds)->delete();
        $this->ajaxReturn(['status' => 1,'msg' => '操作成功','url'=>url("System/right_list")]);
    }
    
    /**
     * 公共删除数据方法 （慎用）
     * @param  name  string   表名称
     * @param  ids  string   id集合
     */
    public function deleteData(){
        $tableName = input('name','');
        $ids       = input('ids','');
        $field_name= input('field_name','');
        if(empty($tableName) ){
            $this->ajaxReturn(['status'=>0,'msg'=>'删除失败']);
        }
        //删除数据
        $result  = Db::name($tableName)->where([$field_name=>array('in',$ids)])->delete();
        if(!$result)  $this->ajaxReturn(['status'=>0,'msg'=>'删除失败','ss'=>Db::getlastsql()]);

        $this->ajaxReturn(['status'=>1,'msg'=>'删除成功']);
    }

   

    /**
     * 常见问题列表
     */
    public function questionList()
    {
        $field = 'question_id, title, content, add_time, update_time';
        $list = Db::name('question')->field($field)->select()->toArray();

        View::assign('list', $list);
        return View::fetch();
    }

    /**
     * APP相关配置
     */
    public function appConfig()
    {
        $field = 'operating_text, user_protocol, about_we, qr_code, ios_version, android_version';
        $row = Db::name('app_config')->where('id', 1)->field($field)->find();

        View::assign('configInfo', $row);
        return View::fetch();
    }

    /**
     * APP相关配置修改
     */
    public function appConfigChange()
    {
        $params = input();
        $update = Db::name('app_config')->where('id', 1)->update($params);
        $msg = $update ? '更新成功' : '更新失败,无信息变更';
        return dyajaxReturn(1, $msg, [],url('System/appConfig'));
    }
}