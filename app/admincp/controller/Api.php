<?php


namespace app\admincp\controller;

use think\facade\Db;

class Api extends Base
{
    public function getregion()
    {
        $id = input('get.id', '');
        $is_show = input('get.type_id', 0); //伪装is_show
        if (!$id) return dyajaxReturn(0, '参数错误');
        $where = array();
        $where['pid'] = $id;
        if($is_show == 1) $where['is_show'] = $is_show;
        $res = Db::name('region')->where($where)->cache(true, 1800)->field('id,name')->select();
        return dyajaxReturn(1, '请求成功', $res);
    }

    public function gethousetype()
    {
        $id = input('get.id', '');
        if(!$id){
            return dyajaxReturn(1, '请求成功', []);
        }
        $house_type_ids = Db::name('estate')->where('estate_id',$id)->value('house_type_ids');
        if(!$house_type_ids){
            return dyajaxReturn(1, '请求成功', []);
        }
        $house_type = Db::name('label')->where(['is_show'=>1,'typeid'=>4])->whereIn('id',$house_type_ids)->order('sort asc,id desc')->field('id,name')->select()->toArray();
        return dyajaxReturn(1, '请求成功', $house_type);
    }
}