<?php

namespace app\admincp\controller;

use think\facade\View;
use think\facade\Db;

//APP引导
class Guide extends Base
{
    //列表
    public function index()
    {
        $Guide = new \app\admincp\model\Guide();
        $info = $Guide->getList();
        $list = $info['list'];
        $pager = $info['pager'];
        $show = $info['show'];
        $condition = $info['condition'];
        View::assign('list', $list);// 赋值数据集
        View::assign('pager', $pager);// 赋值分页输出
        View::assign('show', $show);// 赋值分页输出
        View::assign('condition', $condition);
        return View::fetch();
    }
    /* 详情信息 */
    public function guide()
    {
        $act = "add";
        $info = [];
        $id = input('get.id/d');
        if ($id > 0) {
            $info = Db::name('guide')->where(['id'=>$id])->find();
            $act = 'edit';
        }
        View::assign('act', $act);
        View::assign('info', $info);
        return View::fetch();
    }
    /* 更新信息 */
    public function guideHandle()
    {
        $param = input('post.');
        $validate = validate('guide.'.$param['act']);
        if (!$validate->batch(true)->check($param)) {
            // 验证失败 输出错误信息
            $error = $validate->getError();
            $error_msg = array_values($error);
            return dyajaxReturn(0, $error_msg[0], $error);
        }
        $act = $param['act'];
        if(in_array($act,['add','edit'])){
            $data = [
                'image'=>$param['image'],
                'bg'=>$param['bg'],
                'is_show'=>$param['is_show'],
                'sort'=>$param['sort'],
            ];
            if ($act == 'add') {
                $data['add_time'] = time();
                $r = Db::name('guide')->insertGetId($data);
                $log_info = "新增引导:".$r;
            } elseif ($act == 'edit') {
                $r = Db::name('guide')->where('id', $param['id'])->update($data);
                $log_info = "更新引导:".$param['id'];
            }
        }else{
            if ($act == 'del') {
                $r = Db::name('guide')->where(['id' => $param['id']])->delete();
                $log_info = "删除引导:".$param['id'];
            }
        }
        if (!$r) {
            return dyajaxReturn(0, '操作失败或未做修改');
        }
        adminLog($log_info);
        return dyajaxReturn(1, '操作成功');
    }
}
