<?php

namespace app\admincp\controller;

use app\admincp\model\House;
use app\common\model\Users;
use think\facade\Db;
use think\facade\View;
use think\Page;

class User extends Base
{
    public function __construct()
    {
        parent::__construct();
        //户型列表
        $estate = Db::name('estate')->where(['is_deleted'=>0])->order('sort asc,estate_id desc')->field('estate_id,name')->select()->toArray();
        View::assign('estate', $estate);
    }
    /**
     * 用户列表
     */
    public function userList()
    {
        $Users = new Users();
        $field = 'user_id,nickname,mobile,add_time,last_login,status,is_platform';
        $res = $Users->getList(0,$field);
        View::assign('condition', $res['condition']);
        View::assign('show', $res['show']);// 赋值分页输出
        View::assign('pager', $res['pager']);// 赋值页面记录
        View::assign('list', $res['list']);// 赋值数据集
        return View::fetch();
    }

    public function user()
    {
        $act = input('act','add');
        View::assign('act',$act);
        return View::fetch();
    }

    public function delUser()
    {
        return $this->userHandle();
    }

    //用户操作
    public function userHandle()
    {
        $param = input('post.');
        $act = $param['act'];
        $validate = validate(\app\common\validate\User::class);
        if (!$validate->scene($act)->batch(true)->check($param)) {
            // 验证失败 输出错误信息
            $error = $validate->getError();
            $error_msg = array_values($error);
            return dyajaxReturn(0, $error_msg[0], $error);
        }
        if(in_array($act,['add'])){
            $data = [
                'mobile'=>$param['mobile'],
                'nickname'=>$param['nickname'],
            ];
            if($param['password']){
                $data['password'] = encrypt($param['password']);
            }
            if ($act == 'add') {
                $data['add_time'] = time();
                $r = Db::name('users')->insertGetId($data);
                $log_info = "新增用户:".$param['mobile'];
            }
//            elseif ($act == 'edit') {
//                $r = Db::name('user')->where('user_id',$param['user_id'])->update($data);
//                $log_info = "更新用户:".$param['mobile'];
//            }
        }else{
            if($act=='del'){
                $name = Db::name('users')->where('user_id', $param['user_id'])->value('mobile');
                $r = Db::name('users')->where(['user_id' => $param['user_id']])->update(['is_deleted'=>1]);
                $log_info = "删除用户:".$name;
            }
            if($act=='cancel'){
                //取消
                $name = Db::name('users')->where('user_id', $param['user_id'])->value('mobile');
                $r = Db::name('users')->where(['user_id' => $param['user_id']])->update(['designer_type'=>0,'is_authen'=>2]);
                if($r){
                    Db::name('user_authen')->where(['user_id' => $param['user_id']])->update(['status'=>2]);
                }
                $log_info = "取消用户认证:".$name;
            }
        }
        if (!$r) {
            return dyajaxReturn(0, '操作失败或未做修改');
        }
        adminLog($log_info);
        return dyajaxReturn(1, '操作成功');
    }

    //用户详情页
    public function userInfo()
    {
        $user_id = input('user_id/d', 0);
        if(!$user_id){
            $this->error('缺少id');
        }
        $where['user_id'] = $user_id;
        $info = Db::name('users')->where($where)->find();
        if(!$info){
            $this->error('信息不存在');
        }
        $is_weixin = Db::name('oauth_users')->where(['user_id'=>$user_id,'oauth'=>'weixin'])->value('tu_id');
        $is_qq = Db::name('oauth_users')->where(['user_id'=>$user_id,'oauth'=>'qq'])->value('tu_id');
        $info['is_weixin'] = $is_weixin?1:0;
        $info['is_qq'] = $is_qq?1:0;
        $info['add_time'] = get_date($info['add_time'],'Y-m-d H:i:s');
        $info['last_login'] = get_date($info['last_login'],'Y-m-d H:i:s');
        View::assign('info', $info);
        return View::fetch();
    }

    /**
     *用户审核
     * @return string
     */
    public function applyList()
    {
        $where = $condition = array();
        $keywords = input('keywords/s', false, 'trim');
        if ($keywords) {
            $where['u.nickname|ua.realname|ua.mobile'] = ['like','%'.$keywords.'%'];
            $condition['keywords'] = $keywords;
        }
        //状态
        $condition['status'] = $status = input('status/d','-999');
        if ($status != -999) {
            $where['ua.status'] = $status;
        }
        //页码模块
        $count = Db::name('user_authen')->alias('ua')
            ->join('users u', 'ua.user_id = u.user_id','inner')
            ->where($where)
            ->count();
        $pager = new Page($count, $this->page_size);// 实例化分页类 传入总记录数和每页显示的记录数
        $show = $pager->show();// 分页显示输出
        $field = 'ua.id,ua.realname,ua.mobile,ua.cre_no,ua.status,ua.add_time,ua.typeid';
        $field .= ',ua.cre_z,ua.cre_f,u.user_id,u.nickname,ua.typeid,ua.credit_code';
        $list = Db::name('user_authen')->alias('ua')
            ->join('users u', 'ua.user_id = u.user_id','inner')
            ->where($where)
            ->field($field)
            ->limit($pager->firstRow, $pager->listRows)
            ->order('ua.add_time desc,ua.id desc')
            ->select()->toArray();
        $designer_type = config('designer_type');
        if ($list) {
            foreach ($list as &$val) {
                $val['add_time'] = get_date($val['add_time']);
                $val['typeid_name'] = $designer_type[$val['typeid']];
            }
        }
        View::assign('condition', $condition);
        View::assign('show', $show);// 赋值分页输出
        View::assign('pager', $pager);// 赋值页面记录
        View::assign('list', $list);// 赋值数据集
        return View::fetch();
    }

    //审核详情页
    public function applyInfo()
    {
        $id = input('id/d', 0);
        if(!$id){
            $this->error('缺少id');
        }
        $where['id'] = $id;
        $info = Db::name('user_authen')->where($where)->find();
        if(!$info){
            $this->error('信息不存在');
        }
        $designer_type = config('designer_type');
        $cre_type = config('cre_type');
        $info['typeid_name'] = $designer_type[$info['typeid']];
        $info['cre_type_name'] = $cre_type[$info['cre_type']];
        View::assign('info', $info);
        return View::fetch();
    }

    /**
     * 用户认证是否通过
     */
    public function checkApply()
    {
        $act = input('act/s', '');
        $id = input('id/d', 0);
        $url = url('User/applyList');
        $log_info = "用户认证";
        $data['url'] = $url;
        if ($act == 'agree' && $id > 0) {
            $update = [
                'id'=>$id,'status'=>1,'admin_time'=>time(),'admin_id'=>session('admin_id'),
            ];
            Db::startTrans();
            try {
                $apply = Db::name('user_authen')->where(['id'=>$id])->find();
                if (!$apply) {
                    throw new \think\Exception('处理失败');
                }
                $res = Db::name('user_authen')->where(['id'=>$id])->update($update);
                if (!$res) {
                    throw new \think\Exception('处理失败');
                }
                $where['user_id'] = $apply['user_id'];
                $res2 = Db::name('users')->where($where)->update([
                    'is_authen'=>1,
                    'realname'=>$apply['realname'],
                    'designer_type'=>$apply['typeid'],
                    'company_name'=>$apply['typeid']==1?"独立设计师":$apply['company_name'],
                ]);
                if (!$res2) {
                    throw new \think\Exception('处理失败');
                }
                Db::commit();
            } catch (\Exception $e) {
                Db::rollback();
                return dyajaxReturn(0, $e->getMessage());
            }
            adminLog($log_info.'通过');
            return dyajaxReturn(1, '处理成功，用户认证已通过', $data);
        } elseif ($act == 'refuse' && $id > 0) {
            $update = [
                'id'=>$id,'status'=>2,'admin_time'=>time(),'admin_id'=>session('admin_id'),
            ];
            Db::startTrans();
            try {
                $apply = Db::name('user_authen')->where(['id'=>$id])->find();
                if (!$apply) {
                    throw new \think\Exception('处理失败');
                }
                $res = Db::name('user_authen')->where(['id'=>$id])->update($update);
                if (!$res) {
                    throw new \think\Exception('处理失败');
                }
                $where['user_id'] = $apply['user_id'];
                $res2 = Db::name('users')->where($where)->update(['is_authen'=>2]);
                if (!$res2) {
                    throw new \think\Exception('处理失败');
                }
                Db::commit();
            } catch (\Exception $e) {
                Db::rollback();
                return dyajaxReturn(0, $e->getMessage());
            }
            adminLog($log_info.'失败');
            return dyajaxReturn(1, '处理成功，用户认证已拒绝', $data);
        }
        adminLog($log_info.'失败');
        return dyajaxReturn(0, '处理失败');
    }

    //房屋列表
    public function houseList()
    {
        $House = new House();
        $info = $House->getList();
        $list = $info['list'];
        $pager = $info['pager'];
        $show = $info['show'];
        $condition = $info['condition'];
        View::assign('list', $list);// 赋值数据集
        View::assign('pager', $pager);// 赋值分页输出
        View::assign('show', $show);// 赋值分页输出
        View::assign('condition', $condition);
        return View::fetch();
    }

    //设计师列表
    public function designerList()
    {
        $Users = new Users();
        $field = 'user_id,nickname,mobile,add_time,last_login,status,is_recommend,is_expert';
        $res = $Users->getList(1,$field);
        View::assign('condition', $res['condition']);
        View::assign('show', $res['show']);// 赋值分页输出
        View::assign('pager', $res['pager']);// 赋值页面记录
        View::assign('list', $res['list']);// 赋值数据集
        $label = Db::name('label')->whereIn('typeid',[1,2,3])->order('sort asc,id desc')->field('id,name,typeid')->select()->toArray();
        $expe = [];
        $style = [];
        $price = [];
        foreach ($label as $v){
            switch ($v['typeid']){
                case 1:
                    $expe[] = $v;
                    break;
                case 2:
                    $style[] = $v;
                    break;
                case 3:
                    $price[] = $v;
                    break;
            }
        }
        View::assign('expe',$expe);
        View::assign('style',$style);
        View::assign('price',$price);
        View::assign('designer_type',config('designer_type'));
        return View::fetch();
    }

    //取消认证
    public function cancelUser()
    {
        return $this->userHandle();
    }

    public function billboardList()
    {
        $keywords = trim(input('keywords'));
        $condition['keywords'] = $keywords;
        $field = 'user_id,nickname,mobile,add_time,browse,virtually_browse';
        $where['is_deleted'] = 0;
        $where['status'] = 1;
        if($keywords){
            $where['nickname|mobile'] = ['like','%'.$keywords.'%'];
        }
        $count = Db::name('users')->where($where)
            ->where(function ($Query){
                $Query->where('is_authen',1)->whereOr('is_platform',1);
            })
            ->count();// 查询满足要求的总记录数
        $pager = new Page($count, $this->page_size);// 实例化分页类 传入总记录数和每页显示的记录数
        $list = Db::name('users')->where($where)
            ->where(function ($Query){
                $Query->where('is_authen',1)->whereOr('is_platform',1);
            })
            ->field($field)
            ->orderRaw('virtually_browse+browse desc,user_id desc')
            ->limit($pager->firstRow, $pager->listRows)
            ->select()->toArray();
        foreach ($list as &$value){
            $value['add_time'] = get_date($value['add_time']);
        }
        $show = $pager->show();// 分页显示输出
        View::assign('list', $list);// 赋值数据集
        View::assign('pager', $pager);// 赋值分页输出
        View::assign('show', $show);// 赋值分页输出
        View::assign('condition', $condition);
        return View::fetch();
    }
}
