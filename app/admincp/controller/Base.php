<?php


namespace app\admincp\controller;
use think\facade\View;
use think\facade\Db;
use app\BaseController;

class Base  extends BaseController
{

    use \liliuwei\think\Jump;
    public $page_size = 0;
    public $admin_id = 0;

    /**
     * 析构函数
     */
    function __construct(){
        header("Cache-control: private");
        //用户中心面包屑导航
        $navigate_admin = navigate_admin();
        View::assign('navigate_admin',$navigate_admin);
        //过滤不需要登陆的行为 
        if (!in_array(ACTION_NAME, array('login', 'vertify'))) {
            if (session('admin_id') > 0) {
                $this->check_priv();//检查管理员菜单操作权限
                $this->admin_id = session('admin_id');
            }else {
                //if(ACTION_NAME == 'index') return redirect( url('Admin/login'));
                $this->error('请先登录', url('Admin/login'), null, 1);
            }
        }
        $this->public_assign();
        // 控制器初始化
        $this->initialize();
    }

    /**
     * 保存公告变量到 smarty中 比如 导航 
     */
    public function public_assign(){
        $dyshop_config = array();
        $dy_config = Db::name('config')->cache(true, DYSHOP_CACHE_TIME, 'config')->select();
        if($dy_config){
            foreach($dy_config as $k => $v){
                $dyshop_config[$v['inc_type'].'_'.$v['name']] = $v['value'];
            }
        }
        $this->page_size = config('PAGESIZE');
        View::assign('dyshop_config', $dyshop_config);
        $isauth = false;
        if(session('admin_id') == 1) $isauth = true;
        View::assign('isauth',$isauth);
        //权限
        if (session('admin_id') > 1) {
            $act_list = session('act_list');
            $group = Db::name('system_menu')->where("id", "in", $act_list)->cache(false)->column('group');
            if (isset($group['system']) && $group['system'] == 'system') {
                $right['system'] = 1;
            } else {
                $right['system'] = 0;
            }
            if (isset($group['order']) && $group['order'] == 'order') {
                $right['order'] = 1;
            } else {
                $right['order'] = 0;
            }
            if (isset($group['content']) && $group['content'] == 'content') {
                $right['content'] = 1;
            } else {
                $right['content'] = 0;
            }
        } else {
            $right['system'] = 1;
            $right['order'] = 1;
            $right['content'] = 1;
            $right['device'] = 1;
            $right['user'] = 1;
        }
        View::assign('right', $right);

        //户型列表
        $house_type = Db::name('label')->where(['is_show'=>1,'typeid'=>4])->order('sort asc,id desc')->field('id,name')->select()->toArray();
        View::assign('house_type', $house_type);
    }
    
    public function check_priv(){
    	$ctl = CONTROLLER_NAME;
    	$act = ACTION_NAME;
        $act_list = session('act_list');
		//无需验证的操作
		$uneed_check = array('login','logout','vertifyHandle','vertify','imageUp','upload','videoUp','delupload','login_task','gettwocate','chunkUpload','delupload');
    	if($ctl == 'Index' || $act_list == 'all' || $ctl == 'Wx3rd'){
    		//后台首页控制器无需验证,超级管理员无需验证
    		return true;
    	}elseif((request()->isAjax() && $this->verifyAjaxRequest($act)) || strpos($act,'ajax')!== false || in_array($act,$uneed_check)){
    		//部分ajax请求不需要验证权限
            if($act == 'CloseOrder' || $act == 'OrderRefund'){
                $res = $this->verifyAction();
                if($res['status'] == -1){
                    return dyajaxReturn(0, $res['msg']);
                }; 
            }
    		return true;
    	}else{
            $res = $this->verifyAction();
    		if($res['status'] == -1){
                $this->error($res['msg'],$res['url']);
            };
    	}
    }
    
    public function ajaxReturn($data,$type = 'json'){                        
        exit(json_encode($data,JSON_UNESCAPED_UNICODE));
    }

    /**
     * 要验证的ajax
     * @param $act
     * @return bool
     */
    private function verifyAjaxRequest($act){
        $verifyAjaxArr = ['delGoodsCategory','delGoodsAttribute','delGoods','supertube_order','delimagetext','delvideo','delscheme'];
        if(request()->isAjax() && in_array($act,$verifyAjaxArr)){
            $res = $this->verifyAction();
            if($res['status'] == -1){
                $this->ajaxReturn($res);
            }else{
                return true;
            };
        }else{
            return true;
        }
    }
    private function verifyAction(){
        $ctl = CONTROLLER_NAME;
        $act = ACTION_NAME;
        $act_list = session('act_list');
        $right = Db::name('system_menu')->where("id", "in", $act_list)->cache(false)->column('right');
        $role_right = $htmlright = '';
        foreach ($right as $val){
            $role_right .= $val.',';
            $htmlright .= str_replace("@","##",$val).',';
        }
        $role_right = explode(',', $role_right);
        $htmlright = explode(',', $htmlright);
        //检查是否拥有此操作权限
        if(!in_array($ctl.'@'.$act, $role_right)){
            //return ['status'=>-1,'msg'=>'您没有操作权限['.($ctl.'@'.$act).'],请联系超级管理员分配权限','url'=>url('Admincp/Index/welcome')];
            return ['status'=>-1,'msg'=>'您没有操作权限,请联系管理员分配权限','url'=>url('Admincp/Index/welcome')];
        } 
        View::assign('htmlright',$htmlright);
        session('htmlright',$htmlright);
    }
}