<?php


namespace app\admincp\model;

use app\common\model\Common;
use think\Page;

class Evaluate extends Common {
    //评价表
    protected $pk = 'evaluate_id';

    //获取列表

    /**
     * @param string $keywords 关键词搜索
     * @return array
     */
    public function getList()
    {
        $keywords = trim(input('keywords'));
        $userData = trim(input('userData'));
        $condition['keywords'] = $keywords;
        $condition['userData'] = $userData;
        $field = 'eva.evaluate_id,eva.content,eva.is_show,eva.add_time,users.nickname,users.avatar,users.last_ip,user_dynamic.title';
        if($keywords){
            $where['eva.content'] = ['like','%'.$keywords.'%'];
        }
        if($userData){
            $where['users.nickname|users.mobile'] = ['like',"%$userData%"];
        }
        $count = $this->alias('eva')->leftjoin('users','eva.user_id = users.user_id')
            ->leftjoin('user_dynamic','eva.dynamic_id = user_dynamic.dynamic_id')->where($where)->count();// 查询满足要求的总记录数
        $pager = new Page($count, $this->page_size);// 实例化分页类 传入总记录数和每页显示的记录数
        $list = $this->where($where)
            ->alias('eva')
            ->field($field)
            ->leftjoin('users','eva.user_id = users.user_id')
            ->leftjoin('user_dynamic','eva.dynamic_id = user_dynamic.dynamic_id')
            ->order('eva.evaluate_id desc')
            ->limit($pager->firstRow, $pager->listRows)
            ->select()->toArray();
        if ($list) {
            foreach ($list as &$val) {
                $val['add_time'] = get_date($val['add_time']);
            }
        }
        $show = $pager->show();// 分页显示输出
        return [
            'list'=>$list,
            'pager'=>$pager,
            'show'=>$show,
            'condition'=>$condition,
        ];
    }
}
