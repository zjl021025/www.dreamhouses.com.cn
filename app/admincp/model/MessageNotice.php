<?php


namespace app\admincp\model;

use app\common\model\Common;
use think\Page;

class MessageNotice extends Common {
    //消息表
    protected $pk = 'message_id';

    //获取列表

    /**
     * @param string $keywords 关键词搜索
     * @return array
     */
    public function getList()
    {
        $keywords = trim(input('keywords'));
        $condition['keywords'] = $keywords;
        $field = 'message_id,content,is_show,send_time,user_type';
        if($keywords){
            $where['content'] = ['like','%'.$keywords.'%'];
        }
        $count = $this->where($where)->count();// 查询满足要求的总记录数
        $pager = new Page($count, $this->page_size);// 实例化分页类 传入总记录数和每页显示的记录数
        $list = $this->where($where)
            ->field($field)
            ->order('message_id desc')
            ->limit($pager->firstRow, $pager->listRows)
            ->select()->toArray();
        if ($list) {
            foreach ($list as &$val) {
                $val['send_time'] = get_date($val['send_time']);
            }
        }
        $show = $pager->show();// 分页显示输出
        return [
            'list'=>$list,
            'pager'=>$pager,
            'show'=>$show,
            'condition'=>$condition,
        ];
    }
}
