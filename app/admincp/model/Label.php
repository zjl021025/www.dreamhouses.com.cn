<?php


namespace app\admincp\model;

use app\common\model\Common;
use think\Page;

class Label extends Common {
    //标签表
    protected $pk = 'id';

    //获取列表

    /**
     * @param  int typeid  1=设计经验  2=设计风格  3=设计费用   4=户型类型 5=房间类型 6=预算类型 7=面积类型
     * @return array
     */
    public function getList($typeid)
    {
        $keywords = trim(input('keywords'));
        $condition['keywords'] = $keywords;
        $field = 'id,name,is_show,sort';
        if($keywords){
            $where['name'] = ['like','%'.$keywords.'%'];
        }
        $where['typeid'] = $typeid;
        $count = $this->where($where)->count();// 查询满足要求的总记录数
        $pager = new Page($count, $this->page_size);// 实例化分页类 传入总记录数和每页显示的记录数
        $list = $this->where($where)
            ->field($field)
            ->order('sort asc,id desc')
            ->limit($pager->firstRow, $pager->listRows)
            ->select()->toArray();
        foreach ($list as &$value){
            if($typeid==7){
                $value['name'] = $value['name'].'㎡';
            }
        }
        $show = $pager->show();// 分页显示输出
        return [
            'list'=>$list,
            'pager'=>$pager,
            'show'=>$show,
            'condition'=>$condition,
        ];
    }
}
