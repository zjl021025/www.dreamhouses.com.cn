<?php

namespace app\admincp\model;

use app\common\model\Common;
use think\Page;

class Article extends Common {
    //资讯表
    protected $pk = 'article_id';

    //获取列表

    /**
     * @param $type 0=楼盘资讯 1=协议 2=装修贴士
     * @param string $keywords 关键词搜索
     * @param int $estate_id 楼盘id
     * @return array
     */
    public function getList($type)
    {
        $keywords = trim(input('keywords'));
        $condition['keywords'] = $keywords;
        $estate_id = input('estate_id/d');
        $condition['estate_id'] = $estate_id;//楼盘id
        $where['article_type'] = $type;
        if($estate_id){
            $where['estate_id'] = $estate_id;
        }
        $field = 'article_id,title,is_show,add_time,estate_id';
        $keywords && $where['title'] = ['like','%'.$keywords.'%'];
        $count = $this->where($where)->count();// 查询满足要求的总记录数
        $pager = new Page($count, $this->page_size);// 实例化分页类 传入总记录数和每页显示的记录数
        $list = $this->where($where)
            ->field($field)
            ->order('article_id desc')
            ->limit($pager->firstRow, $pager->listRows)
            ->select()->toArray();
        if ($list) {
            foreach ($list as &$val) {
                $val['add_time'] = get_date($val['add_time']);
                if($type==0){
                    $val['estate_name'] = get_estate_name($val['estate_id']);
                }
            }
        }
        $show = $pager->show();// 分页显示输出
        return [
            'list'=>$list,
            'pager'=>$pager,
            'show'=>$show,
            'condition'=>$condition,
        ];
    }
}
