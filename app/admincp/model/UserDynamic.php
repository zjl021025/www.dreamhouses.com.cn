<?php


namespace app\admincp\model;

use app\common\model\Common;
use think\facade\Db;
use think\Page;

class UserDynamic extends Common {
    //图文、视频、方案表
    protected $pk = 'id';

    //获取列表

    /**
     * @param  int typeid  1=图片  2=视频   3=方案
     * @return array
     */
    public function getList($typeid)
    {
        $keywords = trim(input('keywords'));
        $mobile = trim(input('mobile'));
        $condition['keywords'] = $keywords;
        $condition['mobile'] = $mobile;
        $field = 'dynamic_id,title,is_show,content,is_recommend,add_time,user_id,is_home,is_expert';
        if($keywords){
            $where['title|content'] = ['like','%'.$keywords.'%'];
        }
        if($mobile){
            $userids = Db::name('users')->whereLike('mobile',"%$mobile%")->column('user_id');
            if($userids){
                $where['user_id'] = ['in',$userids];
            }else{
                $where['user_id'] = 0;
            }
        }
        $where['typeid'] = $typeid;
        $count = $this->where($where)->count();// 查询满足要求的总记录数
        $pager = new Page($count, $this->page_size);// 实例化分页类 传入总记录数和每页显示的记录数
        $list = $this->where($where)
            ->field($field)
            ->order('dynamic_id desc')
            ->limit($pager->firstRow, $pager->listRows)
            ->select()->toArray();
        foreach ($list as &$value){
            $value['add_time'] = get_date($value['add_time']);
            $value['mobile'] = get_user_mobile($value['user_id']);
        }
        $show = $pager->show();// 分页显示输出
        return [
            'list'=>$list,
            'pager'=>$pager,
            'show'=>$show,
            'condition'=>$condition,
        ];
    }
}
