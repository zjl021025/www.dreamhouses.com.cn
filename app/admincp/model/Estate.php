<?php


namespace app\admincp\model;

use app\common\model\Common;
use think\Page;

class Estate extends Common {
    //楼盘表
    protected $pk = 'estate_id';

    //获取列表

    /**
     * @param string $keywords 关键词搜索
     * @return array
     */
    public function getList()
    {
        $keywords = trim(input('keywords'));
        $condition['keywords'] = $keywords;
        $where['is_deleted']  = 0;
        $field = 'estate_id,name,is_show,add_time,province_id,city_id,district_id,address';
        if($keywords){
            $where['name'] = ['like','%'.$keywords.'%'];
        }
        $count = $this->where($where)->count();// 查询满足要求的总记录数
        $pager = new Page($count, $this->page_size);// 实例化分页类 传入总记录数和每页显示的记录数
        $list = $this->where($where)
            ->field($field)
            ->order('estate_id desc')
            ->limit($pager->firstRow, $pager->listRows)
            ->select()->toArray();
        if ($list) {
            foreach ($list as &$val) {
                $val['add_time'] = get_date($val['add_time']);
                $address = '';
                if($val['district_id']){
                    $address = get_region_mername($val['district_id'],' ');
                }
                if(!$address && $val['city_id']){
                    $address = get_region_mername($val['city_id'],' ');
                }
                if(!$address && $val['province_id']){
                    $address = get_region_mername($val['province_id'],' ');
                }
                $val['address'] = $address.' '.$val['address'];
            }
        }
        $show = $pager->show();// 分页显示输出
        return [
            'list'=>$list,
            'pager'=>$pager,
            'show'=>$show,
            'condition'=>$condition,
        ];
    }
}
