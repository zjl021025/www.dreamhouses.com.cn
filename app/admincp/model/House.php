<?php


namespace app\admincp\model;

use app\common\model\Common;
use think\Page;

class House extends Common {
    //房屋表
    protected $pk = 'house_id';

    //获取列表

    /**
     * @return array
     */
    public function getList()
    {
        $keywords = trim(input('keywords'));
        $condition['keywords'] = $keywords;
        $estate_id = input('estate_id/d');
        $condition['estate_id'] = $estate_id;//楼盘id
        $where['is_deleted']  = 0;
        if($estate_id){
            $where['estate_id'] = $estate_id;
        }
        $keywords && $where['name'] = ['like','%'.$keywords.'%'];
        $field = 'house_id,name,add_time,province_id,city_id,district_id,address,user_id,house_type_id,area,estate_id';
        $count = $this->where($where)->count();// 查询满足要求的总记录数
        $pager = new Page($count, $this->page_size);// 实例化分页类 传入总记录数和每页显示的记录数
        $list = $this->where($where)
            ->field($field)
            ->order('estate_id desc')
            ->limit($pager->firstRow, $pager->listRows)
            ->select()->toArray();
        if ($list) {
            foreach ($list as &$val) {
                $address = '';
                if($val['district_id']){
                    $address = get_region_mername($val['district_id'],' ');
                }
                if(!$address && $val['city_id']){
                    $address = get_region_mername($val['city_id'],' ');
                }
                if(!$address && $val['province_id']){
                    $address = get_region_mername($val['province_id'],' ');
                }
                $val['address'] = $address.' '.$val['address'];
                $val['house_type_name'] = get_label_name($val['house_type_id']);
                $val['area'] = $val['area'].'㎡';
                $val['add_time'] = get_date($val['add_time']);
                $val['mobile'] = get_user_mobile($val['user_id']);
                $val['enter_name'] = $val['estate_id']>0?'已录入':'未录入';
                $val['is_enter'] = $val['estate_id']>0?1:0;
            }
        }
        $show = $pager->show();// 分页显示输出
        return [
            'list'=>$list,
            'pager'=>$pager,
            'show'=>$show,
            'condition'=>$condition,
        ];
    }
}
