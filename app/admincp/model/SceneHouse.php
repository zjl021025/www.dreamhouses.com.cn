<?php


namespace app\admincp\model;

use app\common\model\Common;
use think\Page;

class SceneHouse extends Common {
    //户型表
    protected $pk = 'house_id';

    //获取列表

    /**
     * @param string $keywords 关键词搜索
     * @return array
     */
    public function getList()
    {
        $keywords = trim(input('keywords'));
        $condition['keywords'] = $keywords;
        $where['is_deleted'] = 0;
        $field = 'house_id,estate_id,name,add_time,area';
        if($keywords){
            $where['name'] = ['like','%'.$keywords.'%'];
        }
        $count = $this->where($where)->count();// 查询满足要求的总记录数
        $pager = new Page($count, $this->page_size);// 实例化分页类 传入总记录数和每页显示的记录数
        $list = $this->where($where)
            ->field($field)
            ->order('estate_id desc')
            ->limit($pager->firstRow, $pager->listRows)
            ->select()->toArray();
        if ($list) {
            foreach ($list as &$val) {
                $val['add_time'] = get_date($val['add_time']);
                $val['estate_name'] = get_estate_name($val['estate_id']);
                $val['area'] = $val['area'].'㎡';
            }
        }
        $show = $pager->show();// 分页显示输出
        return [
            'list'=>$list,
            'pager'=>$pager,
            'show'=>$show,
            'condition'=>$condition,
        ];
    }
}
