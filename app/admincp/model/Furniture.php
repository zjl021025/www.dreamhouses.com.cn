<?php


namespace app\admincp\model;

use app\common\model\Common;
use think\facade\Db;
use think\Page;

class Furniture extends Common {
    //家居
    protected $pk = 'id';

    //获取列表
    /**
     * @param string $keywords 关键词搜索
     * @return array
     */
    public function getList($typeid=0)
    {
        $keywords = trim(input('keywords'));
        $attrname = trim(input('attrname'));
        $furniture_no = trim(input('furniture_no'));
        $cate_id = trim(input('cate_id'));
        $two_cate_id = trim(input('two_cate_id'));
        $three_cate_id = trim(input('three_cate_id'));
        if($attrname){
            $furniture_ids = Db::name('furniture_attr')->whereLike('value',"%$attrname%")->column('furniture_id');
            if($furniture_ids){
                $furniture_ids = array_unique($furniture_ids);
                $where['id']  = ['in',$furniture_ids];
            }else{
                $where['id']  = 0;
            }
        }
        $condition['keywords'] = $keywords;
        $condition['attrname'] = $attrname;
        $condition['furniture_no'] = $furniture_no;
        $condition['cate_id'] = $cate_id;
        $condition['two_cate_id'] = $two_cate_id;
        $condition['three_cate_id'] = $three_cate_id;
        $where['is_deleted']  = 0;
        $field = 'id,name,is_show,add_time,sort,cate_id,price,filename,two_cate_id,three_cate_id,image,furniture_no';
        if($keywords){
            $where['name'] = ['like','%'.$keywords.'%'];
        }
        if($furniture_no){
            $where['furniture_no'] = ['like','%'.$furniture_no.'%'];
        }
        if($cate_id){
            $where['cate_id'] = $cate_id;
        }
        if($two_cate_id){
            $where['two_cate_id'] = $two_cate_id;
        }
        if($three_cate_id){
            $where['three_cate_id'] = $three_cate_id;
        }
        if($typeid){
            $where['typeid'] = $typeid;
        }
        $count = $this->where($where)->count();// 查询满足要求的总记录数
        $pager = new Page($count, $this->page_size);// 实例化分页类 传入总记录数和每页显示的记录数
        $list = $this->where($where)
            ->field($field)
            ->order('sort asc,id desc')
            ->limit($pager->firstRow, $pager->listRows)
            ->select()->toArray();
        if ($list) {
            foreach ($list as &$val) {
                $val['add_time'] = get_date($val['add_time']);
                $val['cate_name'] = Db::name('furniture_category')->where('id',$val['cate_id'])->value('name');
                $val['two_cate_name'] = Db::name('furniture_category')->where('id',$val['two_cate_id'])->value('name');
                $val['three_cate_name'] = Db::name('furniture_category')->where('id',$val['three_cate_id'])->value('name');
                $attr = Db::name('furniture_attr')->where('furniture_id',$val['id'])->field('name,value')->order('id asc')->select()->toArray();
                $val['attr_name'] = '';
                foreach ($attr as $vv){
                    $val['attr_name'] .= $vv['name'].':'.$vv['value']."<br/>";
                }
            }
        }
        $show = $pager->show();// 分页显示输出
        return [
            'list'=>$list,
            'pager'=>$pager,
            'show'=>$show,
            'condition'=>$condition,
            'count'=>$count,
        ];
    }
}
