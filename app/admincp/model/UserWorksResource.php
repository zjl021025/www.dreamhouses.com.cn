<?php


namespace app\admincp\model;

use app\common\model\Common;
use think\facade\Db;
use think\Page;

class UserWorksResource extends Common {
    //资源
    protected $pk = 'id';

    //获取列表

    /**
     * @param string $keywords 关键词搜索
     * @return array
     */
    public function getList()
    {
        $keywords = trim(input('keywords'));
        $mobile = trim(input('mobile'));
        $typeid = trim(input('typeid'));
        $condition['keywords'] = $keywords;
        $condition['mobile'] = $mobile;
        $condition['typeid'] = $typeid;
        $where['is_deleted']  = 0;
        $where['typeid']  = ['in',[1,2]];
        $field = 'id,name,add_time,image,video,typeid,user_id';
        if($keywords){
            $where['name'] = ['like','%'.$keywords.'%'];
        }
        if($typeid){
            $where['typeid'] = $typeid;
        }
        if($mobile){
            $userids = Db::name('users')->whereLike('mobile',"%$mobile%")->column('user_id');
            if($userids){
                $where['user_id'] = ['in',$userids];
            }else{
                $where['user_id'] = 0;
            }
        }
        $count = $this->where($where)->count();// 查询满足要求的总记录数
        $pager = new Page($count, $this->page_size);// 实例化分页类 传入总记录数和每页显示的记录数
        $list = $this->where($where)
            ->field($field)
            ->order('id desc')
            ->limit($pager->firstRow, $pager->listRows)
            ->select()->toArray();
        if ($list) {
            foreach ($list as &$val) {
                $val['add_time'] = get_date($val['add_time']);
                $val['mobile'] = get_user_mobile($val['user_id']);
            }
        }
        $show = $pager->show();// 分页显示输出
        return [
            'list'=>$list,
            'pager'=>$pager,
            'show'=>$show,
            'condition'=>$condition,
        ];
    }
}
