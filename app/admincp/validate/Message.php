<?php


namespace app\admincp\validate;

use think\facade\Db;
use think\Validate;

/**
 * 消息验证器
 * @author Administrator
 */
class Message extends Validate
{
    //验证规则
    protected $rule = [
        'message_id'   => 'require|checkId',
        'user_type' =>'require|checkType',
        'content' => 'require|max:255',
        'is_show' => 'require|checkShow',
    ];
    
    //错误消息
    protected $message = [
        'message_id.require' => '缺少id',
        'message_id.checkId' => '信息不存在',
        'user_type.require' => '请选择类型',
        'user_type.checkType' => '请选择用户',
        'content.require' => '请输入消息内容',
        'content.max' => '消息内容最多255个字符',
        'is_show.require' => '请选择是否显示',
    ];
    
    //验证场景
    protected $scene = [
        'add'  => ['user_type','content','is_show'],
        'edit' => ['message_id','user_type','content','is_show'],
        'del'  => ['message_id']
    ];
    //校验id
    protected function checkId($value, $rule, $data)
    {
        $res = Db::name('message_notice')->where(['message_id'=>$value])->value('message_id');
        if ($res) {
            return true;
        }
        return false;
    }

    //校验user_type
    protected function checkType($value, $rule, $data)
    {
        if($value==3){
            //单用户
            if(!$data['user_id']){
                return false;
            }
        }
        return true;
    }

    //
    protected function checkShow($value, $rule, $data)
    {
        if ($value == 0 || $value == 1) {
            return true;
        } else {
            return "参数错误";
        }
    }
}
