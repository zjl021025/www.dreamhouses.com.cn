<?php


namespace app\admincp\validate;

use think\facade\Db;
use think\Validate;

/**
 * 家居分类验证器
 * @author Administrator
 */
class FurnitureCategory extends Validate
{
    //验证规则
    protected $rule = [
        'id'   => 'require|checkId',
        'name' => 'require|max:60',
        'folder' => 'require|isEnglish|max:60',
//        'icon'   => 'require',
//        'image'   => 'require',
        'typeid'   => 'require',
    ];
    
    //错误消息
    protected $message = [
        'id.require' => '缺少id',
        'id.checkId' => '信息不存在',
        'name.require' => '请输入名称',
        'name.max' => '名称最多60个字符',
        'folder.require' => '请输入目录',
        'folder.isEnglish' => '目录格式错误',
        'folder.max' => '目录最多60个字符',
//        'icon.require' => '请上传图标',
//        'image.require' => '请上传封面图',
        'typeid.require' => '请选择类型',
    ];
    
    //验证场景
    protected $scene = [
        'add'  => ['name','folder','icon','image','typeid'],
        'edit' => ['id','name','folder','icon','image','typeid'],
        'del'  => ['id']
    ];
    //校验id
    protected function checkId($value, $rule, $data)
    {
        $res = Db::name('furniture_category')->where(['id'=>$value])->value('id');
        if ($res) {
            return true;
        }
        return false;
    }

    protected function isEnglish($value, $rule, $data)
    {
        if(preg_match("/^[a-zA-Z_\s]+$/",$value)){
            return true;
        }
        return false;
    }
}
