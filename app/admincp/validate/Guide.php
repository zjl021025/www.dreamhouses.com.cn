<?php


namespace app\admincp\validate;

use think\facade\Db;
use think\Validate;

/**
 * 引导验证器
 * @author Administrator
 */
class Guide extends Validate
{
    //验证规则
    protected $rule = [
        'id'   => 'require|checkId',
        'image' => 'require',
        'bg' => 'require',
    ];
    
    //错误消息
    protected $message = [
        'id.require' => '缺少id',
        'id.checkId' => '信息不存在',
        'image.require' => '请上传图片',
        'bg.require' => '请上传背景',
    ];
    
    //验证场景
    protected $scene = [
        'add'  => ['bg'],
        'edit' => ['id','bg'],
        'del'  => ['id']
    ];
    //校验id
    protected function checkId($value, $rule, $data)
    {
        $res = Db::name('guide')->where(['id'=>$value])->value('id');
        if ($res) {
            return true;
        }
        return false;
    }

}
