<?php


namespace app\admincp\validate;

use think\facade\Db;
use think\Validate;

/**
 * Description of Article
 *
 * @author Administrator
 */
class Tips extends Validate
{
    //验证规则
    protected $rule = [
        'fitment_id'   => 'require|checkId',
        'name' => 'require|max:80',
        'level' => 'require',
        'is_show' => 'require|checkShow',
    ];
    
    //错误消息
    protected $message = [
        'fitment_id.require' => '缺少id',
        'fitment_id.checkId' => '信息不存在',
        'name.require' => '请输入标题',
        'name.max' => '标题最多80个字符',
        'level.require' => '请输入级别',
        'is_show.require' => '请选择是否显示',
    ];
    
    //验证场景
    protected $scene = [
        'add'  => ['title','level','is_show'],
        'edit' => ['fitment_id','title','level','is_show'],
        'del'  => ['fitment_id']
    ];
    //校验id
    protected function checkId($value, $rule, $data)
    {
        $res = Db::name('fitment')->where(['fitment_id'=>$value])->find();
        if ($res) {
            return true;
        }
        return false;
    }

    //
    protected function checkShow($value, $rule, $data)
    {
        if ($value == 0 || $value == 1) {
            return true;
        } else {
            return "参数错误";
        }
    }
}
