<?php


namespace app\admincp\validate;

use think\facade\Db;
use think\Validate;

/**
 * 家居验证器
 * @author Administrator
 */
class Furniture extends Validate
{
    //验证规则
    protected $rule = [
        'id'   => 'require|checkId',
        'cate_id' => 'require',
        'two_cate_id' => 'require',
        'name' => 'require|max:60',
        'filename' => 'require|max:60|checkFilename',
        'furniture_no' => 'require|max:60',
        'price' => 'require|isPrice',
        'buy_url' => 'require',
        'images'   => 'require',
        'size'   => 'require',
        'material'   => 'require',
        'ids'   => 'require',
    ];
    
    //错误消息
    protected $message = [
        'id.require' => '缺少id',
        'id.checkId' => '信息不存在',
        'cate_id.require' => '请选择一级分类',
        'two_cate_id.require' => '请选择二级分类',
        'name.require' => '请输入名称',
        'name.max' => '名称最多60个字符',
        'filename.require' => '请输入文件名称',
        'filename.max' => '文件名称最多60个字符',
        'filename.checkFilename' => '文件名称已存在，请修改',
        'furniture_no.require' => '请输入编号',
        'furniture_no.max' => '编号最多60个字符',
        'price.require' => '请输入价格',
        'price.isPrice' => '价格格式错误',
        'buy_url.require' => '请输入购买链接',
        'images.require' => '请上传相册图',
        'size.require' => '请输入尺寸',
        'material.require' => '请输入材料',
        'ids.require' => '缺少id',
    ];
    
    //验证场景
    protected $scene = [
        'frock:add'  => ['cate_id','two_cate_id','name','filename','furniture_no','price','images','size','material'],
        'frock:edit' => ['id','cate_id','two_cate_id','name','filename','furniture_no','price','images','size','material'],
        'frock:del'  => ['ids'],
        'soft:add'  => ['cate_id','two_cate_id','name','filename','furniture_no','price','images','size','material'],
        'soft:edit' => ['id','cate_id','two_cate_id','name','filename','furniture_no','price','images','size','material'],
        'soft:del'  => ['ids']
    ];
    //校验id
    protected function checkId($value, $rule, $data)
    {
        $res = Db::name('furniture')->where(['id'=>$value])->value('id');
        if ($res) {
            return true;
        }
        return false;
    }

    protected function checkFilename($value, $rule, $data)
    {
        $where['is_deleted'] = 0;
        $where['filename'] = $value;
        if($data['id']){
            $where['id'] = ['<>',$data['id']];
        }
        $count = Db::name('furniture')->where($where)->count();
        if (!$count) {
            return true;
        }
        return false;
    }

    protected function isPrice($value, $rule, $data)
    {
        if(preg_match("/(^[1-9]\d*(\.\d{1,2})?$)|(^0(\.\d{1,2})?$)/",$value)){
            return true;
        }
        return false;
    }
}
