<?php


namespace app\admincp\validate;

use think\facade\Db;
use think\Validate;

/**
 * Description of Article
 *
 * @author Administrator
 */
class Article extends Validate
{
    //验证规则
    protected $rule = [
        'article_id'   => 'require|checkId',
        'estate_id' => 'require',
        'title' => 'require|max:80',
        'intro' => 'require|max:150',
        'content' => 'require',
        'is_show' => 'require|checkShow',
    ];
    
    //错误消息
    protected $message = [
        'article_id.require' => '缺少id',
        'article_id.checkId' => '信息不存在',
        'estate_id.require' => '请选择楼盘',
        'title.require' => '请输入标题',
        'title.max' => '标题最多80个字符',
        'intro.require' => '请输入简介',
        'intro.max' => '简介最多150个字符',
        'content.require' => '请输入内容',
        'is_show.require' => '请选择是否显示',
    ];
    
    //验证场景
    protected $scene = [
        'add'  => ['estate_id','title','intro','content','is_show'],
        'edit' => ['article_id','estate_id','title','intro','content','is_show'],
        'del'  => ['article_id']
    ];
    //校验id
    protected function checkId($value, $rule, $data)
    {
        $res = Db::name('article')->where(['article_id'=>$value])->find();
        if ($res) {
            return true;
        }
        return false;
    }

    //
    protected function checkShow($value, $rule, $data)
    {
        if ($value == 0 || $value == 1) {
            return true;
        } else {
            return "参数错误";
        }
    }
}
