<?php


namespace app\admincp\validate;

use think\facade\Db;
use think\Validate;

/**
 * 楼盘验证器
 * @author Administrator
 */
class Estate extends Validate
{
    //验证规则
    protected $rule = [
        'estate_id'   => 'require|checkId',
        'name' => 'require|max:60',
        'province_id' => 'require',
        'city_id' => 'require',
        'district_id' => 'require',
        'address' => 'require',
        'house_type' => 'require|checkHousetype',
        'area' => 'require|checkArea',
        'is_show' => 'require|checkShow',
    ];
    
    //错误消息
    protected $message = [
        'estate_id.require' => '缺少id',
        'estate_id.checkId' => '信息不存在',
        'name.require' => '请输入楼盘名称',
        'name.max' => '楼盘名称最多60个字符',
        'province_id.require' => '请选择省',
        'city_id.require' => '请选择市',
        'district_id.require' => '请选择区',
        'address.require' => '请选择详细地址',
        'house_type.require' => '请选择户型',
        'house_type.checkHousetype' => '户型验证失败',
        'area.require' => '请输入户型面积',
        'area.checkArea' => '请完善户型面积',
        'is_show.require' => '请选择是否显示',
    ];
    
    //验证场景
    protected $scene = [
        'add'  => ['name','province_id','city_id','district_id','address','house_type','area','is_show'],
        'edit' => ['estate_id','name','province_id','city_id','district_id','address','house_type','area','is_show'],
        'del'  => ['estate_id']
    ];
    //校验id
    protected function checkId($value, $rule, $data)
    {
        $res = Db::name('estate')->where(['estate_id'=>$value,'is_deleted'=>0])->value('estate_id');
        if ($res) {
            return true;
        }
        return false;
    }

    //
    protected function checkShow($value, $rule, $data)
    {
        if ($value == 0 || $value == 1) {
            return true;
        } else {
            return "参数错误";
        }
    }

    //校验户型
    protected function checkHousetype($value, $rule, $data)
    {
        if(!is_array($value)){
            return false;
        }
        return true;
    }

    //校验面积
    protected function checkArea($value, $rule, $data)
    {
        if(!is_array($value)){
            return false;
        }
        $house_type = $data['house_type'];
        foreach ($house_type as $v){
            if(!isset($value[$v]) || !$value[$v]){
                return false;
            }
        }
        return true;
    }
}
