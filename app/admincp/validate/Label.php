<?php


namespace app\admincp\validate;

use think\facade\Db;
use think\Validate;

/**
 * 标签验证器
 * @author Administrator
 */
class Label extends Validate
{
    //验证规则
    protected $rule = [
        'id'   => 'require|checkId',
        'name' => 'require|max:30',
        'is_show' => 'require|checkShow',
    ];
    
    //错误消息
    protected $message = [
        'id.require' => '缺少id',
        'id.checkId' => '信息不存在',
        'name.require' => '请输入名称',
        'name.max' => '名称最多30个字符',
        'is_show.require' => '请选择是否显示',
    ];
    
    //验证场景
    protected $scene = [
        'add'  => ['name','is_show'],
        'edit' => ['id','name','is_show'],
        'del'  => ['id']
    ];
    //校验id
    protected function checkId($value, $rule, $data)
    {
        $res = Db::name('label')->where(['id'=>$value])->value('id');
        if ($res) {
            return true;
        }
        return false;
    }

    //
    protected function checkShow($value, $rule, $data)
    {
        if ($value == 0 || $value == 1) {
            return true;
        } else {
            return "参数错误";
        }
    }
}
