<?php


namespace app\admincp\validate;

use think\facade\Db;
use think\Validate;

/**
 * 场景户型验证器
 * @author Administrator
 */
class Housetype extends Validate
{
    //验证规则
    protected $rule = [
        'house_id'   => 'require|checkId',
        'images'   => 'require',
        'estate_id'   => 'require',
        'house_type_id'   => 'require',
        'name' => 'require|max:90',
//        'room' => 'require|checkRoom',
    ];
    
    //错误消息
    protected $message = [
        'house_id.require' => '缺少id',
        'house_id.checkId' => '信息不存在',
        'images.require' => '请上传相册图',
        'estate_id.require' => '请选择楼盘',
        'house_type_id.require' => '请选择户型类型',
        'name.require' => '请输入户型名称',
        'name.max' => '户型名称最多90个字符',
//        'room.require' => '请选择房间',
//        'room.checkRoom' => '房间验证失败',
    ];
    
    //验证场景
    protected $scene = [
        'add'  => ['images','estate_id','house_type_id','name'],
        'edit' => ['house_id','images','estate_id','house_type_id','name'],
        'del'  => ['house_id']
    ];
    //校验id
    protected function checkId($value, $rule, $data)
    {
        $res = Db::name('scene_house')->where(['house_id'=>$value])->value('house_id');
        if ($res) {
            return true;
        }
        return false;
    }

    //校验房间
    protected function checkRoom($value, $rule, $data)
    {
        if(!is_array($value)){
            return false;
        }
        return true;
    }
}
