<?php


namespace app\admincp\validate;

use think\facade\Db;
use think\Validate;

/**
 * Description of Article
 *
 * @author Administrator
 */
class Sensitive extends Validate
{
    //验证规则
    protected $rule = [
        'id'   => 'require|checkSensitive',
        'name' => 'require|max:60',
        'is_show' => 'require|checkShow',
    ];
    
    //错误消息
    protected $message = [
        'id.require' => 'id不能为空',
        'name.require' => '标题不能为空',
        'name.max' => '标题最多60个字',
        'is_show.require' => '是否显示不能为空',
    ];
    
    //验证场景
    protected $scene = [
        'add'  => ['name','is_show'],
        'edit' => ['id','name','is_show'],
        'del'  => ['id']
    ];
    //校验id
    protected function checkSensitive($value, $rule, $data)
    {
        $res = Db::name('sensitive')->where(['id'=>$value])->find();
        if ($res) {
            return true;
        }
        return false;
    }

    //
    protected function checkShow($value, $rule, $data)
    {
        if ($value == 0 || $value == 1) {
            return true;
        } else {
            return "参数错误";
        }
    }
}
