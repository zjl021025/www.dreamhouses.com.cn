<?php
namespace app\admincp\validate;
use think\facade\Db;
use think\Validate;
//品牌验证器
class Admin extends Validate
{

    protected $rule=[
        'user_name'    =>'require|unique:admin',
        'password'=>'require',
        'admin_id'=>'require|number',
    ];
    protected $message = [
        'user_name.require'    => '请输入用户名',
        'user_name.unique'     => '已存在相同用户名',
        'password.require'     => '请输入密码',
        'admin_id.require'     => '缺少id',
        'admin_id.number'      => '信息不存在',
    ];
    protected $scene = [
        'add' =>['user_name','password'],
        'edit'=>['user_name','admin_id'],
        'del' =>['admin_id'],
    ];

}