<?php

namespace app\admincp\validate;

use app\common\validate\BaseValidate;
use think\facade\Db;
use think\facade\Validate;

class NodeProblem extends BaseValidate
{
    //验证规则
    protected $rule = [
        'title' => 'require|max:80',
        'title_remind' => 'require',
        'sort' => 'require',
        'is_show' => 'require|checkShow',
    ];

    //错误消息
    protected $message = [
        'title.require' => '请输入标题',
        'title.max' => '标题最多80个字符',
        'title_remind.require' => '请输入标题',
        'sort.require' => '请输入级别',
        'is_show.require' => '请选择是否显示',
    ];

//    //验证场景
//    protected $scene = [
//        'add'  => ['title','sort','is_show','title_remind','content'],
//        'edit' => ['title','sort','is_show','title_remind','content'],
//    ];

    //
    protected function checkShow($value, $rule, $data)
    {
        if ($value == 0 || $value == 1) {
            return true;
        } else {
            return "参数错误";
        }
    }
}