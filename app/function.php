<?php


 use think\facade\Cache;
 
/**
 * @param $arr
 * @param $key_name
 * @return array
 * 将数据库中查出的列表以指定的 id 作为数组的键名
 */
function convert_arr_key($arr, $key_name)
{
    $arr2 = array();
    foreach ($arr as $key => $val) {
        $arr2[$val[$key_name]] = $val;
    }
    return $arr2;
}
function array_allow_keys($array, $keys)
{
    $newArr = [];
    foreach ($keys as $key) {
        if (isset($array[$key])) {
            $newArr[$key] = $array[$key];
        }
    }
    return $newArr;
}
function encrypt($str)
{
    return md5(config("AUTH_CODE").$str);
}
/**
 * 获取数组中的某一列
 * @param array $arr 数组
 * @param string $key_name  列名
 * @return array  返回那一列的数组
 */
function get_arr_column($arr, $key_name)
{
	$arr2 = array();
    foreach ($arr as $key => $val) {
        $arr2[] = $val[$key_name];
    }
    return $arr2;
}
/**
 * 获取url 中的各个参数  类似于 pay_code=alipay&bank_code=ICBC-DEBIT
 * @param string $str
 * @return array
 */
function parse_url_param($str)
{
    $data = array();
    $str = explode('?', $str);
    $str = end($str);
    $parameter = explode('&', $str);
    foreach ($parameter as $val) {
        $tmp = explode('=', $val);
        $data[$tmp[0]] = $tmp[1];
    }
    return $data;
}
/**
 * 二维数组排序
 * @param $arr
 * @param $keys
 * @param string $type
 * @return array
 */
function array_sort($arr, $keys, $type = 'desc')
{
    $key_value = $new_array = array();
    foreach ($arr as $k => $v) {
        $key_value[$k] = $v[$keys];
    }
    if ($type == 'asc') {
        asort($key_value);
    } else {
        arsort($key_value);
    }
    reset($key_value);
    foreach ($key_value as $k => $v) {
        $new_array[$k] = $arr[$k];
    }
    return $new_array;
}
/**
 * 多维数组转化为一维数组
 * @param array $array 多维数组
 * @return array 一维数组
 */
function array_multi2single($array)
{
    static $result_array = array();
    foreach ($array as $value) {
        if (is_array($value)) {
            array_multi2single($value);
        } else
            $result_array [] = $value;
    }
    return $result_array;
}
/**
 * 友好时间显示
 * @param $time
 * @return bool|string
 */
function friend_date($time)
{
    if (!$time)
        return '';
    $fdate = '';
    $d = time() - intval($time);
    $byd = $time - mktime(0, 0, 0, date('m'), date('d') - 2, date('Y'));//前天
    $yd = $time - mktime(0, 0, 0, date('m'), date('d') - 1, date('Y')); //昨天
    $dd = $time - mktime(0, 0, 0, date('m'), date('d'), date('Y')); //今天
    if ($d == 0) {
        $fdate = '刚刚';
    } else {
        switch ($d) {
            case $d < 60:
                $fdate = $d . '秒前';
                break;
            case $d < 3600:
                $fdate = floor($d / 60) . '分钟前';
                break;
            case $d < $dd:
                $fdate = floor($d / 3600) . '小时前';
                break;
            case $d < $yd:
                $fdate = '昨天';
                break;
            case $d < $byd:
//                $fdate = '前天' . date('H:i', $time);//3-30  天前   31~（5月）  月前  6月~  半年前  12月~   年前
                $fdate = '前天';
                break;
            case $d < 30*24*3600:
                $fdate = floor($d / (24*3600)).'天前';
                break;
            case $d < 5*30*24*3600:
                $fdate = floor($d / (30*24*3600)).'月前';
                break;
            case $d < 12*30*24*3600:
                $fdate = '半年前';
                break;
            default:
                $fdate = floor($d / (12*30*24*3600)).'年前';
                break;
        }
    }
    return $fdate;
}

/**
 * @param $arr
 * @param $key_name
  * @param $key_name2
 * @return array
 * 将数据库中查出的列表以指定的 id 作为数组的键名 数组指定列为元素 的一个数组
 */
function get_id_val($arr, $key_name,$key_name2)
{
	$arr2 = array();
	foreach($arr as $key => $val){
		$arr2[$val[$key_name]] = $val[$key_name2];
	}
	return $arr2;
}
// 服务器端IP
function serverIP()
{
    return gethostbyname($_SERVER["SERVER_NAME"]);   
}
/**
  * 自定义函数递归的复制带有多级子目录的目录
  * 递归复制文件夹
  * @param string $src 原目录
  * @param string $dst 复制到的目录
  */
//参数说明
//自定义函数递归的复制带有多级子目录的目录
function recurse_copy($src, $dst)
{
	$now = time();
	$dir = opendir($src);
	@mkdir($dst);
	while (false !== $file = readdir($dir)) {
		if (($file != '.') && ($file != '..')) {
			if (is_dir($src.'/'.$file)) {
				recurse_copy($src.'/'.$file, $dst.'/'.$file);
			}
			else {
				if (file_exists($dst . DIRECTORY_SEPARATOR . $file)) {
					if (!is_writeable($dst . DIRECTORY_SEPARATOR . $file)) {
						exit($dst . DIRECTORY_SEPARATOR . $file . '不可写');
					}
					@unlink($dst . DIRECTORY_SEPARATOR . $file);
				}
				if (file_exists($dst . DIRECTORY_SEPARATOR . $file)) {
					@unlink($dst . DIRECTORY_SEPARATOR . $file);
				}
				$copyrt = copy($src . DIRECTORY_SEPARATOR . $file, $dst . DIRECTORY_SEPARATOR . $file);
				if (!$copyrt) {
					echo 'copy ' . $dst . DIRECTORY_SEPARATOR . $file . ' failed<br>';
				}
			}
		}
	}
	closedir($dir);
}

// 递归删除文件夹
function delFile($path,$delDir = FALSE) {
    if(!is_dir($path))
                return FALSE;		
	$handle = @opendir($path);
	if ($handle) {
		while (false !== ( $item = readdir($handle) )) {
			if ($item != "." && $item != "..")
				is_dir("$path/$item") ? delFile("$path/$item", $delDir) : unlink("$path/$item");
		}
		closedir($handle);
		if ($delDir) return rmdir($path);
	}else {
		if (file_exists($path)) {
			return unlink($path);
		} else {
			return FALSE;
		}
	}
    return true;
}

 
/**
 * 多个数组的笛卡尔积
*
* @param array $data
*/
function combineDika() {
	$data = func_get_args();
	$data = current($data);
	$cnt = count($data);
	$result = array();
    $arr1 = array_shift($data);
	foreach($arr1 as $key=>$item) 
	{
		$result[] = array($item);
	}		

	foreach($data as $key=>$item) 
	{                                
		$result = combineArray($result,$item);
	}
	return $result;
}


/**
 * 两个数组的笛卡尔积
 * @param array $arr1
 * @param array $arr2
*/
function combineArray($arr1,$arr2) {		 
	$result = array();
	foreach ($arr1 as $item1) 
	{
		foreach ($arr2 as $item2) 
		{
			$temp = $item1;
			$temp[] = $item2;
			$result[] = $temp;
		}
	}
	return $result;
}
/**
 * 将二维数组以元素的某个值作为键 并归类数组
 * array( array('name'=>'aa','type'=>'pay'), array('name'=>'cc','type'=>'pay') )
 * array('pay'=>array( array('name'=>'aa','type'=>'pay') , array('name'=>'cc','type'=>'pay') ))
 * @param array $arr 数组
 * @param array $key 分组值的key
 * @return array
 */
function group_same_key($arr,$key){
    $new_arr = array();
    foreach($arr as $k=>$v ){
        $new_arr[$v[$key]][] = $v;
    }
    return $new_arr;
}

/**
 * 获取随机字符串
 * @param int $randLength  长度
 * @param int $addtime  是否加入当前时间戳
 * @param int $includenumber   是否包含数字
 * @return string
 */
function get_rand_str($randLength=6,$addtime=1,$includenumber=0){
    if ($includenumber){
        $chars='abcdefghijklmnopqrstuvwxyzABCDEFGHJKLMNPQEST123456789';
    }else {
        $chars='abcdefghijklmnopqrstuvwxyz';
    }
    $len=strlen($chars);
    $randStr='';
    for ($i=0;$i<$randLength;$i++){
        $randStr.=$chars[rand(0,$len-1)];
    }
    $tokenvalue=$randStr;
    if ($addtime){
        $tokenvalue=$randStr.time();
    }
    return $tokenvalue;
}

/**
 * CURL请求
 * @param $url string 请求url地址
 * @param $method string 请求方法 get post
 * @param mixed $postfields post数据数组
 * @param array $headers 请求header信息
 * @param bool|false $debug  调试开启 默认false
 * @return mixed
 */
function httpRequest($url, $method = "GET", $postfields = null, $headers = array(), $debug = false, $timeout=60)
{
    $method = strtoupper($method);
    $ci = curl_init();
    /* Curl settings */
    curl_setopt($ci, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
    curl_setopt($ci, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:34.0) Gecko/20100101 Firefox/34.0");
    curl_setopt($ci, CURLOPT_CONNECTTIMEOUT, $timeout); /* 在发起连接前等待的时间，如果设置为0，则无限等待 */
    curl_setopt($ci, CURLOPT_TIMEOUT, 7); /* 设置cURL允许执行的最长秒数 */
    curl_setopt($ci, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4); /* 设置使用ip4 */
    curl_setopt($ci, CURLOPT_RETURNTRANSFER, true);
    switch ($method) {
        case "POST":
            curl_setopt($ci, CURLOPT_POST, true);
            if (!empty($postfields)) {
                $tmpdatastr = is_array($postfields) ? http_build_query($postfields) : $postfields;
                curl_setopt($ci, CURLOPT_POSTFIELDS, $tmpdatastr);
            }
            break;
        default:
            curl_setopt($ci, CURLOPT_CUSTOMREQUEST, $method); /* //设置请求方式 */
            break;
    }
    $ssl = preg_match('/^https:\/\//i', $url) ? TRUE : FALSE;
    curl_setopt($ci, CURLOPT_URL, $url);
    if ($ssl) {
        curl_setopt($ci, CURLOPT_SSL_VERIFYPEER, FALSE); // https请求 不验证证书和hosts
        curl_setopt($ci, CURLOPT_SSL_VERIFYHOST, FALSE); // 不从证书中检查SSL加密算法是否存在
    }
    //curl_setopt($ci, CURLOPT_HEADER, true); /*启用时会将头文件的信息作为数据流输出*/
    if (ini_get('open_basedir') == '' && ini_get('safe_mode' == 'Off')) {
    	curl_setopt($ci, CURLOPT_FOLLOWLOCATION, 1);
    }
    curl_setopt($ci, CURLOPT_MAXREDIRS, 2);/*指定最多的HTTP重定向的数量，这个选项是和CURLOPT_FOLLOWLOCATION一起使用的*/
    curl_setopt($ci, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ci, CURLINFO_HEADER_OUT, true);
    /*curl_setopt($ci, CURLOPT_COOKIE, $Cookiestr); * *COOKIE带过去** */
    $response = curl_exec($ci);
    $error = curl_error($ci);
    $requestinfo = curl_getinfo($ci);
    $http_code = curl_getinfo($ci, CURLINFO_HTTP_CODE);
    if ($debug) {
        echo "=====post data======\r\n";
        var_dump($postfields);
        echo "=====error======\r\n";
        var_dump($error);
        echo "=====info===== \r\n";
        print_r($requestinfo);
        echo "=====response=====\r\n";
        print_r($response);
    }
    curl_close($ci);
    return $response;
    //return array($http_code, $response,$requestinfo);
}

/**
 * 过滤数组元素前后空格 (支持多维数组)
 * @param array|string $array 要过滤的数组
 * @return array|string
 */
function trim_array_element($array){
    if(!is_array($array))
        return trim($array);
    return array_map('trim_array_element',$array);
}

/**
 * 检查手机号码格式
 * @param string $mobile 手机号码
 */
function check_mobile($mobile){
    if(preg_match('/1[3456789]\d{9}$/',$mobile))
        return true;
    return false;
}

/**
 * 检查固定电话
 * @param $mobile
 * @return bool
 */
function check_telephone($mobile){
    if(preg_match('/^([0-9]{3,4}-)?[0-9]{7,8}$/',$mobile))
        return true;
    return false;
}

/**
 * 检查邮箱地址格式
 * @param string $email 邮箱地址
 */
function check_email($email){
    if(filter_var($email,FILTER_VALIDATE_EMAIL))
        return true;
    return false;
}


/**
 *   实现中文字串截取无乱码的方法
 */
function getSubstr($string, $start, $length) {
      if(mb_strlen($string,'utf-8')>$length){
          $str = mb_substr($string, $start, $length,'utf-8');
          return $str.'...';
      }else{
          return $string;
      }
}

/**
 * 判断当前访问的用户是  PC端  还是 手机端  返回true 为手机端  false 为PC 端
 * @return boolean
 */
/**
　　* 是否移动端访问访问
　　*
　　* @return bool
　　*/
function isMobile()
{
        // 如果有HTTP_X_WAP_PROFILE则一定是移动设备
    if (isset ($_SERVER['HTTP_X_WAP_PROFILE']))
    return true;

    // 如果via信息含有wap则一定是移动设备,部分服务商会屏蔽该信息
    if (isset ($_SERVER['HTTP_VIA']))
    {
        // 找不到为flase,否则为true
        if(stristr($_SERVER['HTTP_VIA'], "wap"))return true;
    }
    // 脑残法，判断手机发送的客户端标志,兼容性有待提高
    if (isset ($_SERVER['HTTP_USER_AGENT'])){
        $clientkeywords = array ('nokia','sony','ericsson','mot','samsung','htc','sgh','lg','sharp','sie-','philips','panasonic','alcatel','lenovo','iphone','ipod','blackberry','meizu','android','netfront','symbian','ucweb','windowsce','palm','operamini','operamobi','openwave','nexusone','cldc','midp','wap','mobile');
        // 从HTTP_USER_AGENT中查找手机浏览器的关键字
        if (preg_match("/(" . implode('|', $clientkeywords) . ")/i", strtolower($_SERVER['HTTP_USER_AGENT'])))return true;
         
        $pos = strpos(strtolower($_SERVER['HTTP_USER_AGENT']), "android") ||  strpos(strtolower($_SERVER['HTTP_USER_AGENT']), "mobile");
        if($pos)return true;
    }
        // 协议法，因为有可能不准确，放到最后判断
    if (isset ($_SERVER['HTTP_ACCEPT']))
    {
    // 如果只支持wml并且不支持html那一定是移动设备
    // 如果支持wml和html但是wml在html之前则是移动设备
        if ((strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') !== false) && (strpos($_SERVER['HTTP_ACCEPT'], 'text/html') === false || (strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') < strpos($_SERVER['HTTP_ACCEPT'], 'text/html'))))
        {
            return true;
        }
    }
            return false;
 }

function is_weixin() {
    if (strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') !== false) {
        return true;
    } return false;
}
 

function is_qq() {
    if (strpos($_SERVER['HTTP_USER_AGENT'], 'QQ') !== false) {
        return true;
    } return false;
}
function is_alipay() {
    if (strpos($_SERVER['HTTP_USER_AGENT'], 'AlipayClient') !== false) {
        return true;
    } return false;
}
function is_ios()
{
    $agent = strtolower($_SERVER['HTTP_USER_AGENT']);
    if (strpos($agent, 'iphone') || strpos($agent, 'ipad')) {
        return true;
    }
    return false;
}

//php获取中文字符拼音首字母
function getFirstCharter($str){
      if(empty($str))
      {
            return '';          
      }
      $fchar=ord($str{0});
      if($fchar>=ord('A')&&$fchar<=ord('z')) return strtoupper($str{0});
      $s1=iconv('UTF-8','gb2312//TRANSLIT//IGNORE',$str);
      $s2=iconv('gb2312','UTF-8//TRANSLIT//IGNORE',$s1);
      $s=$s2==$str?$s1:$str;
      $asc=ord($s{0})*256+ord($s{1})-65536;
     if($asc>=-20319&&$asc<=-20284) return 'A';
     if($asc>=-20283&&$asc<=-19776) return 'B';
     if($asc>=-19775&&$asc<=-19219) return 'C';
     if($asc>=-19218&&$asc<=-18711) return 'D';
     if($asc>=-18710&&$asc<=-18527) return 'E';
     if($asc>=-18526&&$asc<=-18240) return 'F';
     if($asc>=-18239&&$asc<=-17923) return 'G';
     if($asc>=-17922&&$asc<=-17418) return 'H';
     if($asc>=-17417&&$asc<=-16475) return 'J';
     if($asc>=-16474&&$asc<=-16213) return 'K';
     if($asc>=-16212&&$asc<=-15641) return 'L';
     if($asc>=-15640&&$asc<=-15166) return 'M';
     if($asc>=-15165&&$asc<=-14923) return 'N';
     if($asc>=-14922&&$asc<=-14915) return 'O';
     if($asc>=-14914&&$asc<=-14631) return 'P';
     if($asc>=-14630&&$asc<=-14150) return 'Q';
     if($asc>=-14149&&$asc<=-14091) return 'R';
     if($asc>=-14090&&$asc<=-13319) return 'S';
     if($asc>=-13318&&$asc<=-12839) return 'T';
     if($asc>=-12838&&$asc<=-12557) return 'W';
     if($asc>=-12556&&$asc<=-11848) return 'X';
     if($asc>=-11847&&$asc<=-11056) return 'Y';
     if($asc>=-11055&&$asc<=-10247) return 'Z';
     return null;
}

/**
 * 获取整条字符串汉字拼音首字母
 * @param $zh
 * @return string
 */
function pinyin_long($zh){
    $ret = "";
    $s1 = iconv("UTF-8","gb2312", $zh);
    $s2 = iconv("gb2312","UTF-8", $s1);
    if($s2 == $zh){$zh = $s1;}
    for($i = 0; $i < strlen($zh); $i++){
        $s1 = substr($zh,$i,1);
        $p = ord($s1);
        if($p > 160){
            $s2 = substr($zh,$i++,2);
            $ret .= getFirstCharter($s2);
        }else{
            $ret .= $s1;
        }
    }
    return $ret;
}


function ajaxReturn($data){
    return json($data);
//    exit(json_encode($data, JSON_UNESCAPED_UNICODE));
}

function flash_sale_time_space()
{
    $now_day = date('Y-m-d');
    $now_time = date('H');
    if ($now_time % 2 == 0) {
        $flash_now_time = $now_time;
    } else {
        $flash_now_time = $now_time - 1;
    }
    $flash_sale_time = strtotime($now_day . " " . $flash_now_time . ":00:00");
    $space = 7200;
    $time_space = array(
        '1' => array('font' => date("H:i", $flash_sale_time), 'start_time' => $flash_sale_time, 'end_time' => $flash_sale_time + $space),
        '2' => array('font' => date("H:i", $flash_sale_time + $space), 'start_time' => $flash_sale_time + $space, 'end_time' => $flash_sale_time + 2 * $space),
        '3' => array('font' => date("H:i", $flash_sale_time + 2 * $space), 'start_time' => $flash_sale_time + 2 * $space, 'end_time' => $flash_sale_time + 3 * $space),
        '4' => array('font' => date("H:i", $flash_sale_time + 3 * $space), 'start_time' => $flash_sale_time + 3 * $space, 'end_time' => $flash_sale_time + 4 * $space),
        '5' => array('font' => date("H:i", $flash_sale_time + 4 * $space), 'start_time' => $flash_sale_time + 4 * $space, 'end_time' => $flash_sale_time + 5 * $space),
    );
    return $time_space;
}

/**
 * 验证码操作(不生成图片)
 * @param array $inconfig  配置
 * @param string $id 要生成验证码的标识
 * @param string $incode 验证码,若为null生成验证码,否则检验验证码
 */
function capache($inconfig = [], $id = '', $incode = null)
{  
    $config = array(
        'seKey'     =>  'ThinkPHP.CN',   // 验证码加密密钥
        'codeSet'   =>  '2345678abcdefhijkmnpqrstuvwxyzABCDEFGHJKLMNPQRTUVWXY', // 验证码字符集合
        'expire'    =>  1800,            // 验证码过期时间（s）
        'useZh'     =>  false,           // 使用中文验证码 
        'zhSet'     =>  '们以我到他会作时要动国产的一是工就年阶义发成部民可出能方进在了不和有大这主中人上为来分生对于学下级地个用同行面说种过命度革而多子后自社加小机也经力线本电高量长党得实家定深法表着水理化争现所二起政三好十战无农使性前等反体合斗路图把结第里正新开论之物从当两些还天资事队批点育重其思与间内去因件日利相由压员气业代全组数果期导平各基或月毛然如应形想制心样干都向变关问比展那它最及外没看治提五解系林者米群头意只明四道马认次文通但条较克又公孔领军流入接席位情运器并飞原油放立题质指建区验活众很教决特此常石强极土少已根共直团统式转别造切九你取西持总料连任志观调七么山程百报更见必真保热委手改管处己将修支识病象几先老光专什六型具示复安带每东增则完风回南广劳轮科北打积车计给节做务被整联步类集号列温装即毫知轴研单色坚据速防史拉世设达尔场织历花受求传口断况采精金界品判参层止边清至万确究书术状厂须离再目海交权且儿青才证低越际八试规斯近注办布门铁需走议县兵固除般引齿千胜细影济白格效置推空配刀叶率述今选养德话查差半敌始片施响收华觉备名红续均药标记难存测士身紧液派准斤角降维板许破述技消底床田势端感往神便贺村构照容非搞亚磨族火段算适讲按值美态黄易彪服早班麦削信排台声该击素张密害侯草何树肥继右属市严径螺检左页抗苏显苦英快称坏移约巴材省黑武培著河帝仅针怎植京助升王眼她抓含苗副杂普谈围食射源例致酸旧却充足短划剂宣环落首尺波承粉践府鱼随考刻靠够满夫失包住促枝局菌杆周护岩师举曲春元超负砂封换太模贫减阳扬江析亩木言球朝医校古呢稻宋听唯输滑站另卫字鼓刚写刘微略范供阿块某功套友限项余倒卷创律雨让骨远帮初皮播优占死毒圈伟季训控激找叫云互跟裂粮粒母练塞钢顶策双留误础吸阻故寸盾晚丝女散焊功株亲院冷彻弹错散商视艺灭版烈零室轻血倍缺厘泵察绝富城冲喷壤简否柱李望盘磁雄似困巩益洲脱投送奴侧润盖挥距触星松送获兴独官混纪依未突架宽冬章湿偏纹吃执阀矿寨责熟稳夺硬价努翻奇甲预职评读背协损棉侵灰虽矛厚罗泥辟告卵箱掌氧恩爱停曾溶营终纲孟钱待尽俄缩沙退陈讨奋械载胞幼哪剥迫旋征槽倒握担仍呀鲜吧卡粗介钻逐弱脚怕盐末阴丰雾冠丙街莱贝辐肠付吉渗瑞惊顿挤秒悬姆烂森糖圣凹陶词迟蚕亿矩康遵牧遭幅园腔订香肉弟屋敏恢忘编印蜂急拿扩伤飞露核缘游振操央伍域甚迅辉异序免纸夜乡久隶缸夹念兰映沟乙吗儒杀汽磷艰晶插埃燃欢铁补咱芽永瓦倾阵碳演威附牙芽永瓦斜灌欧献顺猪洋腐请透司危括脉宜笑若尾束壮暴企菜穗楚汉愈绿拖牛份染既秋遍锻玉夏疗尖殖井费州访吹荣铜沿替滚客召旱悟刺脑措贯藏敢令隙炉壳硫煤迎铸粘探临薄旬善福纵择礼愿伏残雷延烟句纯渐耕跑泽慢栽鲁赤繁境潮横掉锥希池败船假亮谓托伙哲怀割摆贡呈劲财仪沉炼麻罪祖息车穿货销齐鼠抽画饲龙库守筑房歌寒喜哥洗蚀废纳腹乎录镜妇恶脂庄擦险赞钟摇典柄辩竹谷卖乱虚桥奥伯赶垂途额壁网截野遗静谋弄挂课镇妄盛耐援扎虑键归符庆聚绕摩忙舞遇索顾胶羊湖钉仁音迹碎伸灯避泛亡答勇频皇柳哈揭甘诺概宪浓岛袭谁洪谢炮浇斑讯懂灵蛋闭孩释乳巨徒私银伊景坦累匀霉杜乐勒隔弯绩招绍胡呼痛峰零柴簧午跳居尚丁秦稍追梁折耗碱殊岗挖氏刃剧堆赫荷胸衡勤膜篇登驻案刊秧缓凸役剪川雪链渔啦脸户洛孢勃盟买杨宗焦赛旗滤硅炭股坐蒸凝竟陷枪黎救冒暗洞犯筒您宋弧爆谬涂味津臂障褐陆啊健尊豆拔莫抵桑坡缝警挑污冰柬嘴啥饭塑寄赵喊垫丹渡耳刨虎笔稀昆浪萨茶滴浅拥穴覆伦娘吨浸袖珠雌妈紫戏塔锤震岁貌洁剖牢锋疑霸闪埔猛诉刷狠忽灾闹乔唐漏闻沈熔氯荒茎男凡抢像浆旁玻亦忠唱蒙予纷捕锁尤乘乌智淡允叛畜俘摸锈扫毕璃宝芯爷鉴秘净蒋钙肩腾枯抛轨堂拌爸循诱祝励肯酒绳穷塘燥泡袋朗喂铝软渠颗惯贸粪综墙趋彼届墨碍启逆卸航衣孙龄岭骗休借',              // 中文验证码字符串
        'length'    =>  4,               // 验证码位数
        'reset'     =>  true,           // 验证成功后是否重置
    );
    $config = array_merge($config, $inconfig);
    $authcode = function ($str) use ($config) {
        $key = substr(md5($config['seKey']), 5, 8);
        $str = substr(md5($str), 8, 10);
        return md5($key . $str);
    };

    /* 生成验证码 */
    if ($incode === null) {
        for ($i = 0; $i<$config['length']; $i++) {
            $code[$i] = $config['codeSet'][mt_rand(0, strlen($config['codeSet'])-1)];
        }
        // 保存验证码
        $code_str   =   implode('', $code);
        $key        =   $authcode($config['seKey']);
        $code       =   $authcode(strtoupper($code_str));
        $secode     =   array();
        $secode['verify_code'] = $code; // 把校验码保存到session
        $secode['verify_time'] = NOW_TIME;  // 验证码创建时间
        session($key.$id, $secode);
        return $code_str;
    } 

    /* 检验验证码 */
    if (is_string($incode)) {
        $key = $authcode($config['seKey']).$id;
        // 验证码不能为空
        $secode = session($key);
        if (empty($incode) || empty($secode)) {
            return false;
        }
        // session 过期
        if (NOW_TIME - $secode['verify_time'] > $config['expire']) {
            session($key, null);
            return false;
        }

        if ($authcode(strtoupper($incode)) == $secode['verify_code']) {
            $config['reset'] && session($key, null);
            return true;
        }
        return false;
    }

    return false;
}

function urlsafe_b64encode($string) 
{
    $data = base64_encode($string);
    $data = str_replace(array('+','/','='),array('-','_',''),$data);
    return $data;
}

/**
 * 当前请求是否是https
 * @return bool
 */
function is_https()
{
    return isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] && $_SERVER['HTTPS'] != 'off';
}
function mobile_hide($mobile){
    return substr_replace($mobile,'****',3,4);
}

/**
 * 	作用：array转xml
 */
function arrayToXml($arr)
{
    $xml = "<xml>";
    foreach ($arr as $key=>$val)
    {
        if (is_numeric($val))
        {
            $xml.="<".$key.">".$val."</".$key.">";
        }
        else
            $xml.="<".$key."><![CDATA[".$val."]]></".$key.">";
    }
    $xml.="</xml>";
    return $xml;
}

/**
 * 作用：将xml转为array
 */
function xmlToArray($xml) {
	// 将XML转为array
	libxml_disable_entity_loader(true);
	libxml_use_internal_errors();
	$array_data = json_decode ( json_encode ( simplexml_load_string ( $xml, 'SimpleXMLElement', LIBXML_NOCDATA ) ), true );
	return $array_data;
}

/**
 * 替换特殊字符
 * @param string 替换字符串
 * @return mixed
 */
function replaceSpecialStr($nickname){
    $clean_text = "";

    // Match Emoticons
    $regexEmoticons = '/[\x{1F600}-\x{1F64F}]/u';
    $clean_text = preg_replace($regexEmoticons, '', $nickname);

    // Match Miscellaneous Symbols and Pictographs
    $regexSymbols = '/[\x{1F300}-\x{1F5FF}]/u';
    $clean_text = preg_replace($regexSymbols, '', $clean_text);

    // Match Transport And Map Symbols
    $regexTransport = '/[\x{1F680}-\x{1F6FF}]/u';
    $clean_text = preg_replace($regexTransport, '', $clean_text);

    // Match Miscellaneous Symbols
    $regexMisc = '/[\x{2600}-\x{26FF}]/u';
    $clean_text = preg_replace($regexMisc, '', $clean_text);

    // Match Dingbats
    $regexDingbats = '/[\x{2700}-\x{27BF}]/u';
    $clean_text = preg_replace($regexDingbats, '', $clean_text);

    return $clean_text;
}
/**
 * 比较两个版本大小, $v1>v2:1 ; $v1=v2:0 ;$v1<v2:0
 * @param string $v1
 * @param string $v2
 * @return number
 */
function compareVersion($v1, $v2) {
    $v1 = explode(".",$v1);
    $v2 =  explode(".",$v2);
    $len = max(count($v1), count($v2));

    while(count($v1) < $len) {
        array_push($v1, 0);
    }

    while(count($v2) < $len) {
        array_push($v2, 0);
    }
    for($i = 0; $i < $len;$i++) {
        $num1 = intval($v1[$i]);
        $num2 = intval($v2[$i]);
        if ($num1 > $num2) {
            return 1;
        } else if ($num1 < $num2) {
            return -1;
        }
    }
    return 0;
}

/**
 * 传时间戳
 * @param $time
 * @return bool|string
 */
function time_to_str($time){
    if ($time > strtotime(date("Y-m-d"))) {
        $text = '今天 ' . date("H:i", $time);
    } elseif ($time > strtotime(date("Y-m-d", strtotime('-1 day')))) {
        $text = '昨天 ' . date("H:i", $time);
    } elseif (empty($time)) {
        $text = '';
    } else {
        $text = date("Y-m-d H:i:s", $time);
    }
    return $text;
}

/**
 * 用于app补全图片连接
 * @param $str
 * @return string
 */
function htmlspecialchars_decode_htm($str){
    $str = htmlspecialchars_decode($str);
    $str = str_replace('src="/','src="'.SITE_URL.'/',$str);
    return $str;
}

/**
 * 去掉字符串换行符,微信分享时用
 * @param $str
 * @return mixed
 */
function del_eol($str){

    return str_replace(PHP_EOL, '', $str);
}
/**
 * 去掉html标签，
 * @param $string
 * @param int $sublen
 * @return string
 */
function cutstr_html($string, $sublen=0){
    $string = strip_tags($string);
    $string = trim($string);
    $string = str_replace("\t","",$string);
    $string = str_replace("\r\n","",$string);
    $string = str_replace("\r","",$string);
    $string = str_replace("\n","",$string);
    $string = str_replace(" ","",$string);
    if(!empty($sublen)){
        $string = getSubstr($string,0,$sublen);
    }
    return trim($string);
}
/**
 * 转换SQL关键字
 *
 * @param string $string
 * @return array
 */
function strip_sql($string) {
    $pattern_arr = array(
        "/\bunion\b/i",
        "/\bwhere\b/i",
        "/\bfrom\b/i",
        "/\bselect\b/i",
        "/\bupdate\b/i",
        "/\bdelete\b/i",
        "/\boutfile\b/i",
        "/\bor\b/i",
        "/\bchar\b/i",
        "/\bconcat\b/i",
        "/\btruncate\b/i",
        "/\bdrop\b/i",
        "/\binsert\b/i",
        "/\brevoke\b/i",
        "/\bgrant\b/i",
        "/\breplace\b/i",
        "/\balert\b/i",
        "/\brename\b/i",
        "/\bcreate\b/i",
        "/\bmaster\b/i",
        "/\bdeclare\b/i",
        "/\bsource\b/i",
        "/\bload\b/i",
        "/\bcall\b/i",
        "/\bexec\b/i",
        "/\bdelimiter\b/i",
    );
    $replace_arr = array(
        'ｕｎｉｏｎ',
        'ｗｈｅｒｅ',
        'ｆｒｏｍ',
        'ｓｅｌｅｃｔ',
        'ｕｐｄａｔｅ',
        'ｄｅｌｅｔｅ',
        'ｏｕｔｆｉｌｅ',
        'ｏｒ',
        'ｃｈａｒ',
        'ｃｏｎｃａｔ',
        'ｔｒｕｎｃａｔｅ',
        'ｄｒｏｐ',
        'ｉｎｓｅｒｔ',
        'ｒｅｖｏｋｅ',
        'ｇｒａｎｔ',
        'ｒｅｐｌａｃｅ',
        'ａｌｅｒｔ',
        'ｒｅｎａｍｅ',
        'ｃｒｅａｔｅ',
        'ｍａｓｔｅｒ',
        'ｄｅｃｌａｒｅ',
        'ｓｏｕｒｃｅ',
        'ｌｏａｄ',
        'ｃａｌｌ',
        'ｅｘｅｃ',
        'ｄｅｌｉｍｉｔｅｒ',
    );
    return is_array($string) ? array_map('strip_sql', $string) : preg_replace($pattern_arr, $replace_arr, $string);
}

/*初始化JSON*/
function dyajaxReturn($errcode=0, $message='', $data = null ,$dyurl='',$typeid=1){
	$res['errcode'] = $errcode;
	$res['message'] = $message;
    if (is_null($data) == false) {
        $res['data'] = $data;
    }
	$dyurl && $res['dyurl'] = $dyurl;
    return ajaxReturn($res);
}


/*初始化JSON*/
function dyjsonReturn($errcode = 0, $message = '', $data = null, $dyurl = '')
{
    $res['errcode'] = $errcode;
    $res['message'] = $message;
    //$data && $res['data'] = $data;
    if (is_null($data) == false) {
        $res['data'] = $data;
    }
    $dyurl && $res['dyurl'] = $dyurl;
    return json_encode($res,JSON_UNESCAPED_UNICODE); 
}


/**
 * 格式化商品规格
 * 
 *
 * @return string
 */
function formatgoodsspec($spec_key_name='') {
    if(empty($spec_key_name)) return '';
    $specname = ''; 
    $spec = explode("##",$spec_key_name);
    foreach($spec as $v1){
        $specname .= explode(":",$v1)[1].', '; 
    }
    return rtrim($specname,', ');
}

//隐藏用户昵称 只显示收尾，中间用*代替
function hidenickname($nickname){
	return dy_cut_str($nickname, 1, 0).'***'.dy_cut_str($nickname, 1, -1); 
}

function dy_cut_str($string, $sublen, $start = 0, $code = 'UTF-8'){ 
    if($code == 'UTF-8'){ 
        $pa = "/[\x01-\x7f]|[\xc2-\xdf][\x80-\xbf]|\xe0[\xa0-\xbf][\x80-\xbf]|[\xe1-\xef][\x80-\xbf][\x80-\xbf]|\xf0[\x90-\xbf][\x80-\xbf][\x80-\xbf]|[\xf1-\xf7][\x80-\xbf][\x80-\xbf][\x80-\xbf]/"; 
        preg_match_all($pa, $string, $t_string);
        if(count($t_string[0]) - $start > $sublen) return join('', array_slice($t_string[0], $start, $sublen));
        return join('', array_slice($t_string[0], $start, $sublen));
    }else{
        $start = $start*2;
        $sublen = $sublen*2;
        $strlen = strlen($string);
        $tmpstr = '';
        for($i=0; $i< $strlen; $i++){
            if($i>=$start && $i< ($start+$sublen)){
                if(ord(substr($string, $i, 1))>129){
                    $tmpstr.= substr($string, $i, 2);
                }else{
                    $tmpstr.= substr($string, $i, 1);
                }
            }
            if(ord(substr($string, $i, 1))>129) $i++;
        }
        return $tmpstr;
    }
}

/*缩略图表*/
function gettables($table='article'){
	switch($table){
        case 'users':
            $arr = array('id'=>'user_id','table'=>'users','thumb'=>'avatar');
            break;
        case 'user_dynamic':
            $arr = array('id'=>'dynamic_id','table'=>'user_dynamic','thumb'=>'image');
            break;
        case 'furniture_category':
            $arr = array('id'=>'id','table'=>'furniture_category','thumb'=>'image');
            break;
        case 'furniture':
            $arr = array('id'=>'id','table'=>'furniture','thumb'=>'image');
            break;
        case 'scene_house':
            $arr = array('id'=>'house_id','table'=>'scene_house','thumb'=>'image');
            break;
        case 'user_works':
            $arr = array('id'=>'works_id','table'=>'user_works','thumb'=>'image');
            break;
        case 'guide':
            $arr = array('id'=>'id','table'=>'guide','thumb'=>'bg');
            break;
		default:
			$arr = array('id'=>'article_id','table'=>'article');  
	}
	return $arr;
}

/*保留两位小数*/
function dykeeptwodecimal($money,$typeid=1){
    switch($typeid){
        case 1: //四舍五入
            $str = sprintf("%.2f",$money); 
        break;
        default: //不四舍五入
            $str = floor($money*100)/100; 
        break;        
    }
	return $str;
}

/* 获取扩展名 */
function fileext($filename) {
    return strtolower(trim(substr(strrchr($filename, '.'), 1, 10)));
}

/**
* 导入excel-获取表内数据
*/
function export_excel($address = "") {
    if ($address == "") {
        $address = "/excel";
    }
    $file = Request()->file('excel_file');
    $ext = $file->getOriginalExtension();
    if ($ext != "xlsx" && $ext != "xls") {
        $data['errcode'] = 0;
        $data['message'] = "上传文件后缀名为xlsx或xls";
        return $data;
    }
    $size = $file->getSize();
    if ($size > 1024*1024) {
        $data['errcode'] = 0;
        $data['message'] = "上传文件过大,请控制在1MB已内";
        return $data;
    }
    $savename = \think\facade\Filesystem::disk('public')->putFile($address, $file,'uniqid');
    $import_path = root_path() . 'public/public/upload/' . $savename;
    $spreadsheet = IOFactory::load($import_path);
    $sheet = $spreadsheet->getActiveSheet();
    $sheetData = $sheet->toArray();
    $num = count($sheetData);
    unset($sheetData[0]);
    //unset($sheetData[$num-1]);
    $sheetData = array_values($sheetData);
    $data['errcode'] = 1;
    $data['data'] = $sheetData;//excel表的数据
    $data['path_url'] = $import_path;//excel表存在的位置
    return $data;
}
/**
* 导出 csv,并压缩
*
* @param string $name  要导出的文件名称
* @param array $list  要导出的数据的集合
* @param array $ext   要导出的数据所对应的名称
* @param string $path_url   数据表所在的文件位置
* @param string $pagesize   每个数据表存放的数据
* @param string $cache_name 当前导出数据量多少的缓存名称
* @param string $cache_zip_url  压缩成zip所在位置的缓存名称
* @return void
*/
function csv_export($name,$list, $ext, $path_url, $pagesize, $cache_name, $cache_zip_url = ""){
    set_time_limit(0);
    session_write_close();//断开AJAX请求，无需等待响应
    foreach ($ext as $key => $item) {
        $ext[$key] = iconv("UTF-8", "GB2312//IGNORE", $item);
    }
    if(!is_dir($path_url)){
        mkdir(iconv("UTF-8", "GBK", $path_url),0755,true);//判断文件夹是否存在，不存在创建
    }
    $all_num = count($list);
    $n = ceil($all_num/$pagesize);//要生成几张csv表
    $h = 0;
    $urls = [];
    $a = [];
    //逐行取出数据，不浪费内存
//    $url_name = '/'.date('Ymd').rand(100000, 999999);
    $url_name = '/'.$name;
    for ($i = 0; $i < $n; $i++) {
        $url = $path_url.$url_name.'-'.($i+1).".csv";
        $urls[] = $url;
        $fp =  fopen($url, 'w');
        fputcsv($fp, $ext);
        if ($n == 1) {
            $num = $all_num;
        } else {
            if ($i+1 == $n) {
                $num = $all_num*1 - $i*$pagesize;
            } else {
                $num = $pagesize;
            }
        }
        for ($x = 0; $x < $num; $x++) {
            foreach ($list[$x+$i*$pagesize] as $k1 => $v1) {
                $a[$k1] = iconv("UTF-8","GBK",$v1."\t");//加\t,是为了不让数字自动进行科学计数法
            }
            fputcsv($fp, $a);
            $h = $h + 1;
            Cache::set($cache_name, $h, 300);
        }
        ob_flush();
        flush();
        fclose($fp);//每生成一个文件关闭
    }
//    $zipname = $path_url.$url_name.".zip";
    @unlink($path_url.$url_name.".zip");
    $zipname = $path_url.$url_name.".zip";
    dy_get_zip($zipname, $urls);//压缩
    Cache::set($cache_zip_url, $zipname, 300);
    foreach($urls as $k => $v) {
        if (file_exists($v)) {
            @unlink($v);
        }
    }
}
/**
 * 写入Excel
 *
 * @param array $title 写入的字段
 * @param array $data //数据
 * @param string $path //路径
 * @param int $pagesize  每张excel存的数据
 * @param string $cache_name 当前导出数据量多少的缓存名称
 * @param string $cache_zip_url  压缩成zip所在位置的缓存名称
 * @return void
 */
function write_excel($title = [], $data = [], $path = '', $pagesize, $cache_name = "", $cache_zip_url = "")
{
    // 获取Spreadsheet对象
    $spreadsheet = new Spreadsheet();
    $sheet = $spreadsheet->getActiveSheet();
    $all_num = count($data);//统计数据的总数量
    if(!is_dir($path)){
        @mkdir(iconv("UTF-8", "GBK", $path),0755,true);//判断文件夹是否存在，不存在创建
    }
    $n = ceil($all_num/$pagesize);//要生成几张excel表
    $h = 0;
    $urls = [];//存放表的地址
    $a = [];
    $url_name = date('ymd').rand(1000,9999);
    // 表头单元格内容 第一行
    for ($x=0;$x<$n; $x++) {
        $titCol = 'A';
        foreach ($title as $value) {
            // 单元格内容写入
            $sheet->setCellValue($titCol . '1', $value);
            $titCol++;
        }
        if ($n == 1) {
            $num = $all_num;
        } else {
            if ($x+1 == $n) {
                $num = $all_num*1 - $x*$pagesize;
            } else {
                $num = $pagesize;
            }
        }
        //从第二行开始写入数据
        $row = 2;
        //$dataCol = "A";
        for ($i=0;$i<$num;$i++) {
            $dataCol = "A";
            foreach ($data[$x*$pagesize+$i] as $value) {
                // 单元格内容写入
                $sheet->setCellValue($dataCol . $row, $value);
                $dataCol++;
            }
            $row++;
            $h = $h + 1;
            Cache::set($cache_name, $h, 300);
        }
        $writer = new Xlsx($spreadsheet);
        $writer->save(root_path() .'public/'.$path.$url_name.($x+1).'.xlsx');
        $urls[] = $path.$url_name.($x+1).'.xlsx';
    }
    $zipname = $path.$url_name.".zip";
    dy_get_zip($zipname, $urls);//压缩
    Cache::set($cache_zip_url, $zipname, 300);
    foreach($urls as $k => $v) {
        if (file_exists($v)) {
            @unlink($v);
        }
    }
}
/**
* 压缩文件
*
* @param [string] $zipName --压缩包放的位置
* @param [string|array] $files  -要压缩的文件
* @return void
*/
function dy_get_zip($zipName, $files){
    $zip = new \ZipArchive();
    if ($zip->open($zipName, \ZipArchive::CREATE) !== TRUE) {
        exit('无法打开文件，或者文件创建失败');
        return 0;
    }
    if (is_array($files) == false) {
        $zip->addFile($files, basename($files));
    } else {
        foreach ($files as $val) {
            $zip->addFile($val, basename($val));
        }
    }
    $zip->close();
}
/**
 * @param string $string 需要加密的字符串
 * @param string $key 密钥 - 可选入参
 * @return string
 */
function dyencrypt($string){
    // openssl_encrypt 加密不同Mcrypt，对秘钥长度要求，超出16加密结果不变
    $data = openssl_encrypt($string, 'AES-128-ECB', config('DY_AES_KEY'), OPENSSL_RAW_DATA);
    $data = base64_encode($data);
    return $data;
}

/**
 * @param string $string 需要解密的字符串
 * @param string $key 密钥 - 可选入参
 * @return string
 */
function dydecrypt($string){
    $decrypted = openssl_decrypt(base64_decode($string), 'AES-128-ECB', config('DY_AES_KEY'), OPENSSL_RAW_DATA);
    return $decrypted;
}

/**
 * 求两个日期之间相差的天数
 * (针对1970年1月1日之后，求之前可以采用泰勒公式)
 * @param string $starttime 时间戳
 * @param string $endtime 时间戳
 * @return number
 */
function diffBetweenTwoDays($starttime, $endtime){
    if ($starttime < $endtime) {
        $tmp = $endtime;
        $endtime = $starttime;
        $starttime = $tmp;
    }
    return ($starttime - $endtime) / 86400;
}

///**
// * 加密方法
// * @param string $str
// * @return string
// */
//function dyencrypt($input) {
//	$size = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_ECB);
//	$input = pkcs5_pad($input, $size);
//	$td = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', MCRYPT_MODE_ECB, '');
//	$iv = mcrypt_create_iv (mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
//	mcrypt_generic_init($td, config('API_SECRET_KEY'), $iv);
//	$data = mcrypt_generic($td, $input);
//	mcrypt_generic_deinit($td);
//	mcrypt_module_close($td);
//	$data = base64_encode($data);
//	return $data;
//}
//function pkcs5_pad ($text, $blocksize) {
//	$pad = $blocksize - (strlen($text) % $blocksize);
//	return $text . str_repeat(chr($pad), $pad);
//}
//function dydecrypt($sStr) {
//	$decrypted= mcrypt_decrypt(
//		MCRYPT_RIJNDAEL_128,
//		config('API_SECRET_KEY'),
//		base64_decode($sStr),
//		MCRYPT_MODE_ECB
//	);
//	$dec_s = strlen($decrypted);
//	$padding = ord($decrypted[$dec_s-1]);
//	$decrypted = substr($decrypted, 0, -$padding);
//	return $decrypted;
//}

/*接口请求方式验证*/
function ispostdata($notaarray){
	if(!IS_POST && !in_array(strtolower(ACTION_NAME),$notaarray))
		return dyajaxReturn(-105,'接口请求异常','');		
	if(dyallheaders()['apihandshakey'] != 'a18ead7f99c0566edd45035c8429ae8e' && !in_array(strtolower(ACTION_NAME),$notaarray))
		return dyajaxReturn(-106,'请勿非法请求接口','');		
}

/**
 * 获取header数据
 */
function dyallheaders(){
	$ignore = array('host','accept','user-agent','x-original-url','content-length','connection','accept-encoding');
	$headers = array();
	foreach($_SERVER as $key=>$value){
		if(substr($key, 0, 5)==='HTTP_'){
			$key = substr($key, 5);
			$key = str_replace('_', ' ', $key);
			$key = str_replace(' ', '-', $key);
			$key = strtolower($key);
			if(!in_array($key, $ignore)) $headers[$key] = $value;
		}
	}
	$ret = array();
	$ret['header'] = $headers;
	$str = json_encode($ret, JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT);
	return $headers;
}

 /*TOKEN*/
 function dysettoken()
 {
    return md5(time().mt_rand(1,999999999));
}

/*未付款订单自动取消时间*/
function canceltimeconv($tid,$overtime=0){
	switch($tid){
		case 1: $str = 900; break; //15分钟	
		case 2: $str = 1800; break; //30分钟	
		case 3: $str = 3600; break; //60分钟	
		case 4: $str = 7200; break; //120分钟	
		case 5: $str = 60; break; //1分钟	
		case 6: $str = rand(10,60); break; //10~60秒的随机数	
		case 7: $str = 172800; break; //48小时	
		case 8: $str = 7000; break; //2小时	
		case 9: $str = 86400; break; //24小时
		default:$str = 900; break; //默认15分钟
	}
	return $str;
}

/**
*  作用：格式化参数，签名过程需要使用
*/
function formatBizQueryParaMap($paraMap, $urlencode){
	$buff = "";
	ksort($paraMap);
	foreach ($paraMap as $k => $v){
		if($urlencode){
			$v = urlencode($v);
		}
		$buff .= $k . "=" . $v . "&";
	}
	$reqPar='';
	if (strlen($buff) > 0){
		$reqPar = substr($buff, 0, strlen($buff)-1);
	}
	return $reqPar;
}

/**
 * 作用：以post方式提交xml到对应的接口url
 */
function postXmlCurl($xml, $url, $second = 30) {
	// 初始化curl
	$ch = curl_init ();
	// 设置超时
	curl_setopt ( $ch, CURLOPT_TIMEOUT, $second );
	curl_setopt ( $ch, CURLOPT_URL, $url );
	curl_setopt ( $ch, CURLOPT_SSL_VERIFYPEER, FALSE );
	curl_setopt ( $ch, CURLOPT_SSL_VERIFYHOST, FALSE );
	// 设置header
	curl_setopt ( $ch, CURLOPT_HEADER, FALSE );
	// 要求结果为字符串且输出到屏幕上
	curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, TRUE );
	// post提交方式
	curl_setopt ( $ch, CURLOPT_POST, TRUE );
	curl_setopt ( $ch, CURLOPT_POSTFIELDS, $xml );
	// 运行curl
	$data = curl_exec ( $ch );
	curl_close ( $ch );
	// 返回结果
	if ($data) {
		//curl_close ( $ch );
		return $data;
	} else {
		$error = curl_errno ( $ch );
		curl_close ( $ch );
		throw new WxException("curl出错，错误码:$error");
	}
}

/* 生成二维码并保存再本地 */
function createrecqrcode($url)
{
    ob_end_clean();
    require "../vendor/phpqrcode/phpqrcode.php";
    error_reporting(E_ERROR);
    $qr_code_path = UPLOAD_PATH.'qr_code/';
    if (!file_exists($qr_code_path)) {
        mkdir($qr_code_path);
    }
    /* 生成二维码 */
    $qr_code_file = $qr_code_path.time().rand(1, 10000).'.png';
    \QRcode::png($url, $qr_code_file, 2,10,2);
    $qrHandle = imagecreatefromstring(file_get_contents($qr_code_file));
    return $qr_code_file;
}

//订单搜索时间转换成where条件
function order_time_change($time)
{
    //与config/app下的order_time相对应
    $t = time();
    switch ($time) {
        case 1:
            //一个月内
            $month_one = strtotime(date('Y-m-d', strtotime("-1 month")).' 00:00:00');//一个月前
            return ['between',[$month_one,$t]];
            break;
        case 2:
            //三个月内
            $month_three = strtotime(date('Y-m-d', strtotime("-3 month")).' 00:00:00');//3个月前
            return ['between',[$month_three,$t]];
            break;
        case 3:
            //六个月内
            $month_six = strtotime(date('Y-m-d', strtotime("-6 month")).' 00:00:00');//6个月前
            return ['between',[$month_six,$t]];
            break;
        case 4:
            //一年内
            $year_one = strtotime(date('Y-m-d', strtotime("-1 year")).' 00:00:00');//一年前
            return ['between',[$year_one,$t]];
            break;
        case 5:
            //一年前
            $year_one = strtotime(date('Y-m-d', strtotime("-1 year")).' 00:00:00');//一年前
            return ['<',$year_one];
            break;
        case 0:
        default:
            //全部
            //不需要条件
            return true;
            break;
    }
}

/*关键词过滤*/
function dykeyfilter($str,$typeid=0)
{
	switch($typeid){
		case 1:
			$arr = array('自治区','自治州','自治县','直辖市','壮族','苗族','回族','维吾尔','地区');
		break;
		default:
			$arr = array('省','市','自治区','自治州','自治县','直辖市','辖区','壮族','苗族','回族','维吾尔','地区');
		break;
	}
	foreach($arr as $v){
        if(strstr($str,$v)) $str = str_replace($v,'',$str);
	}
	return $str;
}

/**
* 生成全球唯一标识
*/
function dyguidv4()
{
    if (function_exists('com_create_guid') === true){
        return trim(com_create_guid(), '{}');
    }
    $data = openssl_random_pseudo_bytes(16);
    $data[6] = chr(ord($data[6]) & 0x0f | 0x40); 
    $data[8] = chr(ord($data[8]) & 0x3f | 0x80); 
    return vsprintf('%s%s-%s-%s-%s-%s%s%s',str_split(bin2hex($data), 4));
}

function dy_zip($file)
{
    $zip = new ZipArchive;//新建一个ZipArchive的对象
    if ($zip->open($file) === TRUE)
    {
        $zip->extractTo('ServerData');
        $zip->close();//关闭处理的zip文件
        @unlink($file);
        $files = [];
        $updata_files = [];
        $to_path = "./public/ServerData";
        find_files('./ServerData', $files,$updata_files,$to_path);
        foreach ($updata_files as $v){
            @chmod($v['form'],0755);
            copy($v['form'],$v['to']);
        }
        deleteDir('./ServerData');
    }
}

/**
 * @param $dir string   要查找的文件路径
 * @param $dir_array  array   存储文件名的数组
 */
function find_files($dir, &$dir_array,&$updata_files,$to_path)
{
    // 读取当前目录下的所有文件和目录（不包含子目录下文件）
    $files = scandir($dir);
    if (is_array($files)) {
        foreach ($files as $val) {
            // 跳过. 和 ..
            if ($val == '.' || $val == '..')
                continue;

            // 判断是否是目录
            if (is_dir($dir . '/' . $val)) {
                // 将当前目录添加进数组
                $dir_array[$dir][] = $val;
                if($val!='ServerData'){
                    $pattern = "/ServerData";
                    $path = str_ireplace($pattern, '', trim($dir . '/' .$val,'.'));
                    !is_dir($to_path.$path) && mkdir($to_path.$path, 0777, true);
                }
                // 递归继续往下寻找
                find_files($dir . '/' . $val, $dir_array,$updata_files,$to_path);
            } else {
                // 不是目录也需要将当前文件添加进数组
                $pattern = "/ServerData";
                $to = str_ireplace($pattern, '', trim($dir . '/' .$val,'.'));
                $updata_files[] = [
                    'form'=>$dir . '/' .$val,
                    'to'=>$to_path.$to,
                ];
                $dir_array[$dir][] = $val;
            }
        }
    }
}

/**
 * 删除当前目录及其目录下的所有目录和文件
 * @param string $path 待删除的目录
 * @note  $path路径结尾不要有斜杠/(例如:正确[$path='./static/image'],错误[$path='./static/image/'])
 */
function deleteDir($path) {

    if (is_dir($path)) {
        //扫描一个目录内的所有目录和文件并返回数组
        $dirs = scandir($path);

        foreach ($dirs as $dir) {
            //排除目录中的当前目录(.)和上一级目录(..)
            if ($dir != '.' && $dir != '..') {
                //如果是目录则递归子目录，继续操作
                $sonDir = $path.'/'.$dir;
                if (is_dir($sonDir)) {
                    //递归删除
                    deleteDir($sonDir);

                    //目录内的子目录和文件删除后删除空目录
                    @rmdir($sonDir);
                } else {

                    //如果是文件直接删除
                    @unlink($sonDir);
                }
            }
        }
        @rmdir($path);
    }
}


