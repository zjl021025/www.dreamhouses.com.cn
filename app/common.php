<?php


use think\facade\Db;

/**
 * 检验登陆
 * @param
 * @return bool
 */
function is_login()
{
    if (isset($_SESSION['admin_id']) && $_SESSION['admin_id'] > 0) {
        return $_SESSION['admin_id'];
    } else {
        return false;
    }
}

/**
 * 获取缓存或者更新缓存
 * @param string $config_key 缓存文件名称
 * @param array $data 缓存数据  array('k1'=>'v1','k2'=>'v3')
 * @return array or string or bool
 */
function dyCache($config_key, $data = array())
{
    $param = explode('.', $config_key);
    if (empty($data)) {
        //如$config_key=shop_info则获取网站信息数组
        //如$config_key=shop_info.logo则获取网站logo字符串
        $config = Cache($param[0]);//直接获取缓存文件
        if (empty($config)) {
            //缓存文件不存在就读取数据库
            $res = Db::name('config')->where("inc_type", $param[0])->select();
            if ($res) {
                foreach ($res as $k => $val) {
                    $config[$val['name']] = $val['value'];
                }
                Cache($param[0], $config);
            }
        }
        if (count($param) > 1) {
            return $config[$param[1]];
        } else {
            return $config;
        }
    } else {
        //更新缓存
        $result = Db::name('config')->where("inc_type", $param[0])->select();
        if ($result) {
            foreach ($result as $val) {
                $temp[$val['name']] = $val['value'];
            }
            foreach ($data as $k => $v) {
                $newArr = array('name' => $k, 'value' => trim($v), 'inc_type' => $param[0]);
                if (!isset($temp[$k])) {
                    Db::name('config')->insert($newArr);//新key数据插入数据库
                    adminLog('新增' . $param[0] . '_' . $k . '=' . trim($v));
                } else {
                    if ($v != $temp[$k])
                        Db::name('config')->where("name", $k)->save($newArr);//缓存key存在且值有变更新此项
                    adminLog('更新' . $param[0] . '_' . $k . '=' . trim($v));
                }
            }
            //更新后的数据库记录
            $newRes = Db::name('config')->where("inc_type", $param[0])->select();
            foreach ($newRes as $rs) {
                $newData[$rs['name']] = $rs['value'];
            }
        } else {
            foreach ($data as $k => $v) {
                $newArr[] = array('name' => $k, 'value' => trim($v), 'inc_type' => $param[0]);
            }
            Db::name('config')->insertAll($newArr);
            adminLog('新增' . $param[0]);
            $newData = $data;
        }
        return Cache($param[0], $newData);
    }
}

/**
 * 清空系统缓存
 */
function clearCache()
{
    \think\facade\Cache::clear();
}

function validateForm($type, $data)
{
    $msg = '';
    //转小写
    switch (strtolower($type)) {
        case 'mobile':
            $result = preg_match('/^1[3-9][0-9]{9}$/', $data);
            if (!$result) {
                $msg = '手机号码格式不对';
            }
            break;
        case 'phone':
            $result = preg_match('/^((\(\d{3}\))|(\d{3}\-))?(\(0\d{2,3}\)|0\d{2,3}-)?[1-9]\d{6,7}$/', $data);
            if (!$result) {
                $msg = '电话格式不对';
            }
            break;
        case 'identity':
            $result = preg_match('/(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/', $data);
            if (!$result) {
                $msg = '身份证格式不对';
            }
            break;
        case 'email':
            $result = preg_match('/[a-z0-9A-Z_-]+@[a-z0-9A-Z_-]+(\.[a-z]{2,5}){1,2}/', $data);
            if (!$result) {
                $msg = '邮箱格式不对';
            }
            break;
        case 'url':
            $result = preg_match('/[a-zA-z]+:\/\/[^\s]*/', $data);
            if (!$result) {
                $msg = '网址格式不对';
            }
            break;
        case 'zip_code':
            $result = preg_match('/^[1-9]\d{5}$/', $data);
            if (!$result) {
                $msg = '邮编格式不对';
            }
            break;
        default:
            $msg = '未知类型';
    }


    return $msg;
}

/**
 *文件上传记录到数据库
 * @param array $data 上传的文件数据
 * @param int $img_id 修改的图片的ID
 * @return  true   成功  false  失败
 */
function update_img_data($data, $img_id)
{
    if ($img_id > 0) {//修改文件的信息（别名）
        $result = Db::name('file')->where('img_id', $img_id)->update($data);
    } else {
        $data['createtime'] = time();
        $data['status'] = 1;
        $result = Db::name('file')->insertGetId($data);
    }
    $number = Db::name('file')->where('category_id', $data['category_id'])->count();
    Db::name('album')->where('category_id', $data['category_id'])->update(['number' => $number]);
    return $img_id ? $img_id : $result;
}


/**
 *  缩略图 给于标签调用 thumb 原始图来裁切出来的
 * @param type $article_id
 * @param type $width 生成缩略图的宽度
 * @param type $height 生成缩略图的高度
 * @param type $maxwidth 生成缩略图的最大高度 0原图尺寸
 * @param type $iswater 是否需要生成水印，默认需要
 * @param type $cutting 是否需要裁切，默认不需要
 * 调用方法 {$info['article_id']|article_thum_images=240,240}
 */
function dythumbimages($dyid, $lastwidth = 300, $lastheight = 300, $maxwidth = 0, $imgpath = 'article', $iswater = 1, $cutting = 0)
{
    if (empty($dyid)) {
        return '';
    }
    $imgarr = gettables($imgpath);
    //判断缩略图是否存在
    $path = UPLOAD_PATH . $imgpath . "/thumb/$dyid/";
    $original_img = Db::name($imgarr['table'])->where("{$imgarr['id']}", $dyid)
        /*->cache(true, 30, $imgarr['thumb'])*/ ->value($imgarr['thumb']);
    $dythumb = '';
    if ($original_img) {
        $original_img_arr = explode('/', $original_img);
        $dythumb = $dythumb = explode('.', end($original_img_arr))[0];
    }
    $article_thumb_name = "{$imgpath}{$iswater}{$dyid}{$dythumb}_{$lastwidth}_{$lastheight}";
    // 这篇文章 已经生成过这个比例的图片就直接返回了
    if (is_file($path . $article_thumb_name . '.jpeg')) {
        return '/' . $path . $article_thumb_name . '.jpeg';
    }
    if (is_file($path . $article_thumb_name . '.jpg')) {
        return '/' . $path . $article_thumb_name . '.jpg';
    }
    if (is_file($path . $article_thumb_name . '.gif')) {
        return '/' . $path . $article_thumb_name . '.gif';
    }
    if (is_file($path . $article_thumb_name . '.png')) {
        return '/' . $path . $article_thumb_name . '.png';
    }
    if ($imgpath == 'brand') {
        if (empty($original_img)) {
            return '/public/images/brand.png';
        }
        $original_img = '.' . $original_img; // 相对路径
        if (!is_file($original_img)) {
            return '/public/images/brand.png';
        }
    } else {
        if (empty($original_img)) {
            return '/public/images/thumb_empty.jpg';
        }
        //2021-04-08 王弘扬添加
        if (strpos($original_img, 'https://') === false) {
            $original_img = '.' . $original_img; // 相对路径
        }
        //暂时报错，先注释掉
//        if (!is_file($original_img)) {
//            //return '/public/images/icon_'.$imgpath.'_thumb_empty_500.jpg';
//            return '/public/images/thumb_empty.jpg';
//        }
    }
    try {
        require_once '../vendor/topthink/think-image/src/Image.php';
        require_once '../vendor/topthink/think-image/src/image/Exception.php';
        if (strstr(strtolower($original_img), '.gif')) {
            require_once '../vendor/topthink/think-image/src/image/gif/Encoder.php';
            require_once '../vendor/topthink/think-image/src/image/gif/Decoder.php';
            require_once '../vendor/topthink/think-image/src/image/gif/Gif.php';
        }
        $info = @getimagesize($original_img);
        if (!$info) {
            return '/public/images/thumb_empty.jpg';
        }
        $image = \think\Image::open($original_img);
        //等比例缩小
        $ywidth = $image->width();//原图宽度
        $yheight = $image->height();//原图高度
        $isfill = $cutting == 1 ? 2 : 1; //是否填充，1-否，2-是
        if (intval($maxwidth) > 0) {
            //裁切
            if ($cutting == 1) {
                $lheight = intval($lastheight / $lastwidth * $ywidth);
                $height = $lheight > $lastheight ? $lastheight : $lheight;
                $width = $lastwidth;
            } else {
                if ($ywidth > $maxwidth) {
                    $width = $maxwidth;//压缩后图片的宽度
                    $height = intval($width * $yheight / $ywidth);//等比缩放图片高度 变整型
                } else {
                    $width = $ywidth;
                    $height = $yheight;
                }
            }
        } else {
            $width = $ywidth;
            $height = $yheight;
        }
        $article_thumb_name = $article_thumb_name . '.' . $image->type();
        //生成缩略图
        !is_dir($path) && mkdir($path, 0777, true);
        $image->thumb($width, $height, $isfill)->save($path . $article_thumb_name, NULL, 100); //按照原图的比例生成一个最大为$width*$height的缩略图并保存
        $img_url = '/' . $path . $article_thumb_name;
        $imgurl = '.' . $img_url;
        $image = \think\Image::open($imgurl);
        if ($iswater == 1) {
            $water = dyCache('water');  //水印配置
            $return_data['mark_type'] = $water['mark_type'];
            if ($water['is_mark'] == 1 && $image->width() > $water['mark_width'] && $image->height() > $water['mark_height']) {
                if ($water['mark_type'] == 'text') {
                    $ttf = './hgzb.ttf';
                    if (file_exists($ttf)) {
                        $size = $water['mark_txt_size'] ? $water['mark_txt_size'] : 30;
                        $color = $water['mark_txt_color'] ?: '#000000';
                        if (!preg_match('/^#[0-9a-fA-F]{6}$/', $color)) {
                            $color = '#000000';
                        }
                        $transparency = intval((100 - $water['mark_degree']) * (127 / 100));
                        $color .= dechex($transparency);
                        $image->open($imgurl)->text($water['mark_txt'], $ttf, $size, $color, $water['sel'])->save($imgurl);
                        $return_data['mark_txt'] = $water['mark_txt'];
                    }
                } else {
                    $waterPath = "." . $water['mark_img'];
                    $quality = $water['mark_quality'] ? $water['mark_quality'] : 80;
                    $waterTempPath = dirname($waterPath) . '/temp_' . basename($waterPath);
                    $image->open($waterPath)->save($waterTempPath, null, $quality);
                    $image->open($imgurl)->water($waterTempPath, $water['sel'], $water['mark_degree'])->save($imgurl);
                    @unlink($waterTempPath);
                }
            }
        }
        return $img_url;
    } catch (think\Exception $e) {
        return $original_img;
    }
}

/* 页面劫持数据入库 */
function hijackpage($data)
{
    $data['ip'] = request()->ip();
    $data['add_time'] = time();
    Db::name('hijack')->insert($data);
}

/*
*上传图片
@param $filenames  文件域
@param $position  存放目录
@param $is_multiple  是否支持多张：yes-多张，no-单张
@param $is_type  类型：1-图片，2-文档附件，3-视频文件
*/
function dymustupload($filenames, $position = 'default', $is_multiple = 'yes', $is_type = 1, $iswater = 0)
{
    if (IS_POST) {
        $position = $position . '/';
        $file = request()->file($filenames);
        if ($is_multiple != 'yes') $file = array($file);
        $imgurl = '';
        switch ($is_type) {
            case 1:
                //图片
                $ext = 'jpg,png,jpeg';
                $maxsize = config('user_image_upload_limit_size');//默认值为1MB
                $extmsg = 'jpg,png,jpeg格式的图片';
                break;
            case 2:
                //附件
                $ext = 'zip,rar,xls,xlsx,doc,docx,ppt,pptx';
                $maxsize = config('user_file_upload_limit_size');//默认值为5MB
                $extmsg = '附件';
                break;
            case 3:
                //视频
                $ext = 'mp4';
                $maxsize = config('user_video_upload_limit_size');//默认值为10MB(改为40MB)
                $extmsg = '视频';
                break;
        }
        if ($file) {
            foreach ($file as $key => $val) {
                if ($val) {
                    $originalName = strtolower($val->getOriginalName());
                    if (strstr($originalName, '.php') || strstr($originalName, '.js')) {
                        $state = '文件格式错误';
                    }
                    if ($val->getSize() > $maxsize) {
                        $state = '上传失败,文件超出大小,请选择' . floor($maxsize / 1024 / 1024) . 'MB以内的文件。';
                    }
                    $extension = strtolower($val->extension());
                    if (!in_array($extension, explode(',', $ext))) {
                        $state = '仅可上传' . $extmsg . '文件';
                    }
                    if ($state) {
                        return dyjsonReturn(0, $state);
                    }
//                    $savename = md5(mt_rand()).'.'.$val->extension();
                    $domain = config('qiniu.domain');
                    $qiniu_file = \think\facade\Filesystem::disk('qiniu')->putFile('image',$val);
                    $pic_path = 'http://'.$domain.'/'.$qiniu_file;
                    @chmod($pic_path, 0755);
                    if ($is_type == 1 && !getimagesize('./' . $pic_path)) {
                        $dydir = 'public/backup/redislog/cancelorders/' . date("Y") . '/';
                        if (!is_dir($dydir)) @mkdir($dydir, 0755, true);
                        $dydir = $dydir . date("m-d") . '/';
                        if (!is_dir($dydir)) @mkdir($dydir, 0755, true);
                        //写入文件做日志 调试用
                        $log = "\r\n\r\n" . '===================' . "\r\n" . date("Y-m-d H:i:s") . "\r\n" . json_encode(json_encode(input()));
                        $log .= "\r\n\r\n" . '===================' . "\r\n" . date("Y-m-d H:i:s") . "\r\n" . json_encode(json_encode(request()->file('dyfile')));
                        $log .= "\r\n\r\n" . '===================' . "\r\n" . date("Y-m-d H:i:s") . "\r\n" . rtrim($imgurl, ',');
                        @file_put_contents($dydir . date("Y-m-d") . '.log', $log, FILE_APPEND);
                        return dyjsonReturn(0, '上传失败');
                    }
                    if ($is_type == 1) {
                        $pic_path = set_water('./' . $pic_path, $iswater, $is_type);
                    } else {
                        if ($is_type == 3 && $iswater == 1) {
                            dyappsetcache('/' . $pic_path, 1, 10);
                        }
                        $pic_path = '/' . $pic_path;
                    }
                }
                $imgurl .= $pic_path . ',';
            }
        }
        $dydir = 'public/backup/redislog/cancelorders/' . date("Y") . '/';
        if (!is_dir($dydir)) @mkdir($dydir, 0755, true);
        $dydir = $dydir . date("m-d") . '/';
        if (!is_dir($dydir)) @mkdir($dydir, 0755, true);
        //写入文件做日志 调试用
        $log = "\r\n\r\n" . '===================' . "\r\n" . date("Y-m-d H:i:s") . "\r\n" . json_encode(json_encode(input()));
        $log .= "\r\n\r\n" . '===================' . "\r\n" . date("Y-m-d H:i:s") . "\r\n" . json_encode(json_encode(request()->file('dyfile')));
        $log .= "\r\n\r\n" . '===================' . "\r\n" . date("Y-m-d H:i:s") . "\r\n" . rtrim($imgurl, ',');
        @file_put_contents($dydir . date("Y-m-d") . '.log', $log, FILE_APPEND);
        return dyjsonReturn(1, '附件上传成功', rtrim($imgurl, ','));
//        return rtrim($imgurl, ',');
    }
}

function set_water($fileurl, $iswater, $is_type)
{
    $water = dyCache('water');  //水印配置
    if ($iswater == 1) {
        switch ($is_type) {
            case 1:
                $waterPath = "." . $water['mark_img'];
                $image = \think\Image::open($waterPath);
                $quality = $water['mark_quality'] ? $water['mark_quality'] : 80;
                $waterTempPath = dirname($waterPath) . '/temp_' . basename($waterPath);
                $image->open($waterPath)->save($waterTempPath, null, $quality);
                $image->open($fileurl)->water($waterTempPath, $water['sel'], $water['mark_degree'])->save($fileurl);
                $new_file = $fileurl;
                break;
            case 3:
                $waterPath = './public' . $water['mark_img'];
                $image = \think\Image::open($waterPath);
                $quality = $water['mark_quality'] ? $water['mark_quality'] : 80;
                $waterTempPath = dirname($waterPath) . '/temp_' . basename($waterPath);
                $image->open($waterPath)->save($waterTempPath, null, $quality);
                $ffprobe = \FFMpeg\FFProbe::create([
                    'ffmpeg.binaries' => app()->getRootPath() . '/public/public/plugins/ffmpeg-git-20220622-amd64-static/ffmpeg',    # 你自己安装的位置
                    'ffprobe.binaries' => app()->getRootPath() . '/public/public/plugins/ffmpeg-git-20220622-amd64-static/ffprobe'    # 你自己安装的位置
                ]);
                $video_info = $ffprobe->streams('./public' . $fileurl)->first();
                $width = $video_info->get('width');
                $height = $video_info->get('height');
                //视频
                $ffmpeg = \FFMpeg\FFMpeg::create([
                    'ffmpeg.binaries' => app()->getRootPath() . '/public/public/plugins/ffmpeg-git-20220622-amd64-static/ffmpeg',    # 你自己安装的位置
                    'ffprobe.binaries' => app()->getRootPath() . '/public/public/plugins/ffmpeg-git-20220622-amd64-static/ffprobe'    # 你自己安装的位置
                ]);
                $video = $ffmpeg->open('./public' . $fileurl);
                [$x, $y] = get_water_locate($waterTempPath, $water['sel'], $width, $height);
                $video->filters()->watermark($waterTempPath, array(
                    'position' => 'relative',# 视频定位
                    'top' => $y,
                    'left' => $x,
                ));
                $file_name = explode('.', ltrim('./public' . $fileurl, '.'));
                $new_file = '.' . $file_name[0] . '_tmp.' . $file_name[1];
                $video->save(new \FFMpeg\Format\Video\X264(), $new_file);    # 保存视频
                @unlink('./public' . $fileurl);
                rename($new_file, './public' . $fileurl);
                $new_file = $fileurl;
                @chmod('./public' . $new_file, 0755);
                break;
            default:
                $new_file = $fileurl;
        }
        @unlink($waterTempPath);
    } else {
        $new_file = $fileurl;
    }
    return ltrim($new_file, '.');
}

function get_water_locate($source, $locate, $width, $height)
{
    $info = getimagesize($source);
    if (false === $info || (IMAGETYPE_GIF === $info[2] && empty($info['bits']))) {
        return [0, 0];
    }
    /* 设定水印位置 */
    switch ($locate) {
        /* 右下角水印 */
        case 9:
            $x = $width - $info[0];
            $y = $height - $info[1];
            break;
        /* 左下角水印 */
        case 7:
            $x = 0;
            $y = $height - $info[1];
            break;
        /* 左上角水印 */
        case 1:
            $x = $y = 0;
            break;
        /* 右上角水印 */
        case 3:
            $x = $width - $info[0];
            $y = 0;
            break;
        /* 居中水印 */
        case 5:
            $x = ($width - $info[0]) / 2;
            $y = ($height - $info[1]) / 2;
            break;
        /* 下居中水印 */
        case 8:
            $x = ($width - $info[0]) / 2;
            $y = $height - $info[1];
            break;
        /* 右居中水印 */
        case 6:
            $x = $width - $info[0];
            $y = ($height - $info[1]) / 2;
            break;
        /* 上居中水印 */
        case 2:
            $x = ($width - $info[0]) / 2;
            $y = 0;
            break;
        /* 左居中水印 */
        case 4:
            $x = 0;
            $y = ($height - $info[1]) / 2;
            break;
    }
    return [$x, $y];
}

/*
*单张图片上传
@param $filenames  文件域 
@param $position  存放目录
*/
function dyuploadimg($filenames, $position = '')
{
    if (IS_POST) {
        $position = $position . '/';
        $file = array(request()->file($filenames));
        $imgurl = '';
        //图片
        $ext = 'jpg,png,gif,jpeg';
        $maxsize = config('image_upload_limit_size');//默认值为1MB
        $extmsg = '图片';
        foreach ($file as $key => $val) {
            if ($val) {
//                if ($val->getSize() > $maxsize) $state = '上传失败,' . $extmsg . '超出大小,请选择' . floor($maxsize / 1024 / 1024) . 'MB以内的文件。';
//                $originalName = strtolower($val->getOriginalName());
//                if (strstr($originalName, '.php') || strstr($originalName, '.js')) $state = $extmsg . '格式错误';
//                $extension = strtolower($val->extension());
//                if (!in_array($extension, explode(',', $ext))) $state = '仅可上传' . $extmsg . '文件';
//                if ($state) return dyajaxReturn(0, $state);
//                $savename = md5(mt_rand()) . '.' . $val->extension();
//                $filepath = $position . date('Y/m-d') . '/';
//                $file_path = UPLOAD_PATH . $filepath;
//                if (!($_exists = file_exists($file_path))) @mkdir($file_path, 0777, true);
//                \think\facade\Filesystem::disk('public')->putFileAs($filepath, $val, $savename);
//                $pic_path = UPLOAD_PATH . $filepath . $savename;
//                @chmod($pic_path, 0755);
//                $pic_path = '/' . $pic_path;
                $domain = config('qiniu.domain');
                $qiniu_file = \think\facade\Filesystem::disk('qiniu')->putFile('image',$val);
                $baseUrl = 'http://'.$domain.'/'.$qiniu_file;
            }
            $imgurl .= $baseUrl . ',';
        }
        $imgurl = rtrim($imgurl, ',');
        if ($imgurl) return dyajaxReturn(1, $extmsg . '上传成功', $imgurl);
        return dyajaxReturn(0, $extmsg . '上传失败');
    }
}

/**
 * 视频上传
 */
function dyuploadvideo($filenames, $position = '')
{
    if (IS_POST) {
        $position = $position . '/';
        $file = request()->file($filenames);
        $videourl = '';
        //图片
        $domain = config('qiniu.domain');
        $qiniu_file = \think\facade\Filesystem::disk('qiniu')->putFile('video',$file);
        $baseUrl = 'http://'.$domain.'/'.$qiniu_file;
//        $ext = 'mp4';
//        $maxsize = config('user_video_upload_limit_size');//默认值为10MB
        $extmsg = '视频';
//        if ($file->getSize() > $maxsize) $state = '上传失败,' . $extmsg . '超出大小,请选择' . floor($maxsize / 1024 / 1024) . 'MB以内的文件。';
//        $originalName = strtolower($file->getOriginalName());
//        if (strstr($originalName, '.php') || strstr($originalName, '.js')) $state = $extmsg . '格式错误';
//        $extension = strtolower($file->extension());
//        if (!in_array($extension, explode(',', $ext))) $state = '仅可上传' . $extmsg . '文件';
//        if ($state) return dyajaxReturn(0, $state);
//        $savename = md5(mt_rand()) . '.' . $file->extension();
//        $filepath = $position . date('Y/m-d') . '/';
//        $file_path = UPLOAD_PATH . $filepath;
//        if (!($_exists = file_exists($file_path))) @mkdir($file_path, 0777, true);
//        \think\facade\Filesystem::disk('public')->putFileAs($filepath, $file, $savename);
//        $pic_path = UPLOAD_PATH . $filepath . $savename;
//        @chmod($pic_path, 0755);
        $videourl = '/' . $baseUrl;
        if ($videourl) return dyajaxReturn(1, $extmsg . '上传成功', $videourl);
        return dyajaxReturn(0, $extmsg . '上传失败');
    }
}

/*
*上传zip
@param $filenames  文件域
@param $position  存放目录
*/
function dyuploadzip($filenames, $position = 'zip')
{
    if (IS_POST) {
        $position = $position . '/';
        $val = request()->file($filenames);
        $file = array();
        $zipurl = '';
        //图片
        $ext = 'zip';
        $maxsize = config('zip_upload_limit_size');//默认值为1MB
        $extmsg = 'ZIP';
        if ($val) {
            $domain = config('qiniu.domain');
            $qiniu_file = \think\facade\Filesystem::disk('qiniu')->putFile('model',$val);
            $baseUrl = 'http://'.$domain.'/'.$qiniu_file;
//            if ($val->getSize() > $maxsize) $state = '上传失败,' . $extmsg . '超出大小,请选择' . floor($maxsize / 1024 / 1024) . 'MB以内的文件。';
//            $originalName = strtolower($val->getOriginalName());
//            if (strstr($originalName, '.php') || strstr($originalName, '.js')) $state = $extmsg . '格式错误';
//            $extension = strtolower($val->extension());
//            if (!in_array($extension, explode(',', $ext))) $state = '仅可上传' . $extmsg . '文件';
//            if ($state) return dyajaxReturn(0, $state);
//            $savename = md5(mt_rand()) . '.' . $val->extension();
//            $filepath = $position . date('Y/m-d') . '/';
//            $file_path = UPLOAD_PATH . $filepath;
//            if (!($_exists = file_exists($file_path))) @mkdir($file_path, 0777, true);
//            \think\facade\Filesystem::disk('public')->putFileAs($filepath, $val, $savename);
//            $pic_path = UPLOAD_PATH . $filepath . $savename;
//            @chmod($pic_path, 0755);
//            $pic_path = '/' . $pic_path;
        }
        $zipurl .= $baseUrl . ',';
        $zipurl = rtrim($zipurl, ',');
        if ($zipurl) return $zipurl;
        return false;
    }
}

/**
 * 发送短信逻辑
 * @param int $scene
 */
function send_sms($scene, $sender, $params, $unique_id = 0)
{
    $smsLogic = new \app\common\logic\SmsLogic;
    return $smsLogic->sendSms($scene, $sender, $params, $unique_id);
}

/*
 * 获取计划任务文件名称
 * $typeid 1-视频水印添加
*/
function get_cronfile($typeid)
{
    switch ($typeid) {
        case 1:
            $str = 'videowater';//视频水印添加
            break;
    }
    return $str;
}

/*
设置
$order_sn  单号
$typeid    类型
$expire    超时时间 默认15分钟
*/
function dyappsetcache($order_sn = '', $typeid = 0, $expire = 900)
{
    if (!in_array($typeid, config('redisstatus')) || empty($order_sn)) return false;
    return \think\facade\Cache::setex(get_cronfile($typeid) . ":" . $order_sn, $expire, $order_sn, $typeid);
}

/*获取*/
function dyappgetcache($order_sn = '', $typeid = 0)
{
    if (!in_array($typeid, config('redisstatus')) || empty($order_sn)) return false;
    $name = get_cronfile($typeid) . ":" . $order_sn;
    return \think\facade\Cache::dyget($name);
}

/*获取*/
function dyappttlcache($order_sn = '', $typeid = 0)
{
    if (!in_array($typeid, config('redisstatus')) || empty($order_sn)) return false;
    $name = get_cronfile($typeid) . ":" . $order_sn;
    return \think\facade\Cache::dyttl($name);
}

/*删除*/
function dyapprmcache($order_sn = '', $typeid = 0)
{
    if (!in_array($typeid, config('redisstatus')) || empty($order_sn)) return false;
    $name = get_cronfile($typeid) . ":" . $order_sn;
    return \think\facade\Cache::dyrm($name);
}

/*配置*/
function dyautoruntask()
{
    ini_set('default_socket_timeout', -1);//不超时
    \think\facade\Cache::setOption();
    return \think\facade\Cache::psubscribe(config('dysetcron'), 'redisCallback');
}

function redisCallback($instance = '', $pattern = '', $channame = '', $msg = array())
{
    //在这里处理相应的业务逻辑
    if (empty($msg[1]) || !$msg) return false;
    $msg = explode(':', $msg);
    //手动启用事务
    Db::startTrans();
    try {
        switch ($msg[0]) {
            case 'videowater'://视频水印添加
                $pic_path = $msg[1];
                set_water($pic_path, 1, 3);
                $dydir = './public/public/backup/redislog/videowater/' . date("Y") . '/';
                if (!is_dir($dydir)) @mkdir($dydir, 0755, true);
                $dydir = $dydir . date("m-d") . '/';
                if (!is_dir($dydir)) @mkdir($dydir, 0755, true);
                //写入文件做日志 调试用
                $log = "\r\n\r\n" . '==========视频水印添加：' . $msg[1] . '=========' . "\r\n" . date("Y-m-d H:i:s") . "\r\n";
                @file_put_contents($dydir . date("Y-m-d") . '.log', $log, FILE_APPEND);
                break;
        }
        Db::commit();// 提交事务
    } catch (\Exception $e) {
        Db::rollback(); // 回滚事务
        $dydir = './public/public/backup/redislog/videowater/' . date("Y") . '/';
        if (!is_dir($dydir)) @mkdir($dydir, 0755, true);
        $dydir = $dydir . date("m-d") . '/';
        if (!is_dir($dydir)) @mkdir($dydir, 0755, true);
        //写入文件做日志 调试用
        $log = "\r\n\r\n" . '==========视频水印添加：' . $msg[1] . '=========' . "\r\n" . date("Y-m-d H:i:s") . "\r\n" . $e->getMessage();
        @file_put_contents($dydir . date("Y-m-d") . '.log', $log, FILE_APPEND);
    }
}

//下载远程微信头像图片
function downloadImage($url, $position = 'user')
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
    $file = curl_exec($ch);
    curl_close($ch);
    $filename = pathinfo($url, PATHINFO_BASENAME);
    $position = $position . '/';
    $filepath = $position . date('Y/m-d') . '/';
    $file_path = UPLOAD_PATH . $filepath;
    if (!($_exists = file_exists($file_path))) {
        @mkdir($file_path, 0777, true);
    }
    $pic_path = $file_path . md5($filename) . '.' . explode('/', getimagesize($url)['mime'])[1];
    $resource = fopen($pic_path, 'a');
    fwrite($resource, $file);
    fclose($resource);
    return '/' . $pic_path;
}

/*去除内容中的表情符号*/
function filterEmoji($str)
{
    $str = preg_replace_callback('/./u',
        function (array $match) {
            return strlen($match[0]) >= 4 ? '' : $match[0];
        },
        $str);
    return $str;
}

function getTimeString($time)
{
    $now_time = time();
    $time = intval($time);
    $unit = array("年", "月", "天", "小时", "分钟");
    switch (true) {
        case $time < ($now_time - 31536000) :
            return floor(($now_time - $time) / 31536000) . $unit[0] . '前';
        case $time < ($now_time - 2592000) :
            return floor(($now_time - $time) / 2592000) . $unit[1] . '前';
        case $time < ($now_time - 86400) :
            return floor(($now_time - $time) / 86400) . $unit[2] . '前';
        case $time < ($now_time - 3600) :
            return floor(($now_time - $time) / 3600) . $unit[3] . '前';
        case $time < ($now_time - 60) :
            return floor(($now_time - $time) / 60) . $unit[4] . '前';
        default :
            return ceil(($now_time - $time) / 60) . $unit[4] . '前';
    }
}

function get_html_decode($content)
{
    return $content ? str_replace('/public/upload/', SITE_URL . '/public/upload/', htmlspecialchars_decode($content)) : '';
}

/**
 * 获取完整地址
 */
function get_region_mername($region_id, $splice = ' ')
{
    if (!$region_id) {
        return '';
    }
    $mername = Db::name('region')->where('id', $region_id)->value('mername');
    if (!$mername) {
        return '';
    }
    $mername = explode(',', $mername);
    unset($mername[0]);
    $mername = implode($splice, $mername);
    return $mername;
}

/**
 * 商品相册缩略图
 */
function get_sub_images($img_id, $image_url, $goods_id, $width, $height, $imgpath)
{
    //判断缩略图是否存在
    $path = UPLOAD_PATH . "{$imgpath}/thumb/$goods_id/";
    $goods_thumb_name = "{$imgpath}_sub_thumb_{$img_id}_{$width}_{$height}";

    //这个缩略图 已经生成过这个比例的图片就直接返回了
    if (is_file($path . $goods_thumb_name . '.jpg')) return '/' . $path . $goods_thumb_name . '.jpg';
    if (is_file($path . $goods_thumb_name . '.jpeg')) return '/' . $path . $goods_thumb_name . '.jpeg';
    if (is_file($path . $goods_thumb_name . '.gif')) return '/' . $path . $goods_thumb_name . '.gif';
    if (is_file($path . $goods_thumb_name . '.png')) return '/' . $path . $goods_thumb_name . '.png';

    $original_img = '.' . $image_url; //相对路径
    if (!is_file($original_img)) {
        return '/public/images/thumb_empty.jpg';
    }

    try {
        require_once '../vendor/topthink/think-image/src/Image.php';
        require_once '../vendor/topthink/think-image/src/image/Exception.php';
        if (strstr(strtolower($original_img), '.gif')) {
            //require_once '../vendor/topthink/think-image/src/image/gif/Encoder.php';
            //require_once '../vendor/topthink/think-image/src/image/gif/Decoder.php';
            //require_once '../vendor/topthink/think-image/src/image/gif/Gif.php';
        }
        $image = \think\Image::open($original_img);

        $goods_thumb_name = $goods_thumb_name . '.' . $image->type();
        // 生成缩略图
        !is_dir($path) && mkdir($path, 0777, true);
        $image->thumb($width, $height, 3)->save($path . $goods_thumb_name, NULL, 100); //按照原图的比例生成一个最大为$width*$height的缩略图并保存
        $img_url = '/' . $path . $goods_thumb_name;
        return $img_url;
    } catch (think\Exception $e) {
        return $original_img;
    }
}

function get_start_endtime_by_range_time($time)
{
    if (!$time) {
        return ['', ''];
    }
    $time = explode('-', $time);
    if (count($time) != 6) {
        return ['', ''];
    }
    $start_time = strtotime(trim($time[0]) . '-' . trim($time[1]) . '-' . trim($time[2]) . ' 00:00:00');
    $end_time = strtotime(trim($time[3]) . '-' . trim($time[4]) . '-' . trim($time[5]) . ' 23:59:59');
    return [$start_time, $end_time];
}

/**
 * 检查短信
 * @param $code
 * @param $sender
 * @param string $type
 * @param int $session_id
 * @param int $scene 1:用户注册,2:登录,3:修改手机号码
 * @return array
 */
function check_validate_code($code, $sender, $scene = 1)
{
    $timeOut = time();
    $inValid = true;  //验证码失效
    //短信发送否开启
    if (!$code) return ['errcode' => 0, 'message' => '请输入短信验证码'];
    //短信
//    $sms_time_out = dyCache('sms.sms_time_out');
//    $sms_time_out = $sms_time_out ? $sms_time_out : 180;
    $sms_time_out = 5 * 60;
    $data = Db::name('sms_log')->where(['mobile' => $sender,'status' => 1])->order('id DESC')->find();
    if (is_array($data) && $data['code'] == $code || $code == '123456') {
        $data['sender'] = $sender;
//        $timeOut = $data['add_time'] + $sms_time_out;
        $timeOut = time()+time();
    } else {
        $inValid = false;
    }
    if (empty($data)) {
        //$res = ['errcode'=>0,'message'=>'请先获取验证码'];
        $res = ['errcode' => 0, 'message' => '验证码有误'];
    } elseif ($timeOut < time()) {
        $res = ['errcode' => 0, 'message' => '验证码已超时失效'];
    } elseif (!$inValid) {
        $res = ['errcode' => 0, 'message' => '验证失败,验证码有误'];
    } else {
        $data['is_check'] = 1; //标示验证通过
        $res = ['errcode' => 1, 'message' => '验证成功'];
    }
    return $res;
}

function hide_mobile($mobile)
{
    if (!$mobile) {
        return '';
    }
    return substr_replace($mobile, '****', 3, 4);
}

/**
 * 读取静态页面缓存
 */
function read_html_cache()
{
    $html_cache_arr = config('HTML_CACHE_ARR');
    $m_c_a_str = MODULE_NAME . '_' . CONTROLLER_NAME . '_' . ACTION_NAME;//模块_控制器_方法
    $m_c_a_str = strtolower($m_c_a_str);
    foreach ($html_cache_arr as $key => $val) {
        $val['mca'] = strtolower($val['mca']);
        if ($val['mca'] != $m_c_a_str) //不是当前 模块 控制器 方法 直接跳过
            continue;

        $filename = $m_c_a_str;
        //组合参数
        if (isset($val['p'])) {
            foreach ($val['p'] as $k => $v) {
                $filename .= '_' . $_GET[$v];
            }
        }
        $filename .= '.html';
        $html = \think\facade\Cache::get($filename);
    }
}

function get_date($time, $format = 'Y-m-d')
{
    if (!$time) {
        return '';
    }
    return date($format, $time);
}

//获取楼盘名称
function get_estate_name($estate_id)
{
    if (!$estate_id) {
        return '';
    }
    return Db::name('estate')->where('estate_id', $estate_id)->value('name');
}

//获取用户账号
function get_user_mobile($user_id)
{
    if (!$user_id) {
        return '';
    }
    return Db::name('users')->where('user_id', $user_id)->value('mobile');
}

//获取 1=设计经验  2=设计风格  3=设计费用   4=户型 名称
function get_label_name($label_id)
{
    if (!$label_id) {
        return '';
    }
    return Db::name('label')->where('id', $label_id)->value('name');
}

//获取楼盘户型面积
function get_estate_area($estate_id, $house_type_id)
{
    if (!$estate_id) {
        return 0;
    }
    $area = Db::name('estate')->where('estate_id', $estate_id)->value('area');
    if (!$area) {
        return 0;
    }
    $area = json_decode($area, true);
    if (!$area[$house_type_id]) {
        return 0;
    }
    return $area[$house_type_id];
}

//获取用户昵称
function get_user_nickname($user_id)
{
    if (!$user_id) {
        return '';
    }
    return Db::name('users')->where('user_id', $user_id)->value('nickname');
}

//获取搜索范围
function get_label_min_max($label_id)
{
    if (!$label_id) {
        return '';
    }
    return Db::name('label')->where('id', $label_id)->field('min,max')->find();
}

function get_user(&$data)
{
    if (!$data['user_id']) {
        return false;
    }
    $user = Db::name('users')->where('user_id', $data['user_id'])->field('nickname,company_name,designer_type,is_platform')->find();
    if ($user) {
        $data['is_platform'] = $user['is_platform'];
        $data['nickname'] = $user['nickname'];
        $data['company_name'] = $user['company_name'];
        if ($user['designer_type'] == 1) {
            $data['company_name'] = "独立设计师";
        }
        if ($user['is_platform'] == 1) {
            $data['company_name'] = "平台设计师";
        }
    }
    return false;
}

function get_img_max_height($images)
{
    $max_height = 0;
    $_height = 0;
    $min_width = 0;
    $max_width = 0;
    $imgs = [];
    foreach ($images as $v) {
        $img = getimagesize('.' . $v);
        $ratio = 1080 / $img[0];
        $height = $img[1] * $ratio;
        $imgs[] = [
            'width' => $img[0],
            'height' => $img[1],
        ];
        if ($height > $_height) {
            $_height = $height;
            $max_height = $img[1];
//            $max_width = $img[0];
        }
    }
    foreach ($imgs as $v) {
        if ($v['height'] == $max_height) {
            if ($min_width == 0 || $min_width > $v['width']) {
                $min_width = $max_width = $v['width'];
            }
        }
    }
    return [$max_height, $max_width, $min_width];
}

