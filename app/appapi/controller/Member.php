<?php

namespace app\appapi\controller;

use app\common\logic\HouseLogic;
use app\common\validate\Authen;
use app\common\validate\House;
use app\common\validate\User;
use think\facade\Cache;
use think\facade\Db;
use app\common\logic\UsersLogic;

class Member extends Base
{
    /**
     * 会员注册
     *
     * @return void
     */
    public function register()
    {
        $typeid = input('typeid/d',1);//1=手机号密码注册  2=QQ授权注册  3=微信授权注册
        $_mobile = input('mobile/s');
        if(empty($_mobile)) return dyajaxReturn(0,'请输入手机号码');
        $mobile = dydecrypt($_mobile);
        if(empty($mobile)) return dyajaxReturn(0,'手机号码异常');
        if(validateForm('mobile',$mobile)!==''){
            return dyajaxReturn(0,'手机号码不正确');
        }
        $pwd = input('password/s');//密码
        $cpwd = input('cpassword/s');//确认密码
        $code = input('smscode/s');//手机验证码
        $nickname = input('nickname/s');//昵称
        $avatar = input('avatar/s');//头像
        $openid = input('openid/s');//第三方唯一标识
        $user = new UsersLogic();
        if(!$pwd){
            return dyajaxReturn(0, '请输入密码');
        }
        if(!$cpwd){
            return dyajaxReturn(0, '请输入确认密码');
        }
        if($cpwd!=$pwd){
            return dyajaxReturn(0, '两次密码不一致');
        }
        $res = $user->reg($mobile, $pwd, $code,$nickname,$avatar,$openid,$typeid);
        if ($res['errcode'] == 1) {
            $data['token'] = $res['token'];
            $data['user_id'] = $res['user_id'];
            return dyajaxReturn($res['errcode'], $res['message'], $data);
        } else {
            return dyajaxReturn($res['errcode'], $res['message']);
        }
    }

    /**
     * 用户名登陆
     *
     * @return void
     */
    public function login()
    {
        $typeid = input('typeid/s');//1=手机号密码，2=手机号验证码 3=找回密码
        $_mobile = input('mobile/s');
        if(empty($_mobile)) return dyajaxReturn(0,'请输入手机号码');
        $mobile = dydecrypt($_mobile);
        if(empty($mobile)) return dyajaxReturn(0,'手机号码异常');
        $smscode = input('smscode/s');
        $pwd = input('password/s');//密码
        $cpwd = input('cpassword/s');//确认密码
        $user = new UsersLogic();
        switch ($typeid) {
            case 1://手机号，密码
                $num = Cache::get('password_error_num'.$mobile);
                if ($num >= 5) {
                    return dyajaxReturn(106, '密码已错误五次，建议更换验证码登录');
                }
                $res = $user->login($mobile, $pwd);
                break;
            case 2://短信验证码登陆
                $res = $user->smslogin($mobile, $smscode);
                break;
            case 3:
                $res = $user->forgetpwd($mobile,$pwd,$cpwd,$smscode);
                break;
            default:
                return dyajaxReturn(0, '登录方式错误');
        }
        if ($res['errcode'] == 1) {
            //查询该用户是否是第一次登录
            $logNum = Db::name('sms_log')->where(['mobile'=>$mobile,'status'=>1])->count();
            if($logNum == 1 || $logNum == 0){
                $is_first = 1;
            }else{
                $is_first = 0;
            }
            $data['token'] = $res['token'];
            $data['user_id'] = $res['user_id'];
            $data['is_first'] = $is_first;
            return dyajaxReturn(1, $res['message'], $data);
        } else {
            return dyajaxReturn($res['errcode'], $res['message']);
        }
    }

    /* 第三方账号登陆（微信、QQ） */
    public function thirdlogin()
    {
		$typeid = input('typeid/d');//2=qq  3=微信
		$source = $typeid==2?'qq':'weixin';
		$openid = input('openid/s');
		$unionid = input('unionid/s');
		$oautharr = [
			'openid' => $openid,
			'unionid' => $unionid,
			'oauth' => $source,//来源
			'oauth_child'=>'open',
			];
		//$where['o.unionid'] = $unionid;
		$where['o.openid'] = $openid;
        $token = Db::name('oauth_users')->alias('o')
                ->join('users u', 'o.user_id=u.user_id', 'left')
                ->where($where)
                ->field('u.token,u.status,u.is_deleted,u.user_id')
                ->find();
		if ($token) {
            /* 直接登陆 */
            if ($token['status'] !== 1) {
                return dyajaxReturn(102, '账号被冻结,无法登录');
            }
            if ($token['is_deleted'] == 1) {
                return dyajaxReturn(0, '账号正在进行注销操作中,无法登录');
            }
            $token1 = dysettoken();//设置登录TOKEN
            Db::name('users')->where(['user_id'=>$token['user_id']])->update(['token'=>$token1]);
			return dyajaxReturn(1,'登录成功', ['token'=>$token1,'user_id'=>$token['user_id']]);
		} else {
            unset($oautharr['oauth_child']);
			return dyajaxReturn(107, '请先绑定账号', $oautharr);
		}
    }

    //获取关注的人列表
    public function getFollowList()
    {
        $user_id = input('user_id/s',0);//会员id
        if($user_id){
            $user_id = dydecrypt($user_id);
            if(!$user_id){
                return dyajaxReturn(0,'缺少用户Id');
            }
        }else{
            if(!$this->user_id){
                return dyajaxReturn(-100,'请先登录');
            }
            $user_id = $this->user_id;
        }
        $page = input('p/d',1);
        if($page<=0){
            $page = 1;
        }
        $listRows = 10;
        $follow_list = Db::name('user_follow')->alias('uf')
            ->leftJoin('users u','u.user_id=uf.aim_id')
            ->where(['uf.user_id'=>$user_id])
            ->field('u.user_id,u.nickname,u.company_name,u.designer_type')
            ->limit(($page-1)*$listRows,$listRows)
            ->select()
            ->toArray();
        foreach ($follow_list as &$value){
            $value['photo'] = SITE_URL.dythumbimages($value['user_id'],120,120,120,'users');//用户头像
//            $value['designer_type']==1 && $value['company_name']='独立设计师';
            $value['is_follow'] = 0;
            $count = Db::name('user_follow')->where(['user_id'=>$this->user_id,'aim_id'=>$value['user_id']])->count();
            if($count){
                $value['is_follow'] = 1;
            }
            $value['is_own'] = 0;//是否是自己
            if($this->user_id==$value['user_id']){
                $value['is_own'] = 1;
            }
            $value['user_id'] = dyencrypt($value['user_id']);
            !$value['nickname'] && $value['nickname']='';
        }
        return dyajaxReturn(1,'获取成功',$follow_list);
    }

    //获取我的粉丝列表
    public function getFansList()
    {
        $user_id = input('user_id/s',0);//会员id
        if($user_id){
            $user_id = dydecrypt($user_id);
            if(!$user_id){
                return dyajaxReturn(0,'缺少用户Id');
            }
        }else{
            if(!$this->user_id){
                return dyajaxReturn(-100,'请先登录');
            }
            $user_id = $this->user_id;
        }
        $page = input('p/d',1);
        if($page<=0){
            $page = 1;
        }
        $listRows = 10;
        $follow_list = Db::name('user_follow')->alias('uf')
            ->leftJoin('users u','u.user_id=uf.user_id')
            ->where(['uf.aim_id'=>$user_id])
            ->field('u.user_id,u.nickname,u.company_name,u.designer_type')
            ->limit(($page-1)*$listRows,$listRows)
            ->select()
            ->toArray();
        foreach ($follow_list as &$value){
            $value['photo'] = SITE_URL.dythumbimages($value['user_id'],120,120,120,'users');//用户头像
//            $value['designer_type']==1 && $value['company_name']='独立设计师';
            $value['is_follow'] = 0;
            $count = Db::name('user_follow')->where(['user_id'=>$this->user_id,'aim_id'=>$value['user_id']])->count();
            if($count){
                $value['is_follow'] = 1;
            }
            $value['is_own'] = 0;//是否是自己
            if($this->user_id==$value['user_id']){
                $value['is_own'] = 1;
            }
            $value['user_id'] = dyencrypt($value['user_id']);
            !$value['nickname'] && $value['nickname']='';
        }
        return dyajaxReturn(1,'获取成功',$follow_list);
    }

    public function userInfo()
    {
        $user = new UsersLogic();
        $userinfo = $user->getInfo($this->user_id,$this->user_id);
        return dyajaxReturn(1, '获取我的', $userinfo);
    }

    //认证
    public function authen()
    {
        $user = $this->user;
        if($user['is_authen']==3){
            return dyajaxReturn(0, '认证审核中');
        }
        $validate = validate(Authen::class);
        $param = input('post.');
        $act = $param['typeid']==1?'typeid1':'typeid2';
        if (!$validate->scene($act)->batch(false)->check($param)) {
            return dyajaxReturn(0, $validate->getError());
        }
        if($user['is_authen']==1){
            Db::name('users')->where('user_id',$user['user_id'])->update(['designer_type'=>0,'company_name'=>'']);
//            return dyajaxReturn(0, '认证已通过');
        }
        $data = [
            'typeid'=>$param['typeid'],
            'add_time'=>time(),
            'user_id'=>$user['user_id'],
            'realname'=>$param['realname'],
            'cre_type'=>$param['cre_type'],
            'cre_no'=>$param['cre_no'],
            'mobile'=>$param['mobile'],
            'company_name'=>$param['company_name']?$param['company_name']:'',
            'credit_code'=>$param['credit_code']?$param['credit_code']:'',
            'cre_z'=>$param['cre_z'],
            'cre_f'=>$param['cre_f'],
            'business'=>$param['business']?$param['business']:'',
        ];
        $id = Db::name('user_authen')->where('user_id',$user['user_id'])->value('id');
        if($id){
            $data['status'] = 0;
            Db::name('user_authen')->where('id',$id)->update($data);
        }else{
            Db::name('user_authen')->insert($data);
        }
        Db::name('users')->where('user_id',$user['user_id'])->update(['is_authen'=>3]);
        return dyajaxReturn(1, '提交成功');
    }

    //认证信息
    public function getAuthen()
    {
        $user = $this->user;
        $authen = Db::name('user_authen')->where('user_id',$user['user_id'])->field('typeid,realname,cre_type,cre_no,mobile,company_name,credit_code,cre_z,cre_f,business,status')->find();
        return dyajaxReturn(1, '认证信息',$authen);
    }

    //设置用户资料
    public function setUser()
    {
        $typeid = input('typeid/d',1);//1=头像 2=昵称 3=设计经验 4=设计风格 5=设计费用  6=水印  7=设置推荐
        if(!in_array($typeid,[1,2,3,4,5,6,7])){
            return dyajaxReturn(0, '类型错误');
        }
        $validate = validate(User::class);
        $param = input('post.');
        $act = 'typeid'.$param['typeid'];
        if (!$validate->scene($act)->batch(false)->check($param)) {
            return dyajaxReturn(0, $validate->getError());
        }
        $data['update_time'] = time();
        if($param['avatar']){
            $data['avatar'] = $param['avatar'];
        }
        if($param['nickname']){
            $data['nickname'] = $param['nickname'];
        }
        if($param['expe_id']){
            $data['expe_id'] = $param['expe_id'];
        }
        if($param['style_ids']){
            $data['style_ids'] = $param['style_ids'];
        }
        if($param['price_id']){
            $data['price_id'] = $param['price_id'];
        }
        if($typeid==6){
            $data['is_water'] = $param['is_water'];
        }
        if($typeid==7){
            $data['is_recd'] = $param['is_recd'];
        }
        Db::name('users')->where('user_id',$this->user_id)->update($data);
        return dyajaxReturn(1, '更新成功');
    }

    //设置用户资料
    public function getSetUser()
    {
        $user =  Db::name('users')->where(['user_id'=>$this->user_id])->find();
        $label_list = Db::name('label')->where(['typeid'=>['in',[1,2,3]]])->order('typeid desc,sort asc,id desc')->field('id,name,typeid')->column('name','id');
        $style_name = "";
        $style_array = [];
        if($user['style_ids']){
            $style_ids = explode(',',$user['style_ids']);
            foreach ($style_ids as $v){
                if($v){
                    $style_array[] = $label_list[$v];
                }
            }
            if($style_array){
                $style_name = implode(',',$style_array);
            }
        }
        $data = [
            'photo'=>SITE_URL.dythumbimages($user['user_id'],120,120,120,'users'),
            'nickname'=>$user['nickname'],
            'mobile'=>$user['mobile'],
            'expe_id'=>$user['expe_id'],
            'expe_name'=>$user['expe_id']?$label_list[$user['expe_id']]:'',
            'style_ids'=>$user['style_ids'],
            'style_name'=>$user['style_ids']?$style_name:'',
            'price_id'=>$user['price_id'],
            'price_name'=>$user['price_id']?$label_list[$user['price_id']]:'',
        ];
        return dyajaxReturn(1, '获取用户信息',$data);
    }

    //房屋列表
    public function getHouseList()
    {
        $HouseLogic = new HouseLogic();
        $where['user_id'] = $this->user_id;
        $list = $HouseLogic->getList($where);
        return dyajaxReturn(1, '房屋列表',$list);
    }

    //房屋详情
    public function getHouse()
    {
        $HouseLogic = new HouseLogic();
        $house_id = input('house_id/d');
        if(!$house_id){
            return dyajaxReturn(0, '缺少信息ID');
        }
        $list = $HouseLogic->getHouse($house_id,$this->user_id);
        return dyajaxReturn(1, '房屋列表',$list);
    }

    //添加编辑房屋
    public function house()
    {
        $validate = validate(House::class);
        $param = input();
        if (!$validate->batch(true)->check($param)) {
            // 验证失败 输出错误信息
            $error = $validate->getError();
            $error_msg = array_values($error);
            return dyajaxReturn(0, $error_msg[0]);
        }
        $city = Db::name('region')->where(['level'=>2,'name'=>$param['city']])->field('id,pid')->find();
        $city_id = $city['id'];
        $province_id = $city['pid'];
        $district_id = Db::name('region')->where(['level'=>3,'name'=>$param['district'],'pid'=>$city_id])->value('id');
        $data = [
            'user_id'=>$this->user_id,
            'name'=>$param['name'],
            'province_id'=>$province_id,
            'city_id'=>$city_id,
            'district_id'=>$district_id,
            'house_type_id'=>$param['house_type_id'],
            'address'=>$param['address'],
            'area'=>$param['area'],
            'estate_id'=>$param['estate_id']?$param['estate_id']:0,
            'lng'=>$param['lng']?$param['lng']:0,
            'lat'=>$param['lat']?$param['lat']:0,
        ];
        if($param['house_id']){
            //编辑
            $res = Db::name('house')->where(['house_id'=>$param['house_id'],'user_id'=>$this->user_id])->update($data);
        }else{
            $count = Db::name('house')
                ->where([
                    'user_id'=>$this->user_id,
                    'district_id'=>$param['district_id'],
                    'name'=>$param['name'],
                    'house_type_id'=>$param['house_type_id'],
                    'is_deleted'=>0,
                ])->value('house_id');
            if($count){
                return dyajaxReturn(0, '该房屋已添加');
            }
            //添加
            $data['add_time'] = time();
            $res = Db::name('house')->insertGetId($data);
        }
        if(!$res && $param['house_id']){
            return dyajaxReturn(0, '未修改信息');
        }
        if(!$res){
            return dyajaxReturn(0, '提交失败');
        }
        return dyajaxReturn(1, '提交成功');
    }

    //删除房屋
    public function delHouse()
    {
        $house_id = input('house_id/d');
        if(!$house_id){
            return dyajaxReturn(0, '缺少信息ID');
        }
        $res = Db::name('house')->where(['house_id'=>$house_id,'user_id'=>$this->user_id])->update(['is_deleted'=>1]);
        if(!$res){
            return dyajaxReturn(0, '删除失败');
        }
        return dyajaxReturn(1, '删除成功');
    }

    //获取房屋户型
    public function getHouseType()
    {
        $city = input('city/s');
        $district = input('district/s');
        $name = input('name/s',false,'trim');
        if(!$city || !$district || !$name){
            return dyajaxReturn(0,'请获取楼盘定位');
        }

        $city_id = Db::name('region')->where(['level'=>2,'name'=>$city])->value('id');
        $district_id = Db::name('region')->where(['level'=>3,'name'=>$district,'pid'=>$city_id])->value('id');
        $estate = Db::name('estate')->where(['district_id'=>$district_id,'name'=>$name])->field('estate_id,house_type_ids,area')->find();
        if($estate){
            $where['id'] = ['in',$estate['house_type_ids']];
            $estate['area'] = json_decode($estate['area'],true);
        }
        $where['is_show'] = 1;
        $where['typeid'] = 4;
        $house_type = Db::name('label')->where($where)->order('sort asc,id desc')->field('id,name')->select()->toArray();
        foreach ($house_type as &$value){
            $value['estate_id'] = 0;
            $value['area'] = 0;
            if($estate){
                $value['estate_id'] = $estate['estate_id'];
                $value['area'] = $estate['area'][$value['id']];
            }
        }
        return dyajaxReturn(1,'获取选项成功',$house_type);
    }

    /**
     * 绑定手机号
     *
     * @return void
     */
    public function bindMobile()
    {
        $_mobile = input('mobile/s');
        if(empty($_mobile)) return dyajaxReturn(0,'请输入手机号码');
        $mobile = dydecrypt($_mobile);
        if(empty($mobile)) return dyajaxReturn(0,'手机号码异常');
        if(validateForm('mobile',$mobile)!==''){
            return dyajaxReturn(0,'手机号码不正确');
        }
        $code = input('smscode/s');//手机验证码
        $user = new UsersLogic();
        $res = $user->bindMobile($mobile, $code,$this->user_id);
        if ($res['errcode'] == 1) {
            return dyajaxReturn($res['errcode'], $res['message']);
        } else {
            return dyajaxReturn($res['errcode'], $res['message']);
        }
    }

    //注销账号
    public function cancelAccount()
    {
        $code = input('smscode/s');
        //验证验证码
        $mobile = $this->user['mobile'];
        $check = check_validate_code($code, $mobile,6);
        if($check['errcode']!=1){
            return dyajaxReturn($check['errcode'], $check['message']);
        }
        $res = Db::name('users')->where('user_id',$this->user_id)->update(['is_deleted'=>1]);
        if(!$res){
            return dyajaxReturn(0, "注销失败");
        }
        Db::name('oauth_users')->where('user_id',$this->user_id)->delete();
        return dyajaxReturn(1, "注销成功");
    }

    public function about()
    {
        $data = dyCache('about');
        return dyajaxReturn(1, "关于我们",$data);
    }

    public function unbind()
    {
        $typeid = input('typeid/d',2);//2=QQ授权解绑  3=微信授权解绑
        $oauth = $typeid==2?'qq':'weixin';
        Db::name('oauth_users')->where('user_id',$this->user_id)->where('oauth', $oauth)->delete();
        return dyajaxReturn(1, "解绑成功");
    }

    public function directBind()
    {
        $typeid = input('typeid/d',2);//2=QQ授权绑定  3=微信授权绑定
        $openid = input('openid/s');//第三方唯一标识
        if(!$openid){
            return dyajaxReturn(0, "缺少唯一标识");
        }
        $oauth = $typeid==2?'qq':'weixin';
        $oauth_user = Db::name('oauth_users')->where('oauth',$oauth)->where('openid','=', $openid)->find();
        if(!$oauth_user){
            $oautharr['user_id'] = $this->user_id;
            $oautharr['openid'] = $openid;
            $oautharr['oauth'] = $typeid==2?'qq':'weixin';
            $oautharr['oauth_child'] = 'open';
            $oautharr['nick_name'] = '';
            $r = Db::name('oauth_users')->insertGetId($oautharr);
            if(!$r){
                return dyajaxReturn(0, "绑定失败");
            }
        }else{
            $r2 = Db::name('oauth_users')->where('oauth',$oauth)->where('openid','=', $openid)->save(['user_id'=>$this->user_id]);
            if(!$r2){
                return dyajaxReturn(0, "绑定失败");
            }
        }
        return dyajaxReturn(1, "绑定成功");
    }

    /**
     * 验证是否可以换绑手机号
     * @return array|false|string|void
     */
    public function updateMobile(){
        $smscode = input('smscode/s');
        $mobile = input('mobile/s');
        if(empty($smscode)||empty($mobile)){
            return dyjsonReturn(0,'数据不可为空');
        }
        $check = check_validate_code($smscode, $mobile,2);
        if($check['errcode']!=1){
            return dyjsonReturn(0,'验证码错误');
        }
        return dyjsonReturn(1,'验证成功');
    }
}
