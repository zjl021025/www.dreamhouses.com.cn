<?php

namespace app\appapi\controller;

use app\common\logic\UsersLogic;
use think\facade\Db;

class Message extends Base
{
    //消息通知
    public function index()
    {
        //判断系统消息
        $UsersLogic = new UsersLogic();
        $message = $UsersLogic->getMessageLast($this->user);
        //判断私信
        $user_message = $UsersLogic->getUserMessageGroup($this->user);
        $list = [];
        if(input('p/d')>1){
            $list = $user_message;
        }else{
            if($message && $user_message){
                $message['is_red'] = $this->user['sys_new_msg'];
                $list = array_merge([$message],$user_message);
            }elseif($message){
                $message['is_red'] = $this->user['sys_new_msg'];
                $list = [$message];
            }elseif($user_message){
                $list = $user_message;
            }
        }
        return dyajaxReturn(1, '通知列表',$list);
    }

    //系统消息
    public function getMessageList()
    {
        $UsersLogic = new UsersLogic();
        $list = $UsersLogic->getMessageList($this->user);
        return dyajaxReturn(1, '系统消息列表',$list);
    }

    //获取私信详细列表
    public function getUserMessageList()
    {
        $user_id = input('user_id/s',0);//私信用户id
        if(!$user_id){
            return dyajaxReturn(0, '用户不存在');
        }
        $user_id = dydecrypt($user_id);
        if(!$user_id){
            return dyajaxReturn(0, '用户不存在');
        }
        $UsersLogic = new UsersLogic();
        //判断私信
        $list = $UsersLogic->getUserMessageList($this->user,$user_id);
        return dyajaxReturn(1, '私信消息列表',$list);
    }

    //获取新私信详细列表
    public function getNewUserMessage()
    {
        $user_id = input('user_id/s',0);//私信用户id
        $msg_id = input('msg_id/d',0);//最后消息id
        if(!$user_id){
            return dyajaxReturn(0, '用户不存在');
        }
        $user_id = dydecrypt($user_id);
        if(!$user_id){
            return dyajaxReturn(0, '用户不存在');
        }
        $UsersLogic = new UsersLogic();
        //判断私信
        $list = $UsersLogic->getUserMessageList($this->user,$user_id,false,$msg_id);
        return dyajaxReturn(1, '私信消息列表',$list);
    }

    //发私信
    public function sendMessage()
    {
        $user_id = input('user_id/s',0);//私信用户id
        if(!$user_id){
            return dyajaxReturn(0, '用户不存在');
        }
        $user_id = dydecrypt($user_id);
        if(!$user_id){
            return dyajaxReturn(0, '用户不存在');
        }
        if($user_id==$this->user_id){
            return dyajaxReturn(0, '不可以给自己发消息');
        }
        $typeid = input('typeid/d',0);//私信类型 0=普通 1=图文  2=视频  3=方案  4=家居
        $content = input('content/s');//内容
        $aim_id = input('aim_id/d',0);//id
        $chat_identify = [$user_id,$this->user_id];
        sort($chat_identify);
        $data = [
            'from_user_id'=>$this->user_id,
            'to_user_id'=>$user_id,
            'chat_identify'=>implode('_',$chat_identify),
            'typeid'=>$typeid,
            'send_time'=>time(),
        ];
        switch ($typeid){
            case 1:
                if(!$aim_id){
                    return dyajaxReturn(0, '图文不存在');
                }
                $data['aim_id'] = $aim_id;
                break;
            case 2:
                if(!$aim_id){
                    return dyajaxReturn(0, '视频不存在');
                }
                $data['aim_id'] = $aim_id;
                break;
            case 3:
                if(!$aim_id){
                    return dyajaxReturn(0, '方案不存在');
                }
                $data['aim_id'] = $aim_id;
                break;
            case 4:
                if(!$aim_id){
                    return dyajaxReturn(0, '家居不存在');
                }
                $data['aim_id'] = $aim_id;
                break;
            default:
                if(!$content){
                    return dyajaxReturn(0, '请输入内容');
                }
                $data['content'] = $content;
        }
        $res = Db::name('chatlog')->insertGetId($data);
        if(!$res){
            return dyajaxReturn(0, '发送失败');
        }
        //自己 获取头像
        $user = $this->user;
        $user['photo'] = SITE_URL.dythumbimages($this->user_id,120,120,120,'users');
        //其他获取 头像 昵称
        $message_user['photo'] = SITE_URL.dythumbimages($user_id,120,120,120,'users');
        $message_user['nickname'] = get_user_nickname($user_id);
        $field = 'from_user_id,content,send_time,typeid,aim_id,msg_id';
        $info = Db::name('chatlog')->where(['msg_id'=>$res])
            ->field($field)
            ->find();
        $info['send_time']=get_date($info['send_time']);
        $info['label'] = [];
        $info['title'] = "";
        $info['thumb'] = "";
        if($info['from_user_id']==$this->user_id){
            //自己消息
            $info['photo'] = $user['photo'];
            $info['nickname'] = $user['nickname'];
            $info['is_own'] = 1;
        }else{
            //他人消息
            $info['photo'] = $message_user['photo'];
            $info['nickname'] = $message_user['nickname'];
            $info['is_own'] = 0;
        }
        switch ($info['typeid']){
            case 1:
                $user_dynamic = Db::name('user_dynamic')->where('dynamic_id',$info['aim_id'])->field('title')->find();
                $info['title'] = $user_dynamic['title'];
                $info['thumb'] = SITE_URL.dythumbimages($info['aim_id'],120,120,120,'user_dynamic');
                $info['content'] = '';
                break;
            case 2:
                $user_dynamic = Db::name('user_dynamic')->where('dynamic_id',$info['aim_id'])->field('content')->find();
                $info['title'] = $user_dynamic['content'];
                $info['thumb'] = SITE_URL.dythumbimages($info['aim_id'],120,120,120,'user_dynamic');
                $info['content'] = '';
                break;
            case 3:
                $user_dynamic = Db::name('user_dynamic')->where('dynamic_id',$info['aim_id'])->field('title,style_id,house_type_id,area')->find();
                $info['title'] = $user_dynamic['title'];
                $info['thumb'] = SITE_URL.dythumbimages($info['aim_id'],120,120,120,'user_dynamic');
                $label = [];
                $label[] = get_label_name($user_dynamic['style_id']);
                $label[] = get_label_name($user_dynamic['house_type_id']);
                $label[] = $user_dynamic['area']."㎡";
                $info['label'] = $label;
                $info['content'] = '';
                break;
            case 4:
                $user_dynamic = Db::name('furniture')->where('id',$info['aim_id'])->field('name')->find();
                $info['title'] = $user_dynamic['name'];
                $info['thumb'] = SITE_URL.dythumbimages($info['aim_id'],120,120,120,'furniture');
                $info['content'] = '';
                break;
        }
        unset($info['from_user_id']);
        return dyajaxReturn(1, '发送成功',$info);
    }
}
