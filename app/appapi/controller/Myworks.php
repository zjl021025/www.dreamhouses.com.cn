<?php

namespace app\appapi\controller;

use app\common\validate\PublishWorks;
use think\facade\Db;
use think\facade\Request;
use function dyajaxReturn;
use function dydecrypt;
use function dyjsonReturn;
use function input;

class Myworks
{
    /**
     * 展示我的作品列表
     * @return void
     */
    public function showMyWorkYes()
    {
        $user_id = dydecrypt(Request::post('user_id'));
        $startPage = input('startPage');
        $endPage = input('endPage');
        //获取用户的作品
        $workData = Db::name('user_works')->where(['user_id' => $user_id,'is_deleted'=>0])->order('works_id','desc')->limit($startPage,$endPage)->select()->toArray();
        $data = [];
        $label = Db::name('label')->select()->toArray();
        foreach ($workData as $key => $val) {
            foreach ($label as $k => $v){
                if($v['id'] == $val['house_type_id']){
                    $workData[$key]['house_type'] = $v['name'];
                    $data[] = $v['name'];
                }
                if($v['id'] == $val['style_id']){
                    $workData[$key]['style'] = $v['name'];
                }
            }
            $data[] = Db::name('region')->where(['id'=>$val['city_id']])->value('name');
            $data[] = $val['area'] . 'm²';
            $workData[$key]['data'] = $data;
            $data = [];
            $workData[$key]['add_time'] = date('Y-m-d', $val['add_time']);
            //计算图片和视频数量
            $workData[$key]['imageNum'] = Db::name('cloud_render_picture')->where(['works_id'=>$val['works_id'],'is_deleted'=>0])->count();
            $workData[$key]['videoNum'] = Db::name('cloud_render_video')->where(['works_id'=>$val['works_id'],'is_deleted'=>0])->count();
        }
        if (empty($workData)) {
            return dyjsonReturn(0, '您未有自己的作品');
        }
        return dyjsonReturn(1, '查询成功', $workData);
    }

    /**
     * 我的作品详情
     * @return void
     */
    public function itemMyWork()
    {
        $user_id = dydecrypt(Request::post('user_id'));
        $works_id = Request::post('works_id');
        $itemWorks = Db::name('user_works')
            ->alias('work')
            ->join('users','work.user_id = users.user_id')
            ->where(['work.works_id' => $works_id, 'work.user_id' => $user_id])
            ->find();
        //查看点赞数量和收藏数量
        $itemWorks['praiseNum'] = Db::name('user_praise_work')->where('aim_id',$itemWorks['works_id'])->count();
        $itemWorks['collectNum'] = Db::name('user_collect_work')->where('aim_id',$itemWorks['works_id'])->count();
        $itemWorks['evaluateNum'] = Db::name('evaluate_work')->where(['works_id'=>$itemWorks['works_id'],'is_show'=>1])->count();
        $itemWorksDesign = Db::name('user_works_room')->where('works_id', $itemWorks['works_id'])->select()->toArray();
        $label = Db::name('label')->select()->toArray();
        foreach ($itemWorksDesign as $key=>$val){
            foreach ($label as $k=>$v){
                if($v['typeid'] == 5){
                    if($val['room_id'] == $v['id']){
                        $itemWorksDesign[$key]['title'] = $v['name'];
                    }
                }
            }
        }
        $itemWorks['rooms'] = $itemWorksDesign;
        //TODO: 详情列表字段添加  风格  属性
        $data[] = Db::name('label')->where('id',$itemWorks['style_id'])->value('name');
        $data[] = Db::name('label')->where('id',$itemWorks['house_type_id'])->value('name');
        $data[] = $itemWorks['area']."m²";
        $itemWorks['label'] = $data;
        if (empty($itemWorks)) {
            return dyjsonReturn(0, '详情获取失败，请重新获取');
        }
        return dyjsonReturn(1, '获取成功', $itemWorks);
    }

    /**
     * 修改我的作品
     * @return void
     */
    public function inUpdate()
    {
        $user_id = dydecrypt(Request::post('user_id'));
        $works_id = Request::post('works_id');
        //获取作品信息
        $itemWorks = Db::name('user_works')->where(['works_id' => $works_id, 'user_id' => $user_id])->find();
        $itemWorksDesign = Db::name('user_works_room')->where('works_id', $itemWorks['works_id'])->select()->toArray();
        $itemWorks['rooms'] = $itemWorksDesign;
        //获取作品风格
        $style = Db::name('label')->where(['id'=>$itemWorks['style_id']])->value('name');
        //获取作品居室
        $house_type = Db::name('label')->where(['id'=>$itemWorks['house_type_id']])->value('name');
        $itemWorks['style'] = $style;
        $itemWorks['house_type'] = $house_type;
        $itemWorks['data'] = [$style,$house_type,$itemWorks['area'].'m²'];
        if (empty($itemWorks)) {
            return dyjsonReturn(0, '查询失败');
        }
        return dyjsonReturn(1, '查询成功', $itemWorks);
    }

    /**
     * 提交修改我的作品
     * @return void
     */
    public function updateWorks()
    {
        $param = Request::param();
        $param['user_id'] = dydecrypt($param['user_id']);
        $dydir = 'public/backup/addResource/' . date("Y") . '/';
        if (!is_dir($dydir)) @mkdir($dydir, 0755, true);
        $dydir = $dydir . date("m-d") . '/';
        if (!is_dir($dydir)) @mkdir($dydir, 0755, true);
        //写入文件做日志 调试用
        $log = "\r\n\r\n" . '=========$param==========' . "\r\n" . date("Y-m-d H:i:s") . "\r\n" . json_encode(json_encode($param));
        @file_put_contents($dydir . date("Y-m-d") . '.log', $log, FILE_APPEND);
        $validate = new PublishWorks();
        $result = $validate->check($param);
        if (!$result) {
            return dyjsonReturn(0, $validate->getError());
        }
        $data = [
            'title' => $param['title'],
            'images' => $param['images'],
            'over_content' => $param['over_content'],
            'image' => $param['image']
        ];
        $roomData = json_decode(htmlspecialchars_decode($param['roomData']), true);
        $roomKey = array_keys($roomData);
        $labelData = [];
        //获取房间信息
        $label = Db::name('label')->where(['typeid'=>5,'is_show'=>1])->select()->toArray();
        foreach ($label as $k=>$v){
            if(in_array($v['id'],$roomKey)){
                $labelData[] = [
                    'room_id'=>$v['id'],
                    'content' => $v['name']
                ];
            }
        }
        $labelData = array_combine(array_column($labelData,'room_id'),$labelData);
        foreach ($labelData as $key => $val){
            foreach ($roomData as $k => $v){
                if($key == $k){
                    $labelData[$key]['images'] = $v;
                }
            }
        }
        if ($param['is_original'] == 1) {
            //原数据修改
            Db::startTrans();
            try {
                //修改我的作品
                Db::name('user_works')->where('works_id',$param['works_id'])->update($data);
                //修改作品下的房间信息
                foreach ($labelData as $key => $val){
                    $data = [
                        'works_id' => $param['works_id'],
                        'room_id' => $val['room_id'],
                        'content' => $val['content'],
                        'images' => $val['images']
                    ];
                    Db::name('user_works_room')->where(['works_id'=>$param['works_id'],'room_id'=>$val['room_id']])->update($data);
                }
                Db::commit();
                return dyjsonReturn(1,'修改成功');
            } catch (\Exception $exception) {
                Db::rollback();
                return dyjsonReturn(0,'修改失败',$exception->getMessage());
            }
        } elseif ($param['is_original'] == 0) {
            //另存为修改
            Db::startTrans();
            try {
                //先获取需要修改作品信息
                $workData = Db::name('user_works')->where('works_id',$param['works_id'])->find();
                $workData['title'] = $param['title'];
                $workData['images'] = $param['images'];
                $workData['over_content'] = $param['over_content'];
                $workData['image'] = $param['image'];
                $workData['add_time'] = time();
                unset($workData['works_id']);
                $workId = Db::name('user_works')->insertGetId($workData);
                //修改作品下的房间信息
                foreach ($labelData as $key => $val){
                    $data = [
                        'works_id' => $workId,
                        'room_id' => $val['room_id'],
                        'content' => $val['content'],
                        'images' => $val['images'],
                        'add_time' => time()
                    ];
                    Db::name('user_works_room')->insert($data);
                }
                Db::commit();
                return dyjsonReturn(1,'修改成功');
            } catch (\Exception $exception) {
                Db::rollback();
                return dyjsonReturn(0,'另存为失败',$exception->getMessage());
            }
        }
    }

    /**
     * 发布作品
     * @return void
     */
    public function publishWorks(){
        $user_id = dydecrypt(input('user_id'));
        $works_id = input('works_id');
        $works = explode(',',$works_id);
        foreach ($works as $value){
            try {
                Db::name('user_works')->where(['works_id'=>$value])->update(['is_issue'=>1]);
            }catch (\Exception $exception){
                return dyjsonReturn(0,"发布".$value."失败");
            }
        }
        return dyjsonReturn(1,'发布成功');
    }

    //动态点赞,取消点赞
    public function praise()
    {
        $aim_id = input('aim_id/d',0);
        if(!$aim_id){
            return dyajaxReturn(0,'缺少信息Id');
        }
        $source = input('source/d',1);//1=点赞 2取消点赞
        $user_id = dydecrypt(input('user_id'));
        $user_praise = Db::name('user_praise_work')->where('aim_id', $aim_id)->where('user_id',$user_id)->value('id');
        $num = Db::name('user_works')->where('works_id', $aim_id)->value('praise');
        if($source==1){
            //点赞
            $data = [
                'user_id' => $user_id,
                'aim_id' => $aim_id,
                'add_time' => time(),
            ];
            if($user_praise){
                return dyajaxReturn(1, '已经点过赞了',['status'=>true,'num'=>$num]);
            }
            //添加成功
            $res = Db::name('user_praise_work')->insert($data);
            if ($res) {
                Db::name('user_works')->where('works_id', $aim_id)->inc('praise')->update();
                return dyajaxReturn(1, '点赞成功',['status'=>true,'num'=>$num+1]);
            } else {
                return dyajaxReturn(0, '点赞失败',['status'=>false,'num'=>$num]);
            }
        }else{
            //取消点赞
            $where['aim_id'] = $aim_id;
            $where['user_id'] = $user_id;
            $res = Db::name('user_praise_work')->where($where)->delete();
            if ($res) {
                Db::name('user_works')->where('works_id', $aim_id)->dec('praise')->update();
                return dyajaxReturn(1, '取消点赞成功',['status'=>false,'num'=>$num-1]);
            }else{
                if($user_praise){
                    return dyajaxReturn(0, '取消点赞失败',['status'=>true,'num'=>$num]);
                }
                return dyajaxReturn(1, '取消点赞成功',['status'=>false,'num'=>$num]);
            }
        }
    }

    //动态收藏,取消收藏
    public function collect()
    {
        $aim_id = input('aim_id/d',0);
        if(!$aim_id){
            return dyajaxReturn(0,'缺少信息Id');
        }
        $source = input('source/d',1);//1=收藏 2取消收藏
        $user = dydecrypt(input('user_id'));
        $user_collect = Db::name('user_collect_work')->where('aim_id', $aim_id)->where('user_id',$user)->value('id');
        $num = Db::name('user_works')->where('works_id', $aim_id)->value('collect');
        if($source==1){
            //点赞
            $data = [
                'user_id' => $user,
                'aim_id' => $aim_id,
                'add_time' => time(),
            ];
            if($user_collect){
                return dyajaxReturn(1, '已经收藏成功了',['status'=>true,'num'=>$num]);
            }
            //添加成功
            $res = Db::name('user_collect_work')->insert($data);
            if ($res) {
                Db::name('user_works')->where('works_id', $aim_id)->inc('collect')->update();
                return dyajaxReturn(1, '收藏成功',['status'=>true,'num'=>$num+1]);
            } else {
                return dyajaxReturn(0, '收藏失败',['status'=>false,'num'=>$num]);
            }
        }else{
            //取消点赞
            $where['aim_id'] = $aim_id;
            $where['user_id'] = $user;
            $res = Db::name('user_collect_work')->where($where)->delete();
            if ($res) {
                Db::name('user_works')->where('works_id', $aim_id)->dec('collect')->update();
                return dyajaxReturn(1, '取消收藏成功',['status'=>false,'num'=>$num-1]);
            }else{
                if($user_collect){
                    return dyajaxReturn(0, '取消收藏失败',['status'=>true,'num'=>$num]);
                }
                return dyajaxReturn(1, '取消收藏成功',['status'=>false,'num'=>$num]);
            }
        }
    }

}