<?php

namespace app\appapi\controller;

use think\Collection;
use think\facade\Db;
use think\facade\Request;

class CloudRender
{
    /**
     * ue模型渲染数据
     * @return void
     */
    public function modelRender(){
        $startPage = input('startPage');
        $endPage = input('endPage');
        $cate_id = input('cate_id');
        $two_cate_id = input('two_cate_id');
        $three_cate_id = input('three_cate_id');
        //获取家具分类的数据
        $furnitureTypeData = Db::name('furniture_category')->where(['typeid'=>2,'is_deleted'=>0,'is_show'=>1,'pid'=>0])->order('sort')->select()->toArray();
        foreach ($furnitureTypeData as $key=>$val){
            $furnitureTypeData[$key]['two_cate'] = Db::name('furniture_category')->where(['typeid'=>2,'is_deleted'=>0,'is_show'=>1,'pid'=>$val['id']])->order('sort')->select()->toArray();
            foreach ($furnitureTypeData[$key]['two_cate'] as $k=>$v){
                $furnitureTypeData[$key]['two_cate'][$k]['three_cate'] = Db::name('furniture_category')->where(['typeid'=>2,'is_deleted'=>0,'is_show'=>1,'pid'=>$v['id']])->order('sort')->select()->toArray();
            }
        }
        $where = ['is_show' => 1, 'is_deleted' => 0];
        if (!empty($cate_id)) {
            $where['cate_id'] = $cate_id;
            if (!empty($two_cate_id)) {
                $where['two_cate_id'] = $two_cate_id;
                if (!empty($three_cate_id)) {
                    $where['three_cate_id'] = $three_cate_id;
                }
            }
        }
        $furnitureData = Db::name('furniture')
            ->where($where)
            ->order('sort', 'desc')
            ->limit($startPage, $endPage)
            ->select()
            ->toArray();
        $data['type'] = $furnitureTypeData;
        $data['furniture'] = $furnitureData;
        return dyjsonReturn(1,'查询成功',$data);
    }

    /**
     * 上传图片
     * @return void
     */
    public function uploadImage(){
        $file = \request()->file('files');
        $user_id = dydecrypt(input('user_id'));
        $works_id = input('works_id');
        $imageData = [];
        $domain = config('qiniu.domain');
        foreach ($file as $val){
            $suffix = $val->getOriginalExtension();
            if($suffix != "jpg" && $suffix != "jpeg" && $suffix != "png" && $suffix != "gif"){
                return dyjsonReturn(0,'请上传jpg、jpeg、png、gif格式的图片！');
            }
            $qiniu_file = \think\facade\Filesystem::disk('qiniu')->putFile('image',$val);
            $baseUrl = 'http://'.$domain.'/'.$qiniu_file;
            $imageData[] = $baseUrl;
            //添加渲染图片
            $data = [
                'image' => $baseUrl,
                'add_time' => time(),
                'user_id' => $user_id,
                'works_id' => $works_id
            ];
            try {
                Db::name('cloud_render_picture')->insert($data);
            }catch (\Exception $exception){
                throw new $exception->getMessage();
            }
        }
        if(empty($data)){
            return dyjsonReturn(0,'上传失败');
        }
        return dyjsonReturn(1,'上传成功',$imageData);
    }

    /**
     * 上传视频
     * @return void
     */
    public function uploadVideo(){
        $file = \request()->file('files');
        $user_id = dydecrypt(input('user_id'));
        $works_id = input('works_id');
        $videoData = [];
        $domain = config('qiniu.domain');
        foreach ($file as $val){
            $suffix = $val->getOriginalExtension();
            if($suffix != 'mp4' && $suffix != 'mov' && $suffix != 'avi'){
                return dyjsonReturn(0,'请上传mp4、mov、avi格式的视频！');
            }
            $qiniu_file = \think\facade\Filesystem::disk('qiniu')->putFile('video',$val);
            $baseUrl = 'http://'.$domain.'/'.$qiniu_file;
            $videoData[] = $baseUrl;
            //添加渲染图片
            $data = [
                'video' => $baseUrl,
                'add_time' => time(),
                'user_id' => $user_id,
                'works_id'=>$works_id
            ];
            try {
                Db::name('cloud_render_video')->insert($data);
            }catch (\Exception $exception){
                throw new $exception->getMessage();
            }
        }
        if(empty($data)){
            return dyjsonReturn(0,'上传失败');
        }
        return dyjsonReturn(1,'上传成功',$videoData);
    }

    /**
     * 上传多张图片
     * @return void
     */
    public function uploadManyImage(){
        if (!request()->file('files')){
            return dyjsonReturn(0,'非法请求');
        }
        $files = request()->file('files');
        $data = [];
        $domain = config('qiniu.domain');
        foreach ($files as $val){
            $qiniu_file = \think\facade\Filesystem::disk('qiniu')->putFile('image',$val);
            $baseUrl = 'http://'.$domain.'/'.$qiniu_file;
            $data[] = $baseUrl;
        }
        return dyjsonReturn(1,'上传成功',$data);
    }

    /**
     * 上传多个视频
     * @return void
     */
    public function uploadManyVideo(){
        if (!request()->file('files')){
            return dyjsonReturn(0,'非法请求');
        }
        $files = request()->file('files');
        $data = [];
        $domain = config('qiniu.domain');
        foreach ($files as $val){
            $qiniu_file = \think\facade\Filesystem::disk('qiniu')->putFile('video',$val);
            $baseUrl = 'http://'.$domain.'/'.$qiniu_file;
            $data[] = $baseUrl;
        }
        return dyjsonReturn(1,'上传成功',$data);
    }

    /**
     * 用户渲染图片列表
     * @return void
     */
    public function cloudRenderUserPictureList(){
        $user_id = dydecrypt(input('user_id'));
        //获取渲染图片列表
        $imageList = Db::name('cloud_render_picture')->where(['user_id'=>$user_id,'is_deleted'=>0])->select()->toArray();
        if(empty($imageList)){
            return dyjsonReturn(0,'获取列表失败');
        }
        return dyjsonReturn(1,'获取成功',$imageList);
    }

    /**
     * 用户渲染图片列表
     * @return void
     */
    public function cloudRenderUserVideoList(){
        $user_id = dydecrypt(input('user_id'));
        //获取渲染图片列表
        $videoList = Db::name('cloud_render_video')->where(['user_id'=>$user_id,'is_deleted'=>0])->select()->toArray();
        if(empty($videoList)){
            return dyjsonReturn(0,'获取列表失败');
        }
        return dyjsonReturn(1,'获取成功',$videoList);
    }

    /**
     * 云渲染接口
     * @return void
     */
    public function cloudRender(){
        $startPage = input('startPage');
        $endPage = input('endPage');
        $where['ios_url'] = ['<>',''];
        $where['Android_url'] = ['<>',''];
        $where['pc_url'] = ['<>',''];
        $where['is_show'] = 1;
        $where['is_deleted'] = 0;
        $sceneHouse = Db::name('scene_house')
            ->field('house_id,name,ios_url,Android_url,pc_url')
            ->where($where)
            ->limit($startPage,$endPage)
            ->select()->toArray();
        foreach ($sceneHouse as $key=>$val){
            $sceneHouse[$key]['type'] = 'house';
            $sceneHouse[$key]['id'] = $val['house_id'];
            unset($val['house_id']);
        }
        $furnitureData = Db::name('furniture')
            ->field('id,name,ios_url,Android_url,pc_url')
            ->where($where)
            ->limit($startPage,$endPage)
            ->select()->toArray();
        foreach ($furnitureData as $key => $val){
            $furnitureData[$key]['type'] = 'furniture';
        }
        $render = array_merge($sceneHouse,$furnitureData);
        foreach ($render as $key=>$value){
            $id[$key] = $value['id'];
        }
        array_multisort($id,SORT_DESC,$render);
        if(empty($render)){
            return dyjsonReturn(0,'查询失败');
        }
        return dyjsonReturn(1,'查询成功',$render);
    }
}