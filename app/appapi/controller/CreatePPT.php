<?php

namespace app\appapi\controller;

use PhpOffice\PhpPresentation\Exception\FileNotFoundException;
use PhpOffice\PhpPresentation\Exception\OutOfBoundsException;
use PhpOffice\PhpPresentation\IOFactory;
use PhpOffice\PhpPresentation\Reader\PowerPoint2007;
use PhpOffice\PhpPresentation\Shape\Drawing\Base64;
use PhpOffice\PhpPresentation\Slide;
use PhpOffice\PhpSpreadsheet\Shared\Drawing;
use think\facade\Db;

class CreatePPT
{
    /**
     * 一键生成ppt
     * @return false|string|void
     */
    public function createPPT()
    {
        $works_id = input('works_id');
        $user_id = dydecrypt(input('user_id'));
        $house = Db::name('user_works')->where(['works_id'=>$works_id,'is_deleted' => 0])->find();
        $house = array_filter($house);
        $houseRoom = Db::name('user_works_room')->where('works_id',$works_id)->select()->toArray();
        foreach ($houseRoom as $key=>$val){
            $houseRoom[$key]['room'] = Db::name('label')->where(['id'=>$val['room_id']])->value('name');
        }
        $house_image = $house['house_image'];
        $pptModelUrl = 'public/upload/ppt/222.ppt';
        $reader = new PowerPoint2007();
        $cans = $reader->canRead($pptModelUrl); //判断是否能读
        if ($cans) {
            $file = IOFactory::createReader('PowerPoint2007');
            $fileClass = $file->load($pptModelUrl); //加载并资源化文件
            $counts = $fileClass->getSlideCount(); //获取总页数00
            for ($i = 0; $i < $counts; $i++) {
                $iSlide = $fileClass->setActiveSlideIndex($i);//设定活动的幻灯片
                foreach ($iSlide->getShapeCollection() as $one => $iShape) {//循环获取所有的形状格式内容
                    try {
                        $iParagraphs = $iShape->getParagraphs();//获取所有段落
                        foreach ($iParagraphs as $two => $iParagraph) {
                            foreach ($iParagraph->getRichTextElements() as $three => $iRichText) {//获取所有富文本标签
                                $test = $iRichText->getText();
                                var_dump((string)$i . $one . $two . $three . '=>' . $test);
//                                //这里面是替换；需要知道所替换的幻灯片页数$i形状格式$one段落$two富文本标签$three
                                switch ((string)$i.$one.$two.$three) {
                                    case '0000':
                                        $iRichText->setText($house['title']);//替换文本内容
                                        break;
                                    case '1000':
                                        $iRichText->setText($house['image']);//替换文本内容
                                        break;
                                    case '0100':
                                        $iRichText->setText($house['over_content']);//替换文本内容
                                        break;
                                    case '2000':
                                        $iRichText->setText('原始户型空间相对局促，无法满足屋主的居住需求和舒适自由的生活状态。空间尺寸局促，光线差，厨房客厅之间动线不舒服，生活阳台，设备间等功能位置没做规划 ，主卧空间被切分的太为碎小空间不够通透，次卧空间较为局促，满足不了儿童以后的居住需求。');//替换文本内容
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                    } catch (\Throwable $t) {
                        try {
                            $iShape->getMimeType();//获取样式，这里解析图
                            $imageData = "data:image/jpg;base64,".base64_encode(file_get_contents($house_image));
                            $iShape->setName('house_image')
                                ->setDescription('house_image')
                                ->setResizeProportional(false)
                                ->setPath($imageData)
                                ->setHeight(400)
                                ->setOffsetX(100)
                                ->setOffsetY(200);
                            echo "$i$one$two$three => $imageData";
                            continue;
                        } catch (\Throwable $t) {
//                            $rows = $iShape->getRows();//获取表格
//                            foreach ($rows as $row) {
//                                $cells = $row->getCells();//获取所有表格单元
//                                foreach ($cells as $cell) {
//                                    $text = $cell->getPlainText();
//                                    var_dump($text);
//                                }
//                            }
//                            continue;
                        }
                    }
                }
            }
            $fileCreate = IOFactory::createWriter($fileClass, 'PowerPoint2007');
            $fileCreate->save(__DIR__ . '/MODEL.pptx');
        }
    }

}