<?php

namespace app\appapi\controller;


use app\common\validate\problemValidate;
use think\facade\Db;
use think\facade\Request;

class HomeSteward
{
    /**
     * 点击家装管家
     * @return void
     */
    public function inHomeSteward()
    {
        $homeStewardData = Db::name('home_steward')->order('sort')->limit(5)->select()->toArray();
        foreach ($homeStewardData as $key => $val) {
            $homeStewardData[$key]['lookNum'] = Db::name('home_steward_look')->where('home_id', $val['id'])->count();
        }
        if (!$homeStewardData) {
            return dyjsonReturn(0, '查询失败');
        }
        return dyjsonReturn(1, '查询成功', $homeStewardData);
    }

    /**
     * 进入新手教程
     * @return void
     */
    public function newCourse()
    {
        //查询教程类型
        $data['courseType'] = Db::name('home_steward_type')->order('sort')->select()->toArray();
        $type_id = $data['courseType'][0]['id'];
        //查询默认类型下的教程
        $data['courseData'] = Db::name('home_steward')->where(['type_id'=>$type_id,'is_deleted'=>0])->select()->toArray();
        foreach ($data['courseData'] as $key => $val) {
            $data['courseData'][$key]['lookNum'] = Db::name('home_steward_look')->where('home_id', $val['id'])->count();
        }
        if (empty($data)) {
            return dyjsonReturn(0, '数据查询失败');
        }
        return dyjsonReturn(1, '查询成功', $data);

    }

    /**
     * 点击分类
     * @return void
     */
    public function clickType()
    {
        $type_id = Request::post('type_id');
        $courseData = Db::name('home_steward')->where(['type_id'=>$type_id,'is_deleted'=>0])->select()->toArray();
        foreach ($courseData as $key => $val) {
            $courseData[$key]['lookNum'] = Db::name('home_steward_look')->where('home_id', $val['id'])->count();
        }
        if (empty($courseData)) {
            return dyjsonReturn(0, '查询失败');
        }
        return dyjsonReturn(1, '查询成功', $courseData);
    }

    /**
     * 搜索教程
     * @return void
     */
    public function searchCourse()
    {
        $content = Request::post('content');
        if (empty($content)) {
            return dyjsonReturn(0, '请输入搜索内容');
        }
        $data = Db::name('home_steward')->where('title', 'like', "%$content%")->where(['is_deleted'=>0])->select()->toArray();
        foreach ($data as $key => $val) {
            $data[$key]['lookNum'] = Db::name('home_steward_look')->where('home_id', $val['id'])->count();
        }
        if (!$data) {
            return dyjsonReturn(0, '查询失败');
        }
        return dyjsonReturn(1, '查询成功', $data);
    }

    /**
     * 新手教程详情
     * @return void
     */
    public function itemCourse()
    {
        $course_id = Request::post('course_id');
        if (empty($course_id)) {
            return dyjsonReturn(0, '请传入正确的数据');
        }
        $data = Db::name('home_steward')->where(['id'=>$course_id,'is_deleted'=>0])->find();
        $data['itemData'] = Db::name('home_steward_item')->where(['steward_id'=>$data['id'],'is_deleted'=>0])->select()->toArray();
        if (!$data) {
            return dyjsonReturn(0, '查询失败');
        }
        return dyjsonReturn(1, '查询成功', $data);
    }

    /**
     * 上传问题
     * @return void
     */
    public function uploadProblem()
    {
        $param = Request::param();
        $validate = new problemValidate();
        $result = $validate->check($param);
        if (!$result) {
            return dyjsonReturn(0, $validate->getError());
        }
        $param['add_time'] = time();
        $param['user_id'] = dydecrypt($param['user_id']);
        unset($param['token']);
        $insert = Db::name('user_feedback')->insert($param);
        if (!$insert) {
            return dyjsonReturn(0, '反馈失败');
        }
        return dyjsonReturn(1, '反馈成功');
    }

    /**
     * 进入问卷调查
     * @return void
     */
    public function inSurvey()
    {
        $user_id = dydecrypt(input('user_id'));
        $styleDataLike = Db::name('user_like_style')->where(['user_id' => $user_id])->select()->toArray();
        $fitmentData = Db::name('user_like_fitment')->where(['user_id' => $user_id])->select()->toArray();
        $styleData['style'] = Db::name('label')->where(['typeid' => 2, 'is_show' => 1])->order('sort')->select()->toArray();
        foreach ($styleData['style'] as $key => $val) {
            if (in_array($val['id'], array_column($styleDataLike, 'style_id'))) {
                $styleData['style'][$key]['is_select'] = 1;
            } else {
                $styleData['style'][$key]['is_select'] = 0;
            }
        }
        $styleData['fitment'] = Db::name('label')->where(['typeid' => 10, 'is_show' => 1])->order('sort')->select()->toArray();
        foreach ($styleData['fitment'] as $key => $val) {
            if (in_array($val['id'], array_column($fitmentData, 'fitment_id'))) {
                $styleData['fitment'][$key]['is_select'] = 1;
            } else {
                $styleData['fitment'][$key]['is_select'] = 0;
            }
        }
        if (!$styleData) {
            return dyjsonReturn(0, '查询失败');
        }
        return dyjsonReturn(1, '查询成功', $styleData);
    }

    /**
     * 确认问卷调查
     * @return void
     */
    public function confirmSurvey()
    {
        $user_id = dydecrypt(input('user_id'));
        $style_id = explode(',', input('style_id'));
        $fitment_id = explode(',', input('fitment_id'));
        if (empty($user_id) || empty($style_id) || empty($fitment_id)) {
            return dyjsonReturn(0, '数据传入错误');
        }
        //判断是否已经选择过
        $style = Db::name('label')->where(['typeid' => 2, 'is_show' => 1])->order('sort')->select()->toArray();
        foreach ($style as $key => $val) {
            if (in_array($val['id'], $style_id)) {
                unset($style[$key]);
            }
        }
        $fitment = Db::name('label')->where(['typeid' => 10, 'is_show' => 1])->order('sort')->select()->toArray();
        foreach ($fitment as $key => $val) {
            if (in_array($val['id'], $fitment_id)) {
                unset($fitment[$key]);
            }
        }
        Db::startTrans();
        try {
            //添加问卷调查
            foreach ($style_id as $value) {
                $styleData = [
                    'user_id' => $user_id,
                    'style_id' => $value,
                    'add_time' => time()
                ];
                //添加所选风格
                Db::name('user_like_style')->insert($styleData);
            }
            foreach ($fitment_id as $value) {
                $fitmentData = [
                    'user_id' => $user_id,
                    'fitment_id' => $value,
                    'add_time' => time()
                ];
                //添加所选装修进度
                Db::name('user_like_fitment')->insert($fitmentData);
            }
            Db::commit();
            return dyjsonReturn(1, '添加成功');
        } catch (\Exception $exception) {
            Db::rollback();
            return dyjsonReturn(0, '添加失败');
        }
    }
}