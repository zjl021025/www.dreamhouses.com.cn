<?php

namespace app\appapi\controller;

use app\common\logic\ArticleLogic;

class Article extends Base
{
    /**
     * 装修贴士
     * @return void
     */
    public function getTipsList()
    {
        $ArticleLogic = new ArticleLogic();
        $where = [];
        $keywords = trim(input('keyword'));
        $keywords && $where['title'] = ['like','%'.$keywords.'%'];
        $list = $ArticleLogic->getList(2,$where);
        foreach ($list as &$value){
            $value['details'] = SITE_URL.url('Currency/article',['article_id'=>$value['article_id']]);
            $value['add_time'] = date('Y.m.d',$value['add_time']);
            unset($value['article_id']);
        }
        return dyajaxReturn(1, '装修贴士', $list);
    }

    /**
     * 楼盘资讯
     * @return void
     */
    public function getEstateList()
    {
        $estate_id = input('estate_id/d');
        $typeid = input('typeid/d');
        $ArticleLogic = new ArticleLogic();
        $where = [];
        $where['estate_id'] = $estate_id;
        $where['typeid'] = $typeid==1?1:0;
        $list = $ArticleLogic->getList(0,$where);
        foreach ($list as &$value){
            $value['details'] = SITE_URL.url('Currency/article',['article_id'=>$value['article_id']]);
            $value['add_time'] = date('Y.m.d',$value['add_time']);
            unset($value['article_id']);
        }
        return dyajaxReturn(1, '楼盘资讯', $list);
    }
}
