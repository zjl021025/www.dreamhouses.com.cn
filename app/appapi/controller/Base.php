<?php

namespace app\appapi\controller;
use think\facade\Db;
use app\BaseController;

class Base extends BaseController
{
    public $end;
    public $page_size = 0;
    public $user_id = 0;
    public $user = [];
    public $token = null;

    /**
     * 析构函数
     */
    function __construct()
    {
        header("Cache-control: private");
        $this->checkToken(); // 检查token
    }

    /*
     * 初始化操作
     */
    public function _initialize() {
		header("Content-type:text/html;charset=utf-8");
        $local_sign = '8250ac2z6a3x4a6pep5a2p4i9a2fb12b';
        $api_secret_key = config('API_SECRET_KEY');
		$dyappsecret = input('post.dyappsecret/s','8250ac2z6a3x4a6pep5a2p4i9a2fb12b');
		if('www.doing.net.cn' != $api_secret_key)
            $this->ajaxReturn(['errcode'=>103,'message'=>'系统密钥错误']);
		$notaarray = array('detail','sendsms','demo','useridentity','register','login','noticelist','forgetpwd','alinotifyurl','wxnotifyurl');
		//ispostdata($notaarray);//开发阶段暂不开启
		// 不参与签名验证的方法
        if(!in_array(strtolower(ACTION_NAME), $notaarray))
            if($local_sign != $dyappsecret) $this->ajaxReturn(['errcode'=>104,'message'=>'签名失败']);
    } 

    /**
     * App端请求签名
     * @return string
     */
    protected function getSign(){
        //header("Content-type:text/html;charset=utf-8");
        $data = $_POST;        
        unset($data['time'],$data['sign']);    // 删除这两个参数再来进行排序     
        ksort($data);
        $str = implode('', $data);        
        $str = $str.config('API_SECRET_KEY');        
        return encrypt($str);
    }

    /**
     * 获取服务器时间
     */
    public function getServerTime(){ 
        $this->ajaxReturn(['errcode'=>1,'message'=>'服务器时间获取成功','time'=>time()]);
    }

    /**
     * 校验token
     */
    public function checkToken(){
        $this->token = input("token/s"); // token
        // 判断哪些控制器的 哪些方法需要登录验证的
        $check_arr = array(
            'member' => array('userinfo','authen','getauthen','setuser','getsetuser','gethouselist','gethouse','house',
                'delhouse','bindmobile','cancelaccount','unbind','directbind'),
            'dynamic' => array('praise','evaluate','follow','addvideo','addimagetext','addscheme','collect','deldynamic','geteditdynamic'),
            'message' => array('getmessagelist','index','getusermessageList','getnewusermessage','sendmessage'),
            'furniture' => array('collect'),
            'works' => array('getworkslist','getworks','delworks','works','addresource','delresource','gethouseimage',
                'gethousevideo','getdesigninfo','adddesigninfo','setdesigninfo','deldesigninfo','getdesignstatistics','setalldesign'),
            );
		//转换为小写
        $controller_name = strtolower(CONTROLLER_NAME);
        $action_name = strtolower(ACTION_NAME);
        $fields = 'user_id,mobile,nickname,token,is_authen,realname,designer_type,status,is_deleted,is_water,sys_new_msg,is_recd';
        if ($this->token || (in_array($controller_name, array_keys($check_arr)) && in_array($action_name, $check_arr[$controller_name]))) {
            if(empty($this->token)) $this->ajaxReturn(['errcode'=>100,'message'=>'请先登录','post'=>input()]);
            $this->user = Db::name('users')
                        ->field($fields)->where(['token'=>$this->token])->find();
            if(empty($this->user)) $this->ajaxReturn(['errcode'=>100,'message'=>'请先登录','post'=>input()]);
            if($this->user['status'] !== 1) $this->ajaxReturn(['errcode'=>102,'message'=>'账号被冻结,无法进行操作']);
            if($this->user['is_deleted'] == 1) $this->ajaxReturn(['errcode'=>0,'message'=>'账号正在进行注销操作中,无法登录']);
            $this->user_id = $this->user['user_id'];
        }
    }

    public function ajaxReturn($data)
    {
        exit(json_encode($data, JSON_UNESCAPED_UNICODE));
    }
}
