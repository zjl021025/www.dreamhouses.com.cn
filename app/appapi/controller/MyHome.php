<?php

namespace app\appapi\controller;

use think\facade\Db;
use think\facade\Request;
use function dydecrypt;
use function dyjsonReturn;

class MyHome
{
    /**
     * 展示我的家
     * @return void
     */
    public function showMyHome()
    {
        $user_id = dydecrypt(Request::post('user_id'));
        $homeData = Db::name('user_house')->where(['user_id' => $user_id, 'is_deleted' => 0])->select()->toArray();
        $data = [];
        $label = Db::name('label')->select()->toArray();
        foreach ($homeData as $key => $val) {
            foreach ($label as $k => $v){
                if($v['id'] == $val['house_type_id']){
                    $homeData[$key]['house_type'] = $v['name'];
                    $data[] = $v['name'];
                }
                if($v['id'] == $val['style_id']){
                    $homeData[$key]['style'] = $v['name'];
                    $data[] = $v['name'];
                }
            }
            $data[] = $val['area'] . 'm²';
            $homeData[$key]['data'] = $data;
            $data = [];
            $homeData[$key]['add_time'] = date('Y-m-d', $val['add_time']);
        }
        if (empty($homeData)) {
            return dyjsonReturn(0, '您还未上传您的家，请先添加哦！！');
        }
        return dyjsonReturn(1, '查询成功', $homeData);
    }

    /**
     * 我的家详情
     * @return void
     */
    public function itemMyHome()
    {
        $user_id = dydecrypt(Request::post('user_id'));
        $dynamic_id = Request::post('house_id');
        $itemHomeData = Db::name('user_house')
            ->alias('house')
            ->join('users','house.user_id = users.user_id')
            ->where(['house.user_id' => $user_id, 'house.house_id' => $dynamic_id])
            ->find();
        $itemHomeDataDesign = Db::name('user_house_room')->where('house_id', $itemHomeData['house_id'])->select()->toArray();
        $label = Db::name('label')->select()->toArray();
        foreach ($itemHomeDataDesign as $key=>$val){
            foreach ($label as $k=>$v){
                if($v['typeid'] == 5){
                    if($val['room_id'] == $v['id']){
                        $itemHomeDataDesign[$key]['title'] = $v['name'];
                    }
                }
            }
        }
        $itemHomeData['style'] = Db::name('label')->where('id',$itemHomeData['style_id'])->value('name');
        $itemHomeData['rooms'] = $itemHomeDataDesign;
        //TODO: 详情列表字段添加  风格  属性
        $data[] = Db::name('label')->where('id',$itemHomeData['style_id'])->value('name');
        $data[] = Db::name('label')->where('id',$itemHomeData['house_type_id'])->value('name');
        $data[] = $itemHomeData['area']."m²";
        $itemHomeData['label'] = $data;
        if (empty($itemHomeData)) {
            return dyjsonReturn(0, '查询失败');
        }
        return dyjsonReturn(1, '查询成功', $itemHomeData);
    }

    /**
     * 修改我的家
     * @return void
     */
    public function inHomeUpdate()
    {
        $user_id = dydecrypt(Request::post('user_id'));
        $dynamic_id = Request::post('house_id');
        //获得方案信息
        $itemHomeData = Db::name('user_house')->where(['user_id' => $user_id, 'house_id' => $dynamic_id])->find();
        $itemHomeDataDesign = Db::name('user_house_room')->where('house_id', $itemHomeData['house_id'])->select();
        $itemHomeData['rooms'] = $itemHomeDataDesign;
        if (empty($itemHomeData)) {
            return dyjsonReturn(0, '查询失败');
        }
        return dyjsonReturn(1, '查询成功', $itemHomeData);
    }

    /**
     * 提交修改我的家
     * @return void
     */
    public function updateMyHome()
    {
        $param = Request::param();
        if ($param['is_original'] == 1) {
            Db::startTrans();
            try {
                unset($param['is_original']);
                unset($param['user_id']);
                Db::name('user_house')->where('house_id',$param['house_id'])->update($param);
                Db::commit();
                return dyjsonReturn(1,'修改成功');
            }catch (\Exception $exception){
                Db::rollback();
                return dyjsonReturn(0,'修改失败');
            }
        } elseif ($param['is_original'] == 0) {
            Db::startTrans();
            try {
                unset($param['is_original']);
                $param['user_id'] = dydecrypt($param['user_id']);
                $dynamicData = Db::name('user_house')->where('house_id',$param['house_id'])->find();
                unset($dynamicData['house_id']);
                $dynamicId = Db::name('user_house')->insertGetId($dynamicData);
                unset($param['house_id']);
                Db::name('user_house')->where('house_id',$dynamicId)->update($param);
                Db::commit();
                return dyjsonReturn(1,'另存为成功');
            }catch (\Exception $exception){
                Db::rollback();
                return dyjsonReturn(0,'另存为失败');
            }
        }
    }

    //动态点赞,取消点赞
    public function praise()
    {
        $aim_id = input('aim_id/d',0);
        if(!$aim_id){
            return dyajaxReturn(0,'缺少信息Id');
        }
        $source = input('source/d',1);//1=点赞 2取消点赞
        $user_id = dydecrypt(input('user_id'));
        $user_praise = Db::name('user_praise_home')->where('aim_id', $aim_id)->where('user_id',$user_id)->value('id');
        $num = Db::name('user_house')->where('house_id', $aim_id)->value('praise');
        if($source==1){
            //点赞
            $data = [
                'user_id' => $user_id,
                'aim_id' => $aim_id,
                'add_time' => time(),
            ];
            if($user_praise){
                return dyajaxReturn(1, '已经点过赞了',['status'=>true,'num'=>$num]);
            }
            //添加成功
            $res = Db::name('user_praise_home')->insert($data);
            if ($res) {
                Db::name('user_house')->where('house_id', $aim_id)->inc('praise')->update();
                return dyajaxReturn(1, '点赞成功',['status'=>true,'num'=>$num+1]);
            } else {
                return dyajaxReturn(0, '点赞失败',['status'=>false,'num'=>$num]);
            }
        }else{
            //取消点赞
            $where['aim_id'] = $aim_id;
            $where['user_id'] = $user_id;
            $res = Db::name('user_praise_home')->where($where)->delete();
            if ($res) {
                Db::name('user_house')->where('house_id', $aim_id)->dec('praise')->update();
                return dyajaxReturn(1, '取消点赞成功',['status'=>false,'num'=>$num-1]);
            }else{
                if($user_praise){
                    return dyajaxReturn(0, '取消点赞失败',['status'=>true,'num'=>$num]);
                }
                return dyajaxReturn(1, '取消点赞成功',['status'=>false,'num'=>$num]);
            }
        }
    }

    //动态收藏,取消收藏
    public function collect()
    {
        $aim_id = input('aim_id/d',0);
        if(!$aim_id){
            return dyajaxReturn(0,'缺少信息Id');
        }
        $source = input('source/d',1);//1=收藏 2取消收藏
        $user = dydecrypt(input('user_id'));
        $user_collect = Db::name('user_collect_home')->where('aim_id', $aim_id)->where('user_id',$user)->value('id');
        $num = Db::name('user_house')->where('house_id', $aim_id)->value('collect');
        if($source==1){
            //点赞
            $data = [
                'user_id' => $user,
                'aim_id' => $aim_id,
                'add_time' => time(),
            ];
            if($user_collect){
                return dyajaxReturn(1, '已经收藏成功了',['status'=>true,'num'=>$num]);
            }
            //添加成功
            $res = Db::name('user_collect_home')->insert($data);
            if ($res) {
                Db::name('user_house')->where('house_id', $aim_id)->inc('collect')->update();
                return dyajaxReturn(1, '收藏成功',['status'=>true,'num'=>$num+1]);
            } else {
                return dyajaxReturn(0, '收藏失败',['status'=>false,'num'=>$num]);
            }
        }else{
            //取消点赞
            $where['aim_id'] = $aim_id;
            $where['user_id'] = $user;
            $res = Db::name('user_collect_home')->where($where)->delete();
            if ($res) {
                Db::name('user_house')->where('house_id', $aim_id)->dec('collect')->update();
                return dyajaxReturn(1, '取消收藏成功',['status'=>false,'num'=>$num-1]);
            }else{
                if($user_collect){
                    return dyajaxReturn(0, '取消收藏失败',['status'=>true,'num'=>$num]);
                }
                return dyajaxReturn(1, '取消收藏成功',['status'=>false,'num'=>$num]);
            }
        }
    }
}