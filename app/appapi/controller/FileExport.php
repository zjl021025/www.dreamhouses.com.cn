<?php

namespace app\appapi\controller;

use app\common\logic\PageLogic;
use app\common\util\Excel;
use think\facade\Db;

class FileExport
{
    /**
     * 文件数据
     * @return void
     */
    public function fileList()
    {
        $is_select = input('is_select');
        $user_id = dydecrypt(input('user_id'));
        if ($is_select == 1) {
            //我的作品
            $userMyWorkData = Db::name('user_works')->where('user_id',$user_id)->select()->toArray();
            if(empty($userMyWorkData)){
                return dyjsonReturn(0,'查询失败');
            }
            return dyjsonReturn(1,'查询成功',$userMyWorkData);
        } elseif ($is_select == 0) {
            //我的家
            $userMyHomeData = Db::name('user_dynamic')->where('user_id',$user_id)->select()->toArray();
            if(empty($userMyHomeData)){
                return dyjsonReturn(0,'查询失败');
            }
            return dyjsonReturn(1,'查询成功',$userMyHomeData);
        }
    }

    /**
     * 文件查看
     * @return void
     */
    public function fileLook(){
        $file_id = input('file_id');
        $is_select = input('is_select');
        //获取文件详情
        if($is_select == 1){
            //我的作品详情图片
            $workItemData = Db::name('user_works')->field('images,over_images,image')->where('works_id',$file_id)->find();
            $roomData = Db::name('user_works_room')->field('images,content')->where('works_id',$file_id)->select()->toArray();
            $workItemData['room'] = $roomData;
            $furnitureData = Db::name('user_works_furniture')->field('name,image')->where('works_id',$file_id)->select()->toArray();
            $workItemData['furniture'] = $furnitureData;
            if(empty($workItemData)){
                return dyjsonReturn(0,'查询失败');
            }
            return dyjsonReturn(1,'查询成功',$workItemData);
        }elseif ($is_select == 0){
            //我的家详情
            $dynamicItemData = Db::name('user_dynamic')->field('images,video,over_images')->where('dynamic_id',$file_id)->find();
            $roomData = Db::name('user_dynamic_room')->field('images,content')->where('scheme_id',$file_id)->select()->toArray();
            $dynamicItemData['room'] = $roomData;
            if(empty($dynamicItemData)){
                return dyjsonReturn(0,'查询失败');
            }
            return dyjsonReturn(1,'查询成功',$dynamicItemData);
        }
    }

    /**
     * 文件导出
     * @return void
     */
    public function fileExport(){
        $data=Db::table('jason_user')->select()->toArray();
        //此处说明：解决数字太长尾数变000的问题
        //由于数字回超过十五为，会被显示成0或小数点处理。造成这种情况是由于Excel内置的数值有效范围是15位。超过15位，如果要显示的话，就需要转换成非数字格式。比如文本格式。
        foreach ($data as $key => $value) {
            $data[$key]['fitment_id'] = "\t".$value['fitment_id']."\t";
            $data[$key]['content'] = "\t".$value['content']."\t";
            $data[$key]['images'] = "\t".$value['images']."\t";
            $data[$key]['over_images'] = "\t".$value['over_images']."\t";
            $data[$key]['image'] = "\t".$value['image']."\t";
            $data[$key]['name'] = "\t".$value['regTime']."\t";
        }
        //这里规定列名
        return Excel::export('用户_'.time().'.xlsx',['id','用户名','密码','手机号','上次登陆时间戳','注册时间戳'],$data);
    }

    /**
     * 文件共享查询
     * @return void
     */
    public function fileSharingData(){
        $user_id =dydecrypt(input('user_id'));
        //查询用户信息
//        $userData = Db::name('users')->where('user_id',$user_id)->find();
        //查询该用户下的文件
        $fileData = Db::name('user_dynamic')->where('user_id',$user_id)->select()->toArray();
        if(empty($fileData)){
            return dyjsonReturn(0,'该用户没有文件');
        }
        return dyjsonReturn(1,'查询成功',$fileData);
    }

    /**
     * 文件共享
     * @return void
     */
    public function fileSharing(){
        $user_mobile =input('mobile');
        $file_id = input('file_id');
        //验证账号
        $userData = Db::name('users')->where('mobile',$user_mobile)->find();
        if(empty($userData)){
            return dyjsonReturn(0,'该用户暂未注册凡华梦想家客户端');
        }
        //分享文件
        $fileData = Db::name('user_dynamic')->where('dynamic_id',$file_id)->find();
        $fileData['room'] = Db::name('user_dynamic_room')->where('scheme_id',$file_id)->select()->toArray();
        if(empty($fileData)){
            return dyjsonReturn(0,'共享失败');
        }
        return dyjsonReturn(1,'共享成功',$fileData);
    }
}