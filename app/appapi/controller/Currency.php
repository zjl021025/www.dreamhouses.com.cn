<?php

 
namespace app\appapi\controller;

use app\common\logic\DynamicLogic;
use app\common\logic\FurnitureLogic;
use think\facade\View;
use think\facade\Db;
use think\facade\Cache;

class Currency extends Base {

	/*获取区域集合*/
    public function getregionlist(){
		$list = Db::name('region')->field('id region_id,name region_name,pid,level')->where(['level'=>1])->select()->toArray();
        foreach($list as &$v1){
            $v1['regiontwo'] = Db::name('region')->field('id region_id,name region_name,pid,level')->where(['pid'=>$v1['region_id'],'level'=>2])->select()->toArray();
            foreach($v1['regiontwo'] as &$v2){
                $v2['regionthree'] = Db::name('region')->field('id region_id,name region_name,pid,level')->where(['pid'=>$v2['region_id'],'level'=>3])->select()->toArray();
			}
        }
		if($list){
            return dyajaxReturn(1,'区域集合',$list); 
		}else{
            return dyajaxReturn(0,'暂无数据');
		}
    }

    public function sendsms(){
		$scene = input('scene/d',1);//1=注册 2=手机号登录 3=绑定手机号验证 4=找回密码  5=绑定新手机号  6=注销账号
        $_mobile = input('mobile/s',false);
        if(empty($_mobile)) return dyajaxReturn(0,'请输入手机号码');
        $mobile = dydecrypt($_mobile);
        if(empty($mobile)) return dyajaxReturn(0,'手机号码异常');
        if(validateForm('mobile',$mobile)!==''){
            return dyajaxReturn(0,'手机号码不正确');
        }
        if($scene==0) return dyajaxReturn(0,'短信应用场景不正确');
        $session_id = input('unique_id' , session_id());

        $newmobile = Db::name('users')->field('user_id,mobile')->where(['mobile'=>$mobile,'is_deleted'=>0])->find();
        switch($scene){
            case 1://注册
            case 5://绑定新手机号
                if($newmobile) return dyajaxReturn(0,'您输入的手机号码已存在');
                break;
            case 2://手机号登录
//                if(!$newmobile) return dyajaxReturn(0,'您输入的手机号码不存在');
//                break;
            case 3://绑定手机号
//                if(!$newmobile) return dyajaxReturn(0,'您输入的手机号码不存在');
                break;
            case 4://找回密码
                if(!$newmobile) return dyajaxReturn(0,'您输入的手机号码不存在');
                break;
            case 6://注销账号
                if(!$newmobile) return dyajaxReturn(0,'手机号码不存在');
                break;
        }
        //获取手机号每天获取次数(0为无限次)
        $sms_today_count = dyCache('basic.sms_today_count');
        if ($sms_today_count > 0) {
            $smscount = Db::name('sms_log')->where(['mobile'=>$mobile])->whereTime('add_time','today')->count();
            if($smscount>$sms_today_count)  return dyajaxReturn(0,'您的手机号码'.$mobile.'获取短信超出了当天的限制');
        }
        //判断是否存在验证码
		$datas = Db::name('sms_log')->where(array('mobile'=>$mobile,'session_id'=>$session_id, 'status'=>1))->order('id DESC')->find();
		//获取时间配置
		$sms_time_out = 60; 
		//120秒以内不可重复发送
        if($datas && (time() - $datas['add_time']) < $sms_time_out) return dyajaxReturn(0,$sms_time_out.'秒内不允许重复发送');
		//随机一个验证码
		$params['smscode'] = rand(100000, 999999);
        $session_id = input('unique_id' , session_id());
		//发送短信
		$resp = send_sms($scene , $mobile , $params, $session_id);
		if($resp['errcode'] == 1){
			//发送成功, 修改发送状态位成功
			 Db::name('sms_log')->where(array('mobile'=>$mobile,'code'=>$params['smscode'],'session_id'=>$session_id , 'status' => 0))->save(array('status' => 1));
             return dyajaxReturn(1,'短信发送成功,请注意查收');
//             return dyajaxReturn(1,'短信发送成功,请注意查收',[$params['smscode']]);
		}else{
             return dyajaxReturn(0,$resp['message']);
		}
	}

    //获取协议详情
    public function agreementinfo()
    {
        $typeid = input('typeid');
        $cache_name = "agreement_".$typeid;
        $res = Cache::get($cache_name);//判断缓存是否存在
        if (!$res) {
            $where[] = ['article_id','=',$typeid];
            $field = "title,intro,content";
            $res = Db::name('article')
                    ->where($where)
                    ->field($field)
                    ->find();
            if ($res) {
                Cache::set($cache_name, $res);
            }
        }
        View::assign('detail', $res);
        return View::fetch('detail');
    }

    //获取协议详情
    public function article()
    {
        $article_id = input('article_id');
        $where[] = ['article_id','=',$article_id];
        $field = "title,intro,content";
        $res = Db::name('article')
            ->where($where)
            ->field($field)
            ->find();
        View::assign('detail', $res);
        return View::fetch('detail');
    }

    /* 
     * 通用上传接口 
     * @param $typeid  附件栏目 1=认证 2=用户头像  3=方案、图文、视频、作品  4=渲染图册，图片、视频、户型
     * @param $filenames  文件域 
     * @param $position  存放目录
     * @param $is_multiple  是否支持多张：yes-多张，no-单张
     * @param $is_type  类型：1-图片，2-文档附件，3-视频文件
    */
    public function dyupload(){
        $typeid = input('typeid/d',0);//
        $_is_multiple = input('is_multiple/d',1); //true false
        $is_multiple = $_is_multiple == 2?'yes':'no';
        $is_type = input('is_type/d',1);
        //$dyfile = $is_multiple == 'yes'? input('dyfile/a') : input('dyfile/s');
        $dyfile = "dyfile";
        $position = input('dypath/s','goods');
        switch($typeid){
            case 1: $position = 'authen';
                break;
            case 2: $position = 'user';
                break;
            case 3: $position = 'user_dynamic';
                break;
//            case 4: $position = 'resource';
            case 4: $position = 'user_dynamic';
                break;
        }
        $iswater = 0;
        if(in_array($typeid,[3,4])){
            $iswater = $this->user['is_water'];
        }
        return dymustupload($dyfile,$position,$is_multiple,$is_type,$iswater);
    }

    public function getMicrotime()
    {
        list($microsecond , $time) = explode(' ', microtime());
        return $microsecond;
    }
    /*
         * 通用上传接口
         * @param 4=渲染图册，图片、视频、户型
         * @param $filenames  文件域
         * @param $position  存放目录
         * @param $is_multiple  是否支持多张：yes-多张，no-单张
         * @param $is_type  类型：1-图片，2-文档附件，3-视频文件
        */
    public function UnityUpload()
    {
        $dydir = 'public/backup/UnityUpload/'.date("Y").'/';
        if(!is_dir($dydir)) @mkdir($dydir,0755,true);
        $dydir = $dydir.date("m-d").'/';
        if(!is_dir($dydir)) @mkdir($dydir,0755,true);
        //写入文件做日志 调试用
        $log = "\r\n\r\n".'=========start=========='."\r\n".date("Y-m-d H:i:s")."\r\n".$this->getMicrotime();
        @file_put_contents($dydir.date("Y-m-d").'.log', $log, FILE_APPEND);
        $typeid = 4;//
        $is_type = input('is_type/d',1);
        $dyfile = $_FILES['dyfile'];
        $position = input('dypath/s','goods');
        switch($typeid){
            case 1: $position = 'authen';
                break;
            case 2: $position = 'user';
                break;
            case 3: $position = 'user_dynamic';
                break;
            case 4: $position = 'user_dynamic';
                break;
        }
        $dydir = 'public/backup/UnityUpload/'.date("Y").'/';
        if(!is_dir($dydir)) @mkdir($dydir,0755,true);
        $dydir = $dydir.date("m-d").'/';
        if(!is_dir($dydir)) @mkdir($dydir,0755,true);
        //写入文件做日志 调试用
        $log = "\r\n\r\n".'=========$_POST=========='."\r\n".date("Y-m-d H:i:s")."\r\n".json_encode(json_encode($_POST));
        $log .= "\r\n\r\n".'=========$_FILES=========='."\r\n".date("Y-m-d H:i:s")."\r\n".json_encode(json_encode($_FILES));
        $log .= "\r\n\r\n".'===========dyfile========'."\r\n".date("Y-m-d H:i:s")."\r\n".json_encode(json_encode($_FILES['dyfile']));
        @file_put_contents($dydir.date("Y-m-d").'.log', $log, FILE_APPEND);
        $iswater = 0;
        if(in_array($typeid,[3,4])){
            $iswater = $this->user['is_water'];
        }
        $position = $position.'/';
        switch ($is_type) {
            case 1:
                //图片
                $ext = 'jpg,png,jpeg';
                $maxsize = config('user_image_upload_limit_size');//默认值为1MB
                $extmsg = 'jpg,png,jpeg格式的图片';
                break;
            case 2:
                //附件
                $ext = 'zip,rar,xls,xlsx,doc,docx,ppt,pptx';
                $maxsize = config('user_file_upload_limit_size');//默认值为5MB
                $extmsg = '附件';
                break;
            case 3:
                //视频
                $ext = 'mp4';
                $maxsize = config('user_video_upload_limit_size');//默认值为10MB(改为40MB)
                $extmsg = '视频';
                break;
        }
        $originalName = $dyfile['name'];
        if (strstr($originalName, '.php') || strstr($originalName, '.js')) {
            $state = '文件格式错误';
        }
        if ($dyfile['size'] > $maxsize) {
            $state = '上传失败,文件超出大小,请选择'.floor($maxsize/1024/1024) . 'MB以内的文件。';
        }
        $extension = strtolower(explode('/',$dyfile['type'])[1]);
        if (!in_array($extension, explode(',', $ext))) {
            $state = '仅可上传'.$extmsg.'文件';
        }
        if ($state) {
            return dyajaxReturn(0, $state);
        }
//                    $savename = md5(mt_rand()).'.'.$val->extension();
        $savename = 'filename_'.uniqid().random_int(100000000,999999999).'.'.$extension;
        $filepath = $position.date('Y/m-d').'/';
        $file_path = UPLOAD_PATH.$filepath;
        if (!(file_exists($file_path))) {
            @mkdir($file_path, 0755, true);
        }
        $pic_path = UPLOAD_PATH.$filepath.$savename;
        $myFile = $_FILES['dyfile']["tmp_name"];
        if(!move_uploaded_file($myFile,$pic_path)){
            return dyajaxReturn(0, '上传失败');
        }
        if($is_type==1){
            $pic_path = set_water('./'.$pic_path,$iswater,$is_type);
        }else{
            if($is_type==3 && $iswater==1){
                dyappsetcache('/'.$pic_path,1,10);
            }
            $pic_path = '/'.$pic_path;
        }
        $log = "\r\n\r\n".'=========path=========='."\r\n".date("Y-m-d H:i:s")."\r\n".rtrim($pic_path, ',');
        $log .= "\r\n\r\n".'=========end=========='."\r\n".date("Y-m-d H:i:s")."\r\n".$this->getMicrotime();
        @file_put_contents($dydir.date("Y-m-d").'.log', $log, FILE_APPEND);
        return dyajaxReturn(1, '附件上传成功', rtrim($pic_path, ','));
    }

    /* 生成缩略图-示例 */
    public function dythumb(){
        $info = dythumbimages(5, 300, 300, 300, 'goods');
        print_r($info);        
    }

    //获取搜索选项
    public function getOptionList()
    {
        $label = Db::name('label')
            ->where(['is_show'=>1,'typeid'=>['in',[1,2,3,4,6,7]]])
            ->cache(true,60*30)
            ->order('sort asc,id desc')
            ->field('id,name,typeid')->select()->toArray();
        $designer_expe = [];
        $designer_style = [];
        $designer_price = [];
        $house_type = [];
        $budget = [];
        $area = [];
        foreach ($label as $v){
            switch ($v['typeid']){
                case 1:
                    unset($v['typeid']);
                    $designer_expe[] = $v;
                    break;
                case 2:
                    unset($v['typeid']);
                    $designer_style[] = $v;
                    break;
                case 3:
                    unset($v['typeid']);
                    $designer_price[] = $v;
                    break;
                case 4:
                    unset($v['typeid']);
                    $house_type[] = $v;
                    break;
                case 6:
                    unset($v['typeid']);
                    $budget[] = $v;
                    break;
                case 7:
                    unset($v['typeid']);
                    $v['name'] = $v['name']."㎡";
                    $area[] = $v;
                    break;
            }
        }
        $data = [
            'expe'=>$designer_expe,
            'style'=>$designer_style,
            'price'=>$designer_price,
            'house_type'=>$house_type,
            'budget'=>$budget,
            'area'=>$area,
            'designer_type'=>[
                [
                    "id"=>1,
                    "name"=>"独立设计师",
                ],
                [
                    "id"=>2,
                    "name"=>"机构设计师",
                ]
            ],
        ];
        return dyajaxReturn(1,'获取选项成功',$data);
    }

    //验证验证码
    public function validateCode()
    {
        $scene = input('scene/d',3);//1=注册 2=手机号登录 3=绑定手机号验证 4=找回密码  5=绑定新手机号
        $_mobile = input('mobile/s',false);
        if(empty($_mobile)) return dyajaxReturn(0,'请输入手机号码');
        $mobile = dydecrypt($_mobile);
        if(empty($mobile)) return dyajaxReturn(0,'手机号码异常');
        if(validateForm('mobile',$mobile)!==''){
            return dyajaxReturn(0,'手机号码不正确');
        }
        if($scene==0) return dyajaxReturn(0,'短信应用场景不正确');
        $code = input('smscode/s');//手机验证码
        if(empty($code)) return dyajaxReturn(0,'请输入验证码');
        //验证验证码
        $check = check_validate_code($code, $mobile,$scene);
        return dyajaxReturn($check['errcode'],$check['message']);
    }

    //获取版本信息
    public function version()
    {
        $cache_name = "agreement_4";
        $res = Cache::get($cache_name);//判断缓存是否存在
        if (!$res) {
            $where[] = ['article_id','=',4];
            $field = "title,intro,content";
            $res = Db::name('article')
                ->where($where)
                ->field($field)
                ->find();
            if ($res) {
                Cache::set($cache_name, $res);
            }
        }
        $data['version'] = config('ios_version');
        $data['content'] = $res['content'];
        return dyajaxReturn(1,'获取版本信息',$data);
    }

    //获取协议简要描述
    public function agreementintro()
    {
        $typeid = input('typeid');
        $cache_name = "agreement_".$typeid;
        $res = Cache::get($cache_name);//判断缓存是否存在
        if (!$res) {
            $where[] = ['article_id','=',$typeid];
            $field = "title,intro,content";
            $res = Db::name('article')
                ->where($where)
                ->field($field)
                ->find();
            if ($res) {
                Cache::set($cache_name, $res);
            }
        }
        $data['title'] = $res['title'];
        $data['url'] = SITE_URL.url("Currency/agreementinfo",['typeid'=>$typeid]);
        $data['intro'] = $res['intro'];
        return dyajaxReturn(1,'获取协议简要描述',$data);
    }

    public function dynamic()
    {
        $typeid = input('typeid/d');//1=图文 2=视频 3=方案
        $tpl = 'imagetext';
        switch ($typeid){
            case 1:
                $field = 'dynamic_id,title,content,images,user_id,scheme_id,praise,collect,evaluate,typeid';
                $tpl = 'imagetext';
                break;
            case 2:
                $field = 'dynamic_id,content,video,user_id,scheme_id,praise,collect,evaluate,typeid,image';
                $tpl = 'video';
                break;
            case 3:
                $field = 'dynamic_id,title,content,images,user_id,praise,collect,evaluate,house_type_id,style_id,area,over_content,over_images,typeid,province_id,city_id,district_id';
                $tpl = 'scheme';
                break;
            default:
                return die('类型错误');
        }
        $dynamic_id = input('dynamic_id/d');
        if(!$dynamic_id){
            return die('缺少信息ID');
        }
        $DynamicLogic = new DynamicLogic();
        $info = $DynamicLogic->getDynamic($typeid,$dynamic_id,0,$field);
        $info['android'] = 'dreamhouse://com.dykj.online_home_decoration/main';
        $info['ios'] = 'onlineohousedecorationapp://';
        $info['android_url'] = '';
        $info['ios_url'] = '';
        View::assign('info',$info);
        return View::fetch($tpl);
    }

    public function furniture()
    {
        $id = input('id/d');
        $FurnitureLogic = new FurnitureLogic();
        $info = $FurnitureLogic->getFurniture($id,0);
        View::assign('info',$info);
        $type = 4;
        $cate_id = $info['cate_id'];//分类ID
        $where = [];
        if($cate_id){
            $where['a.cate_id'] = $cate_id;
        }
        switch ($type){
            case 4:
                $where['a.cate_id'] = $cate_id;
                $where['a.id'] = ['<>',$id];
                break;
        }
        $FurnitureLogic = new FurnitureLogic();
        $list = $FurnitureLogic->getFurnitureList($type,$where,0);
        View::assign('list',$list);
        return View::fetch('furniture');
    }

    public function guide()
    {
        $guide = Cache::get('api_guide');
        if(!$guide){
            $guide = Db::name('guide')->field('image,bg')->where('is_show',1)->order('sort asc,id desc')->select()->toArray();
            if(!count($guide)){
                return dyajaxReturn(0, '');
            }
            foreach ($guide as $k=>$v){
                $guide[$k]['img_width'] = 0;
                $guide[$k]['img_height'] = 0;
                $guide[$k]['bg_width'] = 0;
                $guide[$k]['bg_height'] = 0;
                if($v['image']){
                    $guide[$k]['image'] = SITE_URL.$v['image'];
                    list($width, $height) = getimagesize($guide[$k]['image']);
                    $guide[$k]['img_width'] = $width;
                    $guide[$k]['img_height'] = $height;
                }else{
                    $guide[$k]['image'] = '';
                }
                if($v['bg']){
                    $guide[$k]['bg'] = SITE_URL.$v['bg'];
                    list($width, $height) = getimagesize($guide[$k]['bg']);
                    $guide[$k]['bg_width'] = $width;
                    $guide[$k]['bg_height'] = $height;
                }else{
                    $guide[$k]['bg'] = '';
                }
            }
            if($guide){
                Cache::set('api_guide',$guide);
            }
        }
        return dyajaxReturn(1, '',$guide);
    }
}
