<?php

namespace app\appapi\controller;

use app\common\validate\Property;
use PhpOffice\PhpSpreadsheet\Calculation\Database\DVar;
use think\cache\driver\Redis;
use think\db\exception\DbException;
use think\Exception;
use think\facade\Db;
use think\facade\Log;
use think\facade\Request;
use function dyajaxReturn;
use function dydecrypt;
use function dyjsonReturn;
use function env;
use function input;

//开始设计
class Design
{
    /**
     * 开始设计
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function startDesign()
    {
        $userId = input('user_id', 0);
        $user_id = dydecrypt($userId);
        if (!$user_id) {
            return dyajaxReturn(0, '用户数据为空，请登录重试');
        }
        //获取用户家的数据
        $houseData = Db::name('user_house')->where(['user_id' => $user_id, 'is_deleted' => 0])->select()->toArray();
        $label = Db::name('label')->where('typeid', 4)->select()->toArray();
        foreach ($houseData as $key => $val) {
            foreach ($label as $k => $v) {
                if ($v['id'] == $val['house_type_id']) {
                    $houseData[$key]['house_type'] = $v['name'];
                }
            }
            $houseData[$key]['add_time'] = date('Y-m-d', $val['add_time']);
        }
        if (empty($houseData)) {
            return dyjsonReturn(0, '您还未上传您的家，请先添加哦！！');
        } else {
            return dyjsonReturn(1, '获取成功', $houseData);
        }
    }

    /**
     * 获取户型数据和地区数据
     * @return void
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function getLabel()
    {
        //户型
        $label = Db::name('label')->where(['typeid' => 4, 'is_show' => 1])->select()->toArray();
        // 获取第一级地区
        $provinceList = Db::name('region')->where('pid', 100000)->select()->toArray();
        // 循环获取第二级地区，并将其作为属性保存到对应的省级数组中
        foreach ($provinceList as $key => $province) {
            $cityList = Db::name('region')->where('pid', $province['id'])->select()->toArray();
            $provinceList[$key]['cities'] = $cityList;

        }
        $room = Db::name('label')->where(['typeid' => 5, 'is_show' => 1])->select()->toArray();
        return dyjsonReturn(1, '获取成功', ['label' => $label, 'city' => $provinceList, 'room' => $room]);
    }


    /**
     * 用户上传楼盘
     * @return \think\response\Json|void
     */
    public function uploadProperty()
    {
        $validateData = [
            'city_id' => input('city_id'),
            'cellName' => input('cellName'),
            'house_type_id' => input('house_type_id'),
            'area' => input('area'),
            'images' => input('images'),
            'user_id' => dydecrypt(input('user_id')),
            'videos' => input('videos')
        ];
        $dydir = 'public/backup/addResource/' . date("Y") . '/';
        if (!is_dir($dydir)) @mkdir($dydir, 0755, true);
        $dydir = $dydir . date("m-d") . '/';
        if (!is_dir($dydir)) @mkdir($dydir, 0755, true);
        //写入文件做日志 调试用
        $log = "\r\n\r\n" . '=========$param==========' . "\r\n" . date("Y-m-d H:i:s") . "\r\n" . json_encode(json_encode($validateData));
        @file_put_contents($dydir . date("Y-m-d") . '.log', $log, FILE_APPEND);
        $validate = new Property();
        $result = $validate->check($validateData);
        if (!$result) {
            return dyjsonReturn(0, $validate->getError());
        }
        //解析图片
        $imageData = json_decode(htmlspecialchars_decode($validateData['images']), true);
        $videoData = json_decode(htmlspecialchars_decode($validateData['videos']), true);
        //获取所属一级地区
        $province = Db::name('region')->field('pid')->where('id', $validateData['city_id'])->find();
        //将数据添加入库
        $data = [
            'city_id' => $validateData['city_id'],
            'name' => $validateData['cellName'],
            'house_type_id' => $validateData['house_type_id'],
            'area' => $validateData['area'],
            'user_id' => $validateData['user_id'],
            'province_id' => $province['pid'],
            'add_time' => time()
        ];
        // 开启事务
        Db::startTrans();
        try {
            $addId = Db::name('house')->insertGetId($data);
            foreach ($imageData as $key => $val) {
                $labelName = Db::name('label')->field('name')->where('id', $key)->find();
                $insertData = [
                    'content' => $labelName['name'],
                    'images' => $val,
                    'add_time' => time(),
                    'house_id' => $addId,
                    'room_id' => $key,
                ];
                try {
                    Db::name('house_room')->insert($insertData);
                } catch (\Exception $exception) {
                    throw new $exception->getMessage();
                }
            }
            if($validateData['videos']){
                foreach ($videoData as $key => $val) {
                    $labelName = Db::name('label')->field('name')->where('id', $key)->find();
                    $insertData = [
                        'content' => $labelName['name'],
                        'videos' => $val,
                        'add_time' => time(),
                        'house_id' => $addId,
                        'room_id' => $key,
                    ];
                    try {
                        Db::name('house_room')->insert($insertData);
                    } catch (\Exception $exception) {
                        throw new $exception->getMessage();
                    }
                }
            }
            // 提交事务
            Db::commit();
            return dyjsonReturn(1, '您上传的户型我们已收到，正在为您抓紧绘制');
        } catch (\Exception $exception) {
            Db::rollback();
            //写入文件做日志 调试用
            $log = "\r\n\r\n" . '=========$param==========' . "\r\n" . date("Y-m-d H:i:s") . "\r\n" . $exception->getMessage();
            @file_put_contents($dydir . date("Y-m-d") . '.log', $log, FILE_APPEND);
            return dyjsonReturn(0, '添加失败，请重新上传');
        }
    }

    /**
     * 搜索功能
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function searchHouses()
    {
        $keyword = input('keyword', 0);
        $city = input('city');
        if (empty($keyword)) {
            return dyjsonReturn(0, '请输入搜索内容');
        }
        if (empty($city)) {
            $estateData = Db::name('estate')->where(['is_deleted' => 0, 'is_show' => 1, ['name', 'like', "%$keyword%"]])->select()->toArray();
        } else {
            $cityData = Db::name('region')->where('name','like',"%$city%")->find();
            $estateData = Db::name('estate')->where(['is_deleted' => 0, 'is_show' => 1, ['name', 'like', "%$keyword%"], 'city_id' => $cityData['id']])->select()->toArray();
        }
        //获取楼盘下所有户型信息
        foreach ($estateData as $ks => $vs) {
            $array[] = Db::name('scene_house')->where('estate_id', $vs['estate_id'])->where(['is_show' => 1, 'is_deleted' => 0])->select()->toArray();
        }
        $dynamicData = [];
        foreach ($array as $vals) {
            foreach ($vals as $val) {
                array_push($dynamicData, $val);
            }
        }
        $estate = Db::name('estate')->where(['is_deleted' => 0, 'is_show' => 1])->select()->toArray();
        $label = Db::name('label')->select()->toArray();
        $data = [];
        foreach ($dynamicData as $key => $val) {
            foreach ($label as $keys => $value) {
                if ($value['typeid'] == 4) {
                    if ($val['house_type_id'] == $value['id']) {
                        $data[] = $value['name'];
                        $dynamicData[$key]['room'] = $value['name'];
                    }
                }
                if ($value['typeid'] == 9) {
                    if ($val['style_id'] == $value['id']) {
                        $data[] = $value['name'];
                    }
                }
            }
            foreach ($estate as $k => $v) {
                if ($val['estate_id'] == $v['estate_id']) {
                    $dynamicData[$key]['estate'] = $v['name'];
                }
            }
            $data[] = $val['area'] . 'm²';
            $dynamicData[$key]['house_type'] = $data;
            $data = [];
        }
        if (!$dynamicData) {
            return dyjsonReturn(0, '非常抱歉，未能搜索到您的小区，您可以查看平台其他户型');
        }
        return dyjsonReturn(1, '搜索成功', $dynamicData);
    }

    /**
     * 楼盘详情
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function housesDetails()
    {
        $dynamic_id = input('dynamic_id', 0);
        $user_id = dydecrypt(input('user_id'));
        $dynamicData = Db::name('scene_house')->alias('dy')->leftjoin('label', 'dy.style_id = label.id')
            ->field('dy.house_id,dy.name,dy.house_type_id,dy.estate_id,dy.area,dy.image,dy.add_time,dy.room_ids,dy.house_image,dy.images,dy.over_images,dy.over_content,dy.style_id,dy.ios_url,dy.Android_url,dy.pc_url,dy.browse')
            ->where('dy.house_id', $dynamic_id)->find();
        $dynamicRoomData = Db::name('scene_house_room')->where('house_id', $dynamic_id)->select()->toArray();
        $dynamicData['label'] = $dynamicRoomData;
        $isInsert = Db::name('user_house')->where(['scene_id' => $dynamic_id, 'is_deleted' => 0, 'is_show' => 1, 'user_id' => $user_id])->find();
        if (!empty($isInsert)) {
            $dynamicData['is_myhome'] = 1;
        } else {
            $dynamicData['is_myhome'] = 0;
        }
        $label = Db::name('label')->select()->toArray();
        $data = [];
        foreach ($label as $key => $value) {
            foreach ($dynamicData['label'] as $k => $v) {
                if ($value['typeid'] == 5) {
                    if ($v['room_id'] == $value['id']) {
                        $dynamicData['label'][$k]['title'] = $value['name'];
                    }
                }
            }
            if ($value['typeid'] == 4) {
                if ($dynamicData['house_type_id'] == $value['id']) {
                    $data[] = $value['name'];
                }
            }
            if ($value['typeid'] == 2) {
                if ($dynamicData['style_id'] == $value['id']) {
                    $data[] = $value['name'];
                }
            }
        }
        $data[] = $dynamicData['area'] . 'm²';
        $dynamicData['house_type'] = $data;
        if (!$dynamicData) {
            Log::error('数据搜索失败');
            return dyjsonReturn(0, '查询失败');
        }
        //记录浏览量
        $data = [
            'user_id' => $user_id,
            'aim_id' => $dynamic_id,
            'add_time' => time()
        ];
        $num = $dynamicData['browse'];
        Db::startTrans();
        try {
            Db::name('scene_browse')->insert($data);
            Db::name('scene_house')->where(['house_id' => $dynamic_id])->update(['browse' => $num + 1]);
            Db::commit();
        } catch (\Exception $exception) {
            Db::rollback();
            return dyjsonReturn(0, '查询失败');
        }

        return dyjsonReturn(1, '查询成功', $dynamicData);
    }

    /**
     * 智能设计+我的家（是）
     * @return void
     */
    public function inMyhomeYes()
    {
        $user_id = dydecrypt(input('user_id'));
        $details_id = input('details_id');
        //查找楼盘信息
        $detailsData = Db::name('scene_house')->where('house_id', $details_id)->find();
        //获取对应图片
        $detailsImages = Db::name('scene_house_room')->where('house_id', $detailsData['house_id'])->select()->toArray();
        // 开启事务
        Db::startTrans();
        try {
            //添加我的家
            $data = [
                'name' => $detailsData['name'],
                'images' => $detailsData['images'],
                'add_time' => time(),
                'user_id' => $user_id,
                'style_id' => $detailsData['style_id'],
                'house_type_id' => $detailsData['house_type_id'],
                'area' => $detailsData['area'],
                'over_images' => $detailsData['over_images'],
                'over_content' => $detailsData['over_content'],
                'image' => $detailsData['image'],
                'estate_id' => $detailsData['estate_id'],
                'room_ids' => $detailsData['room_ids'],
                'sort' => $detailsData['sort'],
                'house_image' => $detailsData['house_image'],
                '3d_name' => $detailsData['3d_name'],
                'scene_id' => $detailsData['house_id'],
                'ios_url' => $detailsData['ios_url'],
                'Android_url' => $detailsData['Android_url'],
                'pc_url' => $detailsData['pc_url']
            ];
            $addId = Db::name('user_house')->insertGetId($data);
            foreach ($detailsImages as $val) {
                //添加对应的图片
                Db::name('user_house_room')->insert(['content' => $val['content'], 'images' => $val['images'], 'add_time' => time(), 'house_id' => $addId, 'room_id' => $val['room_id']]);
            }
            Db::commit();
            return dyjsonReturn(1, '添加成功');
        } catch (\Exception $exception) {
            Db::rollback();
            return dyjsonReturn(0, '添加失败');
        }
    }

    /**
     * 删除我的家
     * @return void
     */
    public function delMyhome()
    {
        $house_id = input('house_id');
        $user_id = dydecrypt(input('user_id'));
        $del = Db::name('user_house')->where(['user_id' => $user_id, 'house_id' => $house_id])->update(['is_deleted' => 1, 'is_show' => 0]);
        if (!$del) {
            return dyjsonReturn(0, '删除失败');
        }
        return dyjsonReturn(1, '删除成功');
    }

    /**
     * 获取方案视频
     * @return void
     */
    public function getDynamicVideo()
    {
        $user_id = dydecrypt(input('user_id'));
        $startPage = input('startPage');
        $pageNum = input('pageNum');
        $where['dy.video'] = ['<>', ''];
        $where['dy.is_deleted'] = 0;
        $where['dy.is_show'] = 1;
        $dynamicData = Db::name('user_dynamic')
            ->alias('dy')
            ->field('dy.dynamic_id,dy.title,dy.content,dy.image,dy.video,dy.praise,dy.collect,dy.evaluate,dy.user_id,users.avatar,dy.ios_url,dy.Android_url,dy.pc_url')
            ->leftJoin('users', 'dy.user_id = users.user_id')
            ->where($where)->order(['dy.dynamic_id' => 'desc'])->limit($startPage, $pageNum)->select()->toArray();
        foreach ($dynamicData as $key => $val) {
            //点赞
            $dynamicData[$key]['praise'] = Db::name('user_praise')->where('aim_id', $val['dynamic_id'])->count();
            //收藏
            $dynamicData[$key]['collect'] = Db::name('user_collect')->where(['aim_id' => $val['dynamic_id']])->count();
            //评论
            $dynamicData[$key]['evaluate'] = Db::name('evaluate')->where(['dynamic_id' => $val['dynamic_id'], 'is_show' => 1])->count();
            if($user_id){
                $praise = Db::name('user_praise')->where(['user_id' => $user_id, 'aim_id' => $val['dynamic_id']])->find();
                if (!$praise) {
                    $dynamicData[$key]['is_praise'] = 0;
                } else {
                    $dynamicData[$key]['is_praise'] = 1;
                }
                $collect = Db::name('user_collect')->where(['user_id' => $user_id, 'aim_id' => $val['dynamic_id']])->find();
                if (!$collect) {
                    $dynamicData[$key]['is_collect'] = 0;
                } else {
                    $dynamicData[$key]['is_collect'] = 1;
                }
            }
        }
        if (empty($dynamicData)) {
            return dyjsonReturn(0, '查询失败');
        }
        return dyjsonReturn(1, '查询成功', $dynamicData);
    }

    /**
     * 点击房间标签
     * @return void
     */
    public function clickRoom(){
        $dynamic_id = input('dynamic_id');
        //每个房间
        $roomData = Db::name('user_dynamic_room')->where(['scheme_id' => $dynamic_id])->select()->toArray();
        foreach ($roomData as $k => $v) {
            $roomData[$k]['title'] = Db::name('label')->where(['id' => $v['room_id']])->value('name');
        }
        if(empty($roomData)){
            return dyjsonReturn(0,'该方案房间信息为空');
        }
        return dyjsonReturn(1,'查询成功',$roomData);
    }

    /**
     * 获取方案详情
     * @return void
     */
    public function getDynamicItem()
    {
        $user_id = dydecrypt(input('user_id'));
        $dynamic_id = input('dynamic_id');
        //查找对应的方案
        $itemData = Db::name('user_dynamic')->field('dynamic_id,title,content,images,user_id,video,style_id,house_type_id,area,image,praise,collect,evaluate,browse,house_image,hits,ios_url,Android_url,pc_url')
            ->where(['dynamic_id' => $dynamic_id])->find();
        $praise = Db::name('user_praise')->where(['user_id' => $user_id, 'aim_id' => $dynamic_id])->find();
        $itemData['praise'] = Db::name('user_praise')->where('aim_id', $dynamic_id)->count();
        if (!$praise) {
            $itemData['is_praise'] = 0;
        } else {
            $itemData['is_praise'] = 1;
        }
        //收藏
        $collect = Db::name('user_collect')->where(['user_id' => $user_id, 'aim_id' => $dynamic_id])->find();
        $itemData['collect'] = Db::name('user_collect')->where(['aim_id' => $dynamic_id])->count();
        if (!$collect) {
            $itemData['is_collect'] = 0;
        } else {
            $itemData['is_collect'] = 1;
        }
        //banner
        $banner = explode(',', $itemData['images']);
        array_unshift($banner, $itemData['video']);
        $itemData['banner'] = $banner;
        //风格
        $itemData['style'] = Db::name('label')->where(['id' => $itemData['style_id']])->value('name');
        //户型
        $itemData['house_type'] = Db::name('label')->where(['id' => $itemData['house_type_id']])->value('name');
        //房间信息
        $roomData = Db::name('user_dynamic_room')->where(['scheme_id' => $dynamic_id])->select()->toArray();
        $room = [];
        foreach ($roomData as $key => $val) {
            array_push($room, Db::name('label')->where(['id' => $val['room_id']])->value('name'));
        }
        $itemData['room'] = $room;
        if (!$itemData) {
            return dyjsonReturn(0, '查询失败');
        }
        //记录浏览量
        $data = [
            'user_id' => $user_id,
            'aim_id' => $dynamic_id,
            'add_time' => time()
        ];
        $num = $itemData['browse'];
        Db::startTrans();
        try {
            Db::name('user_browse')->insert($data);
            Db::name('user_dynamic')->where(['dynamic_id' => $dynamic_id])->update(['browse' => $num + 1]);
            Db::commit();
        } catch (\Exception $exception) {
            Db::rollback();
            return dyjsonReturn(0, '查询失败');
        }
        return dyjsonReturn(1, '查询成功', $itemData);
    }

    /**
     * 点击方案标签
     * @return void
     */
    public function clickDynamicTag()
    {
        $code = input('code');
        $user_id = dydecrypt(input('user_id'));
        $startPage = input('startPage');
        $pageNum = input('pageNum');
        $style_id = input('style_id');
        $city = input('city');
        $where['dy.video'] = ['<>', ''];
        $where['dy.is_deleted'] = 0;
        $where['dy.is_show'] = 1;
        //$code  1 = 热门  2 = 地区（杭州） 3 = 风格
        switch ($code){
            case 1:
                $order = ['dy.browse'=>'desc'];
                break;
            case 2:
                $cityData = Db::name('region')->where('name','like',"%$city%")->find();
                $where['city_id'] = $cityData['id'];
                $order = ['dy.dynamic_id'=>'desc'];
                break;
            case 3:
                $where['style_id'] = $style_id;
                $order = ['dy.dynamic_id'=>'desc'];
                break;
            default:
                return dyjsonReturn(0,'请传入正确的标签值');
        }
        $dynamicData = Db::name('user_dynamic')
            ->alias('dy')
            ->field('dy.dynamic_id,dy.title,dy.content,dy.image,dy.video,dy.praise,dy.collect,dy.evaluate,dy.user_id,users.avatar,dy.ios_url,dy.Android_url,dy.pc_url,dy.browse,dy.style_id')
            ->leftJoin('users', 'dy.user_id = users.user_id')
            ->where($where)->order($order)->limit($startPage, $pageNum)->select()->toArray();
        foreach ($dynamicData as $key => $val) {
            //点赞
            $dynamicData[$key]['praise'] = Db::name('user_praise')->where('aim_id', $val['dynamic_id'])->count();
            //收藏
            $dynamicData[$key]['collect'] = Db::name('user_collect')->where(['aim_id' => $val['dynamic_id']])->count();
            //评论
            $dynamicData[$key]['evaluate'] = Db::name('evaluate')->where(['dynamic_id' => $val['dynamic_id'], 'is_show' => 1])->count();
            if($user_id){
                $praise = Db::name('user_praise')->where(['user_id' => $user_id, 'aim_id' => $val['dynamic_id']])->find();
                if (!$praise) {
                    $dynamicData[$key]['is_praise'] = 0;
                } else {
                    $dynamicData[$key]['is_praise'] = 1;
                }
                $collect = Db::name('user_collect')->where(['user_id' => $user_id, 'aim_id' => $val['dynamic_id']])->find();
                if (!$collect) {
                    $dynamicData[$key]['is_collect'] = 0;
                } else {
                    $dynamicData[$key]['is_collect'] = 1;
                }
            }
        }
        if (empty($dynamicData)) {
            return dyjsonReturn(0, '查询失败');
        }
        return dyjsonReturn(1, '查询成功', $dynamicData);
    }

    /**
     * 获取城市信息
     * @return void
     */
    public function getCity()
    {
        $cityData = Db::name('region')->where(['level' => 2])->select()->toArray();
        foreach ($cityData as $key => $val){
            $val['name'] = pinyin_long($val['name']);
            $cityData[$key]['spell'] = strtoupper(mb_substr($val['name'],0,1));
        }
        if (!$cityData) {
            return dyjsonReturn(0, '数据查询失败');
        }
        return dyjsonReturn(1, '查询成功', $cityData);
    }

    /**
     * 搜索方案
     * @return void
     */
    public function searchDynamic()
    {
        $keyword = input('keyword', 0);
        $city = input('city');
        $user_id = dydecrypt(input('user_id'));
        $startPage = input('startPage');
        $pageNum = input('pageNum');
        $where['dy.video'] = ['<>', ''];
        $where['dy.is_deleted'] = 0;
        $where['dy.is_show'] = 1;
        $where['dy.title'] = ['like', "%$keyword%"];
        if (empty($keyword)) {
            return dyjsonReturn(0, '请输入搜索内容');
        }
        if ($city) {
            $cityData = Db::name('region')->where('name','like',"%$city%")->find();
            $where['city_id'] = $cityData['id'];
        }
        $dynamicData = Db::name('user_dynamic')
            ->alias('dy')
            ->field('dy.dynamic_id,dy.title,dy.content,dy.image,dy.video,dy.praise,dy.collect,dy.evaluate,dy.user_id,users.avatar,dy.ios_url,dy.Android_url,dy.pc_url')
            ->leftJoin('users', 'dy.user_id = users.user_id')
            ->where($where)->order(['dy.dynamic_id' => 'desc'])->limit($startPage, $pageNum)->select()->toArray();
        foreach ($dynamicData as $key => $val) {
            //点赞
            $dynamicData[$key]['praise'] = Db::name('user_praise')->where('aim_id', $val['dynamic_id'])->count();
            //收藏
            $dynamicData[$key]['collect'] = Db::name('user_collect')->where(['aim_id' => $val['dynamic_id']])->count();
            //评论
            $dynamicData[$key]['evaluate'] = Db::name('evaluate')->where(['dynamic_id' => $val['dynamic_id'], 'is_show' => 1])->count();
            if($user_id){
                $praise = Db::name('user_praise')->where(['user_id' => $user_id, 'aim_id' => $val['dynamic_id']])->find();
                if (!$praise) {
                    $dynamicData[$key]['is_praise'] = 0;
                } else {
                    $dynamicData[$key]['is_praise'] = 1;
                }
                $collect = Db::name('user_collect')->where(['user_id' => $user_id, 'aim_id' => $val['dynamic_id']])->find();
                if (!$collect) {
                    $dynamicData[$key]['is_collect'] = 0;
                } else {
                    $dynamicData[$key]['is_collect'] = 1;
                }
            }
        }
        if (!$dynamicData) {
            return dyjsonReturn(0, '非常抱歉，未能搜索到对应方案，您可以查看平台其他方案');
        }
        return dyjsonReturn(1, '搜索成功', $dynamicData);
    }

    /**
     * 保存设计
     * @return void
     */
    public function saveHouse()
    {
        $designjson = input('designjson');
        $dynamic_id = input('dynamic_id');
        $house_id = input('house_id');
        $user_id = dydecrypt(input('user_id'));
        $in_home = input('in_home');
        $works_id = input('works_id');
        $image = input('images');
        $images = explode(',', $image);
        $video = input('videos');
        $videos = explode(',', $video);
        //获取方案信息，将其添加到作品里
        $dynamic = Db::name('user_dynamic')->where(['dynamic_id' => $dynamic_id])->find();
        //获取作品信息，传给前端
        if (empty($works_id)) {
            if (empty($house_id) || empty($in_home)) {
                return dyjsonReturn(0, '数据传输错误');
            }
            if ($in_home == 1) {
                $dynamicData = Db::name('user_house')->where('house_id', $house_id)->find();
                $dynamicRoomData = Db::name('user_house_room')->where('house_id', $dynamicData['house_id'])->select()->toArray();
                //添加作品
                $data = [
                    'title' => $dynamicData['name'],
                    'images' => $dynamicData['images'],
                    'add_time' => time(),
                    'user_id' => $user_id,
                    'style_id' => $dynamic['style_id'],
                    'over_images' => $dynamicData['over_images'],
                    'over_content' => $dynamicData['over_content'],
                    'image' => $dynamicData['image'],
                    'house_id' => $dynamicData['scene_id'],
                    'estate_id' => $dynamicData['estate_id'],
                    'designjson' => $designjson,
                    'house_type_id' => $dynamicData['house_type_id'],
                    'area' => $dynamicData['area'],
                    'house_image' => $dynamicData['house_image'],
                    'province_id' => $dynamic['province_id'],
                    'city_id' => $dynamic['city_id'],
                    'district_id' => $dynamic['district_id']
                ];
            } else {
                $dynamicData = Db::name('scene_house')->where('house_id', $house_id)->find();
                $dynamicRoomData = Db::name('scene_house_room')->where('house_id', $dynamicData['house_id'])->select()->toArray();
                //添加作品
                $data = [
                    'title' => $dynamicData['name'],
                    'images' => $dynamicData['images'],
                    'add_time' => time(),
                    'user_id' => $user_id,
                    'style_id' => $dynamic['style_id'],
                    'over_images' => $dynamicData['over_images'],
                    'over_content' => $dynamicData['over_content'],
                    'image' => $dynamicData['image'],
                    'house_id' => $dynamicData['house_id'],
                    'estate_id' => $dynamicData['estate_id'],
                    'designjson' => $designjson,
                    'house_type_id' => $dynamicData['house_type_id'],
                    'area' => $dynamicData['area'],
                    'house_image' => $dynamicData['house_image'],
                    'province_id' => $dynamic['province_id'],
                    'city_id' => $dynamic['city_id'],
                    'district_id' => $dynamic['district_id']
                ];
            }
            Db::startTrans();
            try {
                $workId = Db::name('user_works')->insertGetId($data);
                foreach ($dynamicRoomData as $key => $value) {
                    Db::name('user_works_room')->insert(['content' => $value['content'], 'images' => $value['images'], 'add_time' => time(), 'works_id' => $workId, 'room_id' => $value['room_id']]);
                }
                if ($images) {
                    $imagesData = [];
                    foreach ($images as $k => $v) {
                        $imagesData[] = [
                            'image' => $v,
                            'user_id' => $user_id,
                            'works_id' => $workId,
                            'add_time' => time()
                        ];
                    }
                    Db::name('cloud_render_picture')->insertAll($imagesData);
                }
                if ($videos) {
                    $videosData = [];
                    foreach ($videos as $k => $v) {
                        $videosData[] = [
                            'video' => $v,
                            'user_id' => $user_id,
                            'works_id' => $workId,
                            'add_time' => time()
                        ];
                    }
                    Db::name('cloud_render_video')->insertAll($videosData);
                }
                //记录方案生成数量
                $data = Db::name('user_dynamic')->where(['dynamic_id' => $dynamic_id])->find();
                Db::name('user_dynamic')->where(['dynamic_id' => $dynamic_id])->update(['hits' => $data['hits'] + 1]);
                Db::commit();
                return dyjsonReturn(1, '保存成功', $workId);
            } catch (Exception $exception) {
                Db::rollback();
                return dyjsonReturn(0, '保存失败', $exception->getMessage());
            }
        } else {
            $updateWork = Db::name('user_works')->where(['works_id' => $works_id])->update(['designjson' => $designjson]);
            if (!$updateWork) {
                return dyjsonReturn(0, '保存失败');
            }
            return dyjsonReturn(1, '保存成功', $works_id);
        }
    }

    /**
     * 获取设计
     * @return void
     */
    public function getHouse()
    {
        $works_id = input('works_id');
        if (!$works_id) {
            return dyjsonReturn(0, '获取作品ID失败');
        }
        $data = Db::name('user_works')->where('works_id', $works_id)->field('designjson,house_id')->find();
        $data['designjson'] = $data['designjson'] ? $data['designjson'] : '';
        $data['house'] = Db::name('scene_house')->where('house_id', $data['house_id'])->value('3d_name');
        unset($data['house_id']);
        return dyajaxReturn(1, '获取成功', $data);
    }

    /**
     * 案例库获取评论
     * @return void
     */
    public function getComment()
    {
        $dynamic_id = input('dynamic_id');
        $user_id = dydecrypt(input('user_id'));
        $dynamicData = Db::name('user_dynamic')->where('dynamic_id', $dynamic_id)->find();
        $commentData = Db::name('evaluate')
            ->alias('comment')
            ->field('comment.evaluate_id,comment.content,comment.add_time,comment.dynamic_id,comment.user_id,comment.is_show,users.nickname,users.avatar,users.mobile,users.last_ip')
            ->leftJoin('users', 'comment.user_id = users.user_id')
            ->where(['dynamic_id' => $dynamic_id, 'is_show' => 1, 'last_id' => 0])
            ->select()->toArray();
        foreach ($commentData as $key => $val) {
            $commentData[$key]['last_evaluate'] = Db::name('evaluate')
                ->alias('comment')
                ->field('comment.evaluate_id,comment.content,comment.add_time,comment.dynamic_id,comment.user_id,comment.is_show,users.nickname,users.avatar,users.mobile,users.last_ip')
                ->leftJoin('users', 'comment.user_id = users.user_id')
                ->where(['last_id' => $val['evaluate_id'], 'is_show' => 1])
                ->select()->toArray();
            if ($val['user_id'] == $dynamicData['user_id']) {
                $commentData[$key]['is_framer'] = 1;
            } else {
                $commentData[$key]['is_framer'] = 0;
            }
            $like = Db::name('evaluate_like')->where(['evaluate_id' => $val['evaluate_id'], 'user_id' => $user_id])->find();
            if (!empty($like)) {
                $commentData[$key]['is_user'] = 1;
            } else {
                $commentData[$key]['is_user'] = 0;
            }
            $commentData[$key]['likeNum'] = Db::name('evaluate_like')->where('evaluate_id', $val['evaluate_id'])->count();
            foreach ($commentData[$key]['last_evaluate'] as $k => $v) {
                if ($v['user_id'] == $dynamicData['user_id']) {
                    $commentData[$key]['last_evaluate'][$k]['is_framer'] = 1;
                } else {
                    $commentData[$key]['last_evaluate'][$k]['is_framer'] = 0;
                }
                $like = Db::name('evaluate_like')->where(['evaluate_id' => $v['evaluate_id'], 'user_id' => $user_id])->find();
                if (!empty($like)) {
                    $commentData[$key]['last_evaluate'][$k]['is_user'] = 1;
                } else {
                    $commentData[$key]['last_evaluate'][$k]['is_user'] = 0;
                }
                $commentData[$key]['last_evaluate'][$k]['likeNum'] = Db::name('evaluate_like')->where('evaluate_id', $v['evaluate_id'])->count();
            }
        }
        if (empty($commentData)) {
            return dyjsonReturn(0, '该方案评论为空');
        }
        return dyjsonReturn(1, '查询成功', $commentData);
    }

    /**
     * 案例库写评论
     * @return void
     */
    public function writeComment()
    {
        $user_id = dydecrypt(input('user_id'));
        $dynamic_id = input('dynamic_id');
        $last_id = input('last_id');
        $content = input('content');
        $insert = Db::name('evaluate')->insert(['user_id' => $user_id, 'dynamic_id' => $dynamic_id, 'last_id' => $last_id, 'content' => $content, 'add_time' => time()]);
        if (!$insert) {
            return dyjsonReturn(0, '评论失败');
        }
        return dyjsonReturn(1, '评论成功');
    }

    /**
     * 案例库点赞和取消点赞
     * @return void
     */
    public function likeComment()
    {
        $user_id = dydecrypt(input('user_id'));
        $evaluate_id = input('evaluate_id');
        $is_like = input('is_like');
        if ($is_like == 1) {
            $likes = Db::name('evaluate_like')->where(['user_id' => $user_id, 'evaluate_id' => $evaluate_id])->find();
            if ($likes) {
                return dyjsonReturn(0, '该用户以点过赞');
            }
            $like = Db::name('evaluate_like')->insert(['user_id' => $user_id, 'evaluate_id' => $evaluate_id, 'add_time' => time()]);
            if (!$like) {
                return dyjsonReturn(0, '点赞失败');
            }
            return dyjsonReturn(1, '点赞成功');
        } else {
            $notLike = Db::name('evaluate_like')->where(['user_id' => $user_id, 'evaluate_id' => $evaluate_id])->delete();
            if (!$notLike) {
                return dyjsonReturn(0, '取消点赞失败');
            }
            return dyjsonReturn(1, '取消点赞成功');
        }
    }

    /**
     * 我的作品获取评论
     * @return void
     */
    public function getWorkComment()
    {
        $works_id = input('works_id');
        $user_id = dydecrypt(input('user_id'));
        $dynamicData = Db::name('user_works')->where('works_id', $works_id)->find();
        $commentData = Db::name('evaluate_work')->alias('comment')->leftJoin('users', 'comment.user_id = users.user_id')->where(['works_id' => $works_id, 'is_show' => 1, 'last_id' => 0])->select()->toArray();
        foreach ($commentData as $key => $val) {
            $commentData[$key]['last_evaluate'] = Db::name('evaluate_work')->alias('comment')->leftJoin('users', 'comment.user_id = users.user_id')->where(['last_id' => $val['evaluate_id'], 'is_show' => 1])->select()->toArray();
            if ($val['user_id'] == $dynamicData['user_id']) {
                $commentData[$key]['is_framer'] = 1;
            } else {
                $commentData[$key]['is_framer'] = 0;
            }
            $like = Db::name('evaluate_like_work')->where(['evaluate_id' => $val['evaluate_id'], 'user_id' => $user_id])->find();
            if (empty($like)) {
                $commentData[$key]['is_user'] = 0;
            } else {
                $commentData[$key]['is_user'] = 1;
            }
            $commentData[$key]['likeNum'] = Db::name('evaluate_like_work')->where('evaluate_id', $val['evaluate_id'])->count();
            foreach ($commentData[$key]['last_evaluate'] as $k => $v) {
                if ($v['user_id'] == $dynamicData['user_id']) {
                    $commentData[$key]['last_evaluate'][$k]['is_framer'] = 1;
                } else {
                    $commentData[$key]['last_evaluate'][$k]['is_framer'] = 0;
                }
                $like = Db::name('evaluate_like_work')->where(['evaluate_id' => $v['evaluate_id'], 'user_id' => $user_id])->find();
                if (empty($like)) {
                    $commentData[$key]['last_evaluate'][$k]['is_user'] = 0;
                } else {
                    $commentData[$key]['last_evaluate'][$k]['is_user'] = 1;
                }
                $commentData[$key]['last_evaluate'][$k]['likeNum'] = Db::name('evaluate_like_work')->where('evaluate_id', $v['evaluate_id'])->count();
            }
        }
        if (empty($commentData)) {
            return dyjsonReturn(0, '该方案评论为空');
        }
        return dyjsonReturn(1, '查询成功', $commentData);
    }

    /**
     * 我的作品写评论
     * @return void
     */
    public function writeWorkComment()
    {
        $user_id = dydecrypt(input('user_id'));
        $works_id = input('works_id');
        $last_id = input('last_id');
        $content = input('content');
        $insert = Db::name('evaluate_work')->insert(['user_id' => $user_id, 'works_id' => $works_id, 'last_id' => $last_id, 'content' => $content, 'add_time' => time()]);
        if (!$insert) {
            return dyjsonReturn(0, '评论失败');
        }
        return dyjsonReturn(1, '评论成功');
    }

    /**
     * 我的作品点赞和取消点赞
     * @return void
     */
    public function likeWorkComment()
    {
        $user_id = dydecrypt(input('user_id'));
        $evaluate_id = input('evaluate_id');
        $is_like = input('is_like');
        if ($is_like == 1) {
            $likes = Db::name('evaluate_like_work')->where(['user_id' => $user_id, 'evaluate_id' => $evaluate_id])->find();
            if ($likes) {
                return dyjsonReturn(0, '该用户以点过赞');
            }
            $like = Db::name('evaluate_like_work')->insert(['user_id' => $user_id, 'evaluate_id' => $evaluate_id, 'add_time' => time()]);
            if (!$like) {
                return dyjsonReturn(0, '点赞失败');
            }
            return dyjsonReturn(1, '点赞成功');
        } else {
            $notLike = Db::name('evaluate_like_work')->where(['user_id' => $user_id, 'evaluate_id' => $evaluate_id])->delete();
            if (!$notLike) {
                return dyjsonReturn(0, '取消点赞失败');
            }
            return dyjsonReturn(1, '取消点赞成功');
        }
    }

    /**
     * 删除案例
     * @return void
     */
    public function delDynamic()
    {
        $dynamic_id = input('dynamic_id');
        $user_id = dydecrypt(input('user_id'));
        $del = Db::name('user_dynamic')->where(['dynamic_id' => $dynamic_id, 'user_id' => $user_id])->update(['is_deleted' => 1, 'is_show' => 0]);
        if (!$del) {
            return dyjsonReturn(0, '删除失败');
        }
        return dyjsonReturn(1, '删除成功');
    }

    /**
     * 删除作品
     * @return void
     */
    public function delMyWork()
    {
        $works_id = input('works_id');
        $user_id = dydecrypt(input('user_id'));
        $del = Db::name('user_works')->where(['works_id' => $works_id, 'user_id' => $user_id])->update(['is_deleted' => 1]);
        if (!$del) {
            return dyjsonReturn(0, '删除失败');
        }
        return dyjsonReturn(1, '删除成功');
    }

    /**
     * 搜索页面数据
     * @return false|string
     * @throws DbException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function searchDataShow()
    {
        $city = input('city');
        $num = input('num');
        //查询城市
        $city = Db::name('region')->where('name','like',"%$city%")->find();
        $estateData = Db::name('estate')->where(['is_deleted' => 0, 'is_show' => 1, 'city_id' => $city['id']])->select()->toArray();
        if(empty($estateData)){
            return dyjsonReturn(0,'暂未有该地区的楼盘');
        }
        //获取楼盘下所有户型信息
        foreach ($estateData as $ks => $vs) {
            $arrays[] = Db::name('scene_house')->where('estate_id', $vs['estate_id'])->where(['is_show' => 1, 'is_deleted' => 0])->order('browse','desc')->select()->toArray();
        }
        $house = [];
        foreach ($arrays as $vals) {
            foreach ($vals as $val) {
                array_push($house, $val);
            }
        }
        $data['hotHouse'] = Db::name('scene_house')->where(['is_show'=>1,'is_deleted'=>0])->order('browse','desc')->limit(0,$num)->select()->toArray();
        $data['cityHouse'] = array_slice($house,0,$num);
        $data['like'] = Db::name('scene_house')->where(['is_show' => 1, 'is_deleted' => 0])->field('name')->limit(6)->select()->toArray();
        if (empty($data['house'] || $data['like'])) {
            return dyjsonReturn(0, '查询失败');
        }
        return dyjsonReturn(1, '查询成功', $data);
    }

    /**
     * 智能设计获取用户收藏
     * @return void
     */
    public function intelligentGetUserCollect()
    {
        $startPage = input('startPage');
        $endPage = input('endPage');
        $room_id = input('room_id');
        $style_id = input('style_id');
        $user_id = dydecrypt(input('user_id'));
        $in_console = input('in_console');
        //获取分类信息
        $roomData['label'] = Db::name('label')->where(['typeid' => 5, 'is_show' => 1])->order('sort')->select()->toArray();
        // 0-app获取  1-操控台获取
        switch ($in_console) {
            case 0:
                if (empty($room_id)) {
                    $room_id = $roomData['label'][0]['id'];
                }
                $roomData['room'] = Db::name('intelligent_collect')
                    ->alias('ic')
                    ->leftJoin('intelligent i', 'ic.intelligent_id = i.id')
                    ->field('ic.id,ic.intelligent_id,ic.user_id,ic.room_id,ic.add_time,i.title,i.content,i.image,i.video,i.video_size,i.furniture_ids,i.furniture_tags')
                    ->where(['ic.room_id' => $room_id, 'ic.user_id' => $user_id, 'ic.is_deleted' => 0, 'i.style_id' => $style_id])
                    ->limit($startPage, $endPage)->select()->toArray();
                break;
            case 1:
                if (empty($room_id)) {
                    if (empty($style_id)) {
                        $roomData['room'] = Db::name('intelligent_collect')
                            ->alias('ic')
                            ->leftJoin('intelligent i', 'ic.intelligent_id = i.id')
                            ->field('ic.id,ic.intelligent_id,ic.user_id,ic.room_id,ic.add_time,i.title,i.content,i.image,i.video,i.video_size,i.furniture_ids,i.furniture_tags')
                            ->where(['ic.user_id' => $user_id, 'ic.is_deleted' => 0])
                            ->limit($startPage, $endPage)->select()->toArray();
                    } else {
                        $roomData['room'] = Db::name('intelligent_collect')
                            ->alias('ic')
                            ->leftJoin('intelligent i', 'ic.intelligent_id = i.id')
                            ->field('ic.id,ic.intelligent_id,ic.user_id,ic.room_id,ic.add_time,i.title,i.content,i.image,i.video,i.video_size,i.furniture_ids,i.furniture_tags')
                            ->where(['ic.user_id' => $user_id, 'ic.is_deleted' => 0, 'i.style_id' => $style_id])
                            ->limit($startPage, $endPage)->select()->toArray();
                    }
                } else {
                    if (empty($style_id)) {
                        $roomData['room'] = Db::name('intelligent_collect')
                            ->alias('ic')
                            ->leftJoin('intelligent i', 'ic.intelligent_id = i.id')
                            ->field('ic.id,ic.intelligent_id,ic.user_id,ic.room_id,ic.add_time,i.title,i.content,i.image,i.video,i.video_size,i.furniture_ids,i.furniture_tags')
                            ->where(['ic.room_id' => $room_id, 'ic.user_id' => $user_id, 'ic.is_deleted' => 0])
                            ->limit($startPage, $endPage)->select()->toArray();
                    } else {
                        $roomData['room'] = Db::name('intelligent_collect')
                            ->alias('ic')
                            ->leftJoin('intelligent i', 'ic.intelligent_id = i.id')
                            ->field('ic.id,ic.intelligent_id,ic.user_id,ic.room_id,ic.add_time,i.title,i.content,i.image,i.video,i.video_size,i.furniture_ids,i.furniture_tags')
                            ->where(['ic.room_id' => $room_id, 'ic.user_id' => $user_id, 'ic.is_deleted' => 0, 'i.style_id' => $style_id])
                            ->limit($startPage, $endPage)->select()->toArray();
                    }
                }
                break;
        }
        foreach ($roomData['room'] as $key => $val) {
            $furnitureId = explode(',', $val['furniture_ids']);
            foreach ($furnitureId as $k => $v) {
                $roomData['room'][$key]['furnitureData'][] = Db::name('furniture')->where(['id' => $v, 'is_show' => 1])->find();
            }
        }
        return dyjsonReturn(1, '查询成功', $roomData);
    }

    /**
     * 智能设计用户收藏
     * @return void
     */
    public function intelligentUserCollect()
    {
        $user_id = dydecrypt(input('user_id'));
        $room_id = input('room_id');
        $intelligent_id = input('intelligent_id');
        if (empty($intelligent_id)) {
            return dyjsonReturn(0, '请选择智能设计');
        }
        //查询是否已经收藏
        $data = Db::name('intelligent_collect')->where(['user_id' => $user_id, 'room_id' => $room_id, 'intelligent_id' => $intelligent_id, 'is_deleted' => 0])->find();
        if ($data) {
            return dyjsonReturn(0, '用户已收藏该方案');
        }
        Db::startTrans();
        try {
            $insertData = [
                'intelligent_id' => $intelligent_id,
                'user_id' => $user_id,
                'room_id' => $room_id,
                'add_time' => time(),
            ];
            $intelligentData = Db::name('intelligent_collect')->where(['user_id' => $user_id, 'room_id' => $room_id, 'intelligent_id' => $intelligent_id])->find();
            if (!$intelligentData) {
                Db::name('intelligent_collect')->insert($insertData);
            } else {
                Db::name('intelligent_collect')->where(['user_id' => $user_id, 'room_id' => $room_id, 'intelligent_id' => $intelligent_id])->update(['is_deleted' => 0]);
            }

            Db::commit();
            return dyjsonReturn(1, '收藏成功');
        } catch (\Exception $exception) {
            Db::rollback();
            return dyjsonReturn(0, '收藏失败');
        }
    }

    /**
     * 智能设计用户取消收藏
     * @return void
     */
    public function intelligentCancelCollect()
    {
        $user_id = dydecrypt(input('user_id'));
        $collect_id = input('collect_id');
        Db::startTrans();
        try {
            Db::name('intelligent_collect')->where(['id' => $collect_id, 'user_id' => $user_id])->update(['is_deleted' => 1]);
            Db::commit();
            return dyjsonReturn(1, '取消收藏成功');
        } catch (\Exception $exception) {
            Db::rollback();
            return dyjsonReturn(0, '取消收藏失败');
        }
    }


}