<?php

namespace app\appapi\controller;

use app\common\logic\WorksLogic;
use app\common\logic\WorksresourceLogic;
use app\common\validate\Design;
use app\common\validate\Resource;
use think\facade\Db;

//作品
class Works extends Base
{
    //作品列表
    public function getWorksList()
    {
        $where = [];
        $keyword = input('keyword/s',false,'trim');
        if($keyword){
            $where['a.name'] = ['like',"%$keyword%"];
        }
        $WorksLogic = new WorksLogic();
        $list = $WorksLogic->getWorksList($where,$this->user_id);
        return dyajaxReturn(1, '作品列表',$list);
    }

    //获取作品详情
    public function getWorks()
    {
        $typeid = input('typeid/d',2);//1=户型进入 2=作品进入
        $works_id = input('works_id/d');
        $house_id = input('house_id/d');
        $info = [];
        $info['works_id'] = 0;
        $info['images'] = [];
        $info['title'] = "";
        $info['style_id'] = 0;
        $info['style_name'] = "";
        $info['over_images'] = [];
        $info['over_content'] = "";
        if($typeid==1){
            if(!$house_id){
                return dyajaxReturn(0, '缺少信息ID');
            }
//            $works_id = Db::name('user_works')->where(['is_edit'=>0,'user_id'=>$this->user_id])->value('works_id');
//            if(!$works_id){
//                $works_id = Db::name('user_works')->insertGetId(['user_id'=>$this->user_id,'add_time'=>time(),'house_id'=>$house_id]);
//            }
//            Db::name('user_works')->where('works_id',$works_id)->update(['house_id'=>$house_id]);
//            $info['works_id'] = $works_id;
        }else{
            if(!$works_id){
                return dyajaxReturn(0, '缺少信息ID');
            }
            $works = Db::name('user_works')->where(['works_id'=>$works_id,'user_id'=>$this->user_id])->find();
            if(!$works){
                return dyajaxReturn(0, '信息不存在');
            }
            $house_id = $works['house_id'];
            $info['works_id'] = $works['works_id'];
            $info['images'] = $works['images']?explode(',',$works['images']):[];
            $info['title'] = $works['title'];
            $info['style_id'] = $works['style_id'];
            $info['style_name'] = get_label_name($works['style_id']);
            $info['over_images'] = $works['over_images']?explode(',',$works['over_images']):[];
            $info['over_content'] = $works['over_content'];
        }
        $scene_house = Db::name('scene_house')
            ->alias('sh')
            ->join('estate e','e.estate_id=sh.estate_id')
            ->where('house_id',$house_id)
            ->field('sh.*,e.province_id,e.city_id,e.district_id')
            ->find();
        if($typeid==1){
            $info['title'] = $scene_house['name'];
            $works_id = Db::name('user_works')->where(['is_edit'=>0,'user_id'=>$this->user_id,'house_id'=>$house_id])->value('works_id');
            if(!$works_id){
                $works_id = Db::name('user_works')->insertGetId([
                    'title'=>$scene_house['name'],

                    'user_id'=>$this->user_id,
                    'images'=>$scene_house['images'],
                    'style_id'=>$scene_house['style_id'],
                    'over_images'=>$scene_house['over_images'],
                    'over_content'=>$scene_house['over_content'],
                    'image'=>$scene_house['image'],
                    'house_id'=>$house_id,
                    'estate_id'=>$scene_house['estate_id'],
                ]);
                $room_insert = [];
                $room = Db::name('scene_house_room')->where(['house_id'=>$house_id])->field('content,images,room_id')->select()->toArray();
                foreach ($room as $v){
                    $room_insert[] = [
                        'content'=>$v['content'],
                        'images'=>$v['images'],
                        'works_id'=>$works_id,
                        'room_id'=>$v['room_id'],
                        'add_time'=>time(),
                    ];
                }
                if($room_insert){
                    Db::name('user_works_room')->insertAll($room_insert);
                }
            }
            $info['works_id'] = $works_id;
            $info['style_name'] = get_label_name($scene_house['style_id']);
            $info['images'] = $scene_house['images']?explode(',',$scene_house['images']):[];
            $info['style_id'] = $scene_house['style_id'];
            $info['over_images'] = $scene_house['over_images']?explode(',',$scene_house['over_images']):[];
            $info['over_content'] = $scene_house['over_content']?$scene_house['over_content']:"";
        }
        [$max_height,$max_width,$min_width] = get_img_max_height($info['images']);
        $info['max_height'] = $max_height;
        $info['max_width'] = $max_width;
        $info['min_width'] = $min_width;
        $address = '';
        if($scene_house['district_id']){
            $address = get_region_mername($scene_house['district_id'],'');
        }
        if(!$address && $scene_house['city_id']){
            $address = get_region_mername($scene_house['city_id'],'');
        }
        if(!$address && $scene_house['province_id']){
            $address = get_region_mername($scene_house['province_id'],'');
        }
        $info['house_id'] = $house_id;
        $info['house_thumb'] = SITE_URL.get_sub_images(md5($scene_house['house_image']),$scene_house['house_image'], $house_id, 307, 340, 'scene_house');
        $info['address'] = $address;
        $info['house_type_id'] = $scene_house['house_type_id'];
        $info['estate_id'] = $scene_house['estate_id'];
        $info['area'] = $scene_house['area'];
        $label = [];
        $label[] = get_label_name($info['house_type_id']);
        $label[] = $info['area']."㎡";
        $info['label'] = $label;
        $info['room'] = Db::name('label')->where(['is_show'=>1,'typeid'=>5,'id'=>['in',$scene_house['room_ids']]])
            ->order('sort asc,id desc')->field('id as room_id,name')->select()->toArray();
        foreach ($info['room'] as &$value){
            $value['images'] = [];
            $value['content'] = "";
            $room = Db::name('user_works_room')->where(['works_id'=>$info['works_id'],'room_id'=>$value['room_id']])->field('content,images')->find();
            if($room){
                $value['images'] = $room['images']?explode(',',$room['images']):[];
                $value['content'] = $room['content']?$room['content']:'';
            }
        }
        return dyajaxReturn(1, '户型详情',$info);
    }

    //删除作品
    public function delWorks()
    {
        $user_id = $this->user_id;
        $works_id = input('works_id/d');
        $where['user_id'] = $user_id;
        $where['works_id'] = $works_id;

        $count =  Db::name('user_works')->where($where)->value('works_id');
        if(!$count){
            return dyajaxReturn(0, "作品不存在");
        }
        Db::name('user_works')->where($where)->delete();
        Db::name('user_works_resource')->where($where)->update(['is_deleted'=>1]);
        return dyajaxReturn(1, '删除成功');
    }

    //保存作品
    public function works()
    {
        $validate = validate(\app\common\validate\Works::class);
        $param = input();
        if (!$validate->batch(true)->check($param)) {
            // 验证失败 输出错误信息
            $error = $validate->getError();
            $error_msg = array_values($error);
            return dyajaxReturn(0, $error_msg[0]);
        }
        $images = explode(',',$param['images']);
//        if(count($images)<2){
//            return dyajaxReturn(0, "顶部相册图最少2张");
//        }
        $estate_id = Db::name('scene_house')
            ->where('house_id',$param['house_id'])->value('estate_id');
        $data = [
            'title'=>$param['title'],
            'images'=>$param['images'],
            'style_id'=>$param['style_id'],
            'house_id'=>$param['house_id'],
            'image'=>explode(',',$param['images'])[0],
            'over_images'=>isset($param['over_images'])?$param['over_images']:'',
            'over_content'=>isset($param['over_content'])?$param['over_content']:'',
            'estate_id'=>$estate_id,
            'is_edit'=>1,
        ];
        $room = input('room');
//        if(!$room){
//            return dyajaxReturn(0, "请完善房间内信息");
//        }
        if($room){
            $room = json_decode(htmlspecialchars_decode($room),true);
        }

//        if(!$room){
//            return dyajaxReturn(0, "请完善房间内信息");
//        }
        $is_room = false;
        foreach ($room as $v){
            if(!$v['room_id']){
                return dyajaxReturn(0, "请完善房间内信息");
            }
//            if(!$v['images']){
//                return dyajaxReturn(0, "请上传【".get_label_name($v['room_id'])."】图片");
//            }
//            if(!$v['content']){
//                return dyajaxReturn(0, "请输入【".get_label_name($v['room_id'])."】内容");
//            }
//            if($v['content'] || $v['images']){
//                if(!$v['images']){
//                    return dyajaxReturn(0, "请上传【".get_label_name($v['room_id'])."】图片");
//                }
//                if(!$v['content']){
//                    return dyajaxReturn(0, "请输入【".get_label_name($v['room_id'])."】内容");
//                }
//                $is_room = true;
//            }
        }
//        if(!$is_room){
//            return dyajaxReturn(0, "请完善房间内信息");
//        }
        Db::startTrans();
        try {
            if($param['works_id']>0){
                $data['add_time'] = time();
                $data['user_id'] = $this->user_id;
                $res = Db::name('user_works')->where('works_id',$param['works_id'])->update($data);
                $works_id = $param['works_id'];
            }else{
                $data['add_time'] = time();
                $data['user_id'] = $this->user_id;
                $res = Db::name('user_works')->insertGetId($data);
                if (!$res) {
                    throw new \think\Exception('保存失败');
                }
                $works_id = $res;
            }
            $old_room = Db::name('user_works_room')->where('works_id',$works_id)->order('id asc')->select()->toArray();
//            $room_update = [];
            $room_insert = [];
            if($old_room){
                $count = 0;
                foreach ($old_room as $k=>$v){
                    if(isset($room[$k])){
                        if($room[$k]['content'] || $room[$k]['images']){
                            Db::name('user_works_room')->where('id',$v['id'])
                                ->update([
                                    'content'=>$room[$k]['content'],
                                    'images'=>$room[$k]['images'],
                                    'works_id'=>$works_id,
                                    'room_id'=>$room[$k]['room_id'],
                                    'add_time'=>time(),
                                ]);
                        }else{
                            Db::name('user_works_room')->where('id',$v['id'])->delete();
                        }
                    }else{
                        Db::name('user_works_room')->where('id',$v['id'])->delete();
                    }
                    $count++;
                }

                while (isset($room[$count])){
                    if($room[$count]['content'] || $room[$count]['images']){
                        $room_insert[] = [
                            'content'=>$room[$count]['content'],
                            'images'=>$room[$count]['images'],
                            'works_id'=>$works_id,
                            'room_id'=>$room[$count]['room_id'],
                            'add_time'=>time(),
                        ];
                    }
                    $count++;
                }
            }else{
                foreach ($room as $v){
                    if($v['content'] || $v['images']){
                        $room_insert[] = [
                            'content'=>$v['content'],
                            'images'=>$v['images'],
                            'works_id'=>$works_id,
                            'room_id'=>$v['room_id'],
                            'add_time'=>time(),
                        ];
                    }
                }
            }
            if($room_insert){
                Db::name('user_works_room')->insertAll($room_insert);
            }
            //保存房间信息
            Db::commit();
        } catch (\Exception $e) {
            Db::rollback();
            return dyajaxReturn(0, $e->getMessage());
        }
        return dyajaxReturn(1,'保存成功');
    }

    //添加资源
    public function addResource()
    {
        $param = input();
        $dydir = 'public/backup/addResource/'.date("Y").'/';
        if(!is_dir($dydir)) @mkdir($dydir,0755,true);
        $dydir = $dydir.date("m-d").'/';
        if(!is_dir($dydir)) @mkdir($dydir,0755,true);
        //写入文件做日志 调试用
        $log = "\r\n\r\n".'=========$param=========='."\r\n".date("Y-m-d H:i:s")."\r\n".json_encode(json_encode($param));
        @file_put_contents($dydir.date("Y-m-d").'.log', $log, FILE_APPEND);
        $user_id = $this->user_id;
        $typeid = input('typeid/d');//0-户型 1-图片  2-视频
        $validate = validate(Resource::class);
        if (!$validate->scene('typeid'.$typeid)->batch(true)->check($param)) {
            // 验证失败 输出错误信息
            $error = $validate->getError();
            $error_msg = array_values($error);
            return dyajaxReturn(0, $error_msg[0]);
        }
        if($typeid==2){
            //生成封面图
            $param['image'] = explode('.',$param['video'])[0].'.jpg';
            $file = ".".$param['video'];
            $ffmpeg = \FFMpeg\FFMpeg::create([
                'ffmpeg.binaries'  => app()->getRootPath().'/public/public/plugins/ffmpeg-git-20220622-amd64-static/ffmpeg',	# 你自己安装的位置
                'ffprobe.binaries' => app()->getRootPath().'/public/public/plugins/ffmpeg-git-20220622-amd64-static/ffprobe'	# 你自己安装的位置
            ]);
            if(!file_exists($file)){
                return dyajaxReturn(0, '打开视频文件失败');
            }
            $video = $ffmpeg->open($file);
            $video
                ->frame(\FFMpeg\Coordinate\TimeCode::fromSeconds(2))
                ->save(".".$param['image']);
        }
        $room_id = 0;
        if(isset($param['room_name'])){
            $room_id = Db::name('label')->where('typeid',5)->where('name',$param['room_name'])->value('id');
        }
        $data = [
            'add_time'=>time(),
            'room_id'=>$room_id?$room_id:0,
            'image'=>$param['image']?$param['image']:"",
            'works_id'=>$param['works_id'],
            'user_id'=>$user_id,
            'typeid'=>$typeid,
            'video'=>$param['video']?$param['video']:"",
            'name'=>$param['name']?$param['name']:"",
        ];
        Db::name('user_works_resource')->insertGetId($data);
        Db::name('user_works')->where('works_id',$param['works_id'])->update(['is_edit'=>1]);
        return dyajaxReturn(1, '保存成功');
    }

    //删除资源
    public function delResource()
    {
        $user_id = $this->user_id;
        $id = input('id/d');
        $where['user_id'] = $user_id;
        $where['id'] = $id;
        $count =  Db::name('user_works_resource')->where($where)->value('id');
        if(!$count){
            return dyajaxReturn(0, "资源不存在");
        }
        Db::name('user_works_resource')->where($where)->update(['is_deleted'=>1]);
        return dyajaxReturn(1, '删除成功');
    }

    //获取图片列表
    public function getHouseImage()
    {
        $works_id = input('works_id/d');//作品ID
        $room_id = input('room_id/d');//房间ID
        $where['user_id'] = $this->user_id;
        if($room_id){
            $where['room_id'] = $room_id;
        }
        if($works_id){
            $where['works_id'] = $works_id;
        }
        $WorksresourceLogic = new WorksresourceLogic();
        $ImageList = $WorksresourceLogic->getImageList($where,$works_id);
        $list = [];
        if($ImageList){
            $room_key = -1;
            foreach ($ImageList as $v){
                if(isset($list[$room_key]) && $list[$room_key]['room_id']==$v['room_id']){
                    $list[$room_key]['room_id'] = $v['room_id'];
                    $list[$room_key]['room_name'] = $v['room_name'];
                    unset($v['room_id'],$v['room_name']);
                    $list[$room_key]['child'][] = $v;
                }else{
                    $room_key++;
                    $list[$room_key]['room_id'] = $v['room_id'];
                    $list[$room_key]['room_name'] = $v['room_name'];
                    unset($v['room_id'],$v['room_name']);
                    $list[$room_key]['child'][] = $v;
                }
            }
            $list = array_values($list);
        }
        return dyajaxReturn(1, '获取成功',$list);
    }

    //获取视频列表
    public function getHouseVideo()
    {
        $works_id = input('works_id/d');//作品ID
        $where['user_id'] = $this->user_id;
        if($works_id){
            $where['works_id'] = $works_id;
        }
        $WorksresourceLogic = new WorksresourceLogic();
        $list = $WorksresourceLogic->getVideoList($where);
        return dyajaxReturn(1, '获取成功',$list);
    }

    //获取房间选项
    public function getOptionList()
    {
        $works_id = input('works_id/d');
        if($works_id){
            $room_ids = Db::name('user_works')
                ->alias('uw')
                ->join('scene_house sh','sh.house_id=uw.house_id')
                ->where('uw.works_id',$works_id)
                ->cache(true,60*30)
                ->value('sh.room_ids');
            if($room_ids){
                $rooms = Db::name('label')->order('sort asc,id desc')->whereIn('id',$room_ids)->field('id,name')->select();
            }else{
                $rooms = [];
            }
        }else{
            $rooms = Db::name('label')->order('sort asc,id desc')
                ->where('is_show',1)
                ->where('typeid',5)
                ->cache(true,60*30)
                ->field('id,name')->select();
        }
        return dyajaxReturn(1,'获取选项成功',$rooms);
    }

    //获取作品设计详情
    public function getDesignInfo()
    {

    }

    //添加家具（工装、软装）
    public function addDesignInfo()
    {
        $param = input();
        $data = $this->getDesigndata($param);
        $data['add_time'] = time();
        $res = Db::name('user_works_furniture')->insertGetId($data);
        if(!$res){
            return dyajaxReturn(0,'添加失败');
        }
        Db::name('user_works')->where('works_id',input('works_id/d'))->update(['is_edit'=>1]);
        return dyajaxReturn(1,'添加成功',['id'=>$res]);
    }

    //保存设计
    public function design()
    {
        $designjson = input('designjson');
        $works_id = input('works_id');
        if(!$works_id){
            return dyajaxReturn(0, "获取作品ID失败");
        }
        if(!$designjson){
            return dyajaxReturn(0, "获取设计数据失败");
        }
//        $designjson = htmlspecialchars_decode($designjson);
        Db::name('user_works')->where('works_id',$works_id)->update(['is_edit'=>1,'designjson'=>$designjson]);
        return dyajaxReturn(1,'保存成功');
    }

    //获取设计
    public function getDesign()
    {
        $works_id = input('works_id');
        if(!$works_id){
            return dyajaxReturn(0, "获取作品ID失败");
        }
        $data = Db::name('user_works')->where('works_id',$works_id)->field('designjson,house_id')->find();
        $data['designjson'] = $data['designjson']?$data['designjson']:'';
        $data['house'] = Db::name('scene_house')->where('house_id',$data['house_id'])->value('3d_name');
        unset($data['house_id']);
        return dyajaxReturn(1,'获取成功',$data);
    }

    //修改家具（工装、软装）
    public function setDesignInfo()
    {
        $id = input('id/d');
        if(!$id){
            return dyajaxReturn(0,'缺少ID');
        }
        $param = input();
        $data = $this->getDesigndata($param);
        $data['update_time'] = time();
        $res = Db::name('user_works_furniture')->where('id',$id)->update($data);
        if(!$res){
            return dyajaxReturn(0,'修改失败');
        }
        return dyajaxReturn(1,'修改成功',['id'=>$id]);
    }

    private function getDesigndata($param)
    {
        $validate = validate(Design::class);
        if (!$validate->batch(true)->check($param)) {
            // 验证失败 输出错误信息
            $error = $validate->getError();
            $error_msg = array_values($error);
            return dyajaxReturn(0, $error_msg[0]);
        }
        $material_filename = $param['material_filename'];
        $furniture = Db::name('furniture')->where('id',$param['furniture_id'])->find();
        if(!$furniture){
            return dyajaxReturn(0,'家居不存在');
        }
        $material = [];
        $materials = json_decode($furniture['material'],true);
        foreach ($materials as $v){
            if($v['filename']==$material_filename){
                $material = $v;
                break;
            }
        }
        $data = [
            'user_id'=>$this->user_id,
            'works_id'=>$param['works_id'],
            'room_id'=>$param['room_id'],
            'furniture_id'=>$param['furniture_id'],
            'typeid'=>$furniture['typeid'],
            'length'=>$param['length'],
            'width'=>$param['width'],
            'height'=>$param['height'],
            'size_name'=>$param['length'].'x'.$param['width'].'x'.$param['height'].'厘米',
            'area'=>$param['area']>0?$param['area']:0,
            'site'=>$param['site']>0?$param['site']:0,
            'material_filename'=>$param['material_filename'],
            'name'=>$furniture['name'],
            'image'=>$furniture['image'],
            'price'=>$furniture['price'],
            'material_image'=>$material['image'],
            'material_name'=>$material['name'],
        ];
        return $data;
    }

    public function setAllDesign()
    {
        $param = input();
        if(!isset($param['works_id']) || !$param['works_id']){
            return dyajaxReturn(0, '缺少作品ID');
        }
        $furnitures = input('furnitures');
        if(!$furnitures){
            return dyajaxReturn(0, "没有家具信息");
        }
        $furnitures = json_decode(htmlspecialchars_decode($furnitures),true);
        if(!$furnitures){
            return dyajaxReturn(0, "没有家具信息");
        }
        $furniture_data = [];
        foreach ($furnitures as $value){
            $validate = validate(Design::class);
            if (!$validate->batch(true)->check($value)) {
                // 验证失败 输出错误信息
                $error = $validate->getError();
                $error_msg = array_values($error);
                return dyajaxReturn(0, $error_msg[0]);
            }
            $material_filename = $value['material_filename'];
            $furniture = Db::name('furniture')->where('id',$value['furniture_id'])->find();
            if(!$furniture){
                return dyajaxReturn(0,'家居不存在');
            }
            $material = [];
            $materials = json_decode($furniture['material'],true);
            foreach ($materials as $v){
                if($v['filename']==$material_filename){
                    $material = $v;
                    break;
                }
            }
            $furniture_data[] = [
                'user_id'=>$this->user_id,
                'works_id'=>$param['works_id'],
                'room_id'=>$value['room_id'],
                'furniture_id'=>$value['furniture_id'],
                'typeid'=>$furniture['typeid'],
                'length'=>$value['length'],
                'width'=>$value['width'],
                'height'=>$value['height'],
                'size_name'=>$value['length'].'x'.$value['width'].'x'.$value['height'].'厘米',
                'area'=>$value['area']>0?$value['area']:0,
                'site'=>$value['site']>0?$value['site']:0,
                'material_filename'=>$value['material_filename'],
                'name'=>$furniture['name'],
                'image'=>$furniture['image'],
                'price'=>$furniture['price'],
                'material_image'=>$material['image'],
                'material_name'=>$material['name'],
            ];
        }
        Db::name('user_works_furniture')->where('works_id',$param['works_id'])->delete();
        $res = Db::name('user_works_furniture')->insertAll($furniture_data);
        if(!$res){
            return dyajaxReturn(0,'提交失败');
        }
        Db::name('user_works')->where('works_id',input('works_id/d'))->update(['is_edit'=>1]);
        return dyajaxReturn(1,'提交成功');

    }

    //删除家具（工装、软装）
    public function delDesignInfo()
    {
        $id = input('id/d');
        $res = Db::name('user_works_furniture')->where(['id'=>$id,'user_id'=>$this->user_id])->update(['is_deleted'=>1]);
        if(!$res){
            return dyajaxReturn(0,'删除失败');
        }
        return dyajaxReturn(1,'删除成功');
    }

    //获取报价
    public function getDesignStatistics()
    {
        $works_id = input('works_id/d');
        $typeid = input('typeid/d',1);//1-工装 2-软装
        $room_id = input('room_id/d');
        if(!$works_id){
            return dyajaxReturn(0,'缺少作品ID');
        }
        $where['typeid'] = $typeid;
        $where['works_id'] = $works_id;
        if($room_id){
            $where['room_id'] = $room_id;
        }
        $room_ids = Db::name('user_works')
            ->alias('uw')
            ->join('scene_house sh','sh.house_id=uw.house_id')
            ->where('uw.works_id',$works_id)
            ->cache(true,60*30)
            ->value('sh.room_ids');
        $order_fields = Db::name('label')->order('sort asc,id desc')->whereIn('id',$room_ids)->column('id');
        $order_field = implode(',',$order_fields);
        $order = "field(room_id,".$order_field.") asc,site asc,furniture_id desc,material_filename desc,size_name desc";
        $list = Db::name('user_works_furniture')->where($where)->orderRaw($order)->select()->toArray();
        $total = [
            'title'=>'说明:报价仅供参考,以实际市场价为准',
            'total_price'=>0,
            'area'=>0,
        ];
        $rooms = [];
        //1-工装 2-软装
        $room_key = -1;
        if($typeid==2){
            foreach ($list as $value){
                $total['area'] += $value['area'];
                $total['total_price'] += $value['price'];
                if(isset($rooms[$room_key]) && $rooms[$room_key]['room_id']==$value['room_id']){
                    $rooms[$room_key]['total_price'] += $value['price'];
                    $furniture_key = $value['furniture_id'].$value['material_filename'].$value['length'].$value['width'].$value['height'];
                    if(isset($rooms[$room_key]['child'][$furniture_key])){
                        $rooms[$room_key]['child'][$furniture_key]['num'] += 1;
                        $rooms[$room_key]['child'][$furniture_key]['total_price'] += $value['price'];
                    }else{
                        $rooms[$room_key]['child'][$furniture_key] = [
                            'image'=>SITE_URL.get_sub_images(md5($value['image']),$value['image'], $value['furniture_id'], 60, 60, 'furniture'),
                            'name'=>$value['name'],
                            'size_name'=>$value['size_name'],
                            'num'=>$value['num'],
                            'price'=>$value['price']+0,
                            'total_price'=>$value['price']+0,
                        ];
                    }
                }else{
                    $room_key++;
                    $rooms[$room_key]['room_id'] = $value['room_id'];
                    $rooms[$room_key]['room_name'] = get_label_name($value['room_id']);
                    $rooms[$room_key]['total_price'] = $value['price']+0;
                    $furniture_key = $value['furniture_id'].$value['material_filename'].$value['length'].$value['width'].$value['height'];
                    $rooms[$room_key]['child'][$furniture_key] = [
                        'image'=>SITE_URL.get_sub_images(md5($value['image']),$value['image'], $value['furniture_id'], 60, 60, 'furniture'),
                        'name'=>$value['name'],
                        'size_name'=>$value['size_name'],
                        'num'=>$value['num'],
                        'price'=>$value['price']+0,
                        'total_price'=>$value['price']+0,
                    ];
                }
            }
            $rooms = array_values($rooms);
            foreach ($rooms as &$v){
                $v['child'] = array_values($v['child']);
            }
        }else{
            //1-地面  2-墙面  3-天花板
            $site = [
                1=>'地面',
                2=>'墙面',
                3=>'天花板',
            ];
            foreach ($list as $value){
                $total['area'] += $value['area'];
                $total['total_price'] += $value['area']*$value['price'];
                if(isset($rooms[$room_key]) && $rooms[$room_key]['room_id']==$value['room_id']){
                    $rooms[$room_key]['area'] += $value['area'];
                    $rooms[$room_key]['total_price'] += $value['area']*$value['price'];
                    $rooms[$room_key]['child'][] = [
                        'site_name'=>$site[$value['site']],
                        'area'=>$value['area'],
                        'total_price'=>$value['area']*$value['price'],
                        'price'=>$value['price']+0,
                        'material_name'=>$value['material_name'],
                        'material_image'=>SITE_URL.get_sub_images(md5($value['material_image']),$value['material_image'], $value['furniture_id'], 36, 16, 'furniture'),
                    ];
                }else{
                    $room_key++;
                    $rooms[$room_key]['room_id'] = $value['room_id'];
                    $rooms[$room_key]['room_name'] = get_label_name($value['room_id']);
                    $rooms[$room_key]['area'] = $value['area'];
                    $rooms[$room_key]['total_price'] = $value['area']*$value['price'];
                    $rooms[$room_key]['child'][] = [
                        'site_name'=>$site[$value['site']],
                        'area'=>$value['area'],
                        'total_price'=>$value['area']*$value['price'],
                        'price'=>$value['price']+0,
                        'material_name'=>$value['material_name'],
                        'material_image'=>SITE_URL.get_sub_images(md5($value['material_image']),$value['material_image'], $value['furniture_id'], 36, 16, 'furniture'),
                    ];
                }

            }
            $rooms = array_values($rooms);
        }
        $data = [
            'total'=>$total,
            'rooms'=>$rooms,
        ];
        return dyajaxReturn(1,'获取成功',$data);
    }

    public function copyWorks()
    {
        $param = input();
        $works_id = $param['works_id'];
        if(!$works_id){
            return dyajaxReturn(0,'获取作品ID失败');
        }
        $estate = Db::name('user_works')
            ->where('works_id',$works_id)
            ->find();
        $works_data = [
            'title'=>$estate['title'],
            'images'=>$estate['images'],
            'add_time'=>$estate['add_time'],
            'style_id'=>$estate['style_id'],
            'over_images'=>$estate['over_images'],
            'over_content'=>$estate['over_content'],
            'image'=>$estate['image'],
            'price'=>$estate['price'],
            'user_id'=>$this->user_id,
            'house_id'=>$estate['house_id'],
            'is_edit'=>0,
            'estate_id'=>$estate['estate_id'],
            'designjson'=>$estate['designjson'],
        ];
        $works_id = Db::name('user_works')->insertGetId($works_data);
        if(!$works_id){
            return dyajaxReturn(0,'获取作品ID失败');
        }
        return dyajaxReturn(1,'',['works_id'=>$works_id]);
    }
}
