<?php

namespace app\appapi\controller;

use app\common\logic\FurnitureLogic;
use think\facade\Db;
use think\Page;

class Furniture extends Base
{
    /**
     * 获得室内物品分类列表
     *
     * @return void
     */
    public function category()
    {
        $keywords = trim(input('keyword'));
        $where['is_deleted']  = 0;
        $where['pid']  = 0;
        $where['is_show']  = 1;
        $field = 'id,name,folder,icon';
        if($keywords){
            $where['name'] = ['like','%'.$keywords.'%'];
        }
//        $count = Db::name('furniture_category')->where($where)->count();// 查询满足要求的总记录数
//        $pager = new Page($count, config('PAGESIZE'));// 实例化分页类 传入总记录数和每页显示的记录数
        $list = Db::name('furniture_category')->where($where)
            ->field($field)
            ->order('sort asc,id desc')
//            ->limit($pager->firstRow, $pager->listRows)
            ->select()->toArray();
        foreach ($list as &$value){
            $value['thumb'] = SITE_URL.dythumbimages($value['id'],300,300,300,'furniture_category');
            list($width, $height, $type, $attr) = @getimagesize('.'.$value['icon']);
            $value['icon_width'] = $width;
            $value['icon_height'] = $height;
            $value['icon'] = SITE_URL.$value['icon'];
        }
        return dyajaxReturn(1, "家居分类列表", $list);
    }

    /**
     * 获得室内物品分类列表
     *
     * @return void
     */
    public function categoryAll()
    {
        $where['is_deleted']  = 0;
        $where['pid']  = 0;
        $where['is_show']  = 1;
        $field = 'id,name,folder';
        $list = Db::name('furniture_category')->where($where)
            ->field($field)
            ->order('sort asc,id desc')
//            ->limit($pager->firstRow, $pager->listRows)
            ->select()->toArray();
        foreach ($list as &$value){
            $where['pid'] = $value['id'];
            $value['son'] = Db::name('furniture_category')->where($where)
                ->field($field)
                ->order('sort asc,id desc')
                ->select()->toArray();
        }
        return dyajaxReturn(1, "家居分类列表", $list);
    }

    //家居列表
    public function getFurnitureList()
    {
        $type = input('typeid/d',0);//0=全部 1=首页榜单 2=我的浏览历史  3=我的收藏  4=相关推荐
        $id = input('id/d',0);//0=全部 1=首页榜单 2=我的浏览历史  3=我的收藏  4=相关推荐
        $cate_id = input('cate_id/d',0);//分类ID
        $two_id = input('two_id/d',0);//分类ID
        $where = [];
        $keyword = input('keyword/s',false,'trim');
        if($keyword){
            $where['a.name'] = ['like',"%$keyword%"];
        }
        if($cate_id){
            $where['a.cate_id'] = $cate_id;
        }
        if($two_id){
            $where['a.two_cate_id'] = $two_id;
        }
        if(in_array($type,[2,3]) && !$this->user_id){
            return dyajaxReturn(-100,'请先登录');
        }
        switch ($type){
            case 2:
            case 3:
                $where['b.user_id'] = $this->user_id;
                break;
            case 4:
                $where['a.cate_id'] = $cate_id;
                $where['a.id'] = ['<>',$id];
                break;
        }
        if($type==4 && $this->user_id>0 && $this->user['is_recd']!=1){
            $list = [];
        }else{
            $FurnitureLogic = new FurnitureLogic();
            $list = $FurnitureLogic->getFurnitureList($type,$where,$this->user_id);
        }
        return dyajaxReturn(1, '家居列表',$list);
    }

    //家具详情
    public function getFurniture()
    {
        $id = input('id/d');
        $FurnitureLogic = new FurnitureLogic();
        $info = $FurnitureLogic->getFurniture($id,$this->user_id);
        return dyajaxReturn(1, '家居详情',$info);
    }

    //家具详情
    public function getFurnitureByFilename()
    {
        $name = input('name/s');
        $FurnitureLogic = new FurnitureLogic();
        $info = $FurnitureLogic->getFurniture($name,$this->user_id,2);
        return dyajaxReturn(1, '家居详情',$info);
    }

    //收藏,取消收藏
    public function collect()
    {
        $aim_id = input('aim_id/d',0);
        if(!$aim_id){
            return dyajaxReturn(0,'缺少信息Id');
        }
        $source = input('source/d',1);//1=收藏 2取消收藏
        $user = $this->user;
        $user_collect = Db::name('furniture_collect')->where('aim_id', $aim_id)->where('user_id',$user['user_id'])->value('id');
        $num = Db::name('furniture')->where('id', $aim_id)->value('collect');
        if($source==1){
            //收藏
            $data = [
                'user_id' => $user['user_id'],
                'aim_id' => $aim_id,
                'add_time' => time(),
            ];
            if($user_collect){
                return dyajaxReturn(1, '收藏成功',['status'=>true,'num'=>$num]);
            }
            //添加成功
            $res = Db::name('furniture_collect')->insert($data);
            if ($res) {
                Db::name('furniture')->where('id', $aim_id)->inc('collect')->update();
                return dyajaxReturn(1, '收藏成功',['status'=>true,'num'=>$num+1]);
            } else {
                return dyajaxReturn(0, '收藏失败',['status'=>false,'num'=>$num]);
            }
        }else{
            //取消收藏
            $where['aim_id'] = $aim_id;
            $where['user_id'] = $user['user_id'];
            $res = Db::name('furniture_collect')->where($where)->delete();
            if ($res) {
                Db::name('furniture')->where('id', $aim_id)->dec('collect')->update();
                return dyajaxReturn(1, '取消收藏成功',['status'=>false,'num'=>$num-1]);
            }else{
                if($user_collect){
                    return dyajaxReturn(0, '取消收藏失败',['status'=>true,'num'=>$num]);
                }
                return dyajaxReturn(1, '取消收藏成功',['status'=>false,'num'=>$num]);
            }
        }
    }
    //点赞和取消点赞
    public function praise()
    {
        $aim_id = input('aim_id/d',0);
        if(!$aim_id){
            return dyajaxReturn(0,'缺少信息Id');
        }
        $source = input('source/d',1);//1=收藏 2取消收藏
        $user = $this->user;
        $user_collect = Db::name('furniture_praise')->where('aim_id', $aim_id)->where('user_id',$user['user_id'])->value('id');
        if($source==1){
            //收藏
            $data = [
                'user_id' => $user['user_id'],
                'aim_id' => $aim_id,
                'add_time' => time(),
            ];
            if($user_collect){
                return dyajaxReturn(1, '点赞成功');
            }
            //添加成功
            $res = Db::name('furniture_praise')->insert($data);
            if ($res) {
                return dyajaxReturn(1, '点赞成功');
            } else {
                return dyajaxReturn(0, '点赞失败');
            }
        }else{
            //取消收藏
            $where['aim_id'] = $aim_id;
            $where['user_id'] = $user['user_id'];
            $res = Db::name('furniture_praise')->where($where)->delete();
            if ($res) {
                return dyajaxReturn(1, '取消点赞成功');
            }else{
                if($user_collect){
                    return dyajaxReturn(0, '取消点赞失败');
                }
                return dyajaxReturn(1, '取消点赞成功');
            }
        }
    }

    //获取搜索选项
    public function getOptionList()
    {
        $where['is_deleted']  = 0;
        $where['is_show']  = 1;
        $field = 'id,name,typeid';
        $list = Db::name('furniture_category')->where($where)
            ->field($field)
            ->cache(true,60*30)
            ->order('sort asc,id desc')
            ->select()->toArray();
        $frock = [];
        $soft = [];
        foreach ($list as $value){
            if($value['typeid']==1){
                unset($value['typeid']);
                $frock[] = $value;
            }else{
                unset($value['typeid']);
                $soft[] = $value;
            }
        }
        $data = [
            'frock'=>$frock,
            'soft'=>$soft,
        ];
        return dyajaxReturn(1,'获取选项成功',$data);
    }

    //获取搜索选项
    public function getNewOptionList()
    {
        $where['is_deleted']  = 0;
        $where['pid']  = 0;
        $where['is_show']  = 1;
        $field = 'id,name';
        $list = Db::name('furniture_category')->where($where)
            ->field($field)
            ->cache(true,60*30)
            ->order('sort asc,id desc')
            ->select()->toArray();
        foreach ($list as &$vo){
            $where['pid'] = $vo['id'];
            $son = Db::name('furniture_category')->where($where)
                ->field($field)
                ->order('sort asc,id desc')
                ->select()->toArray();
            foreach ($son as &$vv) {
                $vv['two_id'] = $vv['id'];
                $vv['id'] = $vo['id'];
            }
            $vo['son'] = $son?array_merge([
                [
                    'id'=>$vo['id'],
                    'name'=>'全部',
                    'two_id'=>0
                ]
            ],$son):[
                [
                    'id'=>$vo['id'],
                    'name'=>'全部',
                    'two_id'=>0
                ]
            ];
        }
        $data = $list;
        return dyajaxReturn(1,'获取选项成功',$data);
    }
}
