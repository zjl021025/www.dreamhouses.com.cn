<?php

namespace app\appapi\controller;

use app\common\logic\ScenehouseLogic;

//工作台户型
class Scenehouse extends Base
{
    //户型列表
    public function getScenehouseList()
    {
        $where = [];
        $keyword = input('keyword/s',false,'trim');
        if($keyword){
            $where['a.name'] = ['like',"%$keyword%"];
        }
        //户型
        $house_type_id = input('house_type_id/d',0);
        if($house_type_id){
            $where['a.house_type_id'] = $house_type_id;
        }
        //面积
        $area_id = input('area_id/d',0);
        if($area_id){
            $area = get_label_min_max($area_id);
            $min_area = $area['min'];
            $max_area = $area['max'];
            $where['a.area'] = ['between',[$min_area,$max_area]];
        }
        $ScenehouseLogic = new ScenehouseLogic();
        $mylist = $ScenehouseLogic->getScenehouseList($where,1,$this->user_id);
        $list = $ScenehouseLogic->getScenehouseList($where);
        $data['mylist'] = $mylist;
        $data['list'] = $list;
        return dyajaxReturn(1, '户型列表',$data);
    }
}
