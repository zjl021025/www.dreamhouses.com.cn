<?php

namespace app\appapi\controller;

use app\common\model\Users;
use app\common\validate\SiteValidate;
use app\Request;
use think\facade\Db;

class PersonalCenter
{
    /**
     * 用户信息
     * @return void
     */
    public function userInformation(){
        $user_id = dydecrypt(input('user_id'));
        $user = Db::name('users')->where(['user_id'=>$user_id,'status'=>1])->find();
        if(!$user){
            return dyjsonReturn(0,'该用户已被封禁');
        }
        //查询用户信息
        $userData = Db::name('users')->where('user_id',$user_id)->find();
        $userData['add_time'] = date('Y-m-d',$userData['add_time']);
        $userData['birthday'] = date('Y-m-d',$userData['birthday']);
        if(empty($userData)){
            return dyjsonReturn(0,'用户信息查询失败');
        }
        return dyjsonReturn(1,'用户信息查询成功',$userData);
    }

    /**
     * 修改我的昵称
     * @return void
     */
    public function updateNickname(){
        $user_id = dydecrypt(input('user_id'));
        $user = Db::name('users')->where(['user_id'=>$user_id,'status'=>1])->find();
        if(!$user){
            return dyjsonReturn(0,'该用户已被封禁');
        }
        $nickname = input('nickname');
        $userData = Db::name('users')->where(['user_id'=>$user_id,'nickname'=>$nickname])->find();
        if(!empty($userData)){
            return dyjsonReturn(0,'昵称没有改动');
        }
        $modification = Db::name('users')->where('user_id',$user_id)->update(['nickname'=>$nickname]);
        if($modification == 0){
            return dyjsonReturn(0,'修改失败');
        }
        return dyjsonReturn(1,'修改成功');
    }

    /**
     * 修改个人简介
     * @return void
     */
    public function updatePersonalSign(){
        $user_id = dydecrypt(input('user_id'));
        $user = Db::name('users')->where(['user_id'=>$user_id,'status'=>1])->find();
        if(!$user){
            return dyjsonReturn(0,'该用户已被封禁');
        }
        $personal_sign = input('personal_sign');
        $modification = Db::name('users')->where('user_id',$user_id)->update(['personal_sign'=>$personal_sign]);
        if($modification == 0){
            return dyjsonReturn(0,'修改失败');
        }
        return dyjsonReturn(1,'修改成功');
    }

    /**
     * 账号安全
     * @return void
     */
    public function AccountSecurity(){
        $user_id = dydecrypt(input('user_id'));
        $user = Db::name('users')->where(['user_id'=>$user_id,'status'=>1])->find();
        if(!$user){
            return dyjsonReturn(0,'该用户已被封禁');
        }
        //查询账号信息
        $accountData = Db::name('users')->field('mobile')->where('user_id',$user_id)->find();
        $accountData['qq'] = Db::name('oauth_users')->where(['user_id'=>$user_id,'oauth'=>'qq'])->select()->toArray();
        $accountData['weixin'] = Db::name('oauth_users')->where(['user_id'=>$user_id,'oauth'=>'weixin'])->select()->toArray();
        if(empty($accountData)){
            return dyjsonReturn(0,'查询失败');
        }
        return dyjsonReturn(1,'查询成功',$accountData);
    }

    /**
     * 获取地址信息
     * @return void
     */
    public function getSite(){
        $user_id = dydecrypt(input('user_id'));
        $user = Db::name('users')->where(['user_id'=>$user_id,'status'=>1])->find();
        if(!$user){
            return dyjsonReturn(0,'该用户已被封禁');
        }
        $siteData = Db::name('site')->where(['user_id'=>$user_id,'delete_time'=>null])->select()->toArray();
        if(empty($siteData)){
            return dyjsonReturn(0,'地址为空');
        }
        return dyjsonReturn(1,'查询成功',$siteData);
    }

    /**
     * 设置默认地址
     * @return void
     */
    public function setDefault(){
        $site_id = input('site_id');
        $user_id = dydecrypt(input('user_id'));
        $user = Db::name('users')->where(['user_id'=>$user_id,'status'=>1])->find();
        if(!$user){
            return dyjsonReturn(0,'该用户已被封禁');
        }
        Db::name('site')->where('user_id',$user_id)->update(['is_default'=>0]);
        $upDefault = Db::name('site')->where(['user_id'=>$user_id,'site_id'=>$site_id])->update(['is_default'=>1]);
        if($upDefault == 0){
            return dyjsonReturn(0,'设置失败');
        }
        return dyjsonReturn(1,'设置成功');
    }

    /**
     * 删除地址
     * @return void
     */
    public function delSite(){
        $user_id = dydecrypt(input('user_id'));
        $site_id = input('site_id');
        $user = Db::name('users')->where(['user_id'=>$user_id,'status'=>1])->find();
        if(!$user){
            return dyjsonReturn(0,'该用户已被封禁');
        }
        //删除该地址
        $del_site = Db::name('site')->where(['user_id'=>$user_id,'site_id'=>$site_id])->update(['delete_time'=>time()]);
        if($del_site == 0){
            return dyjsonReturn(0,'删除失败');
        }
        return dyjsonReturn(1,'删除成功');
    }

    /**
     * 添加地址
     * @return void
     */
    public function addSite(){
        $param = input();
        $param['user_id'] = dydecrypt($param['user_id']);
        $validate = new SiteValidate();
        $result = $validate->check($param);
        if (!$result) {
            return dyjsonReturn(0, $validate->getError());
        }
        $user = Db::name('users')->where(['user_id'=>$param['user_id'],'status'=>1])->find();
        if(!$user){
            return dyjsonReturn(0,'该用户已被封禁');
        }
        $param['create_time'] = time();
        //添加地址
        $addSite = Db::name('site')->insert($param);
        if(!$addSite){
            return dyjsonReturn(0,'添加失败');
        }
        return dyjsonReturn(1,'添加成功');
    }

    /**
     * 修改地址
     * @return void
     */
    public function updateSite(){
        $param = input();
        $param['user_id'] = dydecrypt($param['user_id']);
        $validate = new SiteValidate();
        $result = $validate->check($param);
        if (!$result) {
            return dyjsonReturn(0, $validate->getError());
        }
        $user = Db::name('users')->where(['user_id'=>$param['user_id'],'status'=>1])->find();
        if(!$user){
            return dyjsonReturn(0,'该用户已被封禁');
        }
        $param['update_time'] = time();
        //修改地址
        $updateSite = Db::name('site')->where(['user_id'=>$param['user_id'],'site_id'=>$param['site_id']])->update(['consignee'=>$param['consignee'],'mobile'=>$param['mobile'],'area'=>$param['area'],'is_default'=>$param['is_default'],'site'=>$param['site'],'update_time'=>$param['update_time']]);
        if($updateSite == 0){
            return dyjsonReturn(0,'修改失败');
        }
        return dyjsonReturn(1,'修改成功');
    }

    /**
     * 修改生日
     * @return void
     */
    public function updateBirthday(){
        $user_id = dydecrypt(input('user_id'));
        $birthday = input('birthday');
        if(empty($birthday)){
            return dyjsonReturn(0,'生日不可为空');
        }
        $user = Db::name('users')->where(['user_id'=>$user_id,'status'=>1])->find();
        if(!$user){
            return dyjsonReturn(0,'该用户已被封禁');
        }
        $birthday = strtotime($birthday);
        $change = Db::name('users')->where('user_id',$user_id)->update(['birthday'=>$birthday]);
        if(!$change){
            return dyjsonReturn(0,'生日修改失败');
        }
        return dyjsonReturn(1,'修改成功');

    }

    /**
     * 修改生日
     * @return void
     */
    public function updateSex(){
        $user_id = dydecrypt(input('user_id'));
        $sex = input('sex');
        if(empty($sex) && $sex != 0){
            return dyjsonReturn(0,'性别不可为空');
        }
        $user = Db::name('users')->where(['user_id'=>$user_id,'status'=>1])->find();
        if(!$user){
            return dyjsonReturn(0,'该用户已被封禁');
        }
        $change = Db::name('users')->where('user_id',$user_id)->update(['sex'=>$sex]);
        if(!$change){
            return dyjsonReturn(0,'性别修改失败');
        }
        return dyjsonReturn(1,'修改成功');
    }

    /**
     * 修改头像
     * @return void
     */
    public function setAvatar(){
        $user_id = dydecrypt(input('user_id'));
        $avatar = input('avatar');
        if(empty($avatar)){
            return dyjsonReturn(0,'头像不可为空');
        }
        $user = Db::name('users')->where(['user_id'=>$user_id,'status'=>1])->find();
        if(!$user){
            return dyjsonReturn(0,'该用户已被封禁');
        }
        $change = Db::name('users')->where(['user_id'=>$user_id])->update(['avatar'=>$avatar]);
        if(!$change){
            return dyjsonReturn(0,'修改失败');
        }
        return dyjsonReturn(1,'修改成功');
    }

    /**
     * 获取用户基本信息
     * @return void
     */
    public function getUserInfoFirst(){
        $positionData = Db::name('label')->where(['typeid'=>11,'is_show' => 1])->select()->toArray();
        if(!$positionData){
            return dyjsonReturn(0,'查询失败');
        }
        return dyjsonReturn(1,'查询成功',$positionData);
    }

    /**
     * 设置用户信息（第一次进入）
     * @return void
     */
    public function setUserInfo(){
        $param = \think\facade\Request::param();
        if(empty($param['user_id'])||empty($param['nickname'])||empty($param['birthday'])||empty($param['sex']) || empty($param['position'])){
            return dyjsonReturn(0,'数据传输错误');
        }
        $user_id = dydecrypt($param['user_id']);
        $birthday = strtotime($param['birthday']);
        //修改用户信息
        $update = Db::name('users')->where(['user_id'=>$user_id])->update(['avatar' => $param['avatar'],'nickname' => $param['nickname'],'sex' => $param['sex'],'birthday' => $birthday,'position' => $param['position']]);
        if(!$update){
            return dyjsonReturn(0,'修改失败');
        }
        return dyjsonReturn(1,'修改成功');
    }

    /**
     * 点击我的收藏
     * @return void
     */
    public function clickMyCollect(){
        $user_id = dydecrypt(input('user_id'));
        $code = input('code');
        $startPage = input('startPage');
        $pageNum = input('pageNum');
        switch ($code){
            case 1:
                //案例
                $collect = Db::name('user_collect')->where(['user_id'=>$user_id])->limit($startPage,$pageNum)->select()->toArray();
                $collectData = [];
                foreach ($collect as $k => $v){
                    $collectData[] = Db::name('user_dynamic')
                        ->alias('dy')
                        ->field('dy.dynamic_id,dy.title,dy.content,dy.image,dy.video,dy.praise,dy.collect,dy.evaluate,dy.user_id,users.avatar,users.nickname,dy.ios_url,dy.Android_url,dy.pc_url')
                        ->leftJoin('users', 'dy.user_id = users.user_id')
                        ->where(['dynamic_id'=>$v['aim_id']])->order(['dy.dynamic_id' => 'desc'])->find();
                }
                foreach ($collectData as $key => $val) {
                    //点赞
                    $praise = Db::name('user_praise')->where(['user_id' => $user_id, 'aim_id' => $val['dynamic_id']])->find();
                    $collectData[$key]['praise'] = Db::name('user_praise')->where('aim_id', $val['dynamic_id'])->count();
                    if (!$praise) {
                        $collectData[$key]['is_praise'] = 0;
                    } else {
                        $collectData[$key]['is_praise'] = 1;
                    }
                }
                break;
            case 2:
                //家具
                $collect = Db::name('furniture_collect')->where(['user_id'=>$user_id])->limit($startPage,$pageNum)->select()->toArray();
                $collectData = [];
                foreach ($collect as $k => $v){
                    $collectData[] = Db::name('furniture')->field('id,name,content,image')->where(['id'=>$v['aim_id']])->find();
                }
                foreach ($collectData as $key => $val){
                    $collectData[$key]['praise'] = Db::name('furniture_praise')->where(['aim_id'=>$val['id']])->count();
                    $praise = Db::name('furniture_praise')->where(['aim_id'=>$val['id'],'user_id'=>$user_id])->find();
                    if (!$praise) {
                        $collectData[$key]['is_praise'] = 0;
                    } else {
                        $collectData[$key]['is_praise'] = 1;
                    }
                }
                break;
            default:
                break;
        }
        if(empty($collectData)){
            return dyjsonReturn(0,'暂未有收藏列表');
        }
        return dyjsonReturn(1,'查询成功',$collectData);
    }

    /**
     * 点击我的点赞
     * @return void
     */
    public function clickMyPraise(){
        $user_id = dydecrypt(input('user_id'));
        $code = input('code');
        $startPage = input('startPage');
        $pageNum = input('pageNum');
        switch ($code){
            case 1:
                //案例
                $praise = Db::name('user_praise')->where(['user_id'=>$user_id])->limit($startPage,$pageNum)->select()->toArray();
                $praiseData = [];
                foreach ($praise as $k => $v){
                    $praiseData[] = Db::name('user_dynamic')
                        ->alias('dy')
                        ->field('dy.dynamic_id,dy.title,dy.content,dy.image,dy.video,dy.praise,dy.collect,dy.evaluate,dy.user_id,users.avatar,users.nickname,dy.ios_url,dy.Android_url,dy.pc_url')
                        ->leftJoin('users', 'dy.user_id = users.user_id')
                        ->where(['dynamic_id'=>$v['aim_id']])->order(['dy.dynamic_id' => 'desc'])->find();
                }
                foreach ($praiseData as $key => $val) {
                    //点赞
                    $praiseData[$key]['praise'] = Db::name('user_praise')->where('aim_id', $val['dynamic_id'])->count();
                    $praiseData[$key]['is_praise'] = 1;
                }
                break;
            case 2:
                //家具
                $praise = Db::name('furniture_praise')->where(['user_id'=>$user_id])->limit($startPage,$pageNum)->select()->toArray();
                $praiseData = [];
                foreach ($praise as $k => $v){
                    $praiseData[] = Db::name('furniture')->field('id,name,content,image')->where(['id'=>$v['aim_id']])->find();
                }
                foreach ($praiseData as $key => $val){
                    $praiseData[$key]['praise'] = Db::name('furniture_praise')->where(['aim_id'=>$val['id']])->count();
                    $praiseData[$key]['is_praise'] = 1;
                }
                break;
            default:
                break;
        }
        if(empty($praiseData)){
            return dyjsonReturn(0,'暂未有点赞列表');
        }
        return dyjsonReturn(1,'查询成功',$praiseData);

    }

}