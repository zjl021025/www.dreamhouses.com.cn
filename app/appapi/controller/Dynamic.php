<?php


namespace app\appapi\controller;

use app\common\logic\DynamicLogic;
use app\common\logic\UsersLogic;
use app\common\validate\Evaluate;
use app\common\validate\UserDynamic;
use think\facade\Db;

class Dynamic extends Base
{
    //获取方案列表
    public function getSchemeList()
    {
        $type = input('typeid/d',0);//0=全部 1=首页推荐 2=方案推荐 3=专家推荐 4=最热 5=最新  6=我的动态  7=点赞记录  8=浏览记录  9=收藏记录  10=首页榜单
        $city_id = input('city_id/d',0);
        $district_id = input('district_id/d',0);
        $user_id = input('user_id/s',0);//主页用户id
        $where = [];
        if($user_id){
            $user_id = dydecrypt($user_id);
            $where['a.user_id'] = $user_id;
        }
        if(in_array($type,[6,7,8,9]) && !$this->user_id){
            return dyajaxReturn(-100,'请先登录');
        }
        switch ($type){
            case 6:
                $where['a.user_id'] = $this->user_id;
                break;
            case 7:
            case 8:
            case 9:
                $where['b.user_id'] = $this->user_id;
                break;
        }
        if($city_id){
            $where['a.city_id'] = $city_id;
        }
        if($district_id){
            $where['a.district_id'] = $district_id;
        }
        $keyword = input('keyword/s',false,'trim');
        if($keyword){
            $where['a.title'] = ['like',"%$keyword%"];
        }
        //风格
        $style_id = input('style_id/d',0);
        if($style_id){
            $where['a.style_id'] = $style_id;
        }
        //户型
        $house_type_id = input('house_type_id/d',0);
        if($house_type_id){
            $where['a.house_type_id'] = $house_type_id;
        }
        //预算
        $budget_id = input('budget_id/d',0);
        if($budget_id){
            $budget = get_label_min_max($budget_id);
            $min_price = $budget['min'];
            $max_price = $budget['max'];
            $where['a.price'] = ['between',[$min_price,$max_price]];
        }
        //面积
        $area_id = input('area_id/d',0);
        if($area_id){
            $area = get_label_min_max($area_id);
            $min_area = $area['min'];
            $max_area = $area['max'];
            $where['a.area'] = ['between',[$min_area,$max_area]];
        }
        $DynamicLogic = new DynamicLogic();
        $list = $DynamicLogic->getSchemeList($type,$where,$this->user_id);
        return dyajaxReturn(1, '方案列表',$list);
    }

    //视频列表
    public function getVideoList()
    {
        $type = input('typeid/d',0);//0=全部 1=首页推荐 6=我的动态  7=点赞记录  8=浏览记录  9=收藏记录
        $user_id = input('user_id/s',0);//主页用户id
        $where = [];
        if($user_id){
            $user_id = dydecrypt($user_id);
            $where['a.user_id'] = $user_id;
        }
        if(in_array($type,[6,7,8,9]) && !$this->user_id){
            return dyajaxReturn(-100,'请先登录');
        }
        switch ($type){
            case 6:
                $where['a.user_id'] = $this->user_id;
                break;
            case 7:
            case 8:
            case 9:
                $where['b.user_id'] = $this->user_id;
                break;
        }
        $keyword = input('keyword/s',false,'trim');
        if($keyword){
            $where['a.content'] = ['like',"%$keyword%"];
        }
        $DynamicLogic = new DynamicLogic();
        $list = $DynamicLogic->getVideoList($type,$where,$this->user_id);
        return dyajaxReturn(1, '视频列表',$list);
    }

    //图文列表
    public function getImagetextList()
    {
        $type = input('typeid/d',0);//0=全部 1=首页推荐 6=我的动态  7=点赞记录  8=浏览记录  9=收藏记录
        $user_id = input('user_id/s',0);//主页用户id
        $where = [];
        if($user_id){
            $user_id = dydecrypt($user_id);
            $where['a.user_id'] = $user_id;
        }
        if(in_array($type,[6,7,8,9]) && !$this->user_id){
            return dyajaxReturn(-100,'请先登录');
        }
        switch ($type){
            case 6:
                $where['a.user_id'] = $this->user_id;
                break;
            case 7:
            case 8:
            case 9:
                $where['b.user_id'] = $this->user_id;
                break;
        }
        $keyword = input('keyword/s',false,'trim');
        if($keyword){
            $where['a.title'] = ['like',"%$keyword%"];
        }
        $DynamicLogic = new DynamicLogic();
        $list = $DynamicLogic->getImagetextList($type,$where,$this->user_id);
        return dyajaxReturn(1, '图文列表',$list);
    }

    //详情
    public function getDynamic()
    {
        $typeid = input('typeid/d');//1=图文 2=视频 3=方案
        switch ($typeid){
            case 1:
                $field = 'dynamic_id,title,content,images,user_id,scheme_id,praise,collect,evaluate,typeid';
                break;
            case 2:
                $field = 'dynamic_id,content,video,user_id,scheme_id,praise,collect,evaluate,typeid,image';
                break;
            case 3:
                $field = 'dynamic_id,title,content,images,user_id,praise,collect,evaluate,house_type_id,style_id,area,over_content,over_images,typeid,province_id,city_id,district_id,works_id';
                break;
            default:
                return dyajaxReturn(0, '类型错误');
        }
        $dynamic_id = input('dynamic_id/d');
        if(!$dynamic_id){
            return dyajaxReturn(0, '缺少信息ID');
        }
        $DynamicLogic = new DynamicLogic();
        $info = $DynamicLogic->getDynamic($typeid,$dynamic_id,$this->user_id,$field);
        return dyajaxReturn(1, '详情',$info);
    }

    public function delDynamic()
    {
        $dynamic_id = input('dynamic_id/d');
        $where['dynamic_id'] = $dynamic_id;
        $where['user_id'] = $this->user_id;
        $count = Db::name('user_dynamic')->where($where)->value('dynamic_id');
        if(!$count){
            return dyajaxReturn(0, "信息不存在");
        }
        Db::name('user_dynamic')->where($where)->delete();
        return dyajaxReturn(1, '删除成功');
    }
    //获取设计师列表
    public function getDesignerList()
    {
        $typeid = input('typeid/d',0);//0=设计师列表  1=首页榜单
        $keyword = input('keyword/s',false,'trim');
        $where = [];
        if($keyword){
            $where['nickname'] = ['like',"%$keyword%"];
        }
        $designer_type = input('designer_type/d',0);//0=全部 1=独立设计师  2=机构设计师
        if($designer_type){
            $where['designer_type'] = $designer_type;
        }
        //风格
        $style_id = input('style_id/d',0);
        if($style_id){
            $where[] = ['', 'exp', Db::raw("FIND_IN_SET(".$style_id.",style_ids)")];
        }
        //价格
        $price_id = input('price_id/d',0);
        if($price_id){
            $where['price_id'] = $price_id;
        }
        //经验
        $expe_id = input('expe_id/d',0);
        if($expe_id){
            $where['expe_id'] = $expe_id;
        }
        $UsersLogic = new UsersLogic();
        $list = $UsersLogic->getDesignerList($typeid,$where,$this->user_id);
        return dyajaxReturn(1, '设计师列表',$list);
    }

    //设计师主页
    public function userInfo()
    {
        $user_id = input('user_id/s',0);//主页用户id
        if($user_id){
            $user_id = dydecrypt($user_id);
            if(!$user_id){
                return dyajaxReturn(0, '用户不存在');
            }
        }
        $user = new UsersLogic();
        $userinfo = $user->getInfo($user_id,$this->user_id);
        return dyajaxReturn(1, '获取我的', $userinfo);
    }

    //动态点赞,取消点赞
    public function praise()
    {
        $aim_id = input('aim_id/d',0);
        if(!$aim_id){
            return dyajaxReturn(0,'缺少信息Id');
        }
        $source = input('source/d',1);//1=点赞 2取消点赞
        $user = $this->user;
        $user_praise = Db::name('user_praise')->where('aim_id', $aim_id)->where('user_id',$user['user_id'])->value('id');
        $num = Db::name('user_dynamic')->where('dynamic_id', $aim_id)->value('praise');
        if($source==1){
            //点赞
            $data = [
                'user_id' => $user['user_id'],
                'aim_id' => $aim_id,
                'add_time' => time(),
            ];
            if($user_praise){
                return dyajaxReturn(1, '已经点赞成功了',['status'=>true,'num'=>$num]);
            }
            //添加成功
            $res = Db::name('user_praise')->insert($data);
            if ($res) {
                Db::name('user_dynamic')->where('dynamic_id', $aim_id)->inc('praise')->update();
                return dyajaxReturn(1, '点赞成功',['status'=>true,'num'=>$num+1]);
            } else {
                return dyajaxReturn(0, '点赞失败',['status'=>false,'num'=>$num]);
            }
        }else{
            //取消点赞
            $where['aim_id'] = $aim_id;
            $where['user_id'] = $user['user_id'];
            $res = Db::name('user_praise')->where($where)->delete();
            if ($res) {
                Db::name('user_dynamic')->where('dynamic_id', $aim_id)->dec('praise')->update();
                return dyajaxReturn(1, '取消点赞成功',['status'=>false,'num'=>$num-1]);
            }else{
                if($user_praise){
                    return dyajaxReturn(0, '取消点赞失败',['status'=>true,'num'=>$num]);
                }
                return dyajaxReturn(1, '取消点赞成功',['status'=>false,'num'=>$num]);
            }
        }
    }

    //动态收藏,取消收藏
    public function collect()
    {
        $aim_id = input('aim_id/d',0);
//        $typeid = input('typeid/d',0);//1=图文 2=视频 3=方案 4=家居
        if(!$aim_id){
            return dyajaxReturn(0,'缺少信息Id');
        }
        $source = input('source/d',1);//1=收藏 2取消收藏
        $user = $this->user;
        $user_collect = Db::name('user_collect')->where('aim_id', $aim_id)->where('user_id',$user['user_id'])->value('id');
//        $num = 1;
        $num = Db::name('user_dynamic')->where('dynamic_id', $aim_id)->value('collect');
        if($source==1){
            //点赞
            $data = [
                'user_id' => $user['user_id'],
                'aim_id' => $aim_id,
//                'typeid' => $typeid,
                'add_time' => time(),
            ];
            if($user_collect){
                return dyajaxReturn(1, '已经收藏成功了',['status'=>true,'num'=>$num]);
            }
            //添加成功
            $res = Db::name('user_collect')->insert($data);
            if ($res) {
                Db::name('user_dynamic')->where('dynamic_id', $aim_id)->inc('collect')->update();
                return dyajaxReturn(1, '收藏成功',['status'=>true,'num'=>$num+1]);
            } else {
                return dyajaxReturn(0, '收藏失败',['status'=>false,'num'=>$num]);
            }
        }else{
            //取消点赞
            $where['aim_id'] = $aim_id;
            $where['user_id'] = $user['user_id'];
//            $where['typeid'] = $typeid;
            $res = Db::name('user_collect')->where($where)->delete();
            if ($res) {
                Db::name('user_dynamic')->where('dynamic_id', $aim_id)->dec('collect')->update();
                return dyajaxReturn(1, '取消收藏成功',['status'=>false,'num'=>$num-1]);
            }else{
                if($user_collect){
                    return dyajaxReturn(0, '取消收藏失败',['status'=>true,'num'=>$num]);
                }
                return dyajaxReturn(1, '取消收藏成功',['status'=>false,'num'=>$num]);
            }
        }
    }

    //评价
    public function evaluate()
    {
        $user = $this->user;
        $validate = validate(Evaluate::class);
        $param = input();
        if (!$validate->batch(true)->check($param)) {
            // 验证失败 输出错误信息
            $error = $validate->getError();
            $error_msg = array_values($error);
            return dyajaxReturn(0, $error_msg[0]);
        }
        $data = [
            'user_id' => $user['user_id'],
            'dynamic_id' => $param['dynamic_id'],
            'content' => $param['content'],
            'add_time' => time(),
        ];
        //添加成功
        $res = Db::name('evaluate')->insert($data);
        if ($res) {
            Db::name('user_dynamic')->where('dynamic_id', $param['dynamic_id'])->inc('evaluate')->update();
            $evaluate = Db::name('user_dynamic')->where('dynamic_id', $param['dynamic_id'])->value('evaluate');
            return dyajaxReturn(1, '评价成功',['evaluate'=>$evaluate]);
        } else {
            return dyajaxReturn(0, '评价失败');
        }
    }

    //用户关注,取消关注
    public function follow()
    {
        $user = $this->user;
        //关注用户
        $user_id = input('user_id/s');//用户id
        if(!$user_id){
            return dyajaxReturn(0,'请选择用户');
        }
        $user_id = dydecrypt($user_id);
        if(!$user_id){
            return dyajaxReturn(0,'请选择用户');
        }
        $user_count = Db::name('users')->where(['is_deleted'=>0,'user_id'=>$user_id])->value('user_id');
        if(!$user_count){
            return dyajaxReturn(0,'用户不存在');
        }
        $source = input('source/d',1);//1=关注 2取消关注
        $count = Db::name('user_follow')->where(['aim_id'=>$user_id,'user_id'=>$user['user_id']])->value('follow_id');
        if($source!=1){
            //取消关注
            if($count){
                $res = Db::name('user_follow')->where(['aim_id'=>$user_id,'user_id'=>$user['user_id']])->delete();
            }else{
                $res = true;
            }
            $is_follow = 0;
            $msg = '取消关注';
        }else{
            //关注
            if($user_id==$user['user_id']){
                return dyajaxReturn(0,'不能关注自己');
            }
            if(!$count){
                $res = Db::name('user_follow')->insert(['aim_id'=>$user_id,'user_id'=>$user['user_id'],'add_time'=>time()]);
            }else{
                $res = true;
            }
            $msg = '关注';
            $is_follow = 1;
        }
        $count = Db::name('user_follow')->where(['aim_id'=>$user_id])->count();
        if ($res) {
            return dyajaxReturn(1, $msg.'成功',['is_follow'=>$is_follow,'follow_sum'=>$count]);
        } else {
            return dyajaxReturn(1, $msg.'失败');
        }
    }

    //发布视频
    public function addVideo()
    {
        $validate = validate(UserDynamic::class);
        $param = input();
        if (!$validate->scene('video')->batch(true)->check($param)) {
            // 验证失败 输出错误信息
            $error = $validate->getError();
            $error_msg = array_values($error);
            return dyajaxReturn(0, $error_msg[0]);
        }

        # ffmpeg和ffprobe为上面下载的扩展在你项目中的路径
        $file = ".".$param['video'];
        $ffprobe = \FFMpeg\FFProbe::create([
            'ffmpeg.binaries'  => app()->getRootPath().'/public/public/plugins/ffmpeg-git-20220622-amd64-static/ffmpeg',	# 你自己安装的位置
            'ffprobe.binaries' => app()->getRootPath().'/public/public/plugins/ffmpeg-git-20220622-amd64-static/ffprobe'	# 你自己安装的位置
        ]);
        $video_time = (int)$ffprobe->format($file)->get('duration',0);
        $estate_id = 0;
        if($param['scheme_id']){
            $estate_id = Db::name('user_dynamic')
                ->where('dynamic_id',$param['scheme_id'])->value('estate_id');
        }
        $dynamic_id = input('dynamic_id/d');
        $data = [
            'content'=>$param['content'],
            'video'=>$param['video'],
            'scheme_id'=>$param['scheme_id']?$param['scheme_id']:0,
            'image'=>$param['image'],
            'estate_id'=>$estate_id,
            'typeid'=>2,
            'video_time'=>gmdate('i:s',$video_time),
        ];
        return $this->releaseDynamic($data,[],$dynamic_id);
    }

    //发布图文
    public function addImagetext()
    {
        $validate = validate(UserDynamic::class);
        $param = input();
        if (!$validate->scene('imagetext')->batch(true)->check($param)) {
            // 验证失败 输出错误信息
            $error = $validate->getError();
            $error_msg = array_values($error);
            return dyajaxReturn(0, $error_msg[0]);
        }
        if(count(explode(',',$param['images']))>12){
            return dyajaxReturn(0, "最多12张图片");
        }
        $estate_id = 0;
        if($param['scheme_id']){
            $estate_id = Db::name('user_dynamic')
                ->where('dynamic_id',$param['scheme_id'])->value('estate_id');
        }
        $dynamic_id = input('dynamic_id/d');
        $data = [
            'title'=>$param['title'],
            'content'=>$param['content'],
            'images'=>$param['images'],
            'scheme_id'=>$param['scheme_id']?$param['scheme_id']:0,
            'image'=>explode(',',$param['images'])[0],
            'estate_id'=>$estate_id,
            'typeid'=>1,
        ];
        return $this->releaseDynamic($data,[],$dynamic_id);
    }

    //发布方案
    public function addScheme()
    {
        $validate = validate(UserDynamic::class);
        $param = input();
        if (!$validate->scene('scheme')->batch(true)->check($param)) {
            // 验证失败 输出错误信息
            $error = $validate->getError();
            $error_msg = array_values($error);
            return dyajaxReturn(0, $error_msg[0]);
        }
//        $images = explode(',',$param['images']);
//        if(count($images)<2){
//            return dyajaxReturn(0, "顶部相册图最少2张");
//        }
        if(!$param['style_id']){
            return dyajaxReturn(0, "请选择风格");
        }
        $room = input('room');
//        if(!$room){
//            return dyajaxReturn(0, "请完善房间内信息");
//        }
        if($room){
            $room = json_decode(htmlspecialchars_decode($room),true);
//            if(!$room){
//                return dyajaxReturn(0, "请完善房间内信息");
//            }
            $is_room = false;
            foreach ($room as $k=>$v){
                if(!$v['room_id']){
                    return dyajaxReturn(0, "请完善房间内信息");
                }
//            if(!$v['images']){
//                return dyajaxReturn(0, "请上传【".get_label_name($v['room_id'])."】图片");
//            }
//            if(!$v['content']){
//                return dyajaxReturn(0, "请输入【".get_label_name($v['room_id'])."】内容");
//            }
//                if($v['content'] || $v['images']){
//                    if(!$v['images']){
//                        return dyajaxReturn(0, "请上传【".get_label_name($v['room_id'])."】图片");
//                    }
//                    if(!$v['content']){
//                        return dyajaxReturn(0, "请输入【".get_label_name($v['room_id'])."】内容");
//                    }
//                    $is_room = true;
//                }
//                if(!$v['content'] && !$v['images']){
//                    unset($room[$k]);
//                }
            }
//        if(!$is_room){
//            return dyajaxReturn(0, "请完善房间内信息");
//        }
        }

        $dynamic_id = input('dynamic_id/d');
        //作品
        $works_id = $param['works_id'];
        $estate = Db::name('user_works')
            ->alias('w')
            ->where('w.works_id',$works_id)
            ->join('scene_house h','h.house_id = w.house_id')
            ->join('estate e','e.estate_id = h.estate_id')
            ->field('e.province_id,e.city_id,e.district_id,h.house_type_id,h.area,e.estate_id,w.*')
            ->find();
        if($works_id){
            $works_data = [
                'title'=>$estate['title'],
                'images'=>$estate['images'],
                'add_time'=>$estate['add_time'],
                'style_id'=>$estate['style_id'],
                'over_images'=>$estate['over_images'],
                'over_content'=>$estate['over_content'],
                'image'=>$estate['image'],
                'price'=>$estate['price'],
                'house_id'=>$estate['house_id'],
                'is_edit'=>$estate['is_edit'],
                'estate_id'=>$estate['estate_id'],
                'designjson'=>$estate['designjson'],
            ];
            $works_id = Db::name('user_works')->insertGetId($works_data);
        }
        $data = [
            'title'=>$param['title'],
            'content'=>'',
            'images'=>$param['images'],
            'style_id'=>$param['style_id'],
            'house_type_id'=>$estate['house_type_id'],
            'area'=>$estate['area'],
            'image'=>explode(',',$param['images'])[0],
            'typeid'=>3,
            'over_images'=>isset($param['over_images'])?$param['over_images']:'',
            'over_content'=>isset($param['over_content'])?$param['over_content']:'',
            'province_id'=>$estate['province_id']?$estate['province_id']:0,
            'city_id'=>$estate['city_id']?$estate['city_id']:0,
            'district_id'=>$estate['district_id']?$estate['district_id']:0,
            'estate_id'=>$estate['estate_id']?$estate['estate_id']:0,
            'works_id'=>$works_id?$works_id:0,
        ];
        return $this->releaseDynamic($data,$room,$dynamic_id);
    }

    //发布
    protected function releaseDynamic($data,$room=[],$dynamic_id=0)
    {
        if($dynamic_id>0){
            $data['update_time'] = time();
            $res = Db::name('user_dynamic')->where('dynamic_id','=',$dynamic_id)->save($data);
            if(!$res){
                return dyajaxReturn(0,'修改失败');
            }
            if($data['typeid']==3){
                //保存房间信息
                if($room){
                    foreach ($room as $v){
                        if($v['content'] || $v['images']){
                            $room_data = [
                                'content'=>$v['content'],
                                'images'=>$v['images'],
                            ];
                            $count = Db::name('user_dynamic_room')->where(['scheme_id'=>$dynamic_id,'room_id'=>$v['room_id']])->count();
                            if($count){
                                Db::name('user_dynamic_room')->where(['scheme_id'=>$dynamic_id,'room_id'=>$v['room_id']])->save($room_data);
                            }else{
                                $room_insert = [
                                    'content'=>$v['content'],
                                    'images'=>$v['images'],
                                    'scheme_id'=>$dynamic_id,
                                    'room_id'=>$v['room_id'],
                                    'add_time'=>time(),
                                ];
                                Db::name('user_dynamic_room')->insert($room_insert);
                            }
                        }else{
                            Db::name('user_dynamic_room')->where(['scheme_id'=>$dynamic_id,'room_id'=>$v['room_id']])->delete();
                        }
                    }
                }
            }
            return dyajaxReturn(1,'修改成功');
        }else{
            $data['add_time'] = time();
            $data['user_id'] = $this->user_id;
            $res = Db::name('user_dynamic')->insertGetId($data);
            if(!$res){
                return dyajaxReturn(0,'发布失败');
            }
            if($data['typeid']==3){
                //保存房间信息
                $room_insert = [];
                foreach ($room as $v){
                    if($v['images'] && $v['content']){
                        $room_insert[] = [
                            'content'=>$v['content'],
                            'images'=>$v['images'],
                            'scheme_id'=>$res,
                            'room_id'=>$v['room_id'],
                            'add_time'=>time(),
                        ];
                    }
                }
                if($room_insert){
                    Db::name('user_dynamic_room')->insertAll($room_insert);
                }
            }
            return dyajaxReturn(1,'发布成功');
        }
    }

    //获取评价
    public function getEvaluateList()
    {
        $dynamic_id = input('dynamic_id/d');
        $count = 0;
        $DynamicLogic = new DynamicLogic();
        $list = $DynamicLogic->getEvaluateList($dynamic_id,$this->user_id,0,$count);
        return dyajaxReturn(1,$count,$list);
    }

    public function getEditDynamic()
    {
        $dynamic_id = input('dynamic_id/d');
        if(!$dynamic_id){
            return dyajaxReturn(0, '缺少信息ID');
        }
        $where['dynamic_id'] = $dynamic_id;
//        $where['is_show'] = 1;
        $user_id = $this->user_id;
        $where['user_id'] = $user_id;
        $info = Db::name('user_dynamic')->where($where)->find();
        if(!$info){
            return dyajaxReturn(0, '没有找到信息');
        }
        $data = [
            'dynamic_id'=>$info['dynamic_id'],
            'typeid'=>$info['typeid'],
        ];
        switch ($info['typeid']){
            case 1:
                //图文
                $data['title'] = $info['title'];
                $data['content'] = $info['content'];
                $data['images'] = $info['images']?explode(',',$info['images']):[];
                $data['scheme_id'] = $info['scheme_id'];
                break;
            case 2:
                //视频
                $data['content'] = $info['content'];
                $data['video'] = $info['video'];
                $data['image'] = $info['image'];
                $data['scheme_id'] = $info['scheme_id'];
                $file = ".".$data['video'];
                $ffmpeg = \FFMpeg\FFMpeg::create([
                    'ffmpeg.binaries'  => app()->getRootPath().'/public/public/plugins/ffmpeg-git-20220622-amd64-static/ffmpeg',	# 你自己安装的位置
                    'ffprobe.binaries' => app()->getRootPath().'/public/public/plugins/ffmpeg-git-20220622-amd64-static/ffprobe'	# 你自己安装的位置
                ]);
                $video = $ffmpeg->open($file);
                $frame = $video->frame(\FFMpeg\Coordinate\TimeCode::fromSeconds(1));    # 提取第1秒的图像
                $file_path = UPLOAD_PATH."coverimage/";
                if (!(file_exists($file_path))) {
                    @mkdir($file_path, 0777, true);
                }
                $url_image = $file_path.md5($info['dynamic_id']).'.png';	# 生成图片路径
                $frame->save($url_image);	# 保存图片
                $data['coverImageStr'] = '/'.$url_image;
                break;
            case 3:
                //方案
                $data['works_id'] = $info['works_id'];
                $data['title'] = $info['title'];
                $data['video'] = $info['video'];
                $data['images'] = $info['images']?explode(',',$info['images']):[];
                $data['style_id'] = $info['style_id'];
                $data['style_name'] = get_label_name($info['style_id']);
                $address = '';
                if($info['district_id']){
                    $address = get_region_mername($info['district_id'],'');
                }
                if(!$address && $info['city_id']){
                    $address = get_region_mername($info['city_id'],'');
                }
                if(!$address && $info['province_id']){
                    $address = get_region_mername($info['province_id'],'');
                }
                $data['address'] = $address;
                $label = [];
                $label[] = get_label_name($info['house_type_id']);
                $label[] = $info['area']."㎡";
                $data['label'] = $label;
                $data['over_images'] = $info['images']?explode(',',$info['over_images']):[];
                $data['over_content'] = $info['over_content'];
                $dynamic_room = Db::name('user_dynamic_room')->where(['scheme_id'=>$info['dynamic_id']])
                    ->field('room_id,content,images')
                    ->order('id asc')
                    ->select()->toArray();
                $room = [];
                foreach ($dynamic_room as $value){
                    $room[] = [
                        'images'=>$value['images']?explode(',',$value['images']):[],
                        'room_id'=>$value['room_id'],
                        'content'=>$value['content'],
                        'name'=>get_label_name($value['room_id']),
                    ];
                }
                $data['room'] = $room;
                break;
            default:
                return dyajaxReturn(0, '类型错误');
                break;
        }
        if(isset($data['scheme_id']) && $data['scheme_id']){
            $scheme = Db::name('user_dynamic')->where(['is_show'=>1,'dynamic_id'=>$info['scheme_id']])
                ->field('dynamic_id,title,style_id,house_type_id,area,images')->find();
            if($scheme){
                //两图展示
                $data['scheme']['thumbs'] = [];
                $images = explode(',',$scheme['images']);
                if($images[0]){
                    $data['scheme']['thumbs'][] = SITE_URL.get_sub_images(md5($images[0]),$images[0], $scheme['dynamic_id'], 320, 240, 'user_dynamic');
                }
                if($images[1]){
                    $data['scheme']['thumbs'][] = SITE_URL.get_sub_images(md5($images[1]),$images[1], $scheme['dynamic_id'], 320, 240, 'user_dynamic');
                }
                $label = [];
                $label[] = get_label_name($scheme['style_id']);
                $label[] = get_label_name($scheme['house_type_id']);
                $label[] = $scheme['area']."㎡";
                $data['scheme']['label'] = $label;
                $data['scheme']['title'] = $scheme['title'];
            }
        }
        return dyajaxReturn(1, '详情',$data);
    }
}