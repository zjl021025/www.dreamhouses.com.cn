<?php

namespace app\appapi\controller;

use think\facade\Db;
use think\facade\Request;

class FitmentHelper
{
    /**
     * 展示首页列表
     * @return void
     */
    public function showList()
    {
        $listData = Db::name('user_house')->where(['is_show' => 1, 'is_deleted' => 0])->select()->toArray();
        if (empty($listData)) {
            return dyjsonReturn(0, '目前未有首页推荐作品');
        }
        return dyjsonReturn(1, '查询成功', $listData);
    }

    /**
     * 工程进度
     * @return void
     */
    public function progressWord()
    {
        $num = 0;
        $user_id = dydecrypt(input('user_id'));
        $house_id = input('house_id');
//        $fitment_id = input('fitment_id');
        $fitmentData = Db::name('fitment')->where(['fid'=>0,'delete_time'=>0,'is_show'=>1])->order('level')->select()->toArray();
        foreach ($fitmentData as $key => $val) {
            $fitmentDesign = Db::name('fitment')->where(['fid'=>$val['fitment_id'],'delete_time'=>0,'is_show'=>1])->select()->toArray();
            $fitmentKey = array_keys(array_column($fitmentDesign, null, 'fitment_id'));
            $userData = Db::name('fitment_active_user')->where(['user_id' => $user_id, 'house_id' => $house_id])->select()->toArray();
            if (!empty($userData)) {
                foreach ($userData as $value) {
                    if (in_array($value['fitment_id'], $fitmentKey)) {
                        $num++;
                    }
                }
            }
            $sum = count($fitmentKey);
            if ($sum != 0) {
                $fitmentData[$key]['total'] = $num == 0 ? 0 : $num / $sum * 100;
            } else {
                $fitmentData[$key]['total'] = 0;
            }
            $num = 0;
        }
        return dyjsonReturn(1, '查询成功', $fitmentData);
    }

    /**
     * 首页装修助手百分比
     * @return void
     */
    public function inFitment()
    {
        //查询出该用户下所有的我的家
        $num = 0;
        $user_id = dydecrypt(input('user_id'));
        $where['fid'] = ['<>', 0];
        $fitmentData = Db::name('fitment')->where($where)->where(['delete_time'=>0,'is_show'=>1])->select()->toArray();
        $fitmentKey = array_keys(array_column($fitmentData, null, 'fitment_id'));
        $houseData = Db::name('user_house')->where(['user_id'=> $user_id,'is_deleted'=>0])->select()->toArray();
        foreach ($houseData as $key => $val) {
            $userData = Db::name('fitment_active_user')->where(['user_id' => $user_id, 'house_id' => $val['house_id'],'delete_time' => 0])->select()->toArray();
            if (!empty($userData)) {
                foreach ($userData as $value) {
                    if (in_array($value['fitment_id'], $fitmentKey)) {
                        $num++;
                    }
                }
            }
            $sum = count($fitmentKey);
            $houseData[$key]['percent'] = $num == 0 ? 0 : $num / $sum * 100;
            $num = 0;
        }
        return dyjsonReturn(1, '查询成功', $houseData);
    }

    /**
     * 注意事项和问题
     * @return void
     */
    public function notesData()
    {
        $fitment_id = input('fitment_id');
        $user_id = dydecrypt(input('user_id'));
        $house_id = input('house_id');
        if(empty($fitment_id) || empty($user_id) || empty($house_id)){
            return dyjsonReturn(0,'数据传输错误');
        }
        //查询该用户下的选择信息
        $itemData = Db::name('fitment')->where(['fid'=>$fitment_id,'delete_time'=>0,'is_show'=>1])->select()->toArray();
        foreach ($itemData as $key => $value) {
            $userFitment = Db::name('fitment_active_user')->where(['user_id' => $user_id, 'fitment_id' => $value['fitment_id'],'house_id'=>$house_id,'delete_time'=>0])->select()->toArray();
            if (!empty($userFitment)) {
                $itemData[$key]['is_select'] = 1;
            } else {
                $itemData[$key]['is_select'] = 0;
            }
            $notesData = Db::name('notes')->where(['fitment_id' => $value['fitment_id'], 'delete_time' => 0,'is_show'=>1])->select()->toArray();
            foreach ($notesData as $k=>$v){
                $notesData[$k]['itemData'] = Db::name('notes_item')->where(['is_show'=>1,'is_deleted'=>0,'notes_id'=>$v['id']])->select()->toArray();
            }
            $itemData[$key]['noteData'] = $notesData;
        }
        //查询注意事项数据
        if (empty($itemData)) {
            return dyjsonReturn(0, '查询失败');
        }
        return dyjsonReturn(1, '查询成功', $itemData);
    }

    /**
     * 是否核对（是）
     * @return void
     */
    public function isCheck(){
        $fitment_id = input('fitment_id');
        $user_id = dydecrypt(input('user_id'));
        $house_id = input('house_id');
        $num = 0;
        if(empty($fitment_id) || empty($user_id) || empty($house_id)){
            return dyjsonReturn(0,'数据传输错误');
        }
        $data = Db::name('fitment_active_user')->where(['fitment_id'=>$fitment_id,'user_id'=>$user_id,'house_id'=>$house_id])->find();
        if($data){
            return dyjsonReturn(0,'该选项已经选过');
        }
        Db::startTrans();
        try {
            //添加核对信息
            $insertData = Db::name('fitment_active_user')->insert(['fitment_id'=>$fitment_id,'user_id'=>$user_id,'house_id'=>$house_id,'create_time'=>time()]);
            //重新计算百分比
            $lastId = Db::name('fitment')->where('fitment_id',$fitment_id)->find();
            $lastCodeNum = Db::name('fitment')->where(['fid'=>$lastId['fid'],'delete_time'=>0,'is_show'=>1])->select()->toArray();
            $fitmentKey = array_keys(array_column($lastCodeNum, null, 'fitment_id'));
            $userData = Db::name('fitment_active_user')->where(['user_id' => $user_id, 'house_id' => $house_id])->select()->toArray();
            if (!empty($userData)) {
                foreach ($userData as $value) {
                    if (in_array($value['fitment_id'], $fitmentKey)) {
                        $num++;
                    }
                }
            }
            $sum = count($fitmentKey);
            $percent['percent'] = $num == 0 ? 0 : $num / $sum * 100;
            Db::commit();
            return dyjsonReturn(1,'选择成功',$percent);
        }catch (\Exception $exception){
            Db::rollback();
            return dyjsonReturn(0,'失败',$exception->getMessage());
        }
    }

    /**
     * 是否核对（否）
     * @return void
     */
    public function isCheckNo(){
        $fitment_id = input('fitment_id');
        $user_id = dydecrypt(input('user_id'));
        $house_id = input('house_id');
        $num = 0;
        if(empty($fitment_id) || empty($user_id) || empty($house_id)){
            return dyjsonReturn(0,'数据传输错误');
        }
        $data = Db::name('fitment_active_user')->where(['fitment_id'=>$fitment_id,'user_id'=>$user_id,'house_id'=>$house_id])->find();
        if(!$data){
            return dyjsonReturn(0,'该选项暂未选择');
        }
        $del = Db::name('fitment_active_user')->where(['fitment_id'=>$fitment_id,'user_id'=>$user_id,'house_id'=>$house_id])->delete();
        //计算百分比
        $lastId = Db::name('fitment')->where('fitment_id',$fitment_id)->find();
        $lastCodeNum = Db::name('fitment')->where(['fid'=>$lastId['fid'],'delete_time'=>0,'is_show'=>1])->select()->toArray();
        $fitmentKey = array_keys(array_column($lastCodeNum, null, 'fitment_id'));
        $userData = Db::name('fitment_active_user')->where(['user_id' => $user_id, 'house_id' => $house_id])->select()->toArray();
        if (!empty($userData)) {
            foreach ($userData as $value) {
                if (in_array($value['fitment_id'], $fitmentKey)) {
                    $num++;
                }
            }
        }
        $sum = count($fitmentKey);
        $percent['percent'] = $num == 0 ? 0 : $num / $sum * 100;
        if(!$del){
            return dyjsonReturn(0,'删除失败');
        }
        return dyjsonReturn(1,'删除成功',$percent);
    }

    /**
     * 重置该节点的数据
     * @return void
     */
    public function fitmentReset(){
        $fitment_id = input('fitment_id');
        $user_id = dydecrypt(input('user_id'));
        $house_id = input('house_id');
        //查询重置的第一节点
        $firstFitment = Db::name('fitment')->where(['fitment_id'=>$fitment_id])->select()->toArray();
        foreach ($firstFitment as $key => $val){
            $fitmentNextData = Db::name('fitment')->where(['fid'=>$val['fitment_id'],'delete_time'=>0,'is_show'=>1])->select()->toArray();
            $fitmentKey = array_keys(array_column($fitmentNextData, null, 'fitment_id'));
        }
        if(empty($fitmentKey)){
            return dyjsonReturn(0,'该模块下没有问题节点数据');
        }
        $userData = Db::name('fitment_active_user')->where(['user_id' => $user_id, 'house_id' => $house_id])->select()->toArray();
        $userFitmentData = array_keys(array_column($userData, null, 'fitment_id'));
        $alike = array_intersect($fitmentKey,$userFitmentData);
        if (count($alike) == 0) {
            return dyjsonReturn(0,'不可重复重置');
        }
        Db::startTrans();
        try {
            foreach ($fitmentKey as $value) {
                Db::name('fitment_active_user')->where(['fitment_id'=>$value,'user_id'=>$user_id,'house_id'=>$house_id])->delete();
            }
            Db::commit();
            return dyjsonReturn(1,'重置成功');
        }catch (\Exception $exception){
            Db::rollback();
            return dyjsonReturn(0,'重置失败');
        }
    }

}
