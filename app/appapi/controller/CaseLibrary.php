<?php

namespace app\appapi\controller;


use think\facade\Db;
use function dydecrypt;
use function dyjsonReturn;
use function input;

class CaseLibrary
{
    /**
     * 房屋列表
     * @return void
     */
    public function houseList(){
        $user_id = dydecrypt(input('user_id'));
        $pageData = [];
        $list = Db::name('user_dynamic')->where('is_deleted',0)->select()->toArray();
        $label = Db::name('label')->select()->toArray();
        $data = [];
        foreach ($list as $key => $val){
            //收藏
            $collect = Db::name('user_collect')->where(['user_id'=>$user_id,'aim_id'=>$val['dynamic_id']])->find();
            if(!$collect){
                $list[$key]['is_collect'] = 0;
            }else{
                $list[$key]['is_collect'] = 1;
            }
            //点赞
            $praise = Db::name('user_praise')->where(['user_id'=>$user_id,'aim_id'=>$val['dynamic_id']])->find();
            $praiseNum = Db::name('user_praise')->where('aim_id',$val['dynamic_id'])->count();
            if(!$praise){
                $list[$key]['is_praise'] = 0;
            }else{
                $list[$key]['is_praise'] = 1;
            }
            $list[$key]['praiseNum'] = $praiseNum;
            //风格属性
            foreach ($label as $keys => $value){
                if($value['typeid'] == 4){
                    if($val['house_type_id'] == $value['id']){
                        $data[] = $value['name'];
                    }
                }
                if($value['typeid'] == 2){
                    if($val['style_id'] == $value['id']){
                        $data[] = $value['name'];
                    }
                }
            }
            $data[] = $val['area'] != 0 ? $val['area'].'m²' : 0;
            $list[$key]['house_type'] = $data;
            $data = [];
        }
        $pageData['caseList'] = $list;
        $pageData['labelList'] = Db::name('label')->where('typeid',4)->select()->toArray();
        $pageData['styleList'] = Db::name('label')->where('typeid',2)->select()->toArray();
        if(empty($pageData)){
            return dyjsonReturn(0,'查询失败');
        }
        return dyjsonReturn(1,'查询成功',$pageData);
    }

    /**
     * 点击分类
     * @return void
     */
    public function selectLabel(){
        $user_id = dydecrypt(input('user_id'));
        $label_id = input('label_id');
        $style_id = input('style_id');
        $keyword = input('keyword', 0);
        if(empty($keyword)){
            if(empty($label_id)){
                $houseList = Db::name('user_dynamic')->where('style_id',$style_id)
                    ->where(['is_show' => 1, 'is_deleted' => 0])
                    ->select()->toArray();
            }elseif (empty($style_id)){
                $houseList = Db::name('user_dynamic')->where('house_type_id',$label_id)
                    ->where(['is_show' => 1, 'is_deleted' => 0])
                    ->select()->toArray();
            }else{
                $houseList = Db::name('user_dynamic')->where(['house_type_id'=>$label_id,'style_id'=>$style_id])
                    ->where(['is_show' => 1, 'is_deleted' => 0])
                    ->select()->toArray();
            }
        }else{
            if(empty($label_id)){
                $houseList = Db::name('user_dynamic')->where('style_id',$style_id)
                    ->where('title', 'like', '%' . $keyword . '%')
                    ->where(['is_show' => 1, 'is_deleted' => 0])
                    ->select()->toArray();
            }elseif (empty($style_id)){
                $houseList = Db::name('user_dynamic')->where('house_type_id',$label_id)
                    ->where('title', 'like', '%' . $keyword . '%')
                    ->where(['is_show' => 1, 'is_deleted' => 0])
                    ->select()->toArray();
            }else{
                $houseList = Db::name('user_dynamic')->where(['house_type_id'=>$label_id,'style_id'=>$style_id])
                    ->where('title', 'like', '%' . $keyword . '%')
                    ->where(['is_show' => 1, 'is_deleted' => 0])
                    ->select()->toArray();
            }
        }
        $label = Db::name('label')->select()->toArray();
        $data = [];
        foreach ($houseList as $key => $val){
            //收藏
            $collect = Db::name('user_collect')->where(['user_id'=>$user_id,'aim_id'=>$val['dynamic_id']])->find();
            if(!$collect){
                $houseList[$key]['is_collect'] = 0;
            }else{
                $houseList[$key]['is_collect'] = 1;
            }
            //点赞
            $praise = Db::name('user_praise')->where(['user_id'=>$user_id,'aim_id'=>$val['dynamic_id']])->find();
            $praiseNum = Db::name('user_praise')->where('aim_id',$val['dynamic_id'])->count();
            if(!$praise){
                $houseList[$key]['is_praise'] = 0;
            }else{
                $houseList[$key]['is_praise'] = 1;
            }
            $houseList[$key]['praiseNum'] = $praiseNum;
            //风格属性
            foreach ($label as $keys => $value){
                if($value['typeid'] == 4){
                    if($val['house_type_id'] == $value['id']){
                        $data[] = $value['name'];
                    }
                }
                if($value['typeid'] == 2){
                    if($val['style_id'] == $value['id']){
                        $data[] = $value['name'];
                    }
                }
            }
            $data[] = $val['area'] != 0 ? $val['area'].'m²' : 0;
            $houseList[$key]['house_type'] = $data;
            $data = [];
        }
        if(empty($houseList)){
            return dyjsonReturn(0,'查询为空,没有该类型的房屋');
        }
        return dyjsonReturn(1,'查询成功',$houseList);
    }

    /**
     * 搜索功能
     * @return void
     *
     */
    public function searchHouse(){
        $user_id = dydecrypt(input('user_id'));
        $keyword = input('keyword', 0);
        if(empty($keyword)){
            return dyjsonReturn(0, '请输入搜索内容');
        }
        $dynamicData = Db::name('user_dynamic')->where('title', 'like', '%' . $keyword . '%')->where(['is_show' => 1, 'is_deleted' => 0])->select()->toArray();
        $label = Db::name('label')->select()->toArray();
        $data = [];
        foreach ($dynamicData as $key => $val){
            //收藏
            $collect = Db::name('user_collect')->where(['user_id'=>$user_id,'aim_id'=>$val['dynamic_id']])->find();
            if(!$collect){
                $dynamicData[$key]['is_collect'] = 0;
            }else{
                $dynamicData[$key]['is_collect'] = 1;
            }
            //点赞
            $praise = Db::name('user_praise')->where(['user_id'=>$user_id,'aim_id'=>$val['dynamic_id']])->find();
            $praiseNum = Db::name('user_praise')->where('aim_id',$val['dynamic_id'])->count();
            if(!$praise){
                $dynamicData[$key]['is_praise'] = 0;
            }else{
                $dynamicData[$key]['is_praise'] = 1;
            }
            $dynamicData[$key]['praiseNum'] = $praiseNum;
            foreach ($label as $keys => $value){
                if($value['typeid'] == 4){
                    if($val['house_type_id'] == $value['id']){
                        $data[] = $value['name'];
                    }
                }
                if($value['typeid'] == 2){
                    if($val['style_id'] == $value['id']){
                        $data[] = $value['name'];
                    }
                }
            }
            $data[] = $val['area'].'m²';
            $dynamicData[$key]['house_type'] = $data;
            $data = [];

        }
        if (!$dynamicData) {
            return dyjsonReturn(0, '非常抱歉，未能搜索到您的小区，您可以查看平台其他户型');
        }
        return dyjsonReturn(1, '搜索成功', $dynamicData);
    }

    /**
     * 首页案例库展示
     * @return void
     */
    public function indexShow(){
        //首页banner图
        $data['bannerImage'] = Db::name('user_dynamic')->field('dynamic_id,image')->where(['is_deleted'=>0,'is_show'=>1])->order('dynamic_id','desc')->limit(4)->select()->toArray();
        //首页案例分类
        $data['caseStyle'] = Db::name('label')->where('typeid',2)->order('sort')->select()->toArray();
        //首页分类案例
        $data['default'] = Db::name('user_dynamic')->alias('dy')->leftJoin('users','dy.user_id = users.user_id')->order('dynamic_id','desc')->where(['dy.is_deleted'=>0,'dy.style_id'=>5,'is_home'=>1])->find();
        if(empty($data)){
            return dyjsonReturn(0,'查询失败');
        }
        return dyjsonReturn(1,'查询成功',$data);
    }

    /**
     * 案例详情
     * @return void
     */
    public function itemCase(){
        $dynamic_id = input('dynamic_id');
        $dynamicData = Db::name('user_dynamic')
            ->alias('dy')
            ->leftJoin('users','dy.user_id = users.user_id')
            ->where('dy.dynamic_id',$dynamic_id)
            ->find();
        //查看点赞数量和收藏数量
        $dynamicData['praiseNum'] = Db::name('user_praise')->where('aim_id',$dynamicData['dynamic_id'])->count();
        $dynamicData['collectNum'] = Db::name('user_collect')->where('aim_id',$dynamicData['dynamic_id'])->count();
        $dynamicData['evaluateNum'] = Db::name('evaluate')->where(['dynamic_id'=>$dynamicData['dynamic_id'],'is_show'=>1])->count();
        $itemDynamicDesign = Db::name('user_dynamic_room')->where('scheme_id', $dynamicData['dynamic_id'])->select()->toArray();
        $label = Db::name('label')->select()->toArray();
        foreach ($itemDynamicDesign as $key=>$val){
            foreach ($label as $k=>$v){
                if($v['typeid'] == 5){
                    if($val['room_id'] == $v['id']){
                        $itemDynamicDesign[$key]['title'] = $v['name'];
                    }
                }
            }
        }
        $dynamicData['rooms'] = $itemDynamicDesign;
        $data[] = Db::name('label')->where('id',$dynamicData['style_id'])->value('name');
        $data[] = Db::name('label')->where('id',$dynamicData['house_type_id'])->value('name');
        $data[] = $dynamicData['area']."m²";
        $dynamicData['label'] = $data;
        if (empty($dynamicData)) {
            return dyjsonReturn(0, '详情获取失败，请重新获取');
        }
        return dyjsonReturn(1, '获取成功', $dynamicData);
    }

    /**
     * 点击首页分类展示
     * @return void
     */
    public function clickIndexStyle(){
        $style_id = input('style_id');
        $dynamicData = Db::name('user_dynamic')->alias('dy')->leftJoin('users','dy.user_id = users.user_id')->where(['dy.is_deleted'=>0,'dy.style_id'=>$style_id])->find();
        if(!$dynamicData){
            return dyjsonReturn(0,'查询失败');
        }
        return dyjsonReturn(1,'查询成功',$dynamicData);
    }

    /**
     * 首页商业合作轮播图
     * @return void
     */
    public function businessCooperation(){
        //展示首页商业合作轮播图
        $business = Db::name('business_cooperation')->where(['is_show'=>1,'is_deleted'=>0])->select()->toArray();
        if(!$business){
            return dyjsonReturn(0,'查询失败');
        }
        return dyjsonReturn(1,'查询成功',$business);
    }
}