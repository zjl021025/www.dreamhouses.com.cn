<?php

namespace app\appapi\controller;

use app\common\logic\DynamicLogic;
use app\common\logic\HouseLogic;
use think\facade\Db;

class Index extends Base
{
    //关注
    public function index()
    {
        $user_id = $this->user_id;
        $DynamicLogic = new DynamicLogic();
        $list = $DynamicLogic->getFollowList($user_id);
        return dyajaxReturn(1, '关注列表',$list);
    }

    //灵感
    public function inspiration()
    {
        $user_id = $this->user_id;
        $typeid = input('typeid',0);
        if($typeid!=0 && !$this->user_id){
            return dyajaxReturn(100,'请先登录');
        }
        if($typeid!=0){
            $HouseLogic = new HouseLogic();
            $where['user_id'] = $this->user_id;
            $count = $HouseLogic->getCount($where);
            if($count<=0){
                return dyajaxReturn(2,'完善房屋信息');
            }
        }
        $DynamicLogic = new DynamicLogic();
        $list = $DynamicLogic->getInspirationList($typeid,$user_id,$this->user);
        return dyajaxReturn(1, '灵感列表',$list);
    }

    public function cs()
    {
        $list = Db::name('user_works_resource')->where('works_id',18)->select()->toArray();
        foreach ($list as &$value){
            unset($value['id']);
            $value['add_time'] = time();
            $value['works_id'] = 37;
            $value['user_id'] = 10;
        }
        Db::name('user_works_resource')->insertAll($list);
    }

    public function cscs()
    {
        $list = Db::name('user_works_resource')->select()->toArray();
        foreach ($list as $value){
            if($value['video']){
                $value['video'] = str_replace('/resource/','/resources/',$value['video']);
            }
            $value['image'] = str_replace('/resource/','/resources/',$value['image']);
            Db::name('user_works_resource')->save($value);
        }

        $list = Db::name('user_works_room')->select()->toArray();
        foreach ($list as $value){
            $value['images'] = str_replace('/resource/','/resources/',$value['images']);
            Db::name('user_works_room')->save($value);
        }

        $list = Db::name('user_works')->select()->toArray();
        foreach ($list as $value){
            if($value['images']){
                $value['images'] = str_replace('/resource/','/resources/',$value['images']);
            }
            if($value['over_images']){
                $value['over_images'] = str_replace('/resource/','/resources/',$value['over_images']);
            }
            if($value['image']){
                $value['image'] = str_replace('/resource/','/resources/',$value['image']);
            }
            Db::name('user_works')->save($value);
        }

        $list = Db::name('user_dynamic')->select()->toArray();
        foreach ($list as $value){
            if($value['images']){
                $value['images'] = str_replace('/resource/','/resources/',$value['images']);
            }
            if($value['video']){
                $value['video'] = str_replace('/resource/','/resources/',$value['video']);
            }
            if($value['over_images']){
                $value['over_images'] = str_replace('/resource/','/resources/',$value['over_images']);
            }
            if($value['image']){
                $value['image'] = str_replace('/resource/','/resources/',$value['image']);
            }
            Db::name('user_dynamic')->save($value);
        }

        $list = Db::name('user_dynamic_room')->select()->toArray();
        foreach ($list as $value){
            if($value['images']){
                $value['images'] = str_replace('/resource/','/resources/',$value['images']);
            }
            Db::name('user_dynamic_room')->save($value);
        }
    }
}
