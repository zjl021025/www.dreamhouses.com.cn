<?php

namespace app\common\model;

use think\facade\Db;
use think\Model;

class Region extends Model
{

    protected $pk = 'id';

    //自定义初始化
    protected static function init()
    {
        //TODO:自定义的初始化
    }
    //查询多个地址
    public function selectRegion($where = [], $field = "")
    {
        $res = $this->where($where)
                    ->field($field)
                    ->cache(true)
                    ->select();
        return $res;
    }
    //查询单个地址
    public function findRegion($where = [], $field = "")
    {
        $res = $this->where($where)
                    ->field($field)
                    ->find();
        return $res;
    }

}
