<?php


namespace app\common\model;

use think\Model;

class Common extends Model
{
    public $page_size = 0;
    public function __construct(array $data = [])
    {
        parent::__construct($data);
        $this->page_size = config('PAGESIZE');
    }
}