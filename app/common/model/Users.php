<?php

namespace app\common\model;

use think\facade\Db;
use think\Page;

/**
 * @package Home\Model
 */
class Users extends Common
{
    protected $pk = 'user_id';

    //is_designer 0=用户 1=设计师
    public function getList($is_designer=0,$field='*')
    {
        $where = $condition = [];
        //是否删除
        $where['is_deleted'] = 0;
        $add_time = input('add_time/s',false,'trim');
        if($add_time){
            $condition['add_time'] = $add_time;
            [$start_time,$end_time] = get_start_endtime_by_range_time($add_time);
            if($start_time && $end_time){
                $where['add_time'] = array('between',[$start_time,$end_time]);
            }
        }
        $keywords = input('keywords/s',false,'trim');
        if($keywords){
            $condition['keywords'] = $keywords;
            $where['mobile|nickname'] = array('like','%'.$keywords.'%');
        }
        //设计师
        if($is_designer==1){
            $where['is_authen'] = 1;
        }
        //设计师类型
        $designer_type = input('designer_type/d');
        if($designer_type){
            $condition['designer_type'] = $designer_type;
            $where['designer_type'] = $designer_type;
        }
        //设计师经验
        $expe_id = input('expe_id/d');
        if($expe_id){
            $condition['expe_id'] = $expe_id;
            $where['expe_id'] = $expe_id;
        }
        //设计师风格
        $style_id = input('style_id/d');
        if($style_id){
            $condition['style_id'] = $style_id;
            $where[] = ['', 'exp', Db::raw("FIND_IN_SET('{$style_id}',style_ids)")];
        }
        //设计师费用
        $price_id = input('price_id/d');
        if($price_id){
            $condition['price_id'] = $price_id;
            $where['price_id'] = $price_id;
        }
        $count = $this
            ->field($field)
            ->where($where)->count();
        $pager       = new Page($count, $this->page_size);// 实例化分页类 传入总记录数和每页显示的记录数
        $show        = $pager->show();// 分页显示输出
        $list        = $this->where($where)
            ->field($field)
            ->order('user_id desc')
            ->limit($pager->firstRow, $pager->listRows)->select()->toArray();
        foreach ($list as &$value){
            $value['add_time'] = get_date($value['add_time']);
            $value['last_login'] = get_date($value['last_login']);
        }
        return [
            'list'=>$list,
            'pager'=>$pager,
            'show'=>$show,
            'condition'=>$condition,
        ];
    }
}