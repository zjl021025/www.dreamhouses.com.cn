<?php


namespace app\common\logic;

use app\common\model\Users;
use think\facade\Db;
use think\Page;

/**
 * 用户房屋逻辑定义
 * Class CatsLogic
 * @package Home\Logic
 */
class HouseLogic
{
    /**
     * @return array
     */
    public function getList($where=[])
    {
        $where['is_deleted']  = 0;
        $field = 'house_id,name,province_id,city_id,district_id,address,house_type_id,area';
        $count = Db::name('house')->where($where)->count();// 查询满足要求的总记录数
        $pager = new Page($count, config('PAGESIZE'));// 实例化分页类 传入总记录数和每页显示的记录数
        $list = Db::name('house')->where($where)
            ->field($field)
            ->order('estate_id desc')
            ->limit($pager->firstRow, $pager->listRows)
            ->select()->toArray();
        if ($list) {
            foreach ($list as &$val) {
                $address = '';
                if($val['district_id']){
                    $address = get_region_mername($val['district_id'],' ');
                }
                if(!$address && $val['city_id']){
                    $address = get_region_mername($val['city_id'],' ');
                }
                if(!$address && $val['province_id']){
                    $address = get_region_mername($val['province_id'],' ');
                }
                $val['address'] = $address.' '.$val['address'];
                $label = [];
                $label[] = get_label_name($val['house_type_id']);
                $label[] = $val['area']."㎡";
                $val['label'] = $label;
                unset($val['province_id'],$val['city_id'],$val['district_id'],$val['house_type_id'],$val['area']);
            }
        }
        return $list;
    }

    /**
     * @return array
     */
    public function getCount($where=[])
    {
        $where['is_deleted']  = 0;
        $count = Db::name('house')->where($where)->count();// 查询满足要求的总记录数
        return $count;
    }

    //房屋详情
    public function getHouse($house_id,$user_id)
    {
        $where['is_deleted']  = 0;
        $where['house_id']  = $house_id;
        $where['user_id']  = $user_id;
        $field = 'house_id,name,province_id,city_id,district_id,address,house_type_id,area,estate_id,lng,lat';
        $info = Db::name('house')->where($where)
            ->field($field)
            ->find();
        if ($info) {
            $d_address = '';
            if($info['district_id']){
                $d_address = get_region_mername($info['district_id'],'');
            }
            if(!$d_address && $info['city_id']){
                $d_address = get_region_mername($info['city_id'],'');
            }
            if(!$d_address && $info['province_id']){
                $d_address = get_region_mername($info['province_id'],'');
            }
            $info['city'] = Db::name('region')->where(['level'=>2,'id'=>$info['city_id']])->value('name');
            $info['district'] = Db::name('region')->where(['level'=>3,'id'=>$info['district_id']])->value('name');
            $info['d_address'] = $d_address;
            $info['house_type_name'] = get_label_name($info['house_type_id']);
        }
        return $info;
    }
}
