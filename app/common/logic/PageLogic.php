<?php


namespace app\common\logic;
use think\facade\Db;

use think\Model;

/**
 * Class
 * @package Home\Model
 */
class PageLogic 
{

    /**
     * 分页
     * @param $result
     * @param $page
     * @return array
     */
    public static function getPage($result, $page)
    {
        $return = $result;   //保存原来的数据
        $result['page'] = $page;     //分页数据处理
        if ($result['page'] == 1 && $result[0] || $result['page'] > 1) {
            //分页显
            if ($result[0]) {
                return array('status' => 1, 'msg' => '获取成功', 'result' => $return);
            } else {
                return array('status' => 1, 'msg' => '已加载完毕', 'result' => array());
            }
        } else if (count($return) == 0) {
            return array('status' => 1, 'msg' => '还没有相关信息', 'result' => array());
        } else {
            return array('status' => -1, 'msg' => '获取失败');
        }
    }

}