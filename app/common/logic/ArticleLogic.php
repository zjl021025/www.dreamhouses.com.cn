<?php


namespace app\common\logic;

use think\facade\Db;
use think\Page;

/**
 * 分类逻辑定义
 * Class CatsLogic
 * @package Home\Logic
 */
class ArticleLogic
{
    /**
     * @param int $type 0=楼盘资讯 1=协议 2=装修贴士
     * @param array $where 条件
     * @return array
     */
    public function getList($type,$where=[])
    {
        $where['article_type'] = $type;
        $where['is_show'] = 1;
        $field = 'article_id,title,intro,add_time';
        $count = Db::name('article')->where($where)->count();// 查询满足要求的总记录数
        $pager = new Page($count, config('PAGESIZE'));// 实例化分页类 传入总记录数和每页显示的记录数
        $list = Db::name('article')->where($where)
            ->field($field)
            ->order('article_id desc')
            ->limit($pager->firstRow, $pager->listRows)
            ->select()->toArray();
        return $list;
    }
}
