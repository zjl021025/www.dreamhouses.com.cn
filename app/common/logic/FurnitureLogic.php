<?php


namespace app\common\logic;

use think\facade\Db;
use think\Page;

/**
 * 家居逻辑定义
 * Class CatsLogic
 * @package Home\Logic
 */
class FurnitureLogic
{
    /**
     * 家居列表
     * $type 0=全部  1=首页榜单 2=我的浏览历史  3=我的收藏
     * @return array
     */
    public function getFurnitureList($type=0,$where=[],$user_id=0)
    {
        $field = 'a.id,a.name,a.cate_id,a.filename,a.size,a.material,a.price,a.two_cate_id,a.content';
        $order = 'a.sort asc,a.id desc';
        //0=全部 1=首页榜单
        switch ($type){
            case 1:
                $order = 'a.virtually_browse+a.browse desc,'.$order;
                $where[] = Db::Raw('virtually_browse+browse > 0');
                break;
        }
        $where['a.is_show'] = 1;
        $where['a.is_deleted'] = 0;
        $query = Db::name('furniture')->alias('a');
        switch ($type){
            case 2:
                $query = $query->join('furniture_browse b','a.id=b.aim_id');
                break;
            case 3:
                $where['a.id'] = ['>',0];
                $query = $query->join('furniture_collect b','a.id=b.aim_id');
                break;
        }
        $count = $query->where($where)->count();// 查询满足要求的总记录数
        $pager = new Page($count, config('PAGESIZE'));// 实例化分页类 传入总记录数和每页显示的记录数
        $list = $query
            ->alias('a')
            ->where($where)
            ->field($field)
            ->orderRaw($order)
            ->limit($pager->firstRow, $pager->listRows)
            ->select()->toArray();
        foreach ($list as &$value){
            $value['praise'] = Db::name('furniture_praise')->where(['aim_id'=>$value['id']])->count();
            $value['thumb'] = SITE_URL.dythumbimages($value['id'],400,400,400,'furniture');
            $size = json_decode($value['size'],true);
            $material = json_decode($value['material'],true);
            unset($value['size'],$value['material']);
            $value['sku_name'] = $size[0]['size_name'];//规格
            $value['length'] = $size[0]['length'];
            $value['width'] = $size[0]['width'];
            $value['height'] = $size[0]['height'];
            $value['material_filename'] = $material[0]['filename'];
            $value['is_collect'] = 0;//是否收藏
            $category = Db::name('furniture_category')->where('id',$value['cate_id'])->field('name,folder')->find();
            $value['cate_name'] = $category['name'];
            $value['folder'] = $category['folder'];
            $twocategory = Db::name('furniture_category')->where('id',$value['two_cate_id'])->field('name,folder')->find();
            $value['two_cate_name'] = $twocategory?$twocategory['name']:'';
            $value['two_folder'] = $twocategory?$twocategory['folder']:'';
            if($user_id){
                $is_collect = Db::name('furniture_collect')->where(['aim_id'=>$value['id'],'user_id'=>$user_id])->value('id');
                $is_collect>0 && $value['is_collect'] = 1;
            }
        }
        return $list;
    }

    //家居详情
    public function getFurniture($id,$user_id,$type=1)
    {
        $field = "id,name,cate_id,two_cate_id,size,price,buy_url,furniture_no,material,filename,typeid,images";
        if($type==1){
            $where['id'] = $id;
        }else{
            $where['filename'] = $id;
        }

        $where['is_show'] = 1;
        $info = Db::name('furniture')->where($where)->field($field)->find();
        if($info){
//            if($user_id){
//                $this->browse($user_id,$id);
//            }
            if($type==1){
                $this->browse($user_id,$id);
            }
            $info['thumb'] = SITE_URL.dythumbimages($info['id'],800,800,800,'furniture');
            $images = explode(',',$info['images']);
            $thumbs = [];
            [$max_height,$max_width,$min_width] = get_img_max_height($images);
            foreach ($images as $v){
//                $thumbs[] = SITE_URL.get_sub_images(md5($v),$v,$id,800,800,'furniture');
                $thumbs[] = SITE_URL.$v;
            }
            $info['max_height'] = $max_height;
            $info['max_width'] = $max_width;
            $info['min_width'] = $min_width;
            $info['thumbs'] = $thumbs;
            $info['size'] = json_decode($info['size'],true);//尺寸
            $info['material'] = json_decode($info['material'],true);
            foreach ($info['material'] as $k=>$v){
                $info['material'][$k]['image'] = SITE_URL.get_sub_images(md5($v['image']),$v['image'], $info['id'], 80, 40, 'furniture');
            }
            $info['is_collect'] = 0;//是否收藏
            if($user_id){
                $is_collect = Db::name('furniture_collect')->where(['aim_id'=>$info['id'],'user_id'=>$user_id])->value('id');
                $is_collect>0 && $info['is_collect'] = 1;
            }
            $info['share_title'] = $info['name'];
            $info['share_thumb'] = $info['thumb'];
            $info['share_url'] = SITE_URL.url('Currency/furniture',['id'=>$info['id']]);
            unset($info['images']);
        }
        return $info;
    }

    //浏览记录
    protected function browse($user_id,$aim_id)
    {
        $id = Db::name('furniture_browse')->where(['user_id'=>$user_id,'aim_id'=>$aim_id])->value('id');
        if($id){
            Db::name('furniture_browse')->where('id',$id)->update(['add_time'=>time()]);
        }else{
            $data = [
                'user_id'=>$user_id,
                'aim_id'=>$aim_id,
                'add_time'=>time(),
            ];
            Db::name('furniture_browse')->insertGetId($data);
            Db::name('furniture')->where('id', $aim_id)->inc('browse', 1)->update();
        }
    }
}
