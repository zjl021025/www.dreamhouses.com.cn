<?php


namespace app\common\logic;

use think\facade\Db;

/**
 * Description of SmsLogic
 *
 * 短信类
 */
class SmsLogic
{
    private $config;
    /**
     * 发送短信逻辑
     * @param int $scene
     */
    public function sendSms($scene, $sender, $params, $unique_id = 0, $types = 0)
    {
        $scenes = config('SEND_SCENE');
        $smsTemp['sms_sign'] = $scenes[$scene][2];
        $smsTemp['sms_tpl_code'] = 'SMS';
        $code = !empty($params['smscode']) ? $params['smscode'] : false;//验证码
        if (empty($unique_id)) {
            $session_id = session_id();
        } else {
            $session_id = $unique_id;
        }
        $smsParams = [ // 短信模板中字段的值
            1 => ['code'=>$code],                                                                                                          //1. 用户注册 (验证码类型短信只能有一个变量)
        ];
        $smsParam = $smsParams[1];
        //提取发送短信内容
        $msg = $scenes[$scene][1];
        foreach ($smsParam as $k => $v) {
            $msg = str_replace('${' . $k . '}', $v, $msg);
        }
        //发送记录存储数据库
        $insert_log = [
            'mobile' => $sender, 'code' => $code, 'add_time' => time(),
            'session_id' => $session_id, 'status' => 0, 'scene' => $scene, 'msg' => $msg
        ];
        $log_id = Db::name('sms_log')->insertGetId($insert_log);
        if ($sender != '' && validateForm('mobile',$sender)==='') {//如果是正常的手机号码才发送
            try {
                $resp = $this->realSendSms($sender, $smsTemp['sms_sign'], $smsParam, $msg, $smsTemp['sms_tpl_code'], $types);
//                $resp = ['errcode' => 1, 'message' => $code];
            } catch (\Exception $e) {
                $resp = ['errcode' => -1, 'message' => $e->getMessage()];
            }
            if ($resp['errcode'] == 1) {
                Db::name('sms_log')->where(array('id'=>$log_id))->save(array('status' => 1)); //修改发送状态为成功
            } else {
                //发送失败, 将发送失败信息保存数据库
                Db::name('sms_log')->where(array('id'=>$log_id))->update(array('error_msg'=>$resp['msg']));
            }
            return $resp;
        } else {
            return $result = ['errcode' => -1, 'message' => '接收手机号不正确['.$sender.']'];
        }
    }
    private function realSendSms($mobile, $smsSign, $smsParam, $msg, $templateCode, $types)
    {
        $type = 1;
        switch ($type) {
            case 1:
                $result = $this->sendSmsByWodong($mobile, $smsSign, $smsParam, $msg, $templateCode, $types);
                break;
            default:
                $result = ['errcode' => -1, 'message' => '不支持的短信平台'];
        }
        return $result;
    }
    /**
     * 发送短信（沃动短信）
     * @param string $mobile  手机号码
     * @param string $code    验证码
     * @return bool    短信发送成功返回true失败返回false
     * types  类型： 1-营销短信。0-通知短信
     */
    private function sendSmsByWodong($mobile, $smsSign, $smsParam, $msg, $templateCode, $types)
    {
        
//        $post_data = array();
//        $account = '923336';
//        $password = 'HCrFfh';
//        $post_data['action'] = 'send';
//        $post_data['account'] = $account;
//        $post_data['password'] = $password;
//        $post_data['content'] = $msg.'【梦想家】';
//        $post_data['mobile'] = $mobile;
//        $post_data['extno'] = '10690100';
//        $post_data['rt'] = 'json';
//
//        $url='http://wdapi.movek.net/sms?';
//        $o='';
//        foreach ($post_data as $k => $v) {
//            $o.="$k=".urlencode($v).'&';
//        }
//        $post_data = substr($o, 0, -1);
//        $ch = curl_init();
//        curl_setopt($ch, CURLOPT_POST, 1);
//        curl_setopt($ch, CURLOPT_HEADER, 0);
//        curl_setopt($ch, CURLOPT_URL, $url);
//        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//        $result = curl_exec($ch);
//        $result = json_decode($result,true);
//        if($result['status'] == 0){
//            return array('errcode' => 1, 'message' => '已发送成功, 请注意查收');
//        }else{
//            return array('errcode' => 0, 'message' => '验证码发送失败');
//        }
        
        
        
        
         $post_data = array();
        $post_data['action'] = 'send';
        $post_data['userid'] = "855";
        $post_data['account'] = "mxjyzm824";
        $post_data['password'] = "mxjyzm824";
        $post_data['mobile'] = $mobile;
        $post_data['content'] = $msg.'【梦想家】';
        $post_data['sendtime'] = '';
        $post_data['extno'] = '';
        $url='http://120.78.188.185:8088/sms.aspx';
        $o='';
        foreach ($post_data as $k => $v) {
            $o.="$k=".urlencode($v).'&';
        }
        $post_data = substr($o, 0, -1);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        $result = xmlToArray($result);
        if ($result['returnstatus']=='Success' && $result['message']=='ok') {
            return array('errcode' => 1, 'message' => '已发送成功, 请注意查收');
        } else {
            return array('errcode' => 0, 'message' => '验证码发送失败');
        }
    }
}
