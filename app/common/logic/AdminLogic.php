<?php


namespace app\common\logic;

use think\facade\Db;
use think\facade\Session;

class AdminLogic
{
    public function login($username, $password)
    {
        if (empty($username) || empty($password)) {
            return ['errcode' => 0, 'message' => '请填写账号密码'];
        }

        $condition['a.user_name'] = $username;
        $condition['a.password'] = encrypt($password);
        $admin = Db::name('admin')
                 ->alias('a')
                 ->join('admin_role ar', 'a.role_id=ar.role_id')
                 ->where($condition)
                 ->find();
        if (!$admin) {
            return ['errcode' => 0, 'message' => '账号密码不正确'];
        }

        $this->handleLogin($admin, $admin['act_list']);

//        $url = session('from_url') ? session('from_url') : url('Index/index');
        $url = url('Index/index');
        return ['errcode' => 1,'message' => '登录成功', 'dyurl' => $url];
    }

    public function handleLogin($admin, $actList)
    {
        Db::name('admin')->where('admin_id', $admin['admin_id'])->save([
            'last_login' => time(),
            'last_ip' => request()->ip()
        ]);

        session('act_list', $actList);
        session('admin_id', $admin['admin_id']);
        session('last_login_time', $admin['last_login']);
        session('last_login_ip', $admin['last_ip']);
        session('admin_city_id', $admin['city_id']);//管理员所在城市

        adminLog('后台登录');
    }

    public function logout()
    {
        session_unset();
        session(null);
        Session::clear();
    }
}
