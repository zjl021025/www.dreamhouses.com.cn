<?php


namespace app\common\logic;

use think\facade\Db;
use think\Page;

/**
 * 工作台户型逻辑定义
 * Class CatsLogic
 * @package Home\Logic
 */
class ScenehouseLogic
{
    /**
     * 户型列表
     * $type 0=全部  1=我的户型
     * @return array
     */
    public function getScenehouseList($where=[],$type = 0,$user_id=0)
    {
        $where['a.is_deleted'] = 0;
        $field = 'a.house_id,a.name,a.house_type_id,a.area';
        $order = 'a.sort asc,a.house_id desc';
        $where['a.is_show'] = 1;
        $query = Db::name('scene_house')->alias('a');
        if($type==1){
            $houses = Db::name('house')->where(['is_deleted'=>0,'user_id'=>$user_id])->field('estate_id,house_type_id')->select()->toArray();
            if($houses){
                $sql = "";
                foreach ($houses as $v){
                    $sql .= " ( a.estate_id = ".$v['estate_id']." and a.house_type_id = ".$v['house_type_id']." ) or";
                }
                $sql = rtrim($sql,'or');
                $where[] = ['',"exp",Db::raw($sql)];
            }else{
                $where['a.house_id'] = 0;
            }
            $list = $query
                ->alias('a')
                ->where($where)
                ->field($field)
                ->order($order)
                ->select()->toArray();
        }else{
            $count = $query->where($where)->count();// 查询满足要求的总记录数
            $pager = new Page($count, config('PAGESIZE'));// 实例化分页类 传入总记录数和每页显示的记录数
            $list = $query
                ->alias('a')
                ->where($where)
                ->field($field)
                ->order($order)
                ->limit($pager->firstRow, $pager->listRows)
                ->select()->toArray();
        }

        foreach ($list as &$value){
            $value['thumb'] = SITE_URL.dythumbimages($value['house_id'],160,120,160,'scene_house');
            $label = [];
            $label[] = get_label_name($value['house_type_id']);
            $label[] = $value['area']."㎡";
            $value['label'] = $label;
            unset($value['house_type_id'],$value['area']);
        }
        return $list;
    }
}
