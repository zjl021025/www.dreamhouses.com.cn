<?php


namespace app\common\logic;

use app\common\model\Users;
use think\facade\Db;
use think\Page;

/**
 * 分类逻辑定义
 * Class CatsLogic
 * @package Home\Logic
 */
class UsersLogic
{
    protected $user_id = 0;
    /**
     * 设置用户ID
     * @param $user_id
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * 注册
     * @param string $mobile - 手机号
     * @param string $pwd - 手机号
     * @param string $nickname - 用户昵称
     * @param string $head_pic - 用户头像
     * @param string $openid - 第三方openid
     * @param int $typeid - 1=验证码注册  2=QQ  3=微信
     * @return array
     */
    public function reg($mobile, $pwd,$code,$nickname='',$avatar='', $openid='',$typeid=1)
    {
        if($typeid==1){
            if(!$code){
                return ['errcode'=>0,'message'=>'请输入验证码'];
            }
            //验证验证码
            $check = check_validate_code($code, $mobile,1);
            if($check['errcode']!=1){
                return $check;
            }
        }else{
            if(!$code){
                return ['errcode'=>0,'message'=>'请输入验证码'];
            }
            //验证验证码
            $check = check_validate_code($code, $mobile,3);
            if($check['errcode']!=1){
                return $check;
            }
            if(!$openid){
                return ['errcode'=>0,'message'=>'授权信息获取失败'];
            }
        }
        $oautharr = $insert = array();
        $time = time();
        $insert['token'] = dysettoken();
        $insert['nickname'] = !empty($avatar) ? filterEmoji($nickname):hide_mobile($mobile);
        $insert['mobile'] = $mobile;
        $insert['password'] = encrypt($pwd);
        $insert['avatar'] = !empty($avatar) ? $avatar : "http://qiniu.dreamhouses.com.cn/image/20231129/dbf14bbffe98983084e4ad90ca8bac2b.png";
        if(strpos($insert['avatar'],'https://') !== false){
            //保存到本地
            $insert['avatar'] = downloadImage($insert['avatar'], 'user');
        }
        $insert['add_time'] = $time;
        $insert['last_login'] = $time;
        $insert['last_ip'] = request()->ip();//获取登录IP
        // 启动事务
        Db::startTrans();
        try {
            $user = Db::name('users')->where('mobile', $mobile)->where('is_deleted',0)->find();
            if($user){
                $res = Db::name('users')->where('user_id', $user['user_id'])->update(['token'=>$insert['token'],'password'=>encrypt($pwd)]);
                if(!$res){
                    return ['errcode'=>0,'message'=>'注册失败'];
                }
                $res = $user['user_id'];
            }else{
                $res = Db::name('users')->insertGetId($insert);
                if (!$res) {
                    return ['errcode'=>0,'message'=>'注册失败'];
                }
            }
            if($openid){
                $oauth = $typeid==2?'qq':'weixin';
                $oauth_user = Db::name('oauth_users')->where('oauth',$oauth)->where('openid', $openid)->find();
                if(!$oauth_user){
                    $oautharr['user_id'] = $res;
                    $oautharr['openid'] = $openid;
                    $oautharr['oauth'] = $typeid==2?'qq':'weixin';
                    $oautharr['oauth_child'] = 'open';
                    $oautharr['nick_name'] = $nickname;
                    $r = Db::name('oauth_users')->insertGetId($oautharr);
                    if(!$r){
                        return ['errcode'=>0,'message'=>'第三方绑定失败'];
                    }
                }else{
                    $r2 = Db::name('oauth_users')->where('oauth',$oauth)->where('openid', $openid)->save(['user_id'=>$res]);
                    if(!$r2){
                        return ['errcode'=>0,'message'=>'第三方绑定失败'];
                    }
                }
            }
            // 提交事务
            Db::commit();
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            $result['errcode'] = 0;
            $result['message'] = $e->getMessage();
            return $result;
        }
        $user_id = Db::name('users')->field('user_id')->where('mobile', $mobile)->where('is_deleted',0)->find();
        $this->userloginlog($res);
        $result['errcode'] = 1;
        $result['message'] = $openid?"绑定成功":'注册成功';
        $result['token'] = $insert['token'];
        $result['user_id'] = dyencrypt($user_id);
        return $result;
    }

    /*
    * 登录
    */
    public function login($mobile,$pwd)
    {
        $where['mobile'] = $mobile;
        $where['is_deleted'] = 0;
        $user = Db::name('users')->where($where)->field('user_id,status,password')->find();
        if (!$user) {
            return array('errcode'=>0, 'message'=>'账号不存在');
        }
        if($user['password'] != encrypt($pwd)){
            return array('errcode'=>0, 'message'=>'密码错误');
        }
        if ($user['status'] == 2) {
            return array('errcode'=>0, 'message'=>'账号已被封禁，请联系客服');
        }
        $save_data['token'] = dysettoken();//设置登录token
        $save_data['last_login'] = time();
        $save_data['last_ip'] = request()->ip();
        $save_data['user_id'] = $user['user_id'];
        $res = Db::name('users')->update($save_data);
        if ($res) {
            $this->userloginlog($user['user_id']);
            $result = array('errcode' => 1, 'message' => '登录成功', 'token' => $save_data['token'],'user_id' => dyencrypt($save_data['user_id']));
        } else {
            $result = array('errcode' => 0, 'message' => '登录失败');
        }
        return $result;
    }

    /**
     * 短信验证码登录
     * @param string $mobile 手机号
     * @param string $code 验证码
     * @return array
     */
    public function smslogin($mobile, $smscode)
    {
        //先判断账号能不能登陆
        //验证验证码
        $check = check_validate_code($smscode, $mobile,2);
        if($check['errcode']!=1){
            return $check;
        }
        //先判断是否有账号
        $where['is_deleted'] = 0;
        $where['mobile'] = $mobile;
        $user = Db::name('users')->where($where)->field('user_id,status')->find();
        $token = dysettoken();
        if ($user) {
            //已有账户
            if ($user['status'] == 2) {
                return array('errcode'=>0, 'message'=>'账号被封禁，无法进行操作');
            }
            $update['token'] = $token;
            $update['user_id'] = $user['user_id'];
            $update['last_login'] = time();
            $update['last_ip'] = request()->ip();
            $result = Db::name('users')->update($update);
            $user_id = $user['user_id'];
        } else {
            //没有账户，创建一个账户
//            return array('errcode'=>0,'message'=>'账号不存在');
            $insert['token'] = dysettoken();
            $insert['nickname'] = hide_mobile($mobile);
            $insert['mobile'] = $mobile;
            $insert['avatar'] = !empty($avatar) ? $avatar : "http://qiniu.dreamhouses.com.cn/image/20231129/dbf14bbffe98983084e4ad90ca8bac2b.png";
            $insert['add_time'] = time();
            $insert['last_login'] = time();
            $insert['last_ip'] = request()->ip();//获取登录IP
            $result = Db::name('users')->insertGetId($insert);
            $user_id = $result;
        }
        if ($result) {
            $this->userloginlog($user_id);
            return array('errcode'=>1,'message'=>'登录成功','token'=>$token,'user_id'=>dyencrypt($user_id));
        } else {
            return array('errcode'=>0,'message'=>'登录失败');
        }
    }
    /**
     * 找回密码
     *
     * @param [string] $mobile - 手机号
     * @param [string] $smscode - 验证码
     * @param [string] $pwd - 密码
     * @param [string] $pwd_a - 再次输入的密码
     * @return array
     */
    public function forgetpwd($mobile,$pwd,$cpwd,$smscode)
    {
        if(!$pwd){
            return ['errcode'=>0,'message'=>'请输入密码'];
        }
        if(!$cpwd){
            return ['errcode'=>0,'message'=>'请输入确认密码'];
        }
        if($cpwd!=$pwd){
            return ['errcode'=>0,'message'=>'两次密码不一致'];
        }
        $check = check_validate_code($smscode, $mobile,4);
        if($check['errcode']!=1){
            return $check;
        }
        $where['mobile'] = $mobile;
        $token = dysettoken();
        $res = Users::where($where)->find();
        if ($res->status !== 1) {
            $datas['errcode'] = 0;
            $datas['message'] = "账号被冻结,无法进行操作";
            return $datas;
        } elseif ($res->is_deleted == 1) {
            $datas['errcode'] = 0;
            $datas['message'] = "账号正在进行注销操作中,无法操作";
            return $datas;
        }
        $res->last_update = time();
        $res->password = encrypt($pwd);
        $res->token = $token;
        $res->save();
        if ($res) {
            $datas['token'] = $token;
            $datas['user_id'] = dyencrypt($res['user_id']);
            $datas['errcode'] = 1;
            $datas['message'] = "密码找回成功";
        } else {
            $datas['errcode'] = 0;
            $datas['message'] = "找回密码失败";
        }
        return $datas;
    }

    /* 用户登录记录-每个用户每天只记录一次 */
    public function userloginlog($user_id=0){
        $time = time();
        $start_time = strtotime(date('Y-m-d 00:00:00'));
        $end_time = strtotime(date('Y-m-d 23:59:59'));
        $where['user_id'] = $user_id;
        $where['add_time'] = ['between',[$start_time,$end_time]];
        $count = Db::name('users_log')->where($where)->count();
        if($count == 0){
            Db::name('users_log')->insert(['user_id'=>$user_id,'add_time'=>$time]);
        }
    }

    //主页详情
    public function getInfo($aim_id,$user_id)
    {
        $where['status'] = 1;
        $where['is_deleted'] = 0;
        $where['user_id'] = $aim_id;
        $field = 'user_id,nickname,company_name,is_authen,designer_type,expe_id,mobile,is_water,sys_new_msg,is_platform,is_recd,avatar';
        $personal = Db::name('users')
            ->where($where)
            ->field($field)->find();
//        $personal['photo'] = SITE_URL.dythumbimages($personal['user_id'],120,120,120,'users');
        $personal['is_follow'] = 0;
        if($user_id){
            $is_follow = Db::name('user_follow')->where(['aim_id'=>$personal['user_id'],'user_id'=>$user_id])->value('follow_id');
            $is_follow>0 && $personal['is_follow'] = 1;
        }
        if($personal['designer_type']==1){
            $personal['company_name']='独立设计师';
        }
        if($personal['is_platform']==1){
            $personal['company_name']='平台设计师';
            if($aim_id==$user_id){
                $personal['is_authen'] = 1;
                $personal['designer_type'] = 2;
            }
        }
//        $personal['designer_type']==1 && $personal['company_name']='独立设计师';
        //关注数量
        $personal['follow_sum'] = Db::name('user_follow')->where(['user_id'=>$personal['user_id']])->count();
        //粉丝数量
        $personal['fans_sum'] = Db::name('user_follow')->where(['aim_id'=>$personal['user_id']])->count();
        //获赞数量
        $personal['praise_sum'] = Db::name('user_praise')->where(['user_id'=>$personal['user_id']])->count();
        $personal['is_red'] = 0;
        if($this->getUserMessage($user_id)>0 || $personal['sys_new_msg']==1){
            $personal['is_red'] = 1;
        }
        $personal['is_own'] = 0;
        if($user_id==$aim_id){
            $personal['is_own'] = 1;
        }
        $personal['user_id'] = dyencrypt($personal['user_id']);
        $personal['expe_name'] = "";
        if($personal['expe_id']){
            $personal['expe_name'] = Db::name('label')->where(['id'=>$personal['expe_id']])->value('name');
        }
        unset($personal['expe_id']);
        if($personal['is_own']==1){
            $personal['is_weixin'] = 0;
            $personal['is_qq'] = 0;
            $is_weixin = Db::name('oauth_users')->where('user_id',$user_id)->where('oauth', 'weixin')->value('tu_id');
            if($is_weixin){
                $personal['is_weixin'] = 1;
            }
            $is_qq = Db::name('oauth_users')->where('user_id',$user_id)->where('oauth', 'qq')->value('tu_id');
            if($is_qq){
                $personal['is_qq'] = 1;
            }
        }
        return $personal;
    }

    //接口获取设计师列表  $type 0=设计师列表  1=推荐设计师 2=专家设计师 3=首页榜单
    public function getDesignerList($type=0,$where=[],$user_id=0)
    {
//        $where['is_authen'] = 1;
        $where['is_deleted'] = 0;
        $where['status'] = 1;
        $order = 'user_id desc';
        switch ($type){
            case 0:
            case 1:
                if($type==1){
                    $order = 'virtually_browse+browse desc,'.$order;
                    $where[] = Db::Raw('virtually_browse+browse > 0');
                }
                $field = 'user_id,nickname,company_name,designer_type,style_ids,expe_id,price_id';
                $count = Db::name('users')
                    ->field($field)
                    ->where($where)
                    ->where(function ($Query){
                        $Query->where('is_authen',1)->whereOr('is_platform',1);
                    })
                    ->count();
                $pager       = new Page($count, config('PAGESIZE'));// 实例化分页类 传入总记录数和每页显示的记录数
                $list        = Db::name('users')->where($where)
                    ->where(function ($Query){
                        $Query->where('is_authen',1)->whereOr('is_platform',1);
                    })
                    ->field($field)
                    ->orderRaw($order)
                    ->limit($pager->firstRow, $pager->listRows)->select()->toArray();
                $label_list = Db::name('label')->where(['typeid'=>['in',[1,2,3]]])->order('typeid desc,sort asc,id desc')->field('id,name,typeid')->column('name','id');
                foreach ($list as &$value){
//                    $value['designer_type']==1 && $value['company_name']='独立设计师';
                    get_user($value);
                    $value['photo'] = SITE_URL.dythumbimages($value['user_id'],120,120,120,'users');
                    $value['is_follow'] = 0;//是否关注
                    if($user_id){
                        $is_follow = Db::name('user_follow')->where(['aim_id'=>$value['user_id'],'user_id'=>$user_id])->value('follow_id');
                        $is_follow>0 && $value['is_follow'] = 1;
                    }
                    $value['is_own'] = 0;//是否是自己
                    if($user_id==$value['user_id']){
                        $value['is_own'] = 1;
                    }
                    //获取3个方案
                    $scheme = Db::name('user_dynamic')->where(['is_show'=>1,'user_id'=>$value['user_id'],'typeid'=>3])->limit(3)->column('dynamic_id');
                    if($scheme){
                        $value['thumbs'] = [];
                        foreach ($scheme as $v){
                            $value['thumbs'][] = SITE_URL.dythumbimages($v,320,240,320,'user_dynamic');
                        }
                    }else{
                        $value['thumbs'][] = SITE_URL."/public/images/scheme_empty.jpg";
                    }
                    $value['user_id'] = dyencrypt($value['user_id']);
                    $label = [];
                    $style_ids = explode(',',$value['style_ids']);
                    foreach ($style_ids as $vv){
                        if($vv){
                            $label[] = $label_list[$vv];
                        }
                    }
                    if($value['expe_id']){
                        $label[] = $label_list[$value['expe_id']];
                    }
                    if($value['price_id']){
                        $label[] = $label_list[$value['price_id']];
                    }
                    $value['label'] = $label;
//                    $value['price_name'] = $value['price_id']?$label_list[$value['price_id']]:'';
                    $value['price_name'] = '';
                    unset($value['price_id'],$value['style_ids'],$value['expe_id']);
                }
                break;
        }
        return $list;
    }

    //获取系统消息
    public function getMessageList($user)
    {
        $where['is_show'] = 1;
        $user_id = $user['user_id'];
        Db::name('users')->where('user_id',$user_id)->update(['sys_new_msg'=>0]);
        $designer_type = $user['designer_type'];
        if($designer_type){
            $where[] = ['', 'exp', Db::raw("type = 1 and (user_type = 0 or user_type = ".$designer_type." or (user_type = 3 and user_id = ".$user_id."))")];
        }else{
            $where[] = ['', 'exp', Db::raw("type = 1 and (user_type = 0 or (user_type = 3 and user_id = ".$user_id."))")];
        }
        $field = 'content,send_time';
        $count = Db::name('message_notice')
            ->field($field)
            ->where($where)->count();
        $pager       = new Page($count, config('PAGESIZE'));// 实例化分页类 传入总记录数和每页显示的记录数
        $list        = Db::name('message_notice')->where($where)
            ->field($field)
            ->order('message_id desc')
            ->limit($pager->firstRow, $pager->listRows)->select()->toArray();
        foreach ($list as &$value){
            $value['send_time']=get_date($value['send_time']);
        }
        return $list;
    }

    //获取系统消息
    public function getMessageLast($user)
    {
        $where['is_show'] = 1;
        $user_id = $user['user_id'];
        $designer_type = $user['designer_type'];
        if($designer_type){
            $where[] = ['', 'exp', Db::raw("type = 1 and (user_type = 0 or user_type = ".$designer_type." or (user_type = 3 and user_id = ".$user_id."))")];
        }else{
            $where[] = ['', 'exp', Db::raw("type = 1 and (user_type = 0 or (user_type = 3 and user_id = ".$user_id."))")];
        }
        $field = 'content,send_time';
        $message = Db::name('message_notice')->where($where)
            ->field($field)
            ->order('message_id desc')
            ->limit(1)->find();
        if($message){
            $message['send_time']=get_date($message['send_time']);
            $message['typeid']=0;
        }else{
            $message['content']="暂无系统消息";
            $message['send_time']="";
            $message['typeid']=0;
        }
        return $message;
    }

    public function getUserMessageGroup($user)
    {
        $user_id = $user['user_id'];
        $where['from_user_id|to_user_id'] = $user_id;
        $field = 'content,send_time,from_user_id,to_user_id,typeid,aim_id,chat_identify';
        $count = Db::name('chatlog')
            ->field($field)
            ->where($where)
            ->group('chat_identify')
            ->count();
        $pager       = new Page($count, config('PAGESIZE'));// 实例化分页类 传入总记录数和每页显示的记录数

        $chat_identify = Db::name('chatlog')->where($where)
            ->order('msg_id desc')
            ->group('chat_identify')
            ->limit($pager->firstRow, $pager->listRows)->column('chat_identify');
        $list = [];
        foreach ($chat_identify as $v){
            $value = Db::name('chatlog')->where(['chat_identify'=>$v])
                ->field($field)
                ->order('msg_id desc')
                ->limit(1)->find();
            $value['send_time']=get_date($value['send_time']);
            $message_user_id = $value['from_user_id'];
            if($message_user_id==$user_id){
                $message_user_id = $value['to_user_id'];
            }
            //获取头像 昵称
            $value['photo'] = SITE_URL.dythumbimages($message_user_id,120,120,120,'users');
            $value['nickname'] = get_user_nickname($message_user_id);
            $value['user_id'] = dyencrypt($message_user_id);
            switch ($value['typeid']){
                case 1:
                    $value['content'] = '[图文]'.Db::name('user_dynamic')->where('dynamic_id',$value['aim_id'])->value('title');
                    break;
                case 2:
                    $value['content'] = '[视频]'.Db::name('user_dynamic')->where('dynamic_id',$value['aim_id'])->value('content');
                    break;
                case 3:
                    $value['content'] = '[方案]'.Db::name('user_dynamic')->where('dynamic_id',$value['aim_id'])->value('title');
                    break;
                case 4:
                    $value['content'] = '[家居]'.Db::name('furniture')->where('id',$value['aim_id'])->value('name');
                    break;
            }
            $value['typeid'] = 1;//通知列表  0=系统消息  1=私信
            $value['is_red'] = $this->getUserMessage($user_id,$value['chat_identify'])>0?1:0;
            unset($value['from_user_id'],$value['to_user_id'],$value['aim_id'],$value['chat_identify']);
            $list[] = $value;
        }
        return $list;
    }

    public function getUserMessageList($user,$message_user_id,$is_page=true,$msg_id=0)
    {
        $user_id = $user['user_id'];
        $chat_identify = [$message_user_id,$user_id];
        sort($chat_identify);
        $chat_identify_key = implode('_',$chat_identify);
        $where['chat_identify'] = $chat_identify_key;
        Db::name('chatlog')->where($where)->where(['to_user_id'=>$user_id,'is_read'=>0])->update(['is_read'=>1]);
        $field = 'from_user_id,content,send_time,typeid,aim_id,msg_id';
        //自己 获取头像
        $user['photo'] = SITE_URL.dythumbimages($user_id,120,120,120,'users');
        //其他获取 头像 昵称
        $message_user['photo'] = SITE_URL.dythumbimages($message_user_id,120,120,120,'users');
        $message_user['nickname'] = get_user_nickname($message_user_id);
        if($is_page){
            $count = Db::name('chatlog')
                ->field($field)
                ->where($where)
                ->count();
            $pager       = new Page($count, config('PAGESIZE'));// 实例化分页类 传入总记录数和每页显示的记录数
            $list        = Db::name('chatlog')->where($where)
                ->field($field)
                ->order('msg_id desc')
                ->limit($pager->firstRow, $pager->listRows)->select()->toArray();
            if($list){
                $list = array_sort($list,'msg_id','asc');
                $list = array_values($list);
            }
        }else{
            if($msg_id){
                $where['msg_id'] = ['>',$msg_id];
            }
            $list        = Db::name('chatlog')->where($where)
                ->field($field)
                ->order('msg_id asc')
                ->select()->toArray();
        }
        foreach ($list as &$value){
            $value['send_time']=get_date($value['send_time']);
            if($value['from_user_id']==$user_id){
                //自己消息
                $value['photo'] = $user['photo'];
                $value['nickname'] = $user['nickname'];
                $value['is_own'] = 1;
            }else{
                //他人消息
                $value['photo'] = $message_user['photo'];
                $value['nickname'] = $message_user['nickname'];
                $value['is_own'] = 0;
            }
            switch ($value['typeid']){
                case 1:
                    $user_dynamic = Db::name('user_dynamic')->where('dynamic_id',$value['aim_id'])->field('title')->find();
                    $value['title'] = $user_dynamic['title'];
                    $value['thumb'] = SITE_URL.dythumbimages($value['aim_id'],120,120,120,'user_dynamic');
                    $value['content'] = '';
                    break;
                case 2:
                    $user_dynamic = Db::name('user_dynamic')->where('dynamic_id',$value['aim_id'])->field('content')->find();
                    $value['title'] = $user_dynamic['content'];
                    $value['thumb'] = SITE_URL.dythumbimages($value['aim_id'],120,120,120,'user_dynamic');
                    $value['content'] = '';
                    break;
                case 3:
                    $user_dynamic = Db::name('user_dynamic')->where('dynamic_id',$value['aim_id'])->field('title,style_id,house_type_id,area')->find();
                    $value['title'] = $user_dynamic['title'];
                    $value['thumb'] = SITE_URL.dythumbimages($value['aim_id'],120,120,120,'user_dynamic');
                    $label = [];
                    $label[] = get_label_name($user_dynamic['style_id']);
                    $label[] = get_label_name($user_dynamic['house_type_id']);
                    $label[] = $user_dynamic['area']."㎡";
                    $value['label'] = $label;
                    $value['content'] = '';
                    break;
                case 4:
                    $user_dynamic = Db::name('furniture')->where('id',$value['aim_id'])->field('name')->find();
                    $value['title'] = $user_dynamic['name'];
                    $value['thumb'] = SITE_URL.dythumbimages($value['aim_id'],120,120,120,'furniture');
                    $value['content'] = '';
                    break;
            }
            unset($value['from_user_id']);
        }
        return $list;
    }

    public function getUserMessage($user_id,$chat_identify_key='')
    {
        $where['is_read']=0;
        $where['to_user_id']=$user_id;
        if($chat_identify_key){
            $where['chat_identify'] = $chat_identify_key;
        }
        $count = Db::name('chatlog')
            ->where($where)
            ->count();
        return $count;
    }

    /**
     * 绑定手机号
     * @param string $mobile - 手机号
     * @return void
     */
    public function bindMobile($mobile,$code,$user_id)
    {
        if(!$code){
            return ['errcode'=>0,'message'=>'请输入验证码'];
        }
        //验证验证码
        $check = check_validate_code($code, $mobile,5);
        if($check['errcode']!=1){
            return $check;
        }
        $res = Db::name('users')->where('user_id',$user_id)->update(['mobile'=>$mobile]);
        if(!$res){
            return ['errcode'=>0,'message'=>'绑定失败'];
        }
        return ['errcode'=>1,'message'=>'绑定成功'];
    }
}
