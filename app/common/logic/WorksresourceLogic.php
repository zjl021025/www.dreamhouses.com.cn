<?php


namespace app\common\logic;

use think\facade\Db;
use think\Page;

/**
 * 工作台作品逻辑定义
 * Class CatsLogic
 * @package Home\Logic
 */
class WorksresourceLogic
{
    /**
     * 列表
     * @return array
     */
    public function getImageList($where=[],$works_id=0)
    {
        $field = 'id,name,room_id,image,typeid';
        if($works_id){
            $room_ids = Db::name('user_works')
                ->alias('uw')
                ->join('scene_house sh','sh.house_id=uw.house_id')
                ->where('uw.works_id',$works_id)
                ->cache(true,60*30)
                ->value('sh.room_ids');
            $order_fields = Db::name('label')->order('sort asc,id desc')->whereIn('id',$room_ids)->column('id');
            $order_field = implode(',',$order_fields);
        }else{
            $order_fields = Db::name('label')->order('sort asc,id desc')
                ->where('is_show',1)
                ->where('typeid',5)
                ->cache(true,60*30)
                ->column('id');
            $order_field = implode(',',$order_fields);
        }
        $order = "field(room_id,".$order_field.") asc,id desc";
        $where['is_deleted'] = 0;
        $where['typeid'] = 1;
        return $this->getList($where,$field,$order);
    }

    /**
     * 列表
     * @return array
     */
    public function getVideoList($where=[])
    {
        $field = 'id,name,image,video,typeid';
        $order = 'id desc';
        $where['is_deleted'] = 0;
        $where['typeid'] = 2;
        return $this->getList($where,$field,$order);
    }

    public function getList($where=[],$field='*',$order='id desc')
    {
        $query = Db::name('user_works_resource');
        $count = $query->where($where)->count();// 查询满足要求的总记录数
        $pager = new Page($count, config('PAGESIZE'));// 实例化分页类 传入总记录数和每页显示的记录数
        $list = $query
            ->where($where)
            ->field($field)
            ->orderRaw($order)
            ->limit($pager->firstRow, $pager->listRows)
            ->select()->toArray();
        foreach ($list as &$value){
            if($value['typeid']==1){
                $value['room_name'] = get_label_name($value['room_id']);
            }else{
                $value['image'] = SITE_URL.$value['image'];
            }
        }
        return $list;
    }
}
