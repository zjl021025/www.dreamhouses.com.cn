<?php


namespace app\common\logic;

use think\facade\Db;
use think\Page;

/**
 * 动态逻辑定义
 * Class CatsLogic
 * @package Home\Logic
 */
class DynamicLogic
{
    /**
     * 关注列表
     * $user_id 用户id
     * @return array
     */
    public function getFollowList($user_id=0)
    {
        $where = [];
        $field = 'a.dynamic_id,a.title,a.user_id,a.house_type_id,a.style_id,a.praise,a.collect,a.evaluate,a.area,a.add_time,a.typeid,a.title,a.content,a.video,a.video_time,a.images,a.image';
        if($user_id){
            $aim_ids = Db::name('user_follow')->where(['user_id'=>$user_id])->column('aim_id');
            if($aim_ids){
                $where['a.user_id'] = ['in',$aim_ids];
            }else{
                $where['a.user_id'] = ['=','-1'];
//                $dynamic_ids = Db::name('user_dynamic')
//                    ->where('user_id','<>',$user_id)
//                    ->orderRaw('rand()')->limit(6)->column('dynamic_id');
//                $where['a.dynamic_id'] = ['in',$dynamic_ids];
            }
        }else{
            $where['a.user_id'] = ['=','-1'];
        }
        $list = $this->getList(0,0,$where,$user_id,$field,1);
        return $list;
    }

    /**
     * 灵感列表
     * $user_id 用户id
     * @return array
     */
    public function getInspirationList($typeid=0,$user_id=0,$user=[])
    {
        $where = [];
        $field = 'a.dynamic_id,a.title,a.user_id,a.house_type_id,a.style_id,a.praise,a.collect,a.evaluate,a.area,a.add_time,a.typeid,a.title,a.content,a.video,a.video_time,a.images,a.image';
        if($typeid!=0){
            $estate_ids = Db::name('house')->where(['is_deleted'=>0,'user_id'=>$user_id,'estate_id'=>['>',0]])->column('estate_id');
            if($estate_ids){
                $where['a.estate_id'] = ['in',$estate_ids];
            }/*else{
                $where['a.estate_id'] = '-1';
            }*/
            $type=0;
        }else{
            if($user_id>0 && $user['is_recd']!=1){
                $type=0;
            }else{
                $type=1;
            }
        }
        $list = $this->getList(0,$type,$where,$user_id,$field,0);
        return $list;
    }

    /**
     * 方案列表
     * $type 1=首页推荐 2=方案推荐 3=专家推荐 4=最热 5=最新
     * @return array
     */
    public function getSchemeList($type=0,$where=[],$user_id=0)
    {
        $field = 'a.dynamic_id,a.title,a.user_id,a.house_type_id,a.style_id,a.praise,a.collect,a.evaluate,a.area,a.add_time,a.typeid,a.images,a.image';
        $list = $this->getList(3,$type,$where,$user_id,$field);
        return $list;
    }

    /**
     * 视频列表
     * $type 1=首页推荐
     * @return array
     */
    public function getVideoList($type=0,$where=[],$user_id=0)
    {
        $field = 'a.dynamic_id,a.content,a.user_id,a.praise,a.collect,a.evaluate,a.add_time,a.typeid,a.video,a.video_time,a.image';
        $list = $this->getList(2,$type,$where,$user_id,$field);
        return $list;
    }

    /**
     * 图文列表
     * $type 1=首页推荐
     * @return array
     */
    public function getImagetextList($type=0,$where=[],$user_id=0)
    {
        $field = 'a.dynamic_id,a.title,a.user_id,a.praise,a.collect,a.evaluate,a.add_time,a.typeid,a.images,a.image';
        $list = $this->getList(1,$type,$where,$user_id,$field);
        return $list;
    }
    /**
     * //$typeid 1=图片  2=视频   3=方案
     * $is_index 是否首页
    */
    public function getList($typeid=0,$type=0,$where=[],$user_id=0,$field='*',$is_index=0)
    {
        $order = 'a.dynamic_id desc';
        //0=全部 1=首页推荐 2=方案推荐 3=专家推荐 4=最热 5=最新  6=我的动态  7=点赞记录  8=浏览记录  9=收藏记录  10=首页榜单
        if($is_index == 0) {
            switch ($type) {
                case 1:
//                    $where['a.is_home'] = 1;
                    $order = 'a.is_home desc,a.home_sort asc,' . $order;
                    break;
                case 2:
                    $where['a.is_recommend'] = 1;
                    break;
                case 3:
                    $where['a.is_expert'] = 1;
                    break;
                case 4:
                    $order = 'a.praise desc,' . $order;
                    break;
//            case 5:
//                $order = 'a.browse desc,'.$order;
//                break;
                case 7:
                case 8:
                case 9:
                    $order = 'b.add_time desc,' . $order;
                    break;
                case 10:
                    $order = 'a.virtually_browse+a.browse desc,' . $order;
                    $where[] = Db::Raw('a.virtually_browse+a.browse > 0');
                    break;
            }
        }
        $where['a.is_show'] = 1;
        if($typeid){
            $where['a.typeid'] = $typeid;
        }
        $query = Db::name('user_dynamic')->alias('a');
        switch ($type){
            case 7:
                $query = $query->join('user_praise b','a.dynamic_id=b.aim_id');
                break;
            case 8:
                $query = $query->join('user_browse b','a.dynamic_id=b.aim_id');
                break;
            case 9:
                $where['a.dynamic_id'] = ['>',0];
                $query = $query->join('user_collect b','a.dynamic_id=b.aim_id');
                break;
        }
        $where['c.is_deleted'] = 0;
        $query = $query->join('users c','c.user_id=a.user_id');
        $count = $query->where($where)->count();// 查询满足要求的总记录数
//        echo Db::getLastSql();
        $limit = config('PAGESIZE');
        if($typeid==2 && $type==1){
            $limit = 6;
        }
        $pager = new Page($count, $limit);// 实例化分页类 传入总记录数和每页显示的记录数
        $list = $query
            ->alias('a')
            ->where($where)
            ->field($field)
            ->orderRaw($order)
            ->limit($pager->firstRow, $pager->listRows)
            ->select()->toArray();


        /*//首页关注 没有数据，随机取6条
        if(count($list) <= 0 && $is_index == 1){
            $list = Db::name('user_dynamic')
                ->where('user_id','<>',$user_id)
                ->orderRaw('rand()')->limit(6)->select()->toArray();
        }*/

        foreach ($list as $k=>$value){
            if($value['typeid']==2){
                $list[$k]['video'] = SITE_URL.$value['video'];
            }
            switch ($value['typeid']){
                case 1:
                    //160*120
                    $list[$k]['img_width'] = 160;
                    $list[$k]['img_height'] = 120;
                    $list[$k]['thumb'] = SITE_URL.dythumbimages($value['dynamic_id'],320,240,320,'user_dynamic');
                    break;
                case 2:
                    //170*227
                    $list[$k]['img_width'] = 173;
                    $list[$k]['img_height'] = 231;
                    if($typeid==2 && $type==1 ){
                        if($k==1 || $k==5){
                            $list[$k]['img_width'] = 173;
                            $list[$k]['img_height'] = 173;
//                            $list[$k]['thumb'] = SITE_URL.dythumbimages($value['dynamic_id'],173,173,173,'user_dynamic');
                            $list[$k]['thumb'] = SITE_URL.get_sub_images(md5($value['image']),$value['image'], $value['dynamic_id'], 346,346, 'user_dynamic');
                        }else{
//                            $list[$k]['thumb'] = SITE_URL.dythumbimages($value['dynamic_id'],173,231,173,'user_dynamic');
                            $list[$k]['thumb'] = SITE_URL.get_sub_images(md5($value['image']),$value['image'], $value['dynamic_id'], 346,462, 'user_dynamic');
                        }
                    }else{
                        $list[$k]['thumb'] = SITE_URL.get_sub_images(md5($value['image']),$value['image'], $value['dynamic_id'],346,462,'user_dynamic');
                    }
//                    list($width, $height) = getimagesize($list[$k]['thumb']);
//                    $list[$k]['img_width'] = $width;
//                    $list[$k]['img_height'] = $height;
                    break;
                case 3:
                    //355*268
                    $list[$k]['img_width'] = 355;
                    $list[$k]['img_height'] = 268;
                    $list[$k]['thumb'] = SITE_URL.dythumbimages($value['dynamic_id'],710,536,710,'user_dynamic');
                    break;
            }

            get_user($list[$k]);
            //$value['nickname'] = get_user_nickname($value['user_id']);
            $list[$k]['photo'] = SITE_URL.dythumbimages($value['user_id'],120,120,120,'users');
            $list[$k]['add_time'] = friend_date($value['add_time']);
            $list[$k]['is_own'] = 0;//是否是自己
            if($user_id==$value['user_id']){
                $list[$k]['is_own'] = 1;
            }
            $list[$k]['is_follow'] = 0;//是否关注
            $list[$k]['is_praise'] = 0;//是否点赞
            $list[$k]['is_collect'] = 0;//是否收藏
            $list[$k]['is_evaluate'] = 0;//是否评论
            if($user_id){
                $is_follow = Db::name('user_follow')->where(['aim_id'=>$value['user_id'],'user_id'=>$user_id])->value('follow_id');
                $is_follow>0 && $list[$k]['is_follow'] = 1;

                $is_praise = Db::name('user_praise')->where(['aim_id'=>$value['dynamic_id'],'user_id'=>$user_id])->value('id');
                $is_praise>0 && $list[$k]['is_praise'] = 1;

                $is_collect = Db::name('user_collect')->where(['aim_id'=>$value['dynamic_id'],'user_id'=>$user_id])->value('id');
                $is_collect>0 && $list[$k]['is_collect'] = 1;

                $is_evaluate = Db::name('evaluate')->where(['dynamic_id'=>$value['dynamic_id'],'user_id'=>$user_id])->value('evaluate_id');
                $is_evaluate>0 && $list[$k]['is_evaluate'] = 1;
            }
            $list[$k]['user_id'] = dyencrypt($value['user_id']);
            if($value['typeid']==3) {
                //方案
                $label = [];
                $label[] = get_label_name($value['style_id']);
                $label[] = get_label_name($value['house_type_id']);
                $label[] = $value['area']."㎡";
                $list[$k]['label'] = $label;
                //两图展示
                $list[$k]['thumbs'] = [];
                $images = explode(',',$value['images']);
                if($images[0]){
                    $list[$k]['thumbs'][] = SITE_URL.get_sub_images(md5($images[0]),$images[0], $value['dynamic_id'], 640, 480, 'user_dynamic');
                }
                if(isset($images[1]) && $images[1]){
                    $list[$k]['thumbs'][] = SITE_URL.get_sub_images(md5($images[1]),$images[1], $value['dynamic_id'], 640, 480, 'user_dynamic');
                }
//                unset($value['user_id']);
            }elseif($value['typeid']==1){
                $list[$k]['thumbs'] = [];
                if($value['images']){
                    $images = explode(',',$value['images']);
                    foreach ($images as $kk=>$v){
                        if($kk==9){
                            break;
                        }
                        $list[$k]['thumbs'][] = SITE_URL.get_sub_images(md5($v),$v, $value['dynamic_id'], 640, 480, 'user_dynamic');
                    }
                }
            }
            unset($list[$k]['style_id'],$list[$k]['house_type_id'],$list[$k]['area'],$list[$k]['images'],$list[$k]['image']);
        }
        return $list;
    }

    //$typeid 1=图片  2=视频   3=方案
    public function getDynamic($typeid,$dynamic_id,$user_id,$field)
    {
        $where['dynamic_id'] = $dynamic_id;
        $where['is_show'] = 1;
        $info = Db::name('user_dynamic')->where($where)->field($field)->find();
        if($info){
//            if($user_id){
//
//            }
            $this->browse($typeid,$user_id,$dynamic_id);
            if($typeid==2){
                $info['thumb'] = SITE_URL.$info['image'];
                $info['video'] = SITE_URL.$info['video'];
            }else{
                $images = explode(',',$info['images']);
                [$max_height,$max_width,$min_width] = get_img_max_height($images);
                foreach ($images as &$v){
                    $v = SITE_URL.$v;
                }
                $info['thumbs'] = $images;
                $info['max_height'] = $max_height;
                $info['max_width'] = $max_width;
                $info['min_width'] = $min_width;
                //评价
                $info['evaluates'] = $this->getEvaluateList($info['dynamic_id'],$user_id,2);
            }
            get_user($info);
            //$info['nickname'] = get_user_nickname($info['user_id']);
            $info['photo'] = SITE_URL.dythumbimages($info['user_id'],120,120,120,'users');
            if($typeid==3){
                //方案
                $mername = "";
                if($info['district_id']){
                    $mername = get_region_mername($info['district_id'],'');
                }elseif($info['city_id']){
                    $mername = get_region_mername($info['city_id'],'');
                }elseif($info['province_id']){
                    $mername = get_region_mername($info['province_id'],'');
                }
                $info['address'] = $mername;
                $over_images = [];
                if($info['over_images']){
                    $over_images = explode(',',$info['over_images']);
                    foreach ($over_images as &$v){
                        $v = SITE_URL.$v;
                    }
                }
//                $info['thumbs_over'] = $over_images;
                $label = [];
                $label[] = get_label_name($info['style_id']);
                $label[] = get_label_name($info['house_type_id']);
                $label[] = $info['area']."㎡";
                $info['label'] = $label;
                $room = [];
                if($over_images || $info['over_content']){
                    $room[] = [
                        'thumbs'=>$over_images,
                        'content'=>$info['over_content'],
                        'room_name'=>"总览",
                    ];
                }
                $dynamic_room = Db::name('user_dynamic_room')->where(['scheme_id'=>$info['dynamic_id']])
                    ->field('room_id,content,images')
                    ->order('id asc')
                    ->select()->toArray();
                foreach ($dynamic_room as $value){
                    $images = [];
                    if($value['images']){
                        $images = explode(',',$value['images']);
                        foreach ($images as &$v){
                            $v = SITE_URL.$v;
                        }
                    }
                    $room[] = [
                        'thumbs'=>$images,
                        'content'=>$value['content'],
                        'room_name'=>get_label_name($value['room_id']),
                    ];
                }
                $info['room'] = $room;
            }else{
                if($info['scheme_id']){
                    $scheme = Db::name('user_dynamic')->where(['is_show'=>1,'dynamic_id'=>$info['scheme_id']])
                        ->field('dynamic_id,title,style_id,house_type_id,area')->find();
                    if($scheme){
                        $info['scheme']['thumb'] = SITE_URL.dythumbimages($scheme['dynamic_id'],160,120,160,'user_dynamic');
                        $label = [];
                        $label[] = get_label_name($scheme['style_id']);
                        $label[] = get_label_name($scheme['house_type_id']);
                        $label[] = $scheme['area']."㎡";
                        $info['scheme']['label'] = $label;
                        $info['scheme']['title'] = $scheme['title'];
                    }else{
                        $info['scheme_id'] = 0;
                    }
                }
            }
            $info['is_praise'] = 0;//是否点赞
            $info['is_evaluate'] = 0;//是否评论
            $info['is_follow'] = 0;//是否关注
            $info['is_own'] = 0;//是否是自己
            $info['is_collect'] = 0;//是否收藏
            if($user_id==$info['user_id']){
                $info['is_own'] = 1;
            }
            if($user_id){
                $is_follow = Db::name('user_follow')->where(['aim_id'=>$info['user_id'],'user_id'=>$user_id])->value('follow_id');
                $is_follow>0 && $info['is_follow'] = 1;

                $is_praise = Db::name('user_praise')->where(['aim_id'=>$info['dynamic_id'],'user_id'=>$user_id])->value('id');
                $is_praise>0 && $info['is_praise'] = 1;

                $is_collect = Db::name('user_collect')->where(['aim_id'=>$info['dynamic_id'],'user_id'=>$user_id])->value('id');
                $is_collect>0 && $info['is_collect'] = 1;

                $is_evaluate = Db::name('evaluate')->where(['dynamic_id'=>$info['dynamic_id'],'user_id'=>$user_id])->value('evaluate_id');
                $is_evaluate>0 && $info['is_evaluate'] = 1;
            }
            $info['user_id'] = dyencrypt($info['user_id']);
            unset($info['images'],$info['over_images'],$info['over_content'],$info['province_id'],$info['city_id'],$info['district_id'],$info['image']);
            $info['share_title'] = $info['title']?$info['title']:$info['content'];
            $info['share_thumb'] = SITE_URL.dythumbimages($info['dynamic_id'],355,268,355,'user_dynamic');
            $info['share_url'] = SITE_URL.url('Currency/dynamic',['dynamic_id'=>$info['dynamic_id'],'typeid'=>$typeid]);
        }
        return $info;
    }

    //浏览记录 $typeid 1=图文 2=视频 3=方案
    protected function browse($typeid,$user_id,$aim_id)
    {
        $id = Db::name('user_browse')->where(['user_id'=>$user_id,'typeid'=>$typeid,'aim_id'=>$aim_id])->value('id');
        if($id){
            Db::name('user_browse')->where('id',$id)->update(['add_time'=>time()]);
        }else{
            $data = [
                'user_id'=>$user_id,
                'typeid'=>$typeid,
                'aim_id'=>$aim_id,
                'add_time'=>time(),
            ];
            Db::name('user_browse')->insertGetId($data);
        }
        if(in_array($typeid,[1,2,3])){
            Db::name('user_dynamic')->where('dynamic_id', $aim_id)->inc('browse', 1)->update();
            $dynamic_user_id = Db::name('user_dynamic')->where('dynamic_id', $aim_id)->value('user_id');
            Db::name('users')->where('user_id', $dynamic_user_id)->inc('browse', 1)->update();
        }
    }

    public function getEvaluateList($aim_id,$user_id,$num=0,&$counts=0)
    {
        $where['is_show'] = 1;
        $where['dynamic_id'] = $aim_id;
        $field = 'content,add_time,user_id';
        $count = Db::name('evaluate')
            ->field($field)
            ->where($where)->count();
        $counts = $count;
        $pager       = new Page($count,$num?$num:config('PAGESIZE'));// 实例化分页类 传入总记录数和每页显示的记录数
        $list        = Db::name('evaluate')->where($where)
            ->field($field)
            ->order('evaluate_id desc')
            ->limit($pager->firstRow, $pager->listRows)->select()->toArray();
        foreach ($list as &$value){
            $value['add_time']=get_date($value['add_time']);
            $value['is_follow'] = 0;//是否关注
            if($user_id){
                $is_follow = Db::name('user_follow')->where(['aim_id'=>$value['user_id'],'user_id'=>$user_id])->value('follow_id');
                $is_follow>0 && $value['is_follow'] = 1;
            }
            get_user($value);
            $value['photo'] = SITE_URL.dythumbimages($value['user_id'],120,120,120,'users');
            $value['is_own'] = 0;//是否是自己
            if($user_id==$value['user_id']){
                $value['is_own'] = 1;
            }
            $value['user_id'] = dyencrypt($value['user_id']);
        }
        return $list;
    }
}
