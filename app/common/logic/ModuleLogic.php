<?php


namespace app\common\logic;
use think\facade\Db;


class ModuleLogic
{
    /**
     * 所有模块
     * @var array
     */
    public $modules = [];

    /**
     * 可见模块
     * @var array
     */
    public $showModules = [];

    public function getModules($onlyShow = true){
        if ($this->modules) {
            return $onlyShow ? $this->showModules : $this->modules;
        }

        $modules = [
            [
                'name'  => 'admincp',
                'title' => '总后台',
                'show' => 1,
                'privilege' => [
                    'index'=>'首页管理',
                    'user'=>'用户管理',
                    'house'=>'房屋管理',
                    'dynamic'=>'内容管理',
                    'recommend'=>'推荐管理',
                    'furniture'=>'家居管理',
                    'billboard'=>'榜单管理',
                    'evaluate'=>'评论管理',
                    'label'=>'选项配置',
                    'other'=>'其他信息',
                    'resource'=>'场景渲染',
                    'message'=>'消息管理',
                    'auth'=>'权限设置',
                ],
            ]
        ];

        $this->modules = $modules;
        foreach ($modules as $key => $module) {
            if (!$module['show']) {
                unset($modules[$key]);
            }
        }
        $this->showModules = $modules;

        return $onlyShow ? $this->showModules : $this->modules;
    }

    public function getModule($moduleIdx, $onlyShow = true){
        if (!self::isModuleExist($moduleIdx, $onlyShow)) {
            return [];
        }

        $modules = $this->getModules($onlyShow);
        return $modules[$moduleIdx];
    }

    public function isModuleExist($moduleIdx, $onlyShow = true){
        return key_exists($moduleIdx, $this->getModules($onlyShow));
    }

    public function getPrivilege($moduleIdx, $onlyShow = true){
        $modules = $this->getModules($onlyShow);
        return $modules[$moduleIdx]['privilege'];
    }
}