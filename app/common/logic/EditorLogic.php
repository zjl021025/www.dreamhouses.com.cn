<?php


namespace app\common\logic;
use think\facade\Db;
use app\common\model\WxMaterial;


/**
 * 编辑器类逻辑
 * Class EditorLogic
 * @package app\common\logic
 */
class EditorLogic
{
    /**
     * 水印
     * @param $img_path
     */
    public function waterImage($img_path)
    {
        
        require_once '../vendor/topthink/think-image/src/Image.php';
        require_once '../vendor/topthink/think-image/src/image/Exception.php';
        if(strstr(strtolower($img_path),'.gif'))
        {
            require_once '../vendor/topthink/think-image/src/image/gif/Encoder.php';
            require_once '../vendor/topthink/think-image/src/image/gif/Decoder.php';
            require_once '../vendor/topthink/think-image/src/image/gif/Gif.php';
        }
        
        $image = \think\Image::open($img_path);
        $water = dyCache('water');  //水印配置
        
        $return_data['mark_type'] = $water['mark_type'];
        if ($water['is_mark'] == 1 && $image->width() > $water['mark_width'] && $image->height() > $water['mark_height']) {
            
            if ($water['mark_type'] == 'text') {
                $ttf = root_path().'public/hgzb.ttf';                
                if (file_exists($ttf)) {                    
                    $size = $water['mark_txt_size'] ? $water['mark_txt_size'] : 30;
                    $color = $water['mark_txt_color'] ?: '#000000';
                    if (!preg_match('/^#[0-9a-fA-F]{6}$/', $color)) {
                        $color = '#000000';
                    }                
                    $transparency = intval((100 - $water['mark_degree']) * (127 / 100));
                    $color .= dechex($transparency);
                    $water['mark_txt'] = $this->to_unicode($water['mark_txt']);
                    chmod($img_path,0755);
                    $image->open($img_path)->text($water['mark_txt'], $ttf, $size, $color, $water['sel'])->save($img_path);               
                    $return_data['mark_txt'] = $water['mark_txt'];
                    
                }
            } else {
                $waterPath = "." . $water['mark_img'];
                $quality = $water['mark_quality'] ? $water['mark_quality'] : 80;
                $waterTempPath = dirname($waterPath) . '/temp_' . basename($waterPath);
                chmod($img_path,0755);
                $image->open($waterPath)->save($waterTempPath, null, $quality);              
                $image->open($img_path)->water($waterTempPath, $water['sel'], $water['mark_degree'])->save($img_path);                
                chmod($waterTempPath,0755);
                @unlink($waterTempPath);
            }
        }
    }

    /**
     * http://www.dzc.me/2017/07/php%E6%8A%A5imagettfbbox-any2eucjp-invalid-code-in-input-string%E7%9A%84%E4%B8%A4%E4%B8%AA%E8%A7%A3%E5%86%B3%E5%8A%9E%E6%B3%95/
     * 报imagettfbbox(): any2eucjp(): invalid code in input string的两个解决办法
     * @param $string
     * @return string
     */
    function to_unicode($string)
    {
        $str = mb_convert_encoding($string, 'UCS-2', 'UTF-8');
        $arrstr = str_split($str, 2);
        $unistr = '';
        foreach ($arrstr as $n) {
            $dec = hexdec(bin2hex($n));
            $unistr .= '&#' . $dec . ';';
        }
        return $unistr;
    }
    /**
     * 保存上传的图片
     * @param $file
     * @param $save_path
     * @return array
     */
    public function saveUploadImage($file, $save_path)
    {
        $return_url = '';
        $state = "SUCCESS";
        $new_path = $save_path.date('Y').'/'.date('m-d').'/';

        $waterPaths = ['lbcar/', 'water/']; //哪种路径的图片需要放oss
        $data['name'] = $file->getOriginalName();         
        $data['type'] = $file->getOriginalMime();
        $data['size'] =$file->getSize();
        $data['category_id'] = input('category_id/d',1);
        $data['mini'] = $file->getOriginalMime();
        $data['file_name'] = $file->getOriginalName();
 
        // 上传图片
        try {          
//            $savename = md5(mt_rand()).'.'.$file->extension();
//            \think\facade\Filesystem::disk('public')->putFileAs( $new_path, $file,$savename);
            $domain = config('qiniu.domain');
            $qiniu_file = \think\facade\Filesystem::disk('qiniu')->putFile('image',$file);
            $new_path = 'http://'.$domain.'/'.$qiniu_file;
        } catch (think\exception\ValidateException $e) {
                $upload_error = $e->getMessage();
        }            

        if ($upload_error) {
            $state = "ERROR" . $upload_error;
        } else {
//            $new_path = UPLOAD_PATH.$new_path.$savename;
            @chmod($new_path,0755);
            $return_url = $new_path;
            $pos = strripos($return_url,'.');
            $filetype = substr($return_url, $pos);
            if ($save_path =='lbcar/' && $filetype != '.gif') {  //只有商品图才打水印，GIF格式不打水印                   
                $this->waterImage(".".$return_url);  //水印                     
            }
            $data['file_name'] = $new_path;
            
        }
        
        if($state == 'SUCCESS'){
            //记录数据
            $data['path'] = $return_url;
            update_img_data($data,'');
        }

        return [
            'state' => $state,
            'url'   => $return_url
        ];
    }

}