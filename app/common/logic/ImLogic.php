<?php


namespace app\common\logic;

/**
 * 腾讯云即时通讯
 */
class ImLogic
{
    public function __construct()
    {
        $this->sdkappid = config('Im.appId');//应用ID
        $this->key = config('Im.key');//密钥
        $this->identifier = config('Im.admin');//管理员
        $this->UserSig = $this->createUserSig($this->identifier);//获取管理员的UserSig
        $this->random = (int)$this->createStr();//获取32位随机无符号整数
        $this->postfix = '?sdkappid='.$this->sdkappid.'&identifier='.$this->identifier.'&usersig='.$this->UserSig.'&random='.$this->random.'&contenttype=json';
    }
    /**
     * 创建UserSig
     *
     * @param [string] $UserID 用户ID
     * @param [string] $expire UserSig的有效期
     * @return string|bool
     */
    public function createUserSig($UserID, $expire = "")
    {
        if (!$UserID) {
            return false;//用户ID必须
        }
        if (!$expire) {
            $expire = 86400*7;//设置默认有效期为7天
        }
        $api = new TLSSigAPIv2($this->sdkappid, $this->key);
        return $api->genSig($UserID, $expire);//生成UserSig
    }
    /**
    * 将用户账号的UserSig 失效
    *
    * @param [type] $uid 要失效的用户Id
    * @return void
    */
    public function kickUser($uid)
    {
        $url = 'https://console.tim.qq.com/v4/im_open_login_svc/kick'.$this->postfix;
        $data = json_encode(['Identifier'=>(string)$uid]);
        $info = $this->httpRequest($url, $data);
        return json_decode($info, true);
    }
    /**
     * 随机32位无符号整数创建
     *
     * @return string
     */
    private function createStr()
    {
        $num = 32;
        $result = "";
        $str = '0123456789';
        for ($i = 0; $i < $num; $i++) {
            $result .= $str[rand(0, 9)];
        }
        return $result;
    }
    /**
     * curl请求
     *
     * @param string $url 请求地址
     * @param array $data 数据
     * @param array $headers
     * @return string
     */
    private function httpRequest($url, $data = null, $headers = array())
    {
        $curl = curl_init();
        if (count($headers) >= 1) {
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        }
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        if (!empty($data)) {
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        }
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($curl);
        curl_close($curl);
        return $output;
    }
    /**
     * 创建IM用户
     *
     * @param [string] $uid 用户名
     * @param [string] $nickname 昵称
     * @param [string] $img_url 头像
     * @return void
     */
    public function createAccount($uid, $nickname, $img_url)
    {
        $url = 'https://console.tim.qq.com/v4/im_open_login_svc/account_import'.$this->postfix;
        $list = ['Identifier'=>(string)$uid,'Nick'=>$nickname,'FaceUrl'=>$img_url];
        $json = json_encode($list, JSON_UNESCAPED_UNICODE);
        $info = $this->httpRequest($url, $json);
        $info = json_decode($info, true);
        return $info;
    }

    /**
     * 设置用户资料
     * [portrait_set description]
     * @param [type] $uid [description] 需要设置该 Identifier 的资料
     * @param [type] $type [description] 1=修改昵称  2=修改头像
     * @param [type] $value [description] 昵称或头像值
     * @return [type] [description]
     */
    public function portraitSet($uid, $type,$value)
    {
        if($type==1){
            //修改昵称
            $ProfileItem[0]['Tag'] = "Tag_Profile_IM_Nick";
        }else{
            //修改头像
            $ProfileItem[0]['Tag'] = "Tag_Profile_IM_Image";
        }
        $ProfileItem[0]['Value'] = $value;
        $url = 'https://console.tim.qq.com/v4/profile/portrait_set'.$this->postfix;
        $data = [
            'From_Account' => (string)$uid,'ProfileItem' => $ProfileItem
        ];
        $data = json_encode($data, JSON_UNESCAPED_UNICODE);
        $info = $this->httpRequest($url, $data);
        $info = json_decode($info, true);
        return $info;
    }
    /**
     * 添加好友
     *
     * @param [type] $From_Account 需要为该 UserID 添加好友
     * @param [type] $To_Account 好友的 UserID
     * @param [type] $AddSource 加好友来源关键词 加好友来源的关键字是 Android，则加好友来源字段是：AddSource_Type_Android
     * @param [type] $Remark 好友备注
     * @param [type] $GroupName 分组信息
     * @param [type] $AddWording 形成好友关系时的附言信息
     * @param [type] $AddType 加好友方式 Add_Type_Single 表示单向加好友 Add_Type_Both 表示双向加好友
     * @param [integer] $ForceAddFlags 管理员强制加好友标记：1表示强制加好友，0表示常规加好友方式
     * @return void
     */
    public function friendAdd($From_Account, $To_Account, $AddSource, $Remark, $GroupName, $AddWording = '', $AddType = 'Add_Type_Both', $ForceAddFlags = 1)
    {
        $url = 'https://console.tim.qq.com/v4/sns/friend_add'.$this->postfix;
        $AddFriendItem['To_Account'] = $To_Account;
        $AddFriendItem['AddSource'] = 'AddSource_Type_'.$AddSource;
        if (!empty($Remark)) {
            $AddFriendItem['Remark'] = $Remark;
        }
        if (!empty($GroupName)) {
            $AddFriendItem['GroupName'] = $GroupName;
        }
        if (!empty($AddWording)) {
            $AddFriendItem['AddWording'] = $AddWording;
        }
        $data = [
            'From_Account' => (string)$From_Account,
            'AddFriendItem' => [$AddFriendItem],
            'AddType' => $AddType,
            'ForceAddFlags' => $ForceAddFlags
        ];
        $data = json_encode($data, JSON_UNESCAPED_UNICODE);
        $info = $this->httpRequest($url, $data);
        $info = json_decode($info, true);
        return $info;
    }
    /**
     * 查询用户账号在线状态
     *
     * @param [array] $uids 需要查询这些 UserID 的登录状态，一次最多查询500个 UserID 的状态
     * @param [Integer] $isNeedDetail 是否需要返回详细的登录平台信息。0表示不需要，1表示需要
     * @return void
     */
    public function queryState($uids, $IsNeedDetail = 0)
    {
        $url = 'https://console.tim.qq.com/v4/openim/querystate'.$this->postfix;
        foreach ($uids as $key => $value) {
            $uids[$key] = (string)$value;
        }
        $data['TO_Account'] = $uids;
        if ((int)$IsNeedDetail == 1) {
            $data['IsNeedDetail'] = 1;
        }
        $data = json_encode($data, JSON_UNESCAPED_UNICODE);
        $info = $this->httpRequest($url, $data);
        $info = json_decode($info, true);
        return $info;
    }

    /**
    * 删除用户(仅限体验版账号使用，非体验版账号请使用kickUser()这个方法)
    *
    * @param [array] $uids  要删除的用户账号UserID集合,单次请求最多支持100个帐号
    * @return void
    */
    public function delAccount($uids)
    {
        $url = 'https://console.tim.qq.com/v4/im_open_login_svc/account_delete'.$this->postfix;
        $arr = [];
        foreach ($uids as $key => $value) {
            $arr[] = ['UserID'=>(string)$value];
        }
        $data = ['DeleteItem'=>$arr];
        $data = json_encode($data);
        $info = $this->httpRequest($url, $data);
        $info = json_decode($info, true);
        return $info;
    }
}
