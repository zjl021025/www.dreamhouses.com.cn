<?php


namespace app\common\logic;

use think\facade\Db;
use think\Page;

/**
 * 作品逻辑定义
 * Class CatsLogic
 * @package Home\Logic
 */
class WorksLogic
{
    /**
     * 户型列表
     * $type 0=全部  1=我的户型
     * @return array
     */
    public function getWorksList($where=[],$user_id=0)
    {
        $field = 'a.works_id,a.title,a.style_id,h.house_type_id,h.area';
        $order = 'a.works_id desc';
        $where['a.user_id'] = $user_id;
        $query = Db::name('user_works')->alias('a')
            ->join('scene_house h','h.house_id = a.house_id');
        $count = $query->where($where)->count();// 查询满足要求的总记录数
        $pager = new Page($count, config('PAGESIZE'));// 实例化分页类 传入总记录数和每页显示的记录数
        $list = $query
            ->alias('a')
            ->where($where)
            ->field($field)
            ->order($order)
            ->limit($pager->firstRow, $pager->listRows)
            ->select()->toArray();
        foreach ($list as &$value){
            $value['thumb'] = SITE_URL.dythumbimages($value['works_id'],160,120,160,'user_works');
            $label = [];
            if($value['style_id']){
                $label[] = get_label_name($value['style_id']);
            }
            $label[] = get_label_name($value['house_type_id']);
            $label[] = $value['area']."㎡";
            $value['label'] = $label;
            unset($value['house_type_id'],$value['style_id'],$value['area']);
        }
        return $list;
    }
}
