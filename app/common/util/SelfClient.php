<?php
namespace app\common\util;
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/9/20
 * Time: 10:56
 */

class SelfClient
{
    var $signKey;

    function sign($data)
    {
        if(isset($data['sign'])){
            unset($data['sign']);
        }

        $sign = '';
        ksort($data);
        foreach ($data as $key => $value)
        {
            $sign .= "$key=$value";
        }
        $sign .= $this->signKey;
        $key = md5($sign);
        return $key;
    }

}