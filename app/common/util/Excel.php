<?php

namespace app\common\util;

use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use think\exception\ValidateException;
use think\facade\Filesystem;

class Excel
{
    /**
     * @param string $filename
     * @return array|string
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    public static function import($filename = "")
    {
        $file[] = $filename;
        try {
            // 验证文件大小，名称等是否正确
            validate(['file' => 'filesize:51200|fileExt:xls,xlsx'])->check($file);
            // 把上传的excel文件下载到本地一份
            $savename = \think\facade\Filesystem::disk('public')->putFile('file', $file[0]);
            // 读取本地的excel文件
            $spreadsheet = IOFactory::load('storage/' . $savename);
            // 获取excel中的第一张sheet
            $sheet = $spreadsheet->getSheet(0);
            // 取得总行数(例如: 3)
            $highestRow = $sheet->getHighestRow();
            // 取得总列数(例如：F)
            $highestColumn = $sheet->getHighestColumn();
            Coordinate::columnIndexFromString($highestColumn);
            // 去除表头后的总行数
            $lines = $highestRow - 1;
            if ($lines <= 0) {
                return "数据为空数组";
            }
            // 直接取出excel中的数据
            // toArray() 具体参数参考官方文档
            $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
            // 删除第一个元素（表头）
            array_shift($sheetData);
            // 删除本地下载的文件
            unlink('storage/' . $savename);
            // 返回结果
            return $sheetData;
        } catch (ValidateException $e) {
            return $e->getMessage();
        }
    }



    /**
     * 数据导出Excel(csv文件)
     * @param string $file_name 文件名称
     * @param array $tile 标题
     * @param array $data 数据源
     */
    public static function export($file_name, $tile = [], $data = [])
    {
        ini_set('memory_limit', '512M');
        ini_set('max_execution_time', 0);
        ob_end_clean();
        ob_start();
        header("Content-Type: text/csv");
        header("Content-Disposition:filename=" . $file_name);
        $fp = fopen('php://output', 'w');
        // 转码 防止乱码(比如微信昵称)
        fwrite($fp, chr(0xEF) . chr(0xBB) . chr(0xBF));
        fputcsv($fp, $tile);
        $index = 0;
        foreach ($data as $item) {
            if ($index == 1000) {
                $index = 0;
                ob_flush();
                flush();
            }
            $index++;
            fputcsv($fp, $item);
        }
        ob_flush();
        flush();
        ob_end_clean();
    }
}