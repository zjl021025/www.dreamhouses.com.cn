<?php


namespace app\common\validate;

use think\facade\Db;

class Evaluate extends BaseValidate
{
    //验证规则
    protected $rule = [
        'dynamic_id'   => 'require|checkDynamic',
        'content' => 'require|sensitive|max:350',
    ];

    //错误消息
    protected $message = [
        'dynamic_id.require' => '缺少信息id',
        'dynamic_id.checkDynamic' => '信息不存在',
        'content.require' => '请输入评价内容',
        'content.sensitive' => '评价内容有敏感词',
        'content.max' => '评价内容最多350个字',
    ];

    //校验id
    protected function checkDynamic($value, $rule, $data)
    {
        $dynamic_count = Db::name('user_dynamic')->where(['dynamic_id'=>$value])->count();
        if(!$dynamic_count){
            return false;
        }
        return true;
    }
}
