<?php


namespace app\common\validate;


class UserDynamic extends BaseValidate
{
    //验证规则
    protected $rule = [
        'images'   => 'require',
        'image'   => 'require',
        'video'   => 'require',
        'title'   => 'require|sensitive|max:100',
        'content'   => 'require|sensitive|max:300',
        'style_id'   => 'require',
        'works_id'   => 'require',
        'over_images'   => 'require',
        'over_content'   => 'require',
    ];

    //错误消息
    protected $message = [
        'images.require' => '请上传图片',
        'image.require' => '请上传图片',
        'video.require' => '请上传视频',
        'title.require' => '标题不能为空',
        'title.sensitive' => '标题有敏感词',
        'title.max' => '标题最多100个字',
        'content.require' => '内容不能为空',
        'content.sensitive' => '内容有敏感词',
        'content.max' => '内容最多300个字',
        'style_id.require'=>'请选择风格',
        'works_id.require'=>'请选择作品',
        'over_images.require'=>'请上传总览图片',
        'over_content.require'=>'请输入总览描述',
    ];
    //验证场景
    protected $scene = [
        'video'  => ['content','image','video'],
        'imagetext'  => ['title','content','images'],
        'scheme'  => ['works_id','images','style_id','works_id','title'],//,'over_images','over_content'
    ];
}
