<?php


namespace app\common\validate;

use think\facade\Db;

/**
 * 认证验证器
 * @author Administrator
 */
class Authen extends BaseValidate
{
    //验证规则
    protected $rule = [
        'typeid'   => 'require',
        'realname' => 'require|max:30',
        'cre_type' => 'require',
        'cre_no' => 'require|max:30',
        'mobile' => 'require|checkMobile',
        'company_name' => 'require|max:30',
        'credit_code' => 'require|max:60',
        'cre_z' => 'require',
        'cre_f' => 'require',
        'business' => 'require',
    ];

    //错误消息
    protected $message = [
        'typeid.require' => '请选择认证类型',
        'realname.require' => '请输入真实姓名',
        'realname.max' => '真实姓名最多30个字符',
        'cre_type.require' => '请选择证件类型',
        'cre_no.require' => '请输入证件号',
        'cre_no.max' => '证件号最多30个字符',
        'mobile.require' => '请输入手机号',
        'mobile.checkMobile' => '手机号不正确',
        'company_name.require' => '请输入企业全称',
        'credit_code.require' => '请输入统一信用代码',
        'cre_z.require' => '请上传证件正面',
        'cre_f.require' => '请上传证件反面',
        'business.require' => '请上传营业执照',
    ];
    
    //验证场景
    protected $scene = [
        'typeid1'  => ['typeid','realname','cre_type','cre_no','mobile','cre_z','cre_f'],
        'typeid2'  => ['typeid','realname','cre_type','cre_no','mobile','company_name','credit_code','business','cre_z','cre_f']
    ];
}
