<?php


namespace app\common\validate;


class Works extends BaseValidate
{
    //验证规则
    protected $rule = [
        'images'   => 'require',
        'title'   => 'require|sensitive|max:100',
        'house_id'   => 'require',
        'style_id'   => 'require',
//        'over_images'   => 'require',
//        'over_content'   => 'require',
    ];

    //错误消息
    protected $message = [
        'images.require' => '请上传图片',
        'title.require' => '标题不能为空',
        'title.sensitive' => '标题有敏感词',
        'title.max' => '标题最多100个字',
        'house_id.require'=>'缺少户型ID',
        'style_id.require'=>'请选择风格',
//        'over_images.require'=>'请上传总览图片',
//        'over_content.require'=>'请输入总览描述',
    ];
}
