<?php

namespace app\common\validate;

use think\facade\Db;

class PublishWorks extends BaseValidate
{
    //验证规则
    protected $rule = [
        'user_id'   => 'require|checkId',
        'works_id' => 'require',
        'title' => 'require',
        'over_content' => 'require',
        'image' => 'require',
        'images' => 'require',
    ];

    //错误消息
    protected $message = [
        'user_id.require' => '缺少id',
        'user_id.checkId' => '信息不存在',
        'works_id.require' => '请选择作品',
        'title.require' => '请输入作品名称',
        'over_content.require' => '请选择作品详情',
        'image.require' => '请上传作品封面图',
        'images.require' => '请上传轮播图',
    ];
    //校验id
    protected function checkId($value, $rule, $data)
    {
        $res = Db::name('users')->where(['user_id'=>$value,'is_deleted'=>0])->value('user_id');
        if ($res) {
            return true;
        }
        return false;
    }
}