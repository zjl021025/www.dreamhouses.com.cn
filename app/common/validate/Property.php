<?php

namespace app\common\validate;

use think\facade\Db;

class Property extends BaseValidate
{
    //验证规则
    protected $rule = [
        'user_id'   => 'require|checkId',
        'city_id' => 'require',
        'cellName' => 'require',
        'house_type_id' => 'require',
        'area' => 'require',
        'images' => 'require',
    ];

    //错误消息
    protected $message = [
        'user_id.require' => '缺少id',
        'user_id.checkId' => '信息不存在',
        'city_id.require' => '请选择城市',
        'cellName.require' => '请输入小区名称',
        'house_type_id.require' => '请选择户型',
        'area.require' => '请输入室内面积',
        'images.require' => '请上传图片',
    ];
    //校验id
    protected function checkId($value, $rule, $data)
    {
        $res = Db::name('users')->where(['user_id'=>$value,'is_deleted'=>0])->value('user_id');
        if ($res) {
            return true;
        }
        return false;
    }
}