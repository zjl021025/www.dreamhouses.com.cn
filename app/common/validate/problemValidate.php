<?php

namespace app\common\validate;

use think\facade\Db;

class problemValidate extends BaseValidate
{
    //验证规则
    protected $rule = [
        'user_id'   => 'require',
        'content'   => 'require',
        'mobile'   => 'require',
    ];

    //错误消息
    protected $message = [
        'user_id.require' => '用户不能为空',
        'mobile.require' => '手机号不能为空',
        'content.require' => '内容不能为空',
    ];
    //校验id

    protected function checkId($value, $rule, $data)
    {
        $res = Db::name('users')->where(['user_id'=>$value,'is_deleted'=>0])->value('user_id');
        if ($res) {
            return true;
        }
        return false;
    }
}