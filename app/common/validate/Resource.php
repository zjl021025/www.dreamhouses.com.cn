<?php


namespace app\common\validate;


class Resource extends BaseValidate
{
    //验证规则
    protected $rule = [
        'typeid'   => 'require',
        'works_id'   => 'require',
//        'room_id'   => 'require',
        'room_name'   => 'require',
        'image'   => 'require',
        'video'   => 'require',
    ];

    //错误消息
    protected $message = [
        'typeid.require' => '类型错误',
        'works_id.require' => '缺少作品ID',
//        'room_id.require' => '缺少房间ID',
        'room_name.require' => '缺少房间信息',
        'image.require' => '请上传图片',
        'video.require' => '请上传视频',
    ];
    //验证场景
    protected $scene = [
        'typeid0'  => ['typeid','works_id','image'],
        'typeid1'  => ['typeid','works_id','room_name','image'],
        'typeid2'  => ['typeid','works_id','video'],
    ];
}
