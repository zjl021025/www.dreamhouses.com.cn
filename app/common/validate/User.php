<?php


namespace app\common\validate;

use think\facade\Db;

/**
 * 用户验证器
 * @author Administrator
 */
class User extends BaseValidate
{
    //验证规则
    protected $rule = [
        'user_id'   => 'require|checkId',
        'nickname' => 'require|max:20',
        'mobile' => 'require|checkMobile|checkUserMobile',
        'password' => 'require|max:20|min:6',
        'avatar' => 'require',
        'expe_id' => 'require',
        'style_ids' => 'require',
        'price_id' => 'require',
        'is_water' => 'require',
        'is_recd' => 'require',
    ];
    
    //错误消息
    protected $message = [
        'user_id.require' => '缺少id',
        'user_id.checkId' => '信息不存在',
        'nickname.require' => '请输入昵称',
        'nickname.max' => '昵称最多20个字符',
        'mobile.require' => '请输入手机号',
        'mobile.checkMobile' => '手机号不正确',
        'mobile.checkUserMobile' => '已存在相同手机号',
        'password.require' => '请输入密码',
        'password.min' => '密码范围6~20个字符',
        'password.max' => '密码范围6~20个字符',
        'avatar.require' => '请上传头像',
        'expe_id.require' => '请选择设计经验',
        'style_ids.require' => '请选择设计风格',
        'price_id.require' => '请选择设计费用',
        'is_water.require' => '请选择水印设置',
        'is_recd.require' => '请选择相关推荐设置',
    ];
    
    //验证场景
    protected $scene = [
        'add'  => ['nickname','mobile','password'],
        'del'  => ['user_id'],
        'cancel'  => ['user_id'],
        'typeid1'  => ['avatar'],
        'typeid2'  => ['nickname'],
        'typeid3'  => ['expe_id'],
        'typeid4'  => ['style_ids'],
        'typeid5'  => ['price_id'],
        'typeid6'  => ['is_water'],
        'typeid7'  => ['is_recd'],
    ];
    //校验id
    protected function checkId($value, $rule, $data)
    {
        $res = Db::name('users')->where(['user_id'=>$value,'is_deleted'=>0])->value('user_id');
        if ($res) {
            return true;
        }
        return false;
    }

    //
    protected function checkUserMobile($value, $rule, $data)
    {
        $where['is_deleted'] = 0;
        if($data['user_id']){
            $where['user_id'] = ['<>',$data['user_id']];
        }
        $where['mobile'] = $value;
        $res = Db::name('users')->where($where)->value('user_id');
        if ($res) {
            return false;
        }
        return true;
    }
}
