<?php

namespace app\common\validate;

class SiteValidate extends BaseValidate
{
    //验证规则
    protected $rule = [
        'consignee'   => 'require',
        'mobile'   => 'require',
        'area'   => 'require',
        'site'   => 'require',
    ];

    //错误消息
    protected $message = [
        'consignee.require' => '收货人不能为空',
        'mobile.require' => '手机号不能为空',
        'area.require' => '所在地区不能为空',
        'site.require' => '详细地址',
    ];
}