<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/7/30
 * Time: 17:07
 */
namespace app\common\validate;

use think\facade\Db;
use think\Validate;

class BaseValidate extends Validate
{
    //验证手机号
    protected function checkMobile($value, $rule, $data)
    {
        if(validateForm('mobile',$value)!==''){
            return false;
        }
        return true;
    }

    //校验身份证号
    protected function checkIdCard($value, $rule, $data)
    {
        if(validateForm('identity',$value)!==''){
            return false;
        }
        return true;
    }

    //敏感词验证
    protected function sensitive($value, $rule, $data)
    {
        $supinsert = Db::name('sensitive')->where('is_show',1)->select()->toArray();
        foreach ($supinsert as $v){
            if(strpos($value,$v['name'])!==false ){
                return false;
            }
        }
        return true;
    }

    //验证价格
    protected function checkPrice($value,$rule,$data) {
        if (preg_match('/^[0-9]+\d*(.\d{0,2})?$|^\d+.\d{0,2}$/',$value)) {
            return true;
        } else {
            return false;
        }
    }

    //验证市区选择
    protected function checkRegion($value,$rule,$data) {
        if(!$data['city']){
            $city = Db::name('region')->where('level',2)->where('pid',$value)->value('id');
            if($city){
                return '请选择城市';
            }
        }else{
            if(!$data['district']){
                $district = Db::name('region')->where('level',3)->where('pid',$data['city'])->value('id');
                if($district){
                    return '请选择区域';
                }
            }
        }
        return true;
    }

    //验证电话
    protected function checkPhone($value, $rule, $data)
    {
        if(validateForm('phone',$value)!==''){
            return false;
        }
        return true;
    }
}