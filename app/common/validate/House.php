<?php

namespace app\common\validate;


class House extends BaseValidate
{
    //验证规则
    protected $rule = [
        'district'   => 'require',
        'address'   => 'require',
        'name'   => 'require',
        'house_type_id'   => 'require',
        'area'   => 'require',
    ];

    //错误消息
    protected $message = [
        'district.require' => '获取定位失败',
        'address.require' => '详细地址获取失败',
        'name.require' => '楼盘名称获取失败',
        'house_type_id.require' => '请选择户型',
        'area.require' => '获取面积失败',
    ];
}
