<?php


namespace app\common\validate;


class Design extends BaseValidate
{
    //验证规则
    protected $rule = [
        'works_id'   => 'require',
        'room_id'   => 'require',
        'furniture_id'   => 'require',
        'length'   => 'require',
        'width'   => 'require',
        'height'   => 'require',
        'material_filename'   => 'require',
    ];

    //错误消息
    protected $message = [
        'works_id.require' => '缺少作品ID',
        'room_id.require' => '缺少房间ID',
        'furniture_id.require' => '缺少家居ID',
        'length.require' => '缺少尺寸长',
        'width.require' => '缺少尺寸宽',
        'height.require' => '缺少尺寸高',
        'material_filename.require' => '缺少材料文件名称',
    ];
    //验证场景
//    protected $scene = [
//        'typeid0'  => ['typeid','works_id','image'],
//        'typeid1'  => ['typeid','works_id','room_id','image'],
//        'typeid2'  => ['typeid','works_id','video'],
//    ];
}
