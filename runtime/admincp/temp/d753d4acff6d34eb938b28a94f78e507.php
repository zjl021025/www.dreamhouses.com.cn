<?php /*a:2:{s:79:"F:\phpstudy_pro\WWW\www.dreamhouses.com.cn\app/admincp/dytpl/user/userList.html";i:1698049681;s:79:"F:\phpstudy_pro\WWW\www.dreamhouses.com.cn\app/admincp/dytpl/public/layout.html";i:1698049681;}*/ ?>
<!doctype html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <!-- Apple devices fullscreen -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <!-- Apple devices fullscreen -->
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <link href="/public/static/css/main.css" rel="stylesheet" type="text/css">
    <link href="/public/static/css/list_optimization.css" rel="stylesheet" type="text/css">
    <link href="/public/static/css/page.css" rel="stylesheet" type="text/css">
    <link href="/public/static/font/css/font-awesome.min.css" rel="stylesheet" />
    <link href="/public/static/css/union.css" rel="stylesheet" />
    <link href="/public/js/layui/css/layui.css" rel="stylesheet" />
    <!--[if IE 7]>
    <link rel="stylesheet" href="/public/static/font/css/font-awesome-ie7.min.css">
    <![endif]-->
    <link href="/public/static/js/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
    <link href="/public/static/js/perfect-scrollbar.min.css" rel="stylesheet" type="text/css"/>
    <style type="text/css">
    html, body { overflow: visible;}
    .fine_layer{
		width: 100%;
		padding: 10px;
		box-sizing: border-box;
    }
	.fine_layer *{
		box-sizing: border-box;
    }
	.fine_layer textarea{
		width: 100%;
        height: 80px;
		border: 1px solid #e6e6e6;
        border-radius: 4px;
        padding: 5px 4px;
        resize: none!important;
    }
    .red{
        color: red !important;
    }
    .hDiv th div{
        text-align: center;
    }
    .bDiv td div{
        text-align: center;
    }
    </style>
    <script type="text/javascript" src="/public/static/js/jquery.js"></script>
    <script type="text/javascript" src="/public/static/js/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="/public/static/js/layer/layer.js"></script><!-- 弹窗js 参考文档 http://layer.layui.com/-->
    <script src="/public/js/layui/layui.js"></script>
    <script type="text/javascript" src="/public/static/js/admin.js"></script>
    <script type="text/javascript" src="/public/static/js/jquery.validation.min.js"></script>
    <script type="text/javascript" src="/public/static/js/common.js"></script>
    <script type="text/javascript" src="/public/static/js/perfect-scrollbar.min.js"></script>
    <script type="text/javascript" src="/public/static/js/jquery.mousewheel.js"></script>
    <script src="/public/js/myFormValidate.js"></script>
    <script src="/public/js/myAjax2.js"></script>
    <script src="/public/js/dyglobal.js"></script>
    <script type="text/javascript">
        function in_array(needle, haystack) {
            if(typeof needle == 'string' || typeof needle == 'number') {
                for(var i in haystack) {
                    if(haystack[i] == needle) {
                        return true;
                    }
                }
            }
            return false;
        }
        function checkEmtpy(obj,title) {
            var url = $(obj).attr('href')
            if(!url){
                if(!title){
                    title = '没有图片';
                }
                layer.msg(title)
                return false;
            }
            return true;
        }
        function checkPrice(obj){
            obj.value = obj.value.replace(/[^\d.]/g,"");  //清除“数字”和“.”以外的字符
            obj.value = obj.value.replace(/^\./g,"");  //验证第一个字符是数字而不是.
            obj.value = obj.value.replace(/\.{2,}/g,"."); //只保留第一个. 清除多余的.
            obj.value = obj.value.replace(".","$#$").replace(/\./g,"").replace("$#$",".");
            obj.value = obj.value.replace(/^(\d+)\.(\d\d).*$/,'$1.$2');
        }
        function checkNumber(obj){
            obj.value = obj.value.replace(/[^\d]/g,"");  //清除“数字”以外的字符
        }
    function is_show(obj) {
        var content = $(obj).data('name');
        layer.tips(content, obj, {
            tips: [1, "#4794ec"]
        });
    }
    function selectAll(name,obj){
        $('input[name*='+name+']').prop('checked', $(obj).checked);
    }
    //个人认证/企业认证/车辆认证-通过
    function auth_agree(obj)
    {
        var url = "<?php echo url('User/checkAuth'); ?>";
        var id = $(obj).data('id');
        var type = $(obj).data('type');
        layer.confirm('确认审核通过吗？',{title:'温馨提示',closeBtn:0,icon:3,btn: ['确定','取消'] }, function() {
            $.ajax({
                type : 'post',
                url : url,
                data : {act:'agree',id:id,type:type},
                dataType : 'json',
                success : function(data) {
                    layer.closeAll();
                    if (data.errcode == 1) {
                        layer.msg(data.message, {icon: 1, time: 2000},function(){
                            location.href = data.data.url;
                        });
                    } else {
                        layer.msg(data.message, {icon: 2,time: 2000});
                    }
                }
            })
        }, function(index){
            layer.close(index);
            return false;// 取消
        });
    }
    //-拒绝
    function auth_refuse(obj)
    {
        var url = "<?php echo url('User/checkAuth'); ?>";
        var id = $(obj).data('id');
        var type = $(obj).data('type');
        var html = `<div class="fine_layer">
            <textarea id="remark" maxlength="100" placeholder="请填写拒绝原因(100字内)"></textarea>
        </div>`;
		layer.open({
			type:1,
			title: '审核拒绝',
            content: html,
			area: ['400px', '210px'],
            closeBtn:0,
			btn:['确定', '取消'],
			yes:function(index){
				var content = $("#remark").val();
				/* if (content == "") {
					layer.msg('请填写拒绝原因', {icon: 2,time:2000});
					return false;
				} */
				$.ajax({
					type: 'POST',
					url: url,
					data : {id:id,remark:content,act:'refuse',type:type},
					dataType: 'json',
					success: function(data){
                        if (data.errcode == 1) {
                            layer.msg(data.message, {icon: 1, time: 2000},function(){
                                location.href = data.data.url;
                            });
                        } else {
                            layer.msg(data.message, {icon: 2,time: 2000});
                        }
					},
					error: function(){
						layer.alert("服务器繁忙, 请联系管理员!");
					}
				});
			}
		});
    }

    function delAll() {
        var ids = '';
        $('#flexigrid .trSelected').each(function(i,o) {
            ids += $(o).data('id')+',';
        })
        if (ids == '' || ids == '0,' || ids == '0') {
            layer.confirm('请选择删除项', {title:'温馨提示',closeBtn:0,icon:3});
            return;
        }
        layer.confirm('确认删除？',{title:'温馨提示',closeBtn:0,icon:3,btn: ['确定','取消'] }, function() {
            $.ajax({
                type : 'post',
                url : $('#flexigrid').data('url'),
                data : {act:'del',ids:ids},
                dataType : 'json',
                success : function(data) {
                    layer.closeAll();
                    if (data.errcode == 1) {
                        layer.msg('操作成功', {icon: 1});
                        $('#flexigrid .trSelected').each(function(i,o) {
                            $(o).remove();
                        })
                    } else {
                        layer.msg(data.message, {icon: 2,time: 2000});
                    }
                }
            })
        }, function(index){
            layer.close(index);
            return false;// 取消
        });
    }

    /**
     * 全选
     * @param obj
     */
    function checkAllSign(obj){
        $(obj).toggleClass('trSelected');
        if($(obj).hasClass('trSelected')){
            $('#flexigrid > table>tbody >tr').addClass('trSelected');
        }else{
            $('#flexigrid > table>tbody >tr').removeClass('trSelected');
        }
    }
    //判断是否全选
	function renderCheckAll(){
        var goodsLen = $('#flexigrid tr').length;
        var checkedLen = $('#flexigrid tr.trSelected').length;
        if(goodsLen == checkedLen){
          $('.check-all').addClass('trSelected')
        }else{
          $('.check-all').removeClass('trSelected')
        }
	}
    /**
     * 批量公共操作（删，改）
     * @returns {boolean}
     */
    function publicHandleAll(type){
        var ids = '';
        $('#flexigrid .trSelected').each(function(i,o){
            ids += $(o).data('id')+',';
        });
        if(ids == ''){
            layer.msg('至少选择一项', {icon: 2, time: 2000});
            return false;
        }
        publicHandle(ids,type); //调用删除函数
    }
    /**
     * 批量公共操作（删）新
     * @returns {boolean}
     */
    function publicUpdateAll(name,field_name){
        var ids = '';
        $('#flexigrid .trSelected').each(function(i,o){
            if(ids ==''){
                ids = $(o).data('id');
            }else{
                ids += ','+$(o).data('id');
            }
        });
        if(ids == ''){
            layer.msg('至少选择一项', {icon: 2, time: 2000});
            return false;
        }
        publicUpdate(ids,name,field_name); //调用删除函数
    }
    /**
     * 公共操作（删，改）
     * @param type
     * @returns {boolean}
     */
    function publicHandle(ids,handle_type){
        layer.confirm('确认当前操作？', {
            title:'温馨提示',closeBtn:0,icon:3,
                    btn: ['确定', '取消'] //按钮
                }, function () {
                    // 确定
                    $.ajax({
                        url: $('#flexigrid').data('url'),
                        type:'post',
                        data:{ids:ids,act:handle_type},
                        dataType:'JSON',
                        success: function (data) {
                            layer.closeAll();
                            if (data.errcode == 1){
                                layer.msg(data.message, {icon: 1, time: 2000},function(){
                                    location.href = data.dyurl;
                                });
                            }else{
                                layer.msg(data.message, {icon: 2, time: 2000});
                            }
                        }
                    });
                }, function (index) {
                    layer.close(index);
                }
        );
    }

        /**
         * 公共操作（删，改）新
         * @param type
         * @returns {boolean}
         */
        function publicUpdate(ids,name,field_name){
            layer.confirm('确认当前操作？', {
                title:'温馨提示',closeBtn:0,icon:3,
                        btn: ['确定', '取消'] //按钮
                    }, function () {
                        // 确定
                        $.ajax({
                            url: '/Admincp/System/deleteData',
                            type:'post',
                            data:{ids:ids,name:name,field_name:field_name},
                            dataType:'JSON',
                            success: function (data) {
                                layer.closeAll();
                                if (data.status == 1){
                                    layer.msg(data.msg, {icon: 1, time: 2000},function(){
                                        window.location.reload();
                                    });
                                }else{
                                    layer.msg(data.msg, {icon: 2, time: 2000});
                                }
                            }
                        });
                    }, function (index) {
                        layer.close(index);
                    }
            );
        }

        function delfuntion(obj) {
            // 删除按钮
            layer.confirm('确认刪除？', {
                title:'温馨提示',closeBtn:0,icon:3,
                btn: ['确定', '取消'] //按钮
            }, function () {
                $.ajax({
                    type: 'post',
                    url: '/Admincp/System/deleteData',
                    data : {ids:$(obj).attr('data-id'),name:$(obj).attr('data-name'),field_name:$(obj).attr('data-field_name')},
                    dataType: 'json',
                    success: function (data) {
                        layer.closeAll();
                        if (data.status == 1) {
                            $(obj).parent().parent().parent().remove();
                            layer.closeAll();
                        } else {
                            layer.alert(data, {icon: 2});  //alert('删除失败');
                        }
                    }
                })
            }, function () {
                layer.closeAll();
            });
        }
        function delfunc(obj,field='id') {
            var url = $(obj).data('url');
            // 删除按钮
            layer.confirm('确认刪除？', {
                title:'温馨提示',closeBtn:0,icon:3,
                btn: ['确定', '取消'] //按钮
            }, function () {
                var data = {}
                data[field] = $(obj).data('id')
                data['act'] = 'del'
                $.ajax({
                    type: 'post',
                    url: url,
                    data : data,
                    dataType: 'json',
                    success: function (data) {
                        layer.closeAll();
                        if (data.errcode == 1) {
                            layer.msg(data.message,{icon:1,time:2000},function(){
                                window.location.reload();
                            });
                        } else {
                            layer.msg(data.message, {icon: 2});  //alert('删除失败');
                        }
                    }
                })
            }, function () {
                layer.closeAll();
            });
        }
    </script>
</head>
<style>
    .fa-check-circle,.fa-ban{cursor:pointer}
</style>
<style>
    .flexigrid .sDiv2{
        border: none;
    }
    .flexigrid .sDiv2 .select{
        border: 1px solid #D7D7D7;
        border-radius: 4px;
        margin-right: 10px;
    }
    .flexigrid .sDiv2 .qsbox{
        border: 1px solid #D7D7D7;
        border-radius: 4px;
    }
    .flexigrid .bDiv td div input{
        width: 35px;
    }
    /*导出*/
    #layui-process span {
      display: inline-block;
    }
</style>
<body style="background-color: rgb(255, 255, 255); overflow: auto; cursor: default;">
<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <div class="subject">
                <h3>用户列表</h3>
                <h5>用户列表管理</h5>
            </div>
        </div>
    </div>
    <div id="explanation" class="explanation">
        <div class="bckopa-tips">
            <div class="title">
                <img src="/public/static/images/handd.png" alt="">
                <h4 title="提示相关设置操作时应注意的要点">操作提示</h4>
            </div>
            <ul>
                <li></li>
            </ul>
        </div>
        <span title="收起提示" id="explanationZoom"></span>
    </div>
    <div class="flexigrid" >
        <div class="mDiv">
            <div class="ftitle">
                <h3>用户列表</h3>
                <h5>(共<?php echo $pager->totalRows; ?>条记录)</h5>
            </div>
            <div class="fbutton">
                <a href="<?php echo url('User/user'); ?>"><div class="add" title="添加账号"><span><i class="fa fa-plus"></i>添加账号</span></div></a>
            </div>
            <!--<div class="fbutton">
                <div class="add" onclick="orderExport()"><sapn><i class="fa fa-download"></i>Excel导出</sapn></div>
            </div>-->
            <a href="" class="refresh-date"><div title="刷新数据" class="pReload"><i class="fa fa-refresh"></i></div></a>
            <form class="navbar-form form-inline" id="dyform" action="<?php echo url('User/userList'); ?>" method="post">
                <div class="sDiv">
                    <div class="sDiv2">
                        <input name="add_time" readonly class="qsbox" value="<?php echo $condition['add_time']; ?>" type="text" style="width: 160px;" placeholder="请选择日期" id="add_time">
                    </div>
                    <div class="sDiv2">
                        <input type="text" size="30" name="keywords" id="keywords" class="qsbox" placeholder="请输入用户昵称/手机号" style="width: 200px;" value="<?php echo $condition['keywords']; ?>">
                        <input type="submit" class="btn" value="搜索">
                    </div>
                </div>
            </form>
        </div>
        <div class="hDiv">
            <div class="hDivBox">
                <table cellspacing="0" cellpadding="0">
                    <thead>
                    <tr>
                        <th><div style="width: 100px;">用户id</div></th>
                        <th><div style="width: 100px;">昵称</div></th>
                        <th><div style="width: 130px;">手机号</div></th>
                        <th><div style="width: 150px;">状态</div></th>
                        <th><div style="width: 150px;">账号类型</div></th>
                        <th><div style="width: 120px;">注册时间</div></th>
                        <th><div style="width: 120px;">最后登录时间</div></th>
                        <th><div style="width: 150px;">操作</div></th>
                        <th style="width: 100%;"></th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
        <div style="height: auto;" class="bDiv" id="flexigrid"  data-url="<?php echo url('User/userList'); ?>" >
            <table cellspacing="0" cellpadding="0" id="article_cat_table" class="flex-table autoht">
                <tbody id="treet1">
                <?php if(!(empty($list) || (($list instanceof \think\Collection || $list instanceof \think\Paginator ) && $list->isEmpty()))): if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): if( count($list)==0 ) : echo "" ;else: foreach($list as $k=>$vo): ?>
                        <tr>
                            <td>
                                <div style="width: 100px;">
                                    <?php echo $vo['user_id']; ?>
                                </div>
                            </td>
                            <td>
                                <div style="width: 100px;">
                                    <?php echo $vo['nickname']; ?>
                                </div>
                            </td>
                            <td>
                                <div style="width: 130px;">
                                    <?php echo $vo['mobile']; ?>
                                </div>
                            </td>
                            <td>
                                <div style="width: 150px;">
                                    <?php if($vo["status"] == 1): ?>
                                        <span class="yes" onClick="changeTableVal('users','user_id','<?php echo $vo['user_id']; ?>','status',this,'正常','禁用')" ><i class="fa fa-check-circle"></i>正常</span>
                                        <?php else: ?>
                                        <span class="no" onClick="changeTableVal('users','user_id','<?php echo $vo['user_id']; ?>','status',this,'正常','禁用')" ><i class="fa fa-ban"></i>禁用</span>
                                    <?php endif; ?>
                                </div>
                            </td>
                            <td>
                                <div style="width: 150px;">
                                    <?php if($vo["is_platform"] == 1): ?>
                                        <span class="yes" onClick="changeTableVal('users','user_id','<?php echo $vo['user_id']; ?>','is_platform',this,'平台','普通')" ><i class="fa fa-check-circle"></i>平台</span>
                                        <?php else: ?>
                                        <span class="no" onClick="changeTableVal('users','user_id','<?php echo $vo['user_id']; ?>','is_platform',this,'平台','普通')" ><i class="fa fa-ban"></i>普通</span>
                                    <?php endif; ?>
                                </div>
                            </td>
                            <td>
                                <div style="width: 120px;">
                                    <?php echo $vo['add_time']; ?>
                                </div>
                            </td>
                            <td>
                                <div style="width: 120px;">
                                    <?php echo $vo['last_login']; ?>
                                </div>
                            </td>
                            <td>
                                <div style="width: 150px;">
                                    <a href="<?php echo url('User/userInfo',['user_id'=>$vo['user_id']]); ?>" class="btn blue">查看</a>
                                    <a href="javascript:;" data-url="<?php echo url('User/delUser'); ?>" data-id="<?php echo $vo['user_id']; ?>" onclick="delfunc(this,'user_id')" class="btn red">删除</a>
                                </div>
                            </td>
                            <td style="width: 100%;">
                            </td>
                        </tr>
                    <?php endforeach; endif; else: echo "" ;endif; else: ?>
                    <tr data-id="0">
                        <td class="no-data" colspan="70">
                            <i class="fa fa-exclamation-circle"></i>没有符合条件的记录
                        </td>
                    </tr>
                <?php endif; ?>
                </tbody>
            </table>
            <!--分页位置-->
            <?php echo $show; ?>
        </div>
    </div>
</div>
<div id="show" style="display: none;margin-top: 50px;text-align: center;width: 90%;">
    <div class="layui-progress layui-progress-big" lay-showpercent="true" lay-filter="demo" style="margin-left: 6%;margin-bottom: 10px;">
        <div class="layui-progress-bar layui-bg-blue" id="layui-process" lay-percent="0%"></div>
        <input type="hidden" id="process">
    </div>
    <span id="excel-title">正在导出，请耐心等待......</span>
    <input type="hidden" id="import_url" value="<?php echo url('Excels/getProcess'); ?>">
    <input type="hidden" id="import_leading_num" value="user_leading_num">
    <input type="hidden" id="import_all_num" value="user_all_num">
</div>
<script src="/public/js/layui/layui.js"></script>
<script type="text/javascript" src="/public/js/export.js"></script>
<script type="text/javascript">
    //excel 导出
    function orderExport()
    {
      var url1 = "<?php echo url('Excels/userExport'); ?>";//查询导出数据多少/数据集
      var url2 = "<?php echo url('Excels/getExcelUrl'); ?>";
      var url2_name = "admincp_user_zip_url";
      var url3 = "<?php echo url('Excels/getProcess'); ?>";
      var all_num = "admincp_user_all_num";
      var leading_num = "admincp_user_leading_num"; 
      exportdata(url1, url2, url2_name, url3, all_num, leading_num);
    }
    layui.use(['laydate'], function(){
        var laydate = layui.laydate
        laydate.render({
            elem: '#add_time',
            theme: 'molv',
            trigger: 'click',
            range: true,
            //type:'datetime',
            format: 'yyyy-MM-dd',
        });
    });
</script>
</body>
