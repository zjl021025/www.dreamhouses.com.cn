<?php /*a:3:{s:79:"F:\phpstudy_pro\WWW\www.dreamhouses.com.cn\app/admincp/dytpl/estate/estate.html";i:1699944756;s:79:"F:\phpstudy_pro\WWW\www.dreamhouses.com.cn\app/admincp/dytpl/public/layout.html";i:1698049681;s:75:"F:\phpstudy_pro\WWW\www.dreamhouses.com.cn\app/admincp/dytpl/public/js.html";i:1698049681;}*/ ?>
<!doctype html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <!-- Apple devices fullscreen -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <!-- Apple devices fullscreen -->
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <link href="/public/static/css/main.css" rel="stylesheet" type="text/css">
    <link href="/public/static/css/list_optimization.css" rel="stylesheet" type="text/css">
    <link href="/public/static/css/page.css" rel="stylesheet" type="text/css">
    <link href="/public/static/font/css/font-awesome.min.css" rel="stylesheet" />
    <link href="/public/static/css/union.css" rel="stylesheet" />
    <link href="/public/js/layui/css/layui.css" rel="stylesheet" />
    <!--[if IE 7]>
    <link rel="stylesheet" href="/public/static/font/css/font-awesome-ie7.min.css">
    <![endif]-->
    <link href="/public/static/js/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
    <link href="/public/static/js/perfect-scrollbar.min.css" rel="stylesheet" type="text/css"/>
    <style type="text/css">
    html, body { overflow: visible;}
    .fine_layer{
		width: 100%;
		padding: 10px;
		box-sizing: border-box;
    }
	.fine_layer *{
		box-sizing: border-box;
    }
	.fine_layer textarea{
		width: 100%;
        height: 80px;
		border: 1px solid #e6e6e6;
        border-radius: 4px;
        padding: 5px 4px;
        resize: none!important;
    }
    .red{
        color: red !important;
    }
    .hDiv th div{
        text-align: center;
    }
    .bDiv td div{
        text-align: center;
    }
    </style>
    <script type="text/javascript" src="/public/static/js/jquery.js"></script>
    <script type="text/javascript" src="/public/static/js/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="/public/static/js/layer/layer.js"></script><!-- 弹窗js 参考文档 http://layer.layui.com/-->
    <script src="/public/js/layui/layui.js"></script>
    <script type="text/javascript" src="/public/static/js/admin.js"></script>
    <script type="text/javascript" src="/public/static/js/jquery.validation.min.js"></script>
    <script type="text/javascript" src="/public/static/js/common.js"></script>
    <script type="text/javascript" src="/public/static/js/perfect-scrollbar.min.js"></script>
    <script type="text/javascript" src="/public/static/js/jquery.mousewheel.js"></script>
    <script src="/public/js/myFormValidate.js"></script>
    <script src="/public/js/myAjax2.js"></script>
    <script src="/public/js/dyglobal.js"></script>
    <script type="text/javascript">
        function in_array(needle, haystack) {
            if(typeof needle == 'string' || typeof needle == 'number') {
                for(var i in haystack) {
                    if(haystack[i] == needle) {
                        return true;
                    }
                }
            }
            return false;
        }
        function checkEmtpy(obj,title) {
            var url = $(obj).attr('href')
            if(!url){
                if(!title){
                    title = '没有图片';
                }
                layer.msg(title)
                return false;
            }
            return true;
        }
        function checkPrice(obj){
            obj.value = obj.value.replace(/[^\d.]/g,"");  //清除“数字”和“.”以外的字符
            obj.value = obj.value.replace(/^\./g,"");  //验证第一个字符是数字而不是.
            obj.value = obj.value.replace(/\.{2,}/g,"."); //只保留第一个. 清除多余的.
            obj.value = obj.value.replace(".","$#$").replace(/\./g,"").replace("$#$",".");
            obj.value = obj.value.replace(/^(\d+)\.(\d\d).*$/,'$1.$2');
        }
        function checkNumber(obj){
            obj.value = obj.value.replace(/[^\d]/g,"");  //清除“数字”以外的字符
        }
    function is_show(obj) {
        var content = $(obj).data('name');
        layer.tips(content, obj, {
            tips: [1, "#4794ec"]
        });
    }
    function selectAll(name,obj){
        $('input[name*='+name+']').prop('checked', $(obj).checked);
    }
    //个人认证/企业认证/车辆认证-通过
    function auth_agree(obj)
    {
        var url = "<?php echo url('User/checkAuth'); ?>";
        var id = $(obj).data('id');
        var type = $(obj).data('type');
        layer.confirm('确认审核通过吗？',{title:'温馨提示',closeBtn:0,icon:3,btn: ['确定','取消'] }, function() {
            $.ajax({
                type : 'post',
                url : url,
                data : {act:'agree',id:id,type:type},
                dataType : 'json',
                success : function(data) {
                    layer.closeAll();
                    if (data.errcode == 1) {
                        layer.msg(data.message, {icon: 1, time: 2000},function(){
                            location.href = data.data.url;
                        });
                    } else {
                        layer.msg(data.message, {icon: 2,time: 2000});
                    }
                }
            })
        }, function(index){
            layer.close(index);
            return false;// 取消
        });
    }
    //-拒绝
    function auth_refuse(obj)
    {
        var url = "<?php echo url('User/checkAuth'); ?>";
        var id = $(obj).data('id');
        var type = $(obj).data('type');
        var html = `<div class="fine_layer">
            <textarea id="remark" maxlength="100" placeholder="请填写拒绝原因(100字内)"></textarea>
        </div>`;
		layer.open({
			type:1,
			title: '审核拒绝',
            content: html,
			area: ['400px', '210px'],
            closeBtn:0,
			btn:['确定', '取消'],
			yes:function(index){
				var content = $("#remark").val();
				/* if (content == "") {
					layer.msg('请填写拒绝原因', {icon: 2,time:2000});
					return false;
				} */
				$.ajax({
					type: 'POST',
					url: url,
					data : {id:id,remark:content,act:'refuse',type:type},
					dataType: 'json',
					success: function(data){
                        if (data.errcode == 1) {
                            layer.msg(data.message, {icon: 1, time: 2000},function(){
                                location.href = data.data.url;
                            });
                        } else {
                            layer.msg(data.message, {icon: 2,time: 2000});
                        }
					},
					error: function(){
						layer.alert("服务器繁忙, 请联系管理员!");
					}
				});
			}
		});
    }

    function delAll() {
        var ids = '';
        $('#flexigrid .trSelected').each(function(i,o) {
            ids += $(o).data('id')+',';
        })
        if (ids == '' || ids == '0,' || ids == '0') {
            layer.confirm('请选择删除项', {title:'温馨提示',closeBtn:0,icon:3});
            return;
        }
        layer.confirm('确认删除？',{title:'温馨提示',closeBtn:0,icon:3,btn: ['确定','取消'] }, function() {
            $.ajax({
                type : 'post',
                url : $('#flexigrid').data('url'),
                data : {act:'del',ids:ids},
                dataType : 'json',
                success : function(data) {
                    layer.closeAll();
                    if (data.errcode == 1) {
                        layer.msg('操作成功', {icon: 1});
                        $('#flexigrid .trSelected').each(function(i,o) {
                            $(o).remove();
                        })
                    } else {
                        layer.msg(data.message, {icon: 2,time: 2000});
                    }
                }
            })
        }, function(index){
            layer.close(index);
            return false;// 取消
        });
    }

    /**
     * 全选
     * @param obj
     */
    function checkAllSign(obj){
        $(obj).toggleClass('trSelected');
        if($(obj).hasClass('trSelected')){
            $('#flexigrid > table>tbody >tr').addClass('trSelected');
        }else{
            $('#flexigrid > table>tbody >tr').removeClass('trSelected');
        }
    }
    //判断是否全选
	function renderCheckAll(){
        var goodsLen = $('#flexigrid tr').length;
        var checkedLen = $('#flexigrid tr.trSelected').length;
        if(goodsLen == checkedLen){
          $('.check-all').addClass('trSelected')
        }else{
          $('.check-all').removeClass('trSelected')
        }
	}
    /**
     * 批量公共操作（删，改）
     * @returns {boolean}
     */
    function publicHandleAll(type){
        var ids = '';
        $('#flexigrid .trSelected').each(function(i,o){
            ids += $(o).data('id')+',';
        });
        if(ids == ''){
            layer.msg('至少选择一项', {icon: 2, time: 2000});
            return false;
        }
        publicHandle(ids,type); //调用删除函数
    }
    /**
     * 批量公共操作（删）新
     * @returns {boolean}
     */
    function publicUpdateAll(name,field_name){
        var ids = '';
        $('#flexigrid .trSelected').each(function(i,o){
            if(ids ==''){
                ids = $(o).data('id');
            }else{
                ids += ','+$(o).data('id');
            }
        });
        if(ids == ''){
            layer.msg('至少选择一项', {icon: 2, time: 2000});
            return false;
        }
        publicUpdate(ids,name,field_name); //调用删除函数
    }
    /**
     * 公共操作（删，改）
     * @param type
     * @returns {boolean}
     */
    function publicHandle(ids,handle_type){
        layer.confirm('确认当前操作？', {
            title:'温馨提示',closeBtn:0,icon:3,
                    btn: ['确定', '取消'] //按钮
                }, function () {
                    // 确定
                    $.ajax({
                        url: $('#flexigrid').data('url'),
                        type:'post',
                        data:{ids:ids,act:handle_type},
                        dataType:'JSON',
                        success: function (data) {
                            layer.closeAll();
                            if (data.errcode == 1){
                                layer.msg(data.message, {icon: 1, time: 2000},function(){
                                    location.href = data.dyurl;
                                });
                            }else{
                                layer.msg(data.message, {icon: 2, time: 2000});
                            }
                        }
                    });
                }, function (index) {
                    layer.close(index);
                }
        );
    }

        /**
         * 公共操作（删，改）新
         * @param type
         * @returns {boolean}
         */
        function publicUpdate(ids,name,field_name){
            layer.confirm('确认当前操作？', {
                title:'温馨提示',closeBtn:0,icon:3,
                        btn: ['确定', '取消'] //按钮
                    }, function () {
                        // 确定
                        $.ajax({
                            url: '/Admincp/System/deleteData',
                            type:'post',
                            data:{ids:ids,name:name,field_name:field_name},
                            dataType:'JSON',
                            success: function (data) {
                                layer.closeAll();
                                if (data.status == 1){
                                    layer.msg(data.msg, {icon: 1, time: 2000},function(){
                                        window.location.reload();
                                    });
                                }else{
                                    layer.msg(data.msg, {icon: 2, time: 2000});
                                }
                            }
                        });
                    }, function (index) {
                        layer.close(index);
                    }
            );
        }

        function delfuntion(obj) {
            // 删除按钮
            layer.confirm('确认刪除？', {
                title:'温馨提示',closeBtn:0,icon:3,
                btn: ['确定', '取消'] //按钮
            }, function () {
                $.ajax({
                    type: 'post',
                    url: '/Admincp/System/deleteData',
                    data : {ids:$(obj).attr('data-id'),name:$(obj).attr('data-name'),field_name:$(obj).attr('data-field_name')},
                    dataType: 'json',
                    success: function (data) {
                        layer.closeAll();
                        if (data.status == 1) {
                            $(obj).parent().parent().parent().remove();
                            layer.closeAll();
                        } else {
                            layer.alert(data, {icon: 2});  //alert('删除失败');
                        }
                    }
                })
            }, function () {
                layer.closeAll();
            });
        }
        function delfunc(obj,field='id') {
            var url = $(obj).data('url');
            // 删除按钮
            layer.confirm('确认刪除？', {
                title:'温馨提示',closeBtn:0,icon:3,
                btn: ['确定', '取消'] //按钮
            }, function () {
                var data = {}
                data[field] = $(obj).data('id')
                data['act'] = 'del'
                $.ajax({
                    type: 'post',
                    url: url,
                    data : data,
                    dataType: 'json',
                    success: function (data) {
                        layer.closeAll();
                        if (data.errcode == 1) {
                            layer.msg(data.message,{icon:1,time:2000},function(){
                                window.location.reload();
                            });
                        } else {
                            layer.msg(data.message, {icon: 2});  //alert('删除失败');
                        }
                    }
                })
            }, function () {
                layer.closeAll();
            });
        }
    </script>
</head>
<style>
    .fa-check-circle,.fa-ban{cursor:pointer}
</style>
<style type="text/css">
  .dysubmit{
      background-color: #148eff;
      border-color: #148eff;display: 
      inline-block;
      text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.1);
      font: bold 14px/20px "microsoft yahei", arial;
      color:#FFFFFF;
      text-align: center;
      vertical-align: middle;
      display: inline-block;
      height: 36px;
      padding: 7px 19px;
      border-radius: 3px;
      cursor: pointer; 
    }
  .addressformError{margin-left: 84px;}
  .typelable{padding-right: 15px;}
</style>  
<body style="background-color: #FFF; overflow: auto;">
<div id="toolTipLayer" style="position: absolute; z-index: 9999; display: none; visibility: visible; left: 95px; top: 573px;"></div>
<div id="append_parent"></div>
<div id="ajaxwaitid"></div>
<div class="page">
  <div class="fixed-bar">
    <div class="item-title"><a class="back" href="javascript:history.back();" title="返回列表"><i class="fa fa-arrow-circle-o-left"></i></a>
      <div class="subject">
        <h3>楼盘管理 - <?php if($act == 'add'): ?>新增<?php else: ?>编辑<?php endif; ?>楼盘</h3>
        <h5>楼盘索引与管理</h5>
      </div>
    </div>
  </div>
  <form class="form-horizontal" id="ajaxform">
    <input type="hidden" name="estate_id" value="<?php echo $info['estate_id']; ?>">
    <input type="hidden" name="act" value="<?php echo $act; ?>">
    <div class="ncap-form-default tab_div_1">
      <dl class="row"><h3 class="updt-protit">基本信息</h3></dl>
      <dl class="row">
        <dt class="tit"><em>*</em>
          <label>所在区域</label>
        </dt>
        <dd class="opt">
          <div class="opt" style="float: left;">
            <select id="province_id" class="select validate[required]" name="province_id" style="width: 120px !important;height: 26px;">
              <option value="">选择省份</option>
              <?php if(is_array($province) || $province instanceof \think\Collection || $province instanceof \think\Paginator): if( count($province)==0 ) : echo "" ;else: foreach($province as $k=>$p): ?>
                <option value="<?php echo $p['id']; ?>" <?php if($info['province_id'] == $p['id']): ?>selected<?php endif; ?>><?php echo $p['name']; ?></option>
              <?php endforeach; endif; else: echo "" ;endif; ?>
            </select>
            <select id="city_id" class="select shiinfo validate[required]" name="city_id" style="width: 120px !important;margin-left: 15px;height: 26px;<?php if($info['city_id'] == 0): ?>display: none!important;<?php endif; ?>">
              <option value="">选择城市</option>
              <?php if(is_array($city) || $city instanceof \think\Collection || $city instanceof \think\Paginator): if( count($city)==0 ) : echo "" ;else: foreach($city as $k=>$c): ?>
                <option value="<?php echo $c['id']; ?>" <?php if($info['city_id'] == $c['id']): ?>selected<?php endif; ?> ><?php echo $c['name']; ?></option>
              <?php endforeach; endif; else: echo "" ;endif; ?>
            </select>
            <select id="district_id" class="select shiinfo validate[required]" name="district_id" style="width: 120px !important;margin-left: 15px;height: 26px;<?php if($info['district_id'] == 0): ?>display: none!important;<?php endif; ?>">
              <option value="">选择区域</option>
              <?php if(is_array($district) || $district instanceof \think\Collection || $district instanceof \think\Paginator): if( count($district)==0 ) : echo "" ;else: foreach($district as $k=>$d): ?>
                <option value="<?php echo $d['id']; ?>" <?php if($info['district_id'] == $d['id']): ?>selected<?php endif; ?> ><?php echo $d['name']; ?></option>
              <?php endforeach; endif; else: echo "" ;endif; ?>
            </select>
          </div>
          <p class="notic"><span class="red"></span></p>
        </dd>
      </dl>
      <dl class="row">
        <dt class="tit">
          <label><em>*</em>详细地址</label>
        </dt>
        <dd class="opt">
          <input type="text" id="address" value="<?php echo $info['address']; ?>" name="address" class="input-txt validate[required]" placeholder="请获取详细地址" readonly>
          <input id="map_lng_lat" value="地址获取" class="layui-btn layui-btn-normal layui-btn-sm" style="width:80px;height:26px;line-height:26px">
          <span class="err" id="err_map"></span>
          <p class="notic"></p>
        </dd>
      </dl>
      <dl class="row">
        <dt class="tit">
          <label><em>*</em>楼盘名称：</label>
        </dt>
        <dd class="opt">
          <input type="text" class="input-txt validate[required]" id="name" name="name" value="<?php echo $info['name']; ?>" />
          <span class="err" id="err_title"></span>
          <p class="notic"></p>
        </dd>
      </dl>
      <dl class="row">
        <dt class="tit">
          <label><em>*</em>户型</label>
        </dt>
        <dd class="opt house_type">
          <?php if(is_array($house_type) || $house_type instanceof \think\Collection || $house_type instanceof \think\Paginator): if( count($house_type)==0 ) : echo "" ;else: foreach($house_type as $key=>$vo): ?>
            <label class="typelable"><input type="checkbox" name="house_type[]" data-name="<?php echo $vo['name']; ?>" value="<?php echo $vo['id']; ?>" <?php if(in_array(($vo['id']), is_array($info['house_type_ids'])?$info['house_type_ids']:explode(',',$info['house_type_ids']))): ?>checked<?php endif; ?> /> <?php echo $vo['name']; ?></label>
          <?php endforeach; endif; else: echo "" ;endif; ?>
          <span class="err" id="err_house_type"></span>
          <p class="notic"></p>
        </dd>
      </dl>
      <dl class="row">
        <dt class="tit">
          <label><em>*</em>面积</label>
        </dt>
        <dd class="opt area">
          <div class="hDiv">
            <div class="hDivBox">
              <table cellspacing="0" cellpadding="0">
                <thead>
                <tr >
                  <th>
                    <div style="width: 80px;">户型</div>
                  </th>
                  <th>
                    <div style="width: 200px;">面积</div>
                  </th>
                  <th style="width:100%" axis="col7">
                    <div></div>
                  </th>
                </tr>
                </thead>
              </table>
            </div>
          </div>
          <div class="bDiv" style="height: auto;">
            <div id="flexigrid" cellpadding="0" cellspacing="0">
              <table>
                <tbody id="area_tab">

                </tbody>
              </table>
            </div>
          </div>
          <span class="err" id="err_area"></span>
          <p class="notic"></p>
        </dd>
      </dl>
      <dl class="row">
        <dt class="tit">
          <label><em>*</em>显示：</label>
        </dt>
        <dd class="opt">
          <div class="onoff">
            <label for="article_show1" class="cb-enable <?php if($info['is_show'] == 1): ?>selected<?php endif; ?>">是</label>
            <label for="article_show0" class="cb-disable <?php if($info['is_show'] == 0): ?>selected<?php endif; ?>">否</label>
            <input id="article_show1" name="is_show" value="1" type="radio" <?php if($info['is_show'] == 1): ?> checked="checked"<?php endif; ?>>
            <input id="article_show0" name="is_show" value="0" type="radio" <?php if($info['is_show'] == 0): ?> checked="checked"<?php endif; ?>>
          </div>
          <p class="notic"></p>
        </dd>
      </dl>
    </div>
    <div class="ncap-form-default">
      <div class="bot">
          <input type="submit" id="submit" class="dysubmit" value="确认提交">
      </div>
    </div>
  </form>
</div>
<div id="goTop">
  <a href="JavaScript:void(0);" id="btntop"><i class="fa fa-angle-up"></i></a>
  <a href="JavaScript:void(0);" id="btnbottom"><i class="fa fa-angle-down"></i></a>
</div>
<link href="/public/static/js/validation/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/public/static/js/validation/jquery.validationEngine.js"></script>
<script type="text/javascript" src="/public/static/js/validation/jquery.validationEngine-zh_CN.js"></script>
<script src="/public/js/sortable.js"></script>
<script type="text/javascript">
  //户型点击生成面积表
  $('.house_type input').click(function () {
    get_area_list();
  })
  $(function () {
    area = '<?php echo $info['area']; ?>';
    get_area_list();
    var province_id = "<?php echo $info['province_id']; ?>"
    var city_id = "<?php echo $info['city_id']; ?>"
    var district_id = "<?php echo $info['district_id']; ?>"
    if(province_id){
      get_city(province_id,city_id);
    }
    if(city_id){
      get_district(city_id,district_id);
    }
  })
  //面积表格
  function get_area_list() {
    var str = '';
    var area_value = '';
    if(area){
      area_value = JSON.parse(area);
    }
    $('.house_type input:checked').each(function () {
      var name = $(this).data('name');
      var id = $(this).val();
      var value='';
      if(area_value && area_value[id]){
        value= area_value[id];
      }
      // oninput="checkNumber(this)"
      str += '<tr>\n' +
              '   <td>\n' +
              '     <div style="width: 80px;">'+name+'</div>\n' +
              '   </td>\n' +
              '   <td>\n' +
              '     <div style="width: 200px;"><input type="text" name="area['+id+']"  value="'+value+'">㎡</div>\n' +
              '   </td>\n' +
              '   <td style="width: 100%;">\n' +
              '     <div>&nbsp;</div>\n' +
              '   </td>\n' +
              ' </tr>';
    })
    $('#area_tab').html(str);
  }
  //获取市
  $("#province_id").change(function() {
    var province_id = $(this).val()
    if (!province_id) {
      return ;
    }
    get_city(province_id)
  });

  function get_city(province_id,city_id=''){
    $('#district_id').hide();
    var url = "<?php echo url('Api/getregion'); ?>";
    $.ajax({
      type : "GET",
      url  : url,
      data:{id:province_id,type_id:0},
      dataType:'json',
      success: function(v) {
        if(v.errcode == 1){
          var html =' <option value="">选择城市</option>';
          if(v.data){
            $.each(v.data,function (i,v) {
              if(city_id && v.id==city_id){
                html += '<option value="'+v.id+'" selected>'+v.name+'</option>';
              }else {
                html += '<option value="'+v.id+'">'+v.name+'</option>';
              }
            });
          }
          $('#city_id').html(html);
          $('#city_id').show();
        }
      },error: function(request) {
        alert("服务器繁忙, 请联系管理员!");
      },
    });
  }
  //获取市
  $("#city_id").change(function() {
    var city_id = $(this).val()
    if (!city_id) {
      return ;
    }
    get_district(city_id)
  });
  function get_district(city_id,district_id=''){
    $('#district_id').hide();
    var url = "<?php echo url('Api/getregion'); ?>";
    $.ajax({
      type : "GET",
      url  : url,
      data:{id:city_id,type_id:0},
      dataType:'json',
      success: function(v) {
        if(v.errcode == 1){
          var html =' <option value="">选择区域</option>';
          if(v.data){
            $.each(v.data,function (i,v) {
              if(district_id && district_id==v.id){
                html += '<option value="'+v.id+'" selected>'+v.name+'</option>';
              }else {
                html += '<option value="'+v.id+'">'+v.name+'</option>';
              }
            });
          }
          $('#district_id').html(html);
          $('#district_id').show();
        }
      },error: function(request) {
        alert("服务器繁忙, 请联系管理员!");
      },
    });
  }
  $("#map_lng_lat").click(function(){
    var city_id = $('#city_id').val();
    if(!city_id){
      layer.msg('请先选择省市区');
      return false;
    }
    var url = "<?php echo url('Index/maplnglat'); ?>?city_id="+city_id;
    layer.open({
      type: 2,
      title: '选择经纬度(鼠标左击确认)',
      shadeClose: true,
      shade: 0.2,
      area: ['1020px', '85%'],
      content: url
    });
  });
  function map_call_back(name,address,adcode){
    $("#name").val(name);
    $("#address").val(address);
    $("#district_id").val(adcode);
    layer.closeAll('iframe');
  }
  function showloading(t) {
    if (t == true) {
      var loading = layer.msg('数据提交中...', {
        icon: 16,offset: '300px',shade: 0.2
      });
    } else {
      layer.closeAll(loading);
    }
  }

  $(function(){
    $('#ajaxform').validationEngine('attach', {
      promptPosition : "centerRight",
      autoPositionUpdate : true,
      showOneMessage: true,
      'custom_error_messages': {
        '#province_id': {
          'required': {
            'message': '* 请选择省份！'
          }
        },
        '#city_id': {
          'required': {
            'message': '* 请选择城市！'
          }
        },
        '#district_id': {
          'required': {
            'message': '* 请选择地区！'
          }
        },
        '#address': {
          'required': {
            'message': '* 请在地图上选择位置！'
          }
        },
        '#name': {
          'required': {
            'message': '* 请输入楼盘名称！'
          }
        },
        '#intro': {
          'required': {
            'message': '* 请输入简介！'
          }
        }
      },
      onValidationComplete: function(form, status){
        //表单校验成功并提交
        if(status == true){
          $('span.err').hide();
          $('#submit').attr('disabled', true);
          $.ajax({
            type: "POST",
            url: "<?php echo url('Estate/estateHandle'); ?>",
            data: $("#ajaxform").serialize(),
            async:false,
            dataType: "json",
            error: function () {
              showloading(false);
              layer.alert("服务器繁忙, 请联系管理员!",{title:'温馨提示',closeBtn:0,icon:2});
            },
            beforeSend:function() {
              showloading(true);
            },
            success: function (data) {
              showloading(false);
              if (data.errcode == 1) {
                layer.msg(data.message, {icon: 1,time: 1000}, function() {
                  location.href = "<?php echo url('Estate/estateList'); ?>";
                });
              } else {
                $('#submit').attr('disabled',false);
                $('span.err').text('').show();
                if (data.data) {
                  $.each(data.data, function(index, item) {
                    $('#err_' + index).text(item).show();
                  });
                }
                layer.msg(data.message, {icon: 2,time: 1000});
              }
            }
          })
        }
      }
    });
  })
</script>
</body>
</html>