<?php /*a:2:{s:86:"F:\phpstudy_pro\WWW\www.dreamhouses.com.cn\app/admincp/dytpl/dycase/layoutManager.html";i:1698049681;s:79:"F:\phpstudy_pro\WWW\www.dreamhouses.com.cn\app/admincp/dytpl/public/layout.html";i:1698049681;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link href="/public/static/css/perfect-scrollbar.min.css" rel="stylesheet" type="text/css">
    <link href="/public/static/js/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="/public/static/js/jquery.js"></script>
    <script type="text/javascript" src="/public/static/js/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="/public/static/js/jquery.cookie.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/echarts@5.4.3/dist/echarts.min.js"></script>
    <link rel="stylesheet" href="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<!doctype html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <!-- Apple devices fullscreen -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <!-- Apple devices fullscreen -->
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <link href="/public/static/css/main.css" rel="stylesheet" type="text/css">
    <link href="/public/static/css/list_optimization.css" rel="stylesheet" type="text/css">
    <link href="/public/static/css/page.css" rel="stylesheet" type="text/css">
    <link href="/public/static/font/css/font-awesome.min.css" rel="stylesheet" />
    <link href="/public/static/css/union.css" rel="stylesheet" />
    <link href="/public/js/layui/css/layui.css" rel="stylesheet" />
    <!--[if IE 7]>
    <link rel="stylesheet" href="/public/static/font/css/font-awesome-ie7.min.css">
    <![endif]-->
    <link href="/public/static/js/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
    <link href="/public/static/js/perfect-scrollbar.min.css" rel="stylesheet" type="text/css"/>
    <style type="text/css">
    html, body { overflow: visible;}
    .fine_layer{
		width: 100%;
		padding: 10px;
		box-sizing: border-box;
    }
	.fine_layer *{
		box-sizing: border-box;
    }
	.fine_layer textarea{
		width: 100%;
        height: 80px;
		border: 1px solid #e6e6e6;
        border-radius: 4px;
        padding: 5px 4px;
        resize: none!important;
    }
    .red{
        color: red !important;
    }
    .hDiv th div{
        text-align: center;
    }
    .bDiv td div{
        text-align: center;
    }
    </style>
    <script type="text/javascript" src="/public/static/js/jquery.js"></script>
    <script type="text/javascript" src="/public/static/js/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="/public/static/js/layer/layer.js"></script><!-- 弹窗js 参考文档 http://layer.layui.com/-->
    <script src="/public/js/layui/layui.js"></script>
    <script type="text/javascript" src="/public/static/js/admin.js"></script>
    <script type="text/javascript" src="/public/static/js/jquery.validation.min.js"></script>
    <script type="text/javascript" src="/public/static/js/common.js"></script>
    <script type="text/javascript" src="/public/static/js/perfect-scrollbar.min.js"></script>
    <script type="text/javascript" src="/public/static/js/jquery.mousewheel.js"></script>
    <script src="/public/js/myFormValidate.js"></script>
    <script src="/public/js/myAjax2.js"></script>
    <script src="/public/js/dyglobal.js"></script>
    <script type="text/javascript">
        function in_array(needle, haystack) {
            if(typeof needle == 'string' || typeof needle == 'number') {
                for(var i in haystack) {
                    if(haystack[i] == needle) {
                        return true;
                    }
                }
            }
            return false;
        }
        function checkEmtpy(obj,title) {
            var url = $(obj).attr('href')
            if(!url){
                if(!title){
                    title = '没有图片';
                }
                layer.msg(title)
                return false;
            }
            return true;
        }
        function checkPrice(obj){
            obj.value = obj.value.replace(/[^\d.]/g,"");  //清除“数字”和“.”以外的字符
            obj.value = obj.value.replace(/^\./g,"");  //验证第一个字符是数字而不是.
            obj.value = obj.value.replace(/\.{2,}/g,"."); //只保留第一个. 清除多余的.
            obj.value = obj.value.replace(".","$#$").replace(/\./g,"").replace("$#$",".");
            obj.value = obj.value.replace(/^(\d+)\.(\d\d).*$/,'$1.$2');
        }
        function checkNumber(obj){
            obj.value = obj.value.replace(/[^\d]/g,"");  //清除“数字”以外的字符
        }
    function is_show(obj) {
        var content = $(obj).data('name');
        layer.tips(content, obj, {
            tips: [1, "#4794ec"]
        });
    }
    function selectAll(name,obj){
        $('input[name*='+name+']').prop('checked', $(obj).checked);
    }
    //个人认证/企业认证/车辆认证-通过
    function auth_agree(obj)
    {
        var url = "<?php echo url('User/checkAuth'); ?>";
        var id = $(obj).data('id');
        var type = $(obj).data('type');
        layer.confirm('确认审核通过吗？',{title:'温馨提示',closeBtn:0,icon:3,btn: ['确定','取消'] }, function() {
            $.ajax({
                type : 'post',
                url : url,
                data : {act:'agree',id:id,type:type},
                dataType : 'json',
                success : function(data) {
                    layer.closeAll();
                    if (data.errcode == 1) {
                        layer.msg(data.message, {icon: 1, time: 2000},function(){
                            location.href = data.data.url;
                        });
                    } else {
                        layer.msg(data.message, {icon: 2,time: 2000});
                    }
                }
            })
        }, function(index){
            layer.close(index);
            return false;// 取消
        });
    }
    //-拒绝
    function auth_refuse(obj)
    {
        var url = "<?php echo url('User/checkAuth'); ?>";
        var id = $(obj).data('id');
        var type = $(obj).data('type');
        var html = `<div class="fine_layer">
            <textarea id="remark" maxlength="100" placeholder="请填写拒绝原因(100字内)"></textarea>
        </div>`;
		layer.open({
			type:1,
			title: '审核拒绝',
            content: html,
			area: ['400px', '210px'],
            closeBtn:0,
			btn:['确定', '取消'],
			yes:function(index){
				var content = $("#remark").val();
				/* if (content == "") {
					layer.msg('请填写拒绝原因', {icon: 2,time:2000});
					return false;
				} */
				$.ajax({
					type: 'POST',
					url: url,
					data : {id:id,remark:content,act:'refuse',type:type},
					dataType: 'json',
					success: function(data){
                        if (data.errcode == 1) {
                            layer.msg(data.message, {icon: 1, time: 2000},function(){
                                location.href = data.data.url;
                            });
                        } else {
                            layer.msg(data.message, {icon: 2,time: 2000});
                        }
					},
					error: function(){
						layer.alert("服务器繁忙, 请联系管理员!");
					}
				});
			}
		});
    }

    function delAll() {
        var ids = '';
        $('#flexigrid .trSelected').each(function(i,o) {
            ids += $(o).data('id')+',';
        })
        if (ids == '' || ids == '0,' || ids == '0') {
            layer.confirm('请选择删除项', {title:'温馨提示',closeBtn:0,icon:3});
            return;
        }
        layer.confirm('确认删除？',{title:'温馨提示',closeBtn:0,icon:3,btn: ['确定','取消'] }, function() {
            $.ajax({
                type : 'post',
                url : $('#flexigrid').data('url'),
                data : {act:'del',ids:ids},
                dataType : 'json',
                success : function(data) {
                    layer.closeAll();
                    if (data.errcode == 1) {
                        layer.msg('操作成功', {icon: 1});
                        $('#flexigrid .trSelected').each(function(i,o) {
                            $(o).remove();
                        })
                    } else {
                        layer.msg(data.message, {icon: 2,time: 2000});
                    }
                }
            })
        }, function(index){
            layer.close(index);
            return false;// 取消
        });
    }

    /**
     * 全选
     * @param obj
     */
    function checkAllSign(obj){
        $(obj).toggleClass('trSelected');
        if($(obj).hasClass('trSelected')){
            $('#flexigrid > table>tbody >tr').addClass('trSelected');
        }else{
            $('#flexigrid > table>tbody >tr').removeClass('trSelected');
        }
    }
    //判断是否全选
	function renderCheckAll(){
        var goodsLen = $('#flexigrid tr').length;
        var checkedLen = $('#flexigrid tr.trSelected').length;
        if(goodsLen == checkedLen){
          $('.check-all').addClass('trSelected')
        }else{
          $('.check-all').removeClass('trSelected')
        }
	}
    /**
     * 批量公共操作（删，改）
     * @returns {boolean}
     */
    function publicHandleAll(type){
        var ids = '';
        $('#flexigrid .trSelected').each(function(i,o){
            ids += $(o).data('id')+',';
        });
        if(ids == ''){
            layer.msg('至少选择一项', {icon: 2, time: 2000});
            return false;
        }
        publicHandle(ids,type); //调用删除函数
    }
    /**
     * 批量公共操作（删）新
     * @returns {boolean}
     */
    function publicUpdateAll(name,field_name){
        var ids = '';
        $('#flexigrid .trSelected').each(function(i,o){
            if(ids ==''){
                ids = $(o).data('id');
            }else{
                ids += ','+$(o).data('id');
            }
        });
        if(ids == ''){
            layer.msg('至少选择一项', {icon: 2, time: 2000});
            return false;
        }
        publicUpdate(ids,name,field_name); //调用删除函数
    }
    /**
     * 公共操作（删，改）
     * @param type
     * @returns {boolean}
     */
    function publicHandle(ids,handle_type){
        layer.confirm('确认当前操作？', {
            title:'温馨提示',closeBtn:0,icon:3,
                    btn: ['确定', '取消'] //按钮
                }, function () {
                    // 确定
                    $.ajax({
                        url: $('#flexigrid').data('url'),
                        type:'post',
                        data:{ids:ids,act:handle_type},
                        dataType:'JSON',
                        success: function (data) {
                            layer.closeAll();
                            if (data.errcode == 1){
                                layer.msg(data.message, {icon: 1, time: 2000},function(){
                                    location.href = data.dyurl;
                                });
                            }else{
                                layer.msg(data.message, {icon: 2, time: 2000});
                            }
                        }
                    });
                }, function (index) {
                    layer.close(index);
                }
        );
    }

        /**
         * 公共操作（删，改）新
         * @param type
         * @returns {boolean}
         */
        function publicUpdate(ids,name,field_name){
            layer.confirm('确认当前操作？', {
                title:'温馨提示',closeBtn:0,icon:3,
                        btn: ['确定', '取消'] //按钮
                    }, function () {
                        // 确定
                        $.ajax({
                            url: '/Admincp/System/deleteData',
                            type:'post',
                            data:{ids:ids,name:name,field_name:field_name},
                            dataType:'JSON',
                            success: function (data) {
                                layer.closeAll();
                                if (data.status == 1){
                                    layer.msg(data.msg, {icon: 1, time: 2000},function(){
                                        window.location.reload();
                                    });
                                }else{
                                    layer.msg(data.msg, {icon: 2, time: 2000});
                                }
                            }
                        });
                    }, function (index) {
                        layer.close(index);
                    }
            );
        }

        function delfuntion(obj) {
            // 删除按钮
            layer.confirm('确认刪除？', {
                title:'温馨提示',closeBtn:0,icon:3,
                btn: ['确定', '取消'] //按钮
            }, function () {
                $.ajax({
                    type: 'post',
                    url: '/Admincp/System/deleteData',
                    data : {ids:$(obj).attr('data-id'),name:$(obj).attr('data-name'),field_name:$(obj).attr('data-field_name')},
                    dataType: 'json',
                    success: function (data) {
                        layer.closeAll();
                        if (data.status == 1) {
                            $(obj).parent().parent().parent().remove();
                            layer.closeAll();
                        } else {
                            layer.alert(data, {icon: 2});  //alert('删除失败');
                        }
                    }
                })
            }, function () {
                layer.closeAll();
            });
        }
        function delfunc(obj,field='id') {
            var url = $(obj).data('url');
            // 删除按钮
            layer.confirm('确认刪除？', {
                title:'温馨提示',closeBtn:0,icon:3,
                btn: ['确定', '取消'] //按钮
            }, function () {
                var data = {}
                data[field] = $(obj).data('id')
                data['act'] = 'del'
                $.ajax({
                    type: 'post',
                    url: url,
                    data : data,
                    dataType: 'json',
                    success: function (data) {
                        layer.closeAll();
                        if (data.errcode == 1) {
                            layer.msg(data.message,{icon:1,time:2000},function(){
                                window.location.reload();
                            });
                        } else {
                            layer.msg(data.message, {icon: 2});  //alert('删除失败');
                        }
                    }
                })
            }, function () {
                layer.closeAll();
            });
        }
    </script>
</head>
<style>
    .fa-check-circle,.fa-ban{cursor:pointer}
</style>
<style>
    .flexigrid .sDiv2{
        border: none;
    }
    .flexigrid .sDiv2 .select{
        border: 1px solid #D7D7D7;
        border-radius: 4px;
        margin-right: 10px;
    }
    .flexigrid .sDiv2 .qsbox{
        border: 1px solid #D7D7D7;
        border-radius: 4px;
    }
</style>
<body style="background-color: rgb(255, 255, 255); overflow: auto; cursor: default; -moz-user-select: inherit;">
<div id="append_parent"></div>
<div id="ajaxwaitid"></div>
<div class="page" style="width: 100%;">
    <div class="fixed-bar" style="width: 100%;">
        <div class="item-title" style="width: 100%;">
            <div class="subject" style="width: 100%;background-color: rgb(243,243,243);height: 50px;">
                <div style="width: 10px;height: 30px;background-color: rgb(49,195,166);float:left;margin-top: 10px;margin-left: 20px"></div>
                <h3 style="margin-top: 17px;margin-left: 5px">布局管理</h3>
                <a href=""><button class="btn btn-default" type="submit" style="float: right;margin-right: 80px;margin-top: 8px">刷新</button></a>
            </div>
        </div>
    </div>
    <a href="<?php echo url('Dycase/layoutManagerInsert'); ?>"><button type="button" class="btn btn-default" style="float:left;margin-top: 20px">添加</button></a>
    <button type="button" class="btn btn-default" style="float:left;margin-top: 20px;margin-left: 20px">全部显示</button>
    <button type="button" class="btn btn-default" style="float:left;margin-top: 20px;margin-left: 20px">全部隐藏</button>
    <button type="button" class="btn btn-default" style="float:left;margin-top: 20px;margin-left: 20px">全部删除</button>
    <!--数据列表-->
    <table class="table table-bordered">
        <caption>数据列表</caption>
        <thead>
        <tr>
            <th>
                <input type="checkbox">
            </th>
            <th>名称</th>
            <th>城市</th>
            <th>邮编</th>
            <th>操作</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>
                <input type="checkbox">
            </td>
            <td>Tanmay</td>
            <td>Bangalore</td>
            <td>560001</td>
            <td>
                <a style="color:red;" href="<?php echo url('Dycase/layoutManagerUpdate'); ?>">编辑</a>
                <a style="color:red;">删除</a>
            </td>
        </tr>
        <tr>
            <td>
                <input type="checkbox">
            </td>
            <td>Sachin</td>
            <td>Mumbai</td>
            <td>400003</td>
            <td>
                <a style="color:red;" href="<?php echo url('Dycase/layoutManagerUpdate'); ?>">编辑</a>
                <a style="color:red;">删除</a>
            </td>
        </tr>
        <tr>
            <td>
                <input type="checkbox">
            </td>
            <td>Uma</td>
            <td>Pune</td>
            <td>411027</td>
            <td>
                <a style="color:red;" href="<?php echo url('Dycase/layoutManagerUpdate'); ?>">编辑</a>
                <a style="color:red;">删除</a>
            </td>
        </tr>

        </tbody>
    </table>
    <div style="width: 100%;">
        <ul class="pagination" style="float:right;">
            <li><a href="#">&laquo;</a></li>
            <li class="active"><a href="#">1</a></li>
            <li class="disabled"><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">5</a></li>
            <li><a href="#">&raquo;</a></li>
        </ul>
    </div>
</div>
<script>

</script>
</body>
</html>