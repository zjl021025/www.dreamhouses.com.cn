<?php /*a:3:{s:78:"F:\phpstudy_pro\WWW\www.dreamhouses.com.cn\app/admincp/dytpl/system/about.html";i:1690340037;s:79:"F:\phpstudy_pro\WWW\www.dreamhouses.com.cn\app/admincp/dytpl/public/layout.html";i:1690340037;s:81:"F:\phpstudy_pro\WWW\www.dreamhouses.com.cn\app/admincp/dytpl/public/dyupload.html";i:1690340037;}*/ ?>
<!doctype html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <!-- Apple devices fullscreen -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <!-- Apple devices fullscreen -->
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <link href="/public/static/css/main.css" rel="stylesheet" type="text/css">
    <link href="/public/static/css/list_optimization.css" rel="stylesheet" type="text/css">
    <link href="/public/static/css/page.css" rel="stylesheet" type="text/css">
    <link href="/public/static/font/css/font-awesome.min.css" rel="stylesheet" />
    <link href="/public/static/css/union.css" rel="stylesheet" />
    <link href="/public/js/layui/css/layui.css" rel="stylesheet" />
    <!--[if IE 7]>
    <link rel="stylesheet" href="/public/static/font/css/font-awesome-ie7.min.css">
    <![endif]-->
    <link href="/public/static/js/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
    <link href="/public/static/js/perfect-scrollbar.min.css" rel="stylesheet" type="text/css"/>
    <style type="text/css">
    html, body { overflow: visible;}
    .fine_layer{
		width: 100%;
		padding: 10px;
		box-sizing: border-box;
    }
	.fine_layer *{
		box-sizing: border-box;
    }
	.fine_layer textarea{
		width: 100%;
        height: 80px;
		border: 1px solid #e6e6e6;
        border-radius: 4px;
        padding: 5px 4px;
        resize: none!important;
    }
    .red{
        color: red !important;
    }
    .hDiv th div{
        text-align: center;
    }
    .bDiv td div{
        text-align: center;
    }
    </style>
    <script type="text/javascript" src="/public/static/js/jquery.js"></script>
    <script type="text/javascript" src="/public/static/js/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="/public/static/js/layer/layer.js"></script><!-- 弹窗js 参考文档 http://layer.layui.com/-->
    <script src="/public/js/layui/layui.js"></script>
    <script type="text/javascript" src="/public/static/js/admin.js"></script>
    <script type="text/javascript" src="/public/static/js/jquery.validation.min.js"></script>
    <script type="text/javascript" src="/public/static/js/common.js"></script>
    <script type="text/javascript" src="/public/static/js/perfect-scrollbar.min.js"></script>
    <script type="text/javascript" src="/public/static/js/jquery.mousewheel.js"></script>
    <script src="/public/js/myFormValidate.js"></script>
    <script src="/public/js/myAjax2.js"></script>
    <script src="/public/js/dyglobal.js"></script>
    <script type="text/javascript">
        function in_array(needle, haystack) {
            if(typeof needle == 'string' || typeof needle == 'number') {
                for(var i in haystack) {
                    if(haystack[i] == needle) {
                        return true;
                    }
                }
            }
            return false;
        }
        function checkEmtpy(obj,title) {
            var url = $(obj).attr('href')
            if(!url){
                if(!title){
                    title = '没有图片';
                }
                layer.msg(title)
                return false;
            }
            return true;
        }
        function checkPrice(obj){
            obj.value = obj.value.replace(/[^\d.]/g,"");  //清除“数字”和“.”以外的字符
            obj.value = obj.value.replace(/^\./g,"");  //验证第一个字符是数字而不是.
            obj.value = obj.value.replace(/\.{2,}/g,"."); //只保留第一个. 清除多余的.
            obj.value = obj.value.replace(".","$#$").replace(/\./g,"").replace("$#$",".");
            obj.value = obj.value.replace(/^(\d+)\.(\d\d).*$/,'$1.$2');
        }
        function checkNumber(obj){
            obj.value = obj.value.replace(/[^\d]/g,"");  //清除“数字”以外的字符
        }
    function is_show(obj) {
        var content = $(obj).data('name');
        layer.tips(content, obj, {
            tips: [1, "#4794ec"]
        });
    }
    function selectAll(name,obj){
        $('input[name*='+name+']').prop('checked', $(obj).checked);
    }
    //个人认证/企业认证/车辆认证-通过
    function auth_agree(obj)
    {
        var url = "<?php echo url('User/checkAuth'); ?>";
        var id = $(obj).data('id');
        var type = $(obj).data('type');
        layer.confirm('确认审核通过吗？',{title:'温馨提示',closeBtn:0,icon:3,btn: ['确定','取消'] }, function() {
            $.ajax({
                type : 'post',
                url : url,
                data : {act:'agree',id:id,type:type},
                dataType : 'json',
                success : function(data) {
                    layer.closeAll();
                    if (data.errcode == 1) {
                        layer.msg(data.message, {icon: 1, time: 2000},function(){
                            location.href = data.data.url;
                        });
                    } else {
                        layer.msg(data.message, {icon: 2,time: 2000});
                    }
                }
            })
        }, function(index){
            layer.close(index);
            return false;// 取消
        });
    }
    //-拒绝
    function auth_refuse(obj)
    {
        var url = "<?php echo url('User/checkAuth'); ?>";
        var id = $(obj).data('id');
        var type = $(obj).data('type');
        var html = `<div class="fine_layer">
            <textarea id="remark" maxlength="100" placeholder="请填写拒绝原因(100字内)"></textarea>
        </div>`;
		layer.open({
			type:1,
			title: '审核拒绝',
            content: html,
			area: ['400px', '210px'],
            closeBtn:0,
			btn:['确定', '取消'],
			yes:function(index){
				var content = $("#remark").val();
				/* if (content == "") {
					layer.msg('请填写拒绝原因', {icon: 2,time:2000});
					return false;
				} */
				$.ajax({
					type: 'POST',
					url: url,
					data : {id:id,remark:content,act:'refuse',type:type},
					dataType: 'json',
					success: function(data){
                        if (data.errcode == 1) {
                            layer.msg(data.message, {icon: 1, time: 2000},function(){
                                location.href = data.data.url;
                            });
                        } else {
                            layer.msg(data.message, {icon: 2,time: 2000});
                        }
					},
					error: function(){
						layer.alert("服务器繁忙, 请联系管理员!");
					}
				});
			}
		});
    }

    function delAll() {
        var ids = '';
        $('#flexigrid .trSelected').each(function(i,o) {
            ids += $(o).data('id')+',';
        })
        if (ids == '' || ids == '0,' || ids == '0') {
            layer.confirm('请选择删除项', {title:'温馨提示',closeBtn:0,icon:3});
            return;
        }
        layer.confirm('确认删除？',{title:'温馨提示',closeBtn:0,icon:3,btn: ['确定','取消'] }, function() {
            $.ajax({
                type : 'post',
                url : $('#flexigrid').data('url'),
                data : {act:'del',ids:ids},
                dataType : 'json',
                success : function(data) {
                    layer.closeAll();
                    if (data.errcode == 1) {
                        layer.msg('操作成功', {icon: 1});
                        $('#flexigrid .trSelected').each(function(i,o) {
                            $(o).remove();
                        })
                    } else {
                        layer.msg(data.message, {icon: 2,time: 2000});
                    }
                }
            })
        }, function(index){
            layer.close(index);
            return false;// 取消
        });
    }

    /**
     * 全选
     * @param obj
     */
    function checkAllSign(obj){
        $(obj).toggleClass('trSelected');
        if($(obj).hasClass('trSelected')){
            $('#flexigrid > table>tbody >tr').addClass('trSelected');
        }else{
            $('#flexigrid > table>tbody >tr').removeClass('trSelected');
        }
    }
    //判断是否全选
	function renderCheckAll(){
        var goodsLen = $('#flexigrid tr').length;
        var checkedLen = $('#flexigrid tr.trSelected').length;
        if(goodsLen == checkedLen){
          $('.check-all').addClass('trSelected')
        }else{
          $('.check-all').removeClass('trSelected')
        }
	}
    /**
     * 批量公共操作（删，改）
     * @returns {boolean}
     */
    function publicHandleAll(type){
        var ids = '';
        $('#flexigrid .trSelected').each(function(i,o){
            ids += $(o).data('id')+',';
        });
        if(ids == ''){
            layer.msg('至少选择一项', {icon: 2, time: 2000});
            return false;
        }
        publicHandle(ids,type); //调用删除函数
    }
    /**
     * 批量公共操作（删）新
     * @returns {boolean}
     */
    function publicUpdateAll(name,field_name){
        var ids = '';
        $('#flexigrid .trSelected').each(function(i,o){
            if(ids ==''){
                ids = $(o).data('id');
            }else{
                ids += ','+$(o).data('id');
            }
        });
        if(ids == ''){
            layer.msg('至少选择一项', {icon: 2, time: 2000});
            return false;
        }
        publicUpdate(ids,name,field_name); //调用删除函数
    }
    /**
     * 公共操作（删，改）
     * @param type
     * @returns {boolean}
     */
    function publicHandle(ids,handle_type){
        layer.confirm('确认当前操作？', {
            title:'温馨提示',closeBtn:0,icon:3,
                    btn: ['确定', '取消'] //按钮
                }, function () {
                    // 确定
                    $.ajax({
                        url: $('#flexigrid').data('url'),
                        type:'post',
                        data:{ids:ids,act:handle_type},
                        dataType:'JSON',
                        success: function (data) {
                            layer.closeAll();
                            if (data.errcode == 1){
                                layer.msg(data.message, {icon: 1, time: 2000},function(){
                                    location.href = data.dyurl;
                                });
                            }else{
                                layer.msg(data.message, {icon: 2, time: 2000});
                            }
                        }
                    });
                }, function (index) {
                    layer.close(index);
                }
        );
    }

        /**
         * 公共操作（删，改）新
         * @param type
         * @returns {boolean}
         */
        function publicUpdate(ids,name,field_name){
            layer.confirm('确认当前操作？', {
                title:'温馨提示',closeBtn:0,icon:3,
                        btn: ['确定', '取消'] //按钮
                    }, function () {
                        // 确定
                        $.ajax({
                            url: '/Admincp/System/deleteData',
                            type:'post',
                            data:{ids:ids,name:name,field_name:field_name},
                            dataType:'JSON',
                            success: function (data) {
                                layer.closeAll();
                                if (data.status == 1){
                                    layer.msg(data.msg, {icon: 1, time: 2000},function(){
                                        window.location.reload();
                                    });
                                }else{
                                    layer.msg(data.msg, {icon: 2, time: 2000});
                                }
                            }
                        });
                    }, function (index) {
                        layer.close(index);
                    }
            );
        }

        function delfuntion(obj) {
            // 删除按钮
            layer.confirm('确认刪除？', {
                title:'温馨提示',closeBtn:0,icon:3,
                btn: ['确定', '取消'] //按钮
            }, function () {
                $.ajax({
                    type: 'post',
                    url: '/Admincp/System/deleteData',
                    data : {ids:$(obj).attr('data-id'),name:$(obj).attr('data-name'),field_name:$(obj).attr('data-field_name')},
                    dataType: 'json',
                    success: function (data) {
                        layer.closeAll();
                        if (data.status == 1) {
                            $(obj).parent().parent().parent().remove();
                            layer.closeAll();
                        } else {
                            layer.alert(data, {icon: 2});  //alert('删除失败');
                        }
                    }
                })
            }, function () {
                layer.closeAll();
            });
        }
        function delfunc(obj,field='id') {
            var url = $(obj).data('url');
            // 删除按钮
            layer.confirm('确认刪除？', {
                title:'温馨提示',closeBtn:0,icon:3,
                btn: ['确定', '取消'] //按钮
            }, function () {
                var data = {}
                data[field] = $(obj).data('id')
                data['act'] = 'del'
                $.ajax({
                    type: 'post',
                    url: url,
                    data : data,
                    dataType: 'json',
                    success: function (data) {
                        layer.closeAll();
                        if (data.errcode == 1) {
                            layer.msg(data.message,{icon:1,time:2000},function(){
                                window.location.reload();
                            });
                        } else {
                            layer.msg(data.message, {icon: 2});  //alert('删除失败');
                        }
                    }
                })
            }, function () {
                layer.closeAll();
            });
        }
    </script>
</head>
<style>
    .fa-check-circle,.fa-ban{cursor:pointer}
</style>
<style>
    .system_img_location{text-align: center; width: 120px;position:absolute;top:15px; margin-left:265px;}
</style>
<body style="background-color: #FFF; overflow: auto;">
<div id="append_parent"></div>
<div id="ajaxwaitid"></div>
<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <div class="subject">
                <h3>基础设置</h3>
                <h5>全局内容基本选项设置</h5>
            </div>
            <ul class="tab-base nc-row">
                <?php if(is_array($group_list) || $group_list instanceof \think\Collection || $group_list instanceof \think\Paginator): if( count($group_list)==0 ) : echo "" ;else: foreach($group_list as $k=>$v): ?>
                    <li><a href="<?php echo url('System/index',['inc_type'=> $k]); ?>" <?php if($k==$inc_type): ?>class="current"<?php endif; ?>><span><?php echo $v; ?></span></a></li>
                <?php endforeach; endif; else: echo "" ;endif; ?>
            </ul>
        </div>
    </div>
    <!-- 操作说明 -->
    <div class="explanation" id="explanation" style="display: none;">
        <div class="bckopa-tips">
            <div class="title">
                <img src="/public/static/images/handd.png" alt="">
                <h4 title="提示相关设置操作时应注意的要点">操作提示</h4>
            </div>
            <ul>
            </ul>
        </div>
        <span title="收起提示" id="explanationZoom"></span>
    </div>
    <form method="post" id="handlepost" action="<?php echo url('System/handle'); ?>" enctype="multipart/form-data" name="form1">
        <div class="ncap-form-default">
			<dl class="row">
                <dt class="tit">
                    <label for="title">描述</label>
                </dt>
                <dd class="opt">
                    <textarea id="title" name="title" class="input-txt" ><?php echo $config['title']; ?></textarea>
                    <p class="notic"></p>
                </dd>
            </dl>
            
            <dl class="row">
                <dt class="tit">
                        <label for="kefu">客服</label>
                </dt>
                <dd class="opt">
                    <input id="kefu" name="kefu" value="<?php echo $config['kefu']; ?>" class="input-txt" type="text" />
                    <p class="notic"></p>
                </dd>
            </dl>

			<dl class="row">
                <dt class="tit">
                    <label for="email">Email</label>
                </dt>
                <dd class="opt">
                    <input id="email" name="email" value="<?php echo $config['email']; ?>" class="input-txt" type="text" />
                    <p class="notic"></p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label for="company_url">公司官网</label>
                </dt>
                <dd class="opt">
                    <input id="company_url" name="company_url" value="<?php echo $config['company_url']; ?>" class="input-txt" type="text" />
                    <p class="notic"></p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label for="company_name">公司名称</label>
                </dt>
                <dd class="opt">
                    <input id="company_name" name="company_name" value="<?php echo $config['company_name']; ?>" class="input-txt" type="text" />
                    <p class="notic"></p>
                </dd>
            </dl>

            <div class="bot">
                <input type="hidden" name="inc_type" value="<?php echo $inc_type; ?>" id="inc_type" >
                <a href="JavaScript:void(0);" class="ncap-btn-big ncap-btn-green" onClick="submit()">确认提交</a>
            </div>
        </div>
    </form>
</div>
<div id="goTop"> <a href="JavaScript:void(0);" id="btntop"><i class="fa fa-angle-up"></i></a><a href="JavaScript:void(0);" id="btnbottom"><i class="fa fa-angle-down"></i></a></div>
<script>
    /* 上传图片 */
    isupload = false;
    function dykjUpload(fileInput){
        if(isupload != false){
            layer.msg("其它文件正在上传，请稍后...");
        }else{
            $(fileInput).click();
        }
    }

    function uploadImg(fileInput,target,imgpath,dytypes){
        var loading = layer.msg('图片上传中，请稍等...', {
				icon: 16,offset: '300px',shade: 0.2
			});
        var filename = $(fileInput).val();
        if(filename != '' && filename != null){
            isupload = true;
            var pic = $(fileInput)[0].files[0];
            var dyformdata = new FormData();
            dyformdata.append('dypic', pic);
            dyformdata.append('imgpath', imgpath);
            $.ajax({
                url:"<?php echo url('Index/dyupload'); ?>",
                type:"post",
                dataType:'json',
                data: dyformdata,
                cache: false,
                contentType: false,
                processData: false,
                beforeSend:function(){
                    //图片上传中
                },
                success:function(data){
                    if(data.errcode == 1){
                        layer.msg(data.message,{icon:1,time:500});
                        if(target){
                            $(target).val(data.data);
                        }
                        if(dytypes == 1){
                            $(fileInput).parents('.input-file-show').find(".img_a").attr('href', data.data);
                            $(fileInput).parents('.input-file-show').find(".img_b").val(data.data);
                            $(fileInput).parents('.input-file-show').find(".img_i").attr('onmouseover', "layer.tips('<img src=" + data.data + " width=192>',this,{tips: [1, '#fff']});");
                        }
                    }else{
                        layer.msg(data.message,{icon:2});
                    }
                    layer.close(loading);
                },
                error:function (){
                    layer.msg("上传出错了");
                }
            });
            isupload = false;
        }
        isupload = false;
        layer.close(loading);
    }

    function uploadVideo(fileInput,target,imgpath,dytypes){
        var loading = layer.msg('视频上传中，请稍等...', {
				icon: 16,offset: '300px',shade: 0.2
			});
        var filename = $(fileInput).val();
        if(filename != '' && filename != null){
            isupload = true;
            var video = $(fileInput)[0].files[0];
            var dyformdata = new FormData();
            dyformdata.append('dyvideo', video);
            dyformdata.append('imgpath', imgpath);
            $.ajax({
                url:"<?php echo url('Index/dyuploadVideo'); ?>",
                type:"post",
                dataType:'json',
                data: dyformdata,
                cache: false,
                contentType: false,
                processData: false,
                beforeSend:function(){
                    //图片上传中
                },
                success:function(data){
                    if(data.errcode == 1){
                        layer.close(loading);
                        layer.msg(data.message,{icon:1,time:500});
                        $(target).val(data.data);
                        if(dytypes == 1){
                            if (data.data) {
                                $("#video-button").show();
                                $("#videotext").val(data.data);
                                $("#video_a").attr('href', data.data);
                            }
                        }
                    }else{
                        layer.msg(data.message,{icon:2});
                        layer.close(loading);
                    }
                },
                error:function (){
                    layer.msg("上传出错了");
                }
            });
            isupload = false;
        }
        isupload = false;
        //layer.close(loading);
    }
</script>
</body>
<script type="text/javascript">
    function toupfile(){
        $("#id_thumb_empty").trigger('click')
    }
    function submit(){
        $.post("<?php echo url('System/handle'); ?>",$('#handlepost').serialize(),function(data,status){
          if (status == "success") {
            var result = JSON.parse(data);
            if (result.errcode == 1) {
                layer.msg('提交成功',{icon:1,time:2000},function(){
                    window.location.href = result.dyurl;
                });
            }
          }
        });
    }
        //上传图片回调事件
    function img_call_back(fileurl_tmp) {
        $("#wechat_img").val(fileurl_tmp);
        $("#img_a").attr('href', fileurl_tmp);
        $("#img_i").attr('onmouseover', "layer.tips('<img src=" + fileurl_tmp + " width=300>',this,{tips: [1, '#fff']});");
    }
</script>
</html>