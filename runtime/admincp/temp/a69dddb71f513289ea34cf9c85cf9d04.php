<?php /*a:1:{s:81:"F:\phpstudy_pro\WWW\www.dreamhouses.com.cn\app/admincp/dytpl/index/maplnglat.html";i:1698049681;}*/ ?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no, width=device-width">
    <link rel="stylesheet" href="https://a.amap.com/jsapi_demos/static/demo-center/css/demo-center.css" />
    <link rel="stylesheet" type="text/css" href="https://a.amap.com/jsapi_demos/static/demo-center/css/prety-json.css">
    <style>
        html,
        body,
        #container {
            width: 100%;
            height: 100%;
        }
    </style>
    <title>获取输入提示信息</title>
    <style>
        .name{
            font-weight:bold;
        }
    </style>
</head>

<body>
<div id="container"></div>
<div class="info">
    <div class="input-item">
        <div class="input-item-prepend">
            <span class="input-item-text" style="width:10rem;">请输入关键字</span>
        </div>
        <input id='input' type="text" value='' >
    </div>
    <p><span id="input-info"></span></p>
</div>
<script>
    window._AMapSecurityConfig = {
        securityJsCode:'de66956bd2c324a3f8afaa4184a4170e',
    }
</script>
<script src="https://webapi.amap.com/loader.js"></script>
<script src="https://webapi.amap.com/maps?v=2.0&key=<?php echo $web_key; ?>&plugin=AMap.AutoComplete"></script>
<script type="text/javascript" src="https://a.amap.com/jsapi_demos/static/demo-center/js/jquery-1.11.1.min.js" ></script>
<script type="text/javascript" src="https://a.amap.com/jsapi_demos/static/demo-center/js/underscore-min.js" ></script>
<script type="text/javascript" src="https://a.amap.com/jsapi_demos/static/demo-center/js/backbone-min.js" ></script>
<script type="text/javascript" src='https://a.amap.com/jsapi_demos/static/demo-center/js/prety-json.js'></script>
<script>
    //初始化地图
    var map = new AMap.Map('container', {
        resizeEnable: true, //是否监控地图容器尺寸变化
        zoom: 11, //初始地图级别
    });
    
    function inputinfo(result,type=1) {
        var str = '';
        for (var i=0;i<result.length;i++){
            if(type==1){
                var address = result[i]['district']+result[i]['address'];
            }else {
                var address = result[i]['pname']+result[i]['cityname']+result[i]['adname']+result[i]['address'];
            }
            str += '<div onclick="addressfunc(this)" data-adcode="'+result[i]['adcode']+'" data-name="'+result[i]['name']+'" data-address="'+result[i]['address']+'">' +
                '<p class="name">'+result[i]['name']+'</p>'+
                '<span>'+address+'</span>'+
                '</div>'
        }
        $('#input-info').html(str);
    }
    // 获取输入提示信息
    function autoInput(){
        var keywords = document.getElementById("input").value;
        if(keywords){
            AMap.plugin('AMap.AutoComplete', function(){
                // 实例化Autocomplete
                var autoOptions = {
                    type: '商务住宅', // 兴趣点类别
                    citylimit: true,  //是否强制限制在设置的城市内搜索
                    city: '<?php echo $name; ?>'
                }
                var autoComplete = new AMap.Autocomplete(autoOptions);
                autoComplete.search(keywords, function(status, result) {
                    inputinfo(result.tips)
                })
            })
        }else {
            $('#input-info').html('');
        }
    }
    //autoInput();
    document.getElementById("input").oninput = autoInput;
    function addressfunc(obj) {
        var name = $(obj).data('name');
        var address = $(obj).data('address');
        var adcode = $(obj).data('adcode');
        window.parent.map_call_back(name,address,adcode);
    }
    AMap.plugin('AMap.PlaceSearch',function () {
        var keywords = document.getElementById("input").value;
        //构造地点查询类
        var placeSearch = new AMap.PlaceSearch({
            type: '商务住宅', // 兴趣点类别
            pageSize: 10, // 单页显示结果条数
            pageIndex: 1, // 页码
            city: '<?php echo $name; ?>', // 兴趣点城市
            citylimit: true,  //是否强制限制在设置的城市内搜索
            map: map, // 展现结果的地图实例
            autoFitView: true // 是否自动调整地图视野使绘制的 Marker点都处于视口的可见范围
        });

        var cpoint = ["<?php echo $lng; ?>", "<?php echo $lat; ?>"]; //中心点坐标
        placeSearch.searchNearBy(keywords, cpoint, 200, function(status, result) {
            inputinfo(result.poiList.pois,2)
        });
        //为地图注册click事件获取鼠标点击出的经纬度坐标
        map.on('click', function(e) {
            var cpoint = [e.lnglat.getLng(), e.lnglat.getLat()]; //中心点坐标
            placeSearch.searchNearBy(keywords, cpoint, 200, function(status, result) {
                inputinfo(result.poiList.pois,2)
            });
        });
    })
</script>
</body>
</html>