<?php /*a:1:{s:77:"F:\phpstudy_pro\WWW\www.dreamhouses.com.cn\app/admincp/dytpl/admin/login.html";i:1698049681;}*/ ?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="renderer" content="webkit">
<meta name="keywords" content="">
<meta name="description" content="">
<title>梦想家系统管理中心</title>
<link rel="shortcut icon" href="/public/static/images/favicon.ico">
<link rel="stylesheet" href="/public/static/css/reset.css">
<link rel="stylesheet" href="/public/static/css/style.css">
<link rel="stylesheet" href="/public/js/layui/css/layui.css">
<style type="text/css">
	html,body{
		width: 100%;
		height: 100%;
		background-image: url(/public/static/images/loginbg.jpg);
		background-size: cover;
		background-position: center;
	}
</style>
</head>
<body id="busi-login">	
	<div class="main">
		<div class="title">凡华后台业务管理平台</div>
		<form action="" name='theForm' id="theForm" method="post" class="layui-form">
		<div class="form layui-form">
			<!-- <div class="logo"><img src="/public/static/images/logo/logo.png" ></div> -->
			<div class="line mt20">
				<p class="label">账　号：</p>
				<div class="right">
					<div class="input"><input type="text" maxlength="20" name="username" placeholder="请输入您的账号信息"></div>
				</div>
			</div>
			<div class="line mt20">
				<p class="label">密　码：</p>
				<div class="right">
					<div class="input"><input type="password" maxlength="20" name="password" placeholder="请输入您的密码"></div>
				</div>
			</div>
			<div class="line mt20">
				<p class="label">验证码：</p>
				<div class="right has-yzm">
					<div class="input"><input type="text" maxlength="6" name="vertify" id="vertify" placeholder="请输入验证码"></div>
					<div class="img"><img width="148" class="chicuele" id="imgVerify" alt="" onclick="fleshVerify()"></div>
				</div>
			</div>
			<!-- <div class="check mt20"><input type="checkbox" value="1" id="isauto" lay-filter="isauto" name="isautologin" title="下次自动登录" lay-skin="primary" checked="checked" /></div> -->
			<div class="btn mt20"><button type="button" name="submit">登录</button></div>
		</div>
		</form>
	</div>
	<script type="text/javascript" src="/public/static/js/jquery.js"></script>
<script type="text/javascript" src="/public/static/js/jquery.SuperSlide.2.1.2.js"></script>
<script type="text/javascript" src="/public/static/js/jquery.validation.min.js"></script>
<script type="text/javascript" src="/public/static/js/jquery.cookie.js"></script>
<script type="text/javascript" src="/public/js/dycookie.js"></script>
<script src="/public/js/layui/layui.js"></script>
	<script type="text/javascript" src="/public/static/js/jquery-ui/jquery-ui.min.js"></script>
	<script type="text/javascript" src="/public/static/js/admincp.js"></script>
<script type="text/javascript">
	//若cookie值不存在，则跳出iframe框架
	if(!$.cookie('dyshopActionParam') && $.cookie('admin_type') != 1){
		$.cookie('admin_type','1' , {expires: 1 ,path:'/'});
		//top.location.href = location.href;
	}
	//layui全局初始化
	layui.use(['layer', 'form'], function() {
		var layer = layui.layer,
			form = layui.form;
		form.on("checkbox(isauto)",function(data){
		  if(data.elem.checked){
			  $("#isauto").val("1");
		  }else{
			  $("#isauto").val("0");
		  };
	  });
	});
	$(function(){
		if(self !== top){
			top.location.href = self.location.href;
		}
		$(".formText .input-text").focus(function(){
			$(this).parent().addClass("focus");
		});

		$(".formText .input-text").blur(function(){
			$(this).parent().removeClass("focus");
		});

		$(".checkbox").click(function(){
			if($(this).hasClass("checked")){
				$(this).removeClass("checked");
				$('input[name=remember]').val(0);
			}else{
				$(this).addClass("checked");
				$('input[name=remember]').val(1);
			}
		});

		$(".formText .input-yzm").focus(function(){
			$(this).prev().show();
		});

		$(".formText").blur(function(){
			$(this).prev().hide();
		});	
        
		function loginsubmit(){
			var username=true;
			var password=true;
			var vertify=true;

			if($('#theForm input[name=username]').val() == ''){
				layer.msg('账号不能为空');
				$('#theForm input[name=username]').focus();
				username = false;
				return false;
			}

			if($('#theForm input[name=password]').val() == ''){
				layer.msg('密码不能为空');
				$('#theForm input[name=password]').focus();
				password = false;
				return false;
			}

			if($('#theForm input[name=vertify]').val() == ''){
				layer.msg('验证码不能为空');
				$('#theForm input[name=vertify]').focus();
				vertify = false;
				return false;
			}

			var loading = layer.msg('登录中，请稍后...', {
				icon: 16,offset: '300px',shade: 0.2
			});

			if(vertify && $('#theForm input[name=username]').val() != '' && $('#theForm input[name=password]').val() != ''){
				$.ajax({
					async:false,
					url:"<?php echo url('Admin/login'); ?>",
					data:{'username':$('#theForm input[name=username]').val(),'password':$('#theForm input[name=password]').val(),'isautologin':$('#theForm input[name=isautologin]').val(),vertify:$('#theForm input[name=vertify]').val()},
					type:'post',
					dataType:'json',
					success:function(res){
						layer.close(loading);
						if(res.errcode != 1){
							layer.msg(res.message);
							fleshVerify();
							username=false;
							password=false;
							return false;
						}else{
							openItem('welcome|Index');
							top.location.href = res.dyurl;
						}
					},
					error : function(XMLHttpRequest, textStatus, errorThrown) {
						layer.msg('网络失败，请刷新页面后重试!');
					}
				});
			}else{
				return false;
			}
		}

		$('.submit_span .sub').on('click',function(){
			$('.code').show();
		});
		$('#theForm button[name=submit]').on('click',function(){
			loginsubmit();
		});

		$(document).click(function(e){
			if(e.target.name !='vertify' && !$(e.target).parents("div").is(".submitDiv")){
				$('.code').hide();

			}
		});
		//回车提交
		$(document).keyup(function(event){
			if(event.keyCode ==13){
				var isFocus=$("#vertify").is(":focus");
				if(true==isFocus){
					loginsubmit();
				}
			}
		});

		setTimeout(function(){
			fleshVerify();
	　　},100);
	});

	function fleshVerify(){
		$('#imgVerify').attr('src','/index.php?m=Admincp&c=Admin&a=vertify&r='+Math.floor(Math.random()*100));//重载验证码
	}
</script>
</body>
</html>
