<?php /*a:4:{s:81:"F:\phpstudy_pro\WWW\www.dreamhouses.com.cn\app/admincp/dytpl/scene/housetype.html";i:1700470871;s:79:"F:\phpstudy_pro\WWW\www.dreamhouses.com.cn\app/admincp/dytpl/public/layout.html";i:1698049681;s:81:"F:\phpstudy_pro\WWW\www.dreamhouses.com.cn\app/admincp/dytpl/public/dyupload.html";i:1698049681;s:75:"F:\phpstudy_pro\WWW\www.dreamhouses.com.cn\app/admincp/dytpl/public/js.html";i:1698049681;}*/ ?>
<!doctype html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <!-- Apple devices fullscreen -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <!-- Apple devices fullscreen -->
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <link href="/public/static/css/main.css" rel="stylesheet" type="text/css">
    <link href="/public/static/css/list_optimization.css" rel="stylesheet" type="text/css">
    <link href="/public/static/css/page.css" rel="stylesheet" type="text/css">
    <link href="/public/static/font/css/font-awesome.min.css" rel="stylesheet" />
    <link href="/public/static/css/union.css" rel="stylesheet" />
    <link href="/public/js/layui/css/layui.css" rel="stylesheet" />
    <!--[if IE 7]>
    <link rel="stylesheet" href="/public/static/font/css/font-awesome-ie7.min.css">
    <![endif]-->
    <link href="/public/static/js/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
    <link href="/public/static/js/perfect-scrollbar.min.css" rel="stylesheet" type="text/css"/>
    <style type="text/css">
    html, body { overflow: visible;}
    .fine_layer{
		width: 100%;
		padding: 10px;
		box-sizing: border-box;
    }
	.fine_layer *{
		box-sizing: border-box;
    }
	.fine_layer textarea{
		width: 100%;
        height: 80px;
		border: 1px solid #e6e6e6;
        border-radius: 4px;
        padding: 5px 4px;
        resize: none!important;
    }
    .red{
        color: red !important;
    }
    .hDiv th div{
        text-align: center;
    }
    .bDiv td div{
        text-align: center;
    }
    </style>
    <script type="text/javascript" src="/public/static/js/jquery.js"></script>
    <script type="text/javascript" src="/public/static/js/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="/public/static/js/layer/layer.js"></script><!-- 弹窗js 参考文档 http://layer.layui.com/-->
    <script src="/public/js/layui/layui.js"></script>
    <script type="text/javascript" src="/public/static/js/admin.js"></script>
    <script type="text/javascript" src="/public/static/js/jquery.validation.min.js"></script>
    <script type="text/javascript" src="/public/static/js/common.js"></script>
    <script type="text/javascript" src="/public/static/js/perfect-scrollbar.min.js"></script>
    <script type="text/javascript" src="/public/static/js/jquery.mousewheel.js"></script>
    <script src="/public/js/myFormValidate.js"></script>
    <script src="/public/js/myAjax2.js"></script>
    <script src="/public/js/dyglobal.js"></script>
    <script type="text/javascript">
        function in_array(needle, haystack) {
            if(typeof needle == 'string' || typeof needle == 'number') {
                for(var i in haystack) {
                    if(haystack[i] == needle) {
                        return true;
                    }
                }
            }
            return false;
        }
        function checkEmtpy(obj,title) {
            var url = $(obj).attr('href')
            if(!url){
                if(!title){
                    title = '没有图片';
                }
                layer.msg(title)
                return false;
            }
            return true;
        }
        function checkPrice(obj){
            obj.value = obj.value.replace(/[^\d.]/g,"");  //清除“数字”和“.”以外的字符
            obj.value = obj.value.replace(/^\./g,"");  //验证第一个字符是数字而不是.
            obj.value = obj.value.replace(/\.{2,}/g,"."); //只保留第一个. 清除多余的.
            obj.value = obj.value.replace(".","$#$").replace(/\./g,"").replace("$#$",".");
            obj.value = obj.value.replace(/^(\d+)\.(\d\d).*$/,'$1.$2');
        }
        function checkNumber(obj){
            obj.value = obj.value.replace(/[^\d]/g,"");  //清除“数字”以外的字符
        }
    function is_show(obj) {
        var content = $(obj).data('name');
        layer.tips(content, obj, {
            tips: [1, "#4794ec"]
        });
    }
    function selectAll(name,obj){
        $('input[name*='+name+']').prop('checked', $(obj).checked);
    }
    //个人认证/企业认证/车辆认证-通过
    function auth_agree(obj)
    {
        var url = "<?php echo url('User/checkAuth'); ?>";
        var id = $(obj).data('id');
        var type = $(obj).data('type');
        layer.confirm('确认审核通过吗？',{title:'温馨提示',closeBtn:0,icon:3,btn: ['确定','取消'] }, function() {
            $.ajax({
                type : 'post',
                url : url,
                data : {act:'agree',id:id,type:type},
                dataType : 'json',
                success : function(data) {
                    layer.closeAll();
                    if (data.errcode == 1) {
                        layer.msg(data.message, {icon: 1, time: 2000},function(){
                            location.href = data.data.url;
                        });
                    } else {
                        layer.msg(data.message, {icon: 2,time: 2000});
                    }
                }
            })
        }, function(index){
            layer.close(index);
            return false;// 取消
        });
    }
    //-拒绝
    function auth_refuse(obj)
    {
        var url = "<?php echo url('User/checkAuth'); ?>";
        var id = $(obj).data('id');
        var type = $(obj).data('type');
        var html = `<div class="fine_layer">
            <textarea id="remark" maxlength="100" placeholder="请填写拒绝原因(100字内)"></textarea>
        </div>`;
		layer.open({
			type:1,
			title: '审核拒绝',
            content: html,
			area: ['400px', '210px'],
            closeBtn:0,
			btn:['确定', '取消'],
			yes:function(index){
				var content = $("#remark").val();
				/* if (content == "") {
					layer.msg('请填写拒绝原因', {icon: 2,time:2000});
					return false;
				} */
				$.ajax({
					type: 'POST',
					url: url,
					data : {id:id,remark:content,act:'refuse',type:type},
					dataType: 'json',
					success: function(data){
                        if (data.errcode == 1) {
                            layer.msg(data.message, {icon: 1, time: 2000},function(){
                                location.href = data.data.url;
                            });
                        } else {
                            layer.msg(data.message, {icon: 2,time: 2000});
                        }
					},
					error: function(){
						layer.alert("服务器繁忙, 请联系管理员!");
					}
				});
			}
		});
    }

    function delAll() {
        var ids = '';
        $('#flexigrid .trSelected').each(function(i,o) {
            ids += $(o).data('id')+',';
        })
        if (ids == '' || ids == '0,' || ids == '0') {
            layer.confirm('请选择删除项', {title:'温馨提示',closeBtn:0,icon:3});
            return;
        }
        layer.confirm('确认删除？',{title:'温馨提示',closeBtn:0,icon:3,btn: ['确定','取消'] }, function() {
            $.ajax({
                type : 'post',
                url : $('#flexigrid').data('url'),
                data : {act:'del',ids:ids},
                dataType : 'json',
                success : function(data) {
                    layer.closeAll();
                    if (data.errcode == 1) {
                        layer.msg('操作成功', {icon: 1});
                        $('#flexigrid .trSelected').each(function(i,o) {
                            $(o).remove();
                        })
                    } else {
                        layer.msg(data.message, {icon: 2,time: 2000});
                    }
                }
            })
        }, function(index){
            layer.close(index);
            return false;// 取消
        });
    }

    /**
     * 全选
     * @param obj
     */
    function checkAllSign(obj){
        $(obj).toggleClass('trSelected');
        if($(obj).hasClass('trSelected')){
            $('#flexigrid > table>tbody >tr').addClass('trSelected');
        }else{
            $('#flexigrid > table>tbody >tr').removeClass('trSelected');
        }
    }
    //判断是否全选
	function renderCheckAll(){
        var goodsLen = $('#flexigrid tr').length;
        var checkedLen = $('#flexigrid tr.trSelected').length;
        if(goodsLen == checkedLen){
          $('.check-all').addClass('trSelected')
        }else{
          $('.check-all').removeClass('trSelected')
        }
	}
    /**
     * 批量公共操作（删，改）
     * @returns {boolean}
     */
    function publicHandleAll(type){
        var ids = '';
        $('#flexigrid .trSelected').each(function(i,o){
            ids += $(o).data('id')+',';
        });
        if(ids == ''){
            layer.msg('至少选择一项', {icon: 2, time: 2000});
            return false;
        }
        publicHandle(ids,type); //调用删除函数
    }
    /**
     * 批量公共操作（删）新
     * @returns {boolean}
     */
    function publicUpdateAll(name,field_name){
        var ids = '';
        $('#flexigrid .trSelected').each(function(i,o){
            if(ids ==''){
                ids = $(o).data('id');
            }else{
                ids += ','+$(o).data('id');
            }
        });
        if(ids == ''){
            layer.msg('至少选择一项', {icon: 2, time: 2000});
            return false;
        }
        publicUpdate(ids,name,field_name); //调用删除函数
    }
    /**
     * 公共操作（删，改）
     * @param type
     * @returns {boolean}
     */
    function publicHandle(ids,handle_type){
        layer.confirm('确认当前操作？', {
            title:'温馨提示',closeBtn:0,icon:3,
                    btn: ['确定', '取消'] //按钮
                }, function () {
                    // 确定
                    $.ajax({
                        url: $('#flexigrid').data('url'),
                        type:'post',
                        data:{ids:ids,act:handle_type},
                        dataType:'JSON',
                        success: function (data) {
                            layer.closeAll();
                            if (data.errcode == 1){
                                layer.msg(data.message, {icon: 1, time: 2000},function(){
                                    location.href = data.dyurl;
                                });
                            }else{
                                layer.msg(data.message, {icon: 2, time: 2000});
                            }
                        }
                    });
                }, function (index) {
                    layer.close(index);
                }
        );
    }

        /**
         * 公共操作（删，改）新
         * @param type
         * @returns {boolean}
         */
        function publicUpdate(ids,name,field_name){
            layer.confirm('确认当前操作？', {
                title:'温馨提示',closeBtn:0,icon:3,
                        btn: ['确定', '取消'] //按钮
                    }, function () {
                        // 确定
                        $.ajax({
                            url: '/Admincp/System/deleteData',
                            type:'post',
                            data:{ids:ids,name:name,field_name:field_name},
                            dataType:'JSON',
                            success: function (data) {
                                layer.closeAll();
                                if (data.status == 1){
                                    layer.msg(data.msg, {icon: 1, time: 2000},function(){
                                        window.location.reload();
                                    });
                                }else{
                                    layer.msg(data.msg, {icon: 2, time: 2000});
                                }
                            }
                        });
                    }, function (index) {
                        layer.close(index);
                    }
            );
        }

        function delfuntion(obj) {
            // 删除按钮
            layer.confirm('确认刪除？', {
                title:'温馨提示',closeBtn:0,icon:3,
                btn: ['确定', '取消'] //按钮
            }, function () {
                $.ajax({
                    type: 'post',
                    url: '/Admincp/System/deleteData',
                    data : {ids:$(obj).attr('data-id'),name:$(obj).attr('data-name'),field_name:$(obj).attr('data-field_name')},
                    dataType: 'json',
                    success: function (data) {
                        layer.closeAll();
                        if (data.status == 1) {
                            $(obj).parent().parent().parent().remove();
                            layer.closeAll();
                        } else {
                            layer.alert(data, {icon: 2});  //alert('删除失败');
                        }
                    }
                })
            }, function () {
                layer.closeAll();
            });
        }
        function delfunc(obj,field='id') {
            var url = $(obj).data('url');
            // 删除按钮
            layer.confirm('确认刪除？', {
                title:'温馨提示',closeBtn:0,icon:3,
                btn: ['确定', '取消'] //按钮
            }, function () {
                var data = {}
                data[field] = $(obj).data('id')
                data['act'] = 'del'
                $.ajax({
                    type: 'post',
                    url: url,
                    data : data,
                    dataType: 'json',
                    success: function (data) {
                        layer.closeAll();
                        if (data.errcode == 1) {
                            layer.msg(data.message,{icon:1,time:2000},function(){
                                window.location.reload();
                            });
                        } else {
                            layer.msg(data.message, {icon: 2});  //alert('删除失败');
                        }
                    }
                })
            }, function () {
                layer.closeAll();
            });
        }
    </script>
</head>
<style>
    .fa-check-circle,.fa-ban{cursor:pointer}
</style>
<style type="text/css">
    .dysubmit {
        background-color: #148eff;
        border-color: #148eff;
        display: inline-block;
        text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.1);
        font: bold 14px/20px "microsoft yahei", arial;
        color: #FFFFFF;
        text-align: center;
        vertical-align: middle;
        height: 36px;
        padding: 7px 19px;
        border-radius: 3px;
        cursor: pointer;
    }

    .typelable {
        padding-right: 15px;
    }

    .images {
        float: left;
    }

    .grid-square {
        position: relative;
    }

    .grid-square:hover .fun-line {
        height: 30px;
    }

    .fun-line {
        width: 62px;
        height: 0px;
        background-color: rgba(0, 0, 0, 0.5);
        position: absolute;
        top: 0px;
        left: 0px;
        z-index: 9;
        padding: 0 9px;
        transition: height 0.3s;
        overflow: hidden;
    }

    .fun-line a {
        width: 17px;
        height: 15px;
        display: block;
        float: right;
        background-image: url("/public/plugins/webuploader/images/icons.png");
        background-position: -48px -25px;
        margin-top: 7px;
    }

    .fun-line a:hover {
        background-position: -48px -1px;
    }
</style>
<body style="background-color: #FFF; overflow: auto;">
<div id="toolTipLayer"
     style="position: absolute; z-index: 9999; display: none; visibility: visible; left: 95px; top: 573px;"></div>
<div id="append_parent"></div>
<div id="ajaxwaitid"></div>
<div class="page">
    <div class="fixed-bar">
        <div class="item-title"><a class="back" href="javascript:history.back();" title="返回列表"><i
                class="fa fa-arrow-circle-o-left"></i></a>
            <div class="subject">
                <h3>户型管理 -
                    <?php if($act == 'add'): ?>新增
                        <?php else: ?>
                        编辑
                    <?php endif; ?>
                    户型
                </h3>
                <h5>户型索引与管理</h5>
            </div>
        </div>
    </div>
    <form class="form-horizontal" id="ajaxform">
        <input type="hidden" name="house_id" value="<?php echo $info['house_id']; ?>">
        <input type="hidden" name="act" value="<?php echo $act; ?>">
        <div class="ncap-form-default tab_div_1">
            <dl class="row"><h3 class="updt-protit">基本信息</h3></dl>
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>封面：</label>
                </dt>
                <dd class="opt">
                    <div class="input-file-show">
                        <span class="show">
                                <a target="_blank" class="nyroModal img_a" rel="gal" href="<?php echo $info['image']; ?>">
                                    <i class="fa fa-picture-o img_i"
                                       onMouseOver="layer.tips('<img src=<?php echo $info['image']; ?> width=192>',this,{tips: [1, '#fff']});"
                                       onMouseOut="layer.closeAll();"></i>
                                </a>
                        </span>
                        <span class="type-file-box">
                            <input type="text" id="image" name="image" value="<?php echo $info['image']; ?>" class="type-file-text">
                            <input type="button" name="button" id="button1" value="选择上传..." class="type-file-button"
                                   onclick="dykjUpload('#dykj-image')">
                            <input type="file" class="type-file-file" id="dykj-image"
                                   onchange="uploadImg('#dykj-image','#image','scene_house','1');" size="30"
                                   accept="image/gif,image/jpeg,image/jpg,image/png"
                                   title="点击前方预览图可查看大图，点击按钮选择文件并提交表单后上传生效">
                        </span>
                    </div>
                    <span class="err"></span>

                    <p class="notic">请上传图片格式文件，建议图片尺寸750*350像素</p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>相册图</label>
                </dt>
                <dd class="opt">
                    <div class="tab-pane">
                        <table class="table table-bordered">
                            <tbody>
                            <tr>
                                <td>
                                    <div class="list images" id="images">
                                        <?php if(is_array($info['images']) || $info['images'] instanceof \think\Collection || $info['images'] instanceof \think\Paginator): $i = 0; $__LIST__ = $info['images'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$image): $mod = ($i % 2 );++$i;?>
                                            <div style="width:80px; text-align:center; margin: 5px; display:inline-block;" class="goods_xc grid-square">
                                                <input type="hidden" value="<?php echo $image; ?>" name="images[]">
                                                <img style="width:80px;height:80px;object-fit:cover" src="<?php echo $image; ?>">
                                                <div class="fun-line"><a href="javascript:void(0)" onClick="ClearPicArr2(this,'<?php echo $image; ?>')"></a></div>
<!--                                                <div class="fun-line"><a href="javascript:void(0)" class="uploadQiniuMany"></a></div>-->
                                            </div>
                                        <?php endforeach; endif; else: echo "" ;endif; ?>
                                    </div>
                                    <div class="goods_xc" style="width:80px; text-align:center; margin: 5px; display:inline-block;">
                                        <a href="javascript:void(0);" onClick="GetUploadify(5,'','over_i','images_call_back');"><img src="/public/images/add-button.jpg" width="80" height="80"/></a>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td><p style="color:#AAA">请上传图片格式文件，建议图片尺寸800*800像素，且不超过5张。</p></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label><em></em>户型图：</label>
                </dt>
                <dd class="opt">
                    <div class="input-file-show">
                        <span class="show">
                                <a target="_blank" class="nyroModal img_a" rel="gal" href="<?php echo $info['house_image']; ?>">
                                    <i class="fa fa-picture-o img_i"
                                       onMouseOver="layer.tips('<img src=<?php echo $info['house_image']; ?> width=192>',this,{tips: [1, '#fff']});"
                                       onMouseOut="layer.closeAll();"></i>
                                </a>
                        </span>
                        <span class="type-file-box">
                            <input type="text" id="house_image" name="house_image" value="<?php echo $info['house_image']; ?>"
                                   class="type-file-text">
                            <input type="button" name="button" id="button2" value="选择上传..." class="type-file-button"
                                   onclick="dykjUpload('#dykj-house_image')">
                            <input type="file" class="type-file-file" id="dykj-house_image"
                                   onchange="uploadImg('#dykj-house_image','#house_image','scene_house','1');" size="30"
                                   accept="image/gif,image/jpeg,image/jpg,image/png"
                                   title="点击前方预览图可查看大图，点击按钮选择文件并提交表单后上传生效">
                        </span>
                    </div>
                    <span class="err"></span>

                    <p class="notic">请上传图片格式文件，建议图片尺寸307*340像素</p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit"><em>*</em>
                    <label>楼盘</label>
                </dt>
                <dd class="opt">
                    <div class="opt" style="float: left;">
                        <select id="estate_id" class="select validate[required]" name="estate_id"
                                style="width: 120px !important;height: 26px;">
                            <option value="">选择楼盘</option>
                            <?php if(is_array($estate) || $estate instanceof \think\Collection || $estate instanceof \think\Paginator): if( count($estate)==0 ) : echo "" ;else: foreach($estate as $key=>$vo): ?>
                                <option value="<?php echo $vo['estate_id']; ?>"
                                <?php if($info['estate_id'] == $vo['estate_id']): ?>selected<?php endif; ?>
                                ><?php echo $vo['name']; ?></option>
                            <?php endforeach; endif; else: echo "" ;endif; ?>
                        </select>
                    </div>
                    <p class="notic"><span class="red"></span></p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>户型类型</label>
                </dt>
                <dd class="opt">
                    <div class="house_type">

                    </div>
                    <span class="err" id="err_house_type_id"></span>
                    <p class="notic"></p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>户型名称：</label>
                </dt>
                <dd class="opt">
                    <input type="text" class="input-txt validate[required]" id="name" name="name" value="<?php echo $info['name']; ?>"/>
                    <span class="err" id="err_title"></span>
                    <p class="notic"></p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label><em></em>ios_url：</label>
                </dt>
                <dd class="bot" style="margin-left: 240px;margin-top: -25px;">
                    <div id="iosTotalBar" style="float:left;width:20%;height:20px;border:1px solid;border-radius:3px">
                        <div id="iosTotalBarColor" style="width:0%;border:0;background:#00b7ee; color: #FFF;height:20px;"></div>
                        <span class="ios_speed"></span>
                    </div>
                    <a id="ios_video" class="item" onclick="selectIos()">
                        <i class="icon_emot_photo_video icon_video"></i><span class="layui-btn layui-btn-xs">选择资源</span>
                    </a>
                    <input type="file" name="file" value="" class="upload" id="ios-file"
                           style="display: none" onchange="upFile(this)">
                    <br/>
                    <br/>
                    <a class="ios-item" onclick="uploadIos()">
                        <i class="icon_emot_photo_video icon_video"></i><span class="layui-btn layui-btn-xs">上传</span>
                    </a>
                </dd>

            </dl>
            <div style="margin-left: 240px">
                <input type="text" class="input-txt" id="ios_url" name="ios_url" value="<?php echo $info['ios_url']; ?>"/>
            </div>
            <dl class="row">
                <dt class="tit">
                    <label><em></em>Android_url：</label>
                </dt>
                <dd class="bot" style="margin-left: 240px;margin-top: -25px;">
                    <div id="AndroidTotalBar" style="float:left;width:20%;height:20px;border:1px solid;border-radius:3px">
                        <div id="AndroidTotalBarColor" style="width:0%;border:0;background:#00b7ee; color: #FFF;height:20px;"></div>
                        <span class="Android_speed"></span>
                    </div>
                    <a id="Android_video" class="item" onclick="selectAndroid()">
                        <i class="icon_emot_photo_video icon_video"></i><span class="layui-btn layui-btn-xs">选择资源</span>
                    </a>
                    <input type="file" name="file" value="" class="upload" id="Android-file"
                           style="display: none" onchange="upFile(this)">
                    <br/>
                    <br/>
                    <a class="Android-item" onclick="uploadAndroid()">
                        <i class="icon_emot_photo_video icon_video"></i><span class="layui-btn layui-btn-xs">上传</span>
                    </a>
                </dd>
            </dl>
            <div style="margin-left: 240px">
                <input type="text" class="input-txt" id="Android_url" name="Android_url" value="<?php echo $info['Android_url']; ?>"/>
            </div>
            <dl class="row">
                <dt class="tit">
                    <label><em></em>pc_url：</label>
                </dt>
                <dd class="bot" style="margin-left: 240px;margin-top: -25px;">
                    <div id="pcTotalBar" style="float:left;width:20%;height:20px;border:1px solid;border-radius:3px">
                        <div id="pcTotalBarColor" style="width:0%;border:0;background:#00b7ee; color: #FFF;height:20px;"></div>
                        <span class="pc_speed"></span>
                    </div>
                    <a id="pc_video" class="item" onclick="selectPc()">
                        <i class="icon_emot_photo_video icon_video"></i><span class="layui-btn layui-btn-xs">选择资源</span>
                    </a>
                    <input type="file" name="file" value="" class="upload" id="pc-file"
                           style="display: none" onchange="upFile(this)">
                    <br/>
                    <br/>
                    <a class="pc-item" onclick="uploadPc()">
                        <i class="icon_emot_photo_video icon_video"></i><span class="layui-btn layui-btn-xs">上传</span>
                    </a>
                </dd>
            </dl>
            <div style="margin-left: 240px">
                <input type="text" class="input-txt" id="pc_url" name="pc_url" value="<?php echo $info['pc_url']; ?>"/>
            </div>
            <dl class="row">
                <dt class="tit">
                    <label><em>*</em>方案风格：</label>
                </dt>
                <dd class="opt">
                    <select name="style_id" class="input-txt validate[required]">
                        <?php if(is_array($style) || $style instanceof \think\Collection || $style instanceof \think\Paginator): if( count($style)==0 ) : echo "" ;else: foreach($style as $key=>$vo): ?>
                            <option value="<?php echo $vo['id']; ?>"
                            <?php if($info['style_id'] == $vo['id']): ?>selected<?php endif; ?>
                            ><?php echo $vo['name']; ?></option>
                        <?php endforeach; endif; else: echo "" ;endif; ?>
                    </select>
                    <span class="err" id="err_style_id"></span>
                    <p class="notic"></p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label>总览图片</label>
                </dt>
                <dd class="opt">
                    <div class="tab-pane">
                        <table class="table table-bordered">
                            <tbody>
                            <tr>
                                <td>
                                    <div class="list images" id="over_image">
                                        <?php if(is_array($info['over_images']) || $info['over_images'] instanceof \think\Collection || $info['over_images'] instanceof \think\Paginator): $i = 0; $__LIST__ = $info['over_images'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$image): $mod = ($i % 2 );++$i;?>
                                            <div style="width:80px; text-align:center; margin: 5px; display:inline-block;"
                                                 class="goods_xc grid-square">
                                                <input type="hidden" value="<?php echo $image; ?>" name="over_images[]">
                                                <img style="width:80px;height:80px;object-fit:cover" src="<?php echo $image; ?>">
                                                <div class="fun-line"><a href="javascript:void(0)"
                                                                         onClick="ClearPicArr2(this,'<?php echo $image; ?>')"></a>
                                                </div>
                                            </div>
                                        <?php endforeach; endif; else: echo "" ;endif; ?>
                                    </div>
                                    <div class="goods_xc"
                                         style="width:80px; text-align:center; margin: 5px; display:inline-block;">
                                        <a href="javascript:void(0);"
                                           onClick="GetUploadify(5,'','over_i','over_images_call_back');"><img
                                                src="/public/images/add-button.jpg" width="80" height="80"/></a>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td><p style="color:#AAA">请上传图片格式文件，建议图片尺寸800*800像素，且不超过5张。</p></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label>总览描述：</label>
                </dt>
                <dd class="opt">
                    <textarea type="text" class="" style="width: 300px;height: 200px;resize: none !important;"
                              id="over_content" name="over_content"><?php echo $info['over_content']; ?></textarea>
                    <span class="err" id="err_over_content"></span>
                    <p class="notic"></p>
                </dd>
            </dl>
            <dl class="row">
                <dt class="tit">
                    <label><em></em>房间</label>
                </dt>
                <dd class="opt">
                    <?php if(is_array($room) || $room instanceof \think\Collection || $room instanceof \think\Paginator): if( count($room)==0 ) : echo "" ;else: foreach($room as $key=>$vo): ?>
                        <label class="typelable"><input class="room_ids" type="checkbox" data-name="<?php echo $vo['name']; ?>"
                                                        value="<?php echo $vo['id']; ?>"
                            <?php if(in_array(($vo['id']), is_array($info['room_ids'])?$info['room_ids']:explode(',',$info['room_ids']))): ?>checked<?php endif; ?>
                            /> <?php echo $vo['name']; ?></label>
                    <?php endforeach; endif; else: echo "" ;endif; ?>
                    <span class="err" id="err_room"></span>
                    <p class="notic"></p>
                </dd>
            </dl>
            <div id="room">
                <?php if(is_array($info['room']) || $info['room'] instanceof \think\Collection || $info['room'] instanceof \think\Paginator): if( count($info['room'])==0 ) : echo "" ;else: foreach($info['room'] as $key=>$vo): ?>
                    <dl class="row">
                        <dt class="tit">
                            <label><?php echo $vo['name']; ?>图片</label>
                        </dt>
                        <dd class="opt">
                            <div class="tab-pane">
                                <table class="table table-bordered">
                                    <tbody>
                                    <tr>
                                        <td>
                                            <div class="list images" id="room_images<?php echo $vo['room_id']; ?>">

                                                <?php if(is_array($vo['images']) || $vo['images'] instanceof \think\Collection || $vo['images'] instanceof \think\Paginator): $i = 0; $__LIST__ = $vo['images'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$image): $mod = ($i % 2 );++$i;?>
                                                    <div style="width:80px; text-align:center; margin: 5px; display:inline-block;"
                                                         class="goods_xc grid-square">
                                                        <input type="hidden" value="<?php echo $image; ?>"
                                                               name="room[<?php echo $vo['room_id']; ?>][images][]">
                                                        <img style="width:80px;height:80px;object-fit:cover"
                                                             src="<?php echo $image; ?>">
                                                        <div class="fun-line"><a href="javascript:void(0)"
                                                                                 onClick="ClearPicArr2(this,'<?php echo $image; ?>')"></a>
                                                        </div>
                                                    </div>
                                                <?php endforeach; endif; else: echo "" ;endif; ?>
                                            </div>
                                            <div class="goods_xc"
                                                 style="width:80px; text-align:center; margin: 5px; display:inline-block;">
                                                <a href="javascript:void(0);"
                                                   onClick="GetUploadify(5,'','over_i','room_images<?php echo $vo['room_id']; ?>_call_back');"><img
                                                        src="/public/images/add-button.jpg" width="80" height="80"/></a>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><p style="color:#AAA">请上传图片格式文件，建议图片尺寸800*800像素，且不超过5张。</p></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </dd>
                    </dl>
                    <dl class="row">
                        <dt class="tit">
                            <label><?php echo $vo['name']; ?>描述：</label>
                        </dt>
                        <dd class="opt">
                            <input type="hidden" name="room[<?php echo $vo['room_id']; ?>][room_id]" value="<?php echo $vo['room_id']; ?>">
                            <textarea type="text" class="" style="width: 300px;height: 200px;resize: none !important;"
                                      name="room[<?php echo $vo['room_id']; ?>][content]"><?php echo $vo['content']; ?></textarea>
                            <span class="err"></span>
                            <p class="notic"></p>
                        </dd>
                    </dl>
                <?php endforeach; endif; else: echo "" ;endif; ?>
            </div>
        </div>
        <div class="ncap-form-default">
            <div class="bot">
                <input type="submit" id="submit" class="dysubmit" value="确认提交">
            </div>
        </div>
    </form>
</div>
<script src="/public/js/global.js"></script>
<script src="https://unpkg.com/qiniu-js@2.5.4/dist/qiniu.min.js"></script>
<div id="goTop">
    <a href="JavaScript:void(0);" id="btntop"><i class="fa fa-angle-up"></i></a>
    <a href="JavaScript:void(0);" id="btnbottom"><i class="fa fa-angle-down"></i></a>
</div>
<script>
    /* 上传图片 */
    isupload = false;
    function dykjUpload(fileInput){
        if(isupload != false){
            layer.msg("其它文件正在上传，请稍后...");
        }else{
            $(fileInput).click();
        }
    }

    function uploadImg(fileInput,target,imgpath,dytypes){
        var loading = layer.msg('图片上传中，请稍等...', {
				icon: 16,offset: '300px',shade: 0.2
			});
        var filename = $(fileInput).val();
        if(filename != '' && filename != null){
            isupload = true;
            var pic = $(fileInput)[0].files[0];
            var dyformdata = new FormData();
            dyformdata.append('dypic', pic);
            dyformdata.append('imgpath', imgpath);
            $.ajax({
                url:"<?php echo url('Index/dyupload'); ?>",
                type:"post",
                dataType:'json',
                data: dyformdata,
                cache: false,
                contentType: false,
                processData: false,
                beforeSend:function(){
                    //图片上传中
                },
                success:function(data){
                    if(data.errcode == 1){
                        layer.msg(data.message,{icon:1,time:500});
                        if(target){
                            $(target).val(data.data);
                        }
                        if(dytypes == 1){
                            $(fileInput).parents('.input-file-show').find(".img_a").attr('href', data.data);
                            $(fileInput).parents('.input-file-show').find(".img_b").val(data.data);
                            $(fileInput).parents('.input-file-show').find(".img_i").attr('onmouseover', "layer.tips('<img src=" + data.data + " width=192>',this,{tips: [1, '#fff']});");
                        }
                    }else{
                        layer.msg(data.message,{icon:2});
                    }
                    layer.close(loading);
                },
                error:function (){
                    layer.msg("上传出错了");
                }
            });
            isupload = false;
        }
        isupload = false;
        layer.close(loading);
    }

    function uploadVideo(fileInput,target,imgpath,dytypes){
        var loading = layer.msg('视频上传中，请稍等...', {
				icon: 16,offset: '300px',shade: 0.2
			});
        var filename = $(fileInput).val();
        if(filename != '' && filename != null){
            isupload = true;
            var video = $(fileInput)[0].files[0];
            var dyformdata = new FormData();
            dyformdata.append('dyvideo', video);
            dyformdata.append('imgpath', imgpath);
            $.ajax({
                url:"<?php echo url('Index/dyuploadVideo'); ?>",
                type:"post",
                dataType:'json',
                data: dyformdata,
                cache: false,
                contentType: false,
                processData: false,
                beforeSend:function(){
                    //图片上传中
                },
                success:function(data){
                    if(data.errcode == 1){
                        layer.close(loading);
                        layer.msg(data.message,{icon:1,time:500});
                        $(target).val(data.data);
                        if(dytypes == 1){
                            if (data.data) {
                                $("#video-button").show();
                                $("#videotext").val(data.data);
                                $("#video_a").attr('href', data.data);
                            }
                        }
                    }else{
                        layer.msg(data.message,{icon:2});
                        layer.close(loading);
                    }
                },
                error:function (){
                    layer.msg("上传出错了");
                }
            });
            isupload = false;
        }
        isupload = false;
        //layer.close(loading);
    }
</script>
<link href="/public/static/js/validation/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/public/static/js/validation/jquery.validationEngine.js"></script>
<script type="text/javascript" src="/public/static/js/validation/jquery.validationEngine-zh_CN.js"></script>
<script src="/public/js/sortable.js"></script>
<script type="text/javascript">
    function selectIos(){
        console.log('ios')
        $('#ios-file').click()
    }
    function selectAndroid(){
        console.log('Android')
        $('#Android-file').click()
    }
    function selectPc(){
        console.log('pc')
        $('#pc-file').click()
    }

    function ClearPicArr2(obj, path) {
        $(obj).parents(".grid-square").remove();
    }

    //上传图片回调事件
    function over_images_call_back(paths) {
        for (var i = 0; i < paths.length; i++) {
            let src = paths[i];
            let last_div = `<div style="width:80px; text-align:center; margin: 5px; display:inline-block;" class="goods_xc grid-square">
            <input type="hidden" value="${src}" name="over_images[]">
            <img style="width:80px;height:80px;object-fit:cover" src="${src}" >
            <div class="fun-line">
              <a href="javascript:" data-src="${src}" onclick="ClearPicArr2(this,'${src}',0)"></a>
            </div>
          </div>`;
            $("#over_image").append(last_div);
        }
    }

    //上传图片回调事件
    function images_call_back(paths) {
        for (var i = 0; i < paths.length; i++) {
            let src = paths[i];
            let last_div = `<div style="width:80px; text-align:center; margin: 5px; display:inline-block;" class="goods_xc grid-square">
            <input type="hidden" value="${src}" name="images[]">
            <img style="width:80px;height:80px;object-fit:cover" src="${src}" >
            <div class="fun-line">
              <a href="javascript:" data-src="${src}" onclick="ClearPicArr2(this,'${src}',0)"></a>
            </div>
          </div>`;
            $("#images").append(last_div);
        }
    }

    $(function () {
        var estate_id = "<?php echo $info['estate_id']; ?>"
        var house_type_id = "<?php echo $info['house_type_id']; ?>"
        gethousetype(estate_id, house_type_id)
    })

    function gethousetype(estate_id, house_type_id = '') {
        var url = "<?php echo url('Api/gethousetype'); ?>";
        $.ajax({
            type: "GET",
            url: url,
            data: {id: estate_id},
            dataType: 'json',
            success: function (v) {
                if (v.errcode == 1) {
                    var html = '';
                    if (v.data) {
                        $.each(v.data, function (i, v) {
                            if (house_type_id && v.id == house_type_id) {
                                html += '<label class="typelable"><input type="radio" name="house_type_id" value="' + v.id + '" checked /> ' + v.name + '</label>';
                            } else {
                                html += '<label class="typelable"><input type="radio" name="house_type_id" value="' + v.id + '" />' + v.name + '</label>';
                            }
                        });
                    }
                    $('.house_type').html(html);
                }
            }, error: function (request) {
                alert("服务器繁忙, 请联系管理员!");
            },
        });
    }

    $('#estate_id').change(function () {
        var estate_id = $(this).val();
        gethousetype(estate_id)
    })

    function showloading(t) {
        if (t == true) {
            var loading = layer.msg('数据提交中...', {
                icon: 16, offset: '300px', shade: 0.2
            });
        } else {
            layer.closeAll(loading);
        }
    }

    $(function () {
        $('#ajaxform').validationEngine('attach', {
            promptPosition: "centerRight",
            autoPositionUpdate: true,
            showOneMessage: true,
            'custom_error_messages': {
                '#name': {
                    'required': {
                        'message': '* 请输入户型名称！'
                    }
                },
                '#intro': {
                    'required': {
                        'message': '* 请输入简介！'
                    }
                }
            },
            onValidationComplete: function (form, status) {
                var ios_url = $('#ios_url').val()
                console.log(ios_url)
                var Android_url = $('#Android_url').val()
                var pc_url = $('#pc_url').val()
                // if (!ios_url && !Android_url && !pc_url) {
                //     layer.msg("请先上传文件", {icon: 2});
                //     return false;
                // }
                //表单校验成功并提交
                if (status == true) {
                    $('span.err').hide();
                    $('#submit').attr('disabled', true);
                    console.log($("#ajaxform").serialize())
                    $.ajax({
                        type: "POST",
                        url: "<?php echo url('Scene/housetypeHandle'); ?>",
                        data: $("#ajaxform").serialize(),
                        async: false,
                        dataType: "json",
                        error: function () {
                            showloading(false);
                            layer.alert("服务器繁忙, 请联系管理员!", {title: '温馨提示', closeBtn: 0, icon: 2});
                            $('#submit').attr('disabled', false);
                        },
                        beforeSend: function () {
                            showloading(true);
                        },
                        success: function (data) {
                            showloading(false);
                            if (data.errcode == 1) {
                                layer.msg(data.message, {icon: 1, time: 1000}, function () {
                                    location.href = "<?php echo url('Estate/estateList'); ?>";
                                });
                            } else {
                                $('#submit').attr('disabled', false);
                                $('span.err').text('').show();
                                if (data.data) {
                                    $.each(data.data, function (index, item) {
                                        $('#err_' + index).text(item).show();
                                    });
                                }
                                layer.msg(data.message, {icon: 2, time: 1000});
                            }
                        }
                    })
                }
            }
        });
    })

    $('.room_ids').click(function () {
        set_room();
    })

    function set_room() {
        $('#room').html('');
        var i = 1;
        $('.room_ids:checked').each(function () {
            var id = $(this).val();
            var name = $(this).data('name');
            var str = '<dl class="row">\n' +
                '        <dt class="tit">\n' +
                '          <label>' + name + '图片</label>\n' +
                '        </dt>\n' +
                '        <dd class="opt">\n' +
                '          <div class="tab-pane">\n' +
                '            <table class="table table-bordered">\n' +
                '              <tbody>\n' +
                '              <tr>\n' +
                '                <td>\n' +
                '                  <div class="list images" id="room_images' + id + '">\n' +
                '                  </div>\n' +
                '                  <div class="goods_xc" style="width:80px; text-align:center; margin: 5px; display:inline-block;">\n' +
                '                    <a href="javascript:void(0);" onClick="GetUploadify(5,\'\',\'over_i\',\'room_images' + id + '_call_back\');"><img src="/public/images/add-button.jpg" width="80" height="80" /></a>\n' +
                '                  </div>\n' +
                '                </td>\n' +
                '              </tr>\n' +
                '              <tr>\n' +
                '                <td><p style="color:#AAA">请上传图片格式文件，建议图片尺寸800*800像素，且不超过5张。</p></td>\n' +
                '              </tr>\n' +
                '              </tbody>\n' +
                '            </table>\n' +
                '          </div>\n' +
                '        </dd>\n' +
                '      </dl>';
            str += '<dl class="row">\n' +
                '        <dt class="tit">\n' +
                '          <label>' + name + '描述：</label>\n' +
                '        </dt>\n' +
                '        <dd class="opt">\n' +
                '          <input type="hidden" name="room[' + id + '][room_id]" value="' + id + '">\n' +
                '          <textarea type="text" class="" style="width: 300px;height: 200px;resize: none !important;" name="room[' + id + '][content]" ></textarea>\n' +
                '          <span class="err"></span>\n' +
                '          <p class="notic"></p>\n' +
                '        </dd>\n' +
                '      </dl>'
            $('#room').append(str);
        })
    }

    <?php if(is_array($room) || $room instanceof \think\Collection || $room instanceof \think\Paginator): if( count($room)==0 ) : echo "" ;else: foreach($room as $key=>$vo): ?>
        function room_images<?php echo $vo['id']; ?>_call_back(paths) {
        for (var i=0;i<paths.length ;i++ ){
        let src = paths[i];
        let last_div = `<div style="width:80px; text-align:center; margin: 5px; display:inline-block;" class="goods_xc grid-square">
            <input type="hidden" value="${src}" name="room[<?php echo $vo['id']; ?>][images][]">
            <img style="width:80px;height:80px;object-fit:cover" src="${src}" >
            <div class="fun-line">
              <a href="javascript:" data-src="${src}" onclick="ClearPicArr2(this,'${src}',0)"></a>
            </div>
          </div>`;
        $("#room_images<?php echo $vo['id']; ?>").append(last_div);
    }
    }
    <?php endforeach; endif; else: echo "" ;endif; ?>

    function file_url_call_back(fileurl_tmp) {
        $("#file_url").val(fileurl_tmp);
    }

    $('#ios_video').click(function (){
        $('#file_ios').click()
    })
    $('#Android_video').click(function (){
        $('#file_Android').click()
    })
    $('#pc_video').click(function (){
        $('#file_pc').click()
    })
    $("#file_ios").change(function () {
        var loading = layer.msg('上传中...', {
            icon: 16, offset: '300px', shade: 0.2
        });
        var formData = new FormData();
        formData.append('file', $('#file_ios')[0].files[0]);
        console.log($('#file_ios')[0].files[0])
        $.ajax({
            url: "<?php echo url('Estate/uploadQiniuIos'); ?>",
            type: "post",
            dataType: 'json',
            data: formData,
            cache: false,
            contentType:false,
            processData: false,
            success: function (ios) {
                console.log(ios)
                layer.close(loading)
                if (ios.errcode == 1) {
                    layer.msg('上传成功', {icon: 1, time: 2000}, function () {
                        // alert('上传成功');
                        $('#ios_url').val(ios.data)
                        $('#ios_url_file').append('value',ios.data)
                        console.log($('#ios_url').val())
                    });
                } else {
                    console.log(ios)
                    layer.msg(ios.message, {icon: 2});
                }
            },
            error: function () {
                layer.close(loading)
                layer.msg("上传出错了");
            }
        });
    });
    $("#file_Android").change(function () {
        var loading = layer.msg('上传中...', {
            icon: 16, offset: '300px', shade: 0.2
        });
        var formData = new FormData();
        formData.append('file', $('#file_Android')[0].files[0]);
        console.log($('#file_Android')[0].files[0])
        $.ajax({
            url: "<?php echo url('Estate/uploadQiniuAndroid'); ?>",
            type: "post",
            dataType: 'json',
            data: formData,
            cache: false,
            contentType:false,
            processData: false,
            success: function (Android) {
                console.log(Android)
                layer.close(loading)
                if (Android.errcode == 1) {
                    layer.msg('上传成功', {icon: 1, time: 2000}, function () {
                        // alert('上传成功');
                        $('#Android_url').val(Android.data)
                        $('#Android_url_file').append('value',Android.data)
                        console.log($('#Android_url').val())
                    });
                } else {
                    console.log(Android)
                    layer.msg(Android.message, {icon: 2});
                }
            },
            error: function () {
                layer.close(loading)
                layer.msg("上传出错了");
            }
        });
    });
    $("#file_pc").change(function () {
        var loading = layer.msg('上传中...', {
            icon: 16, offset: '300px', shade: 0.2
        });
        var formData = new FormData();
        formData.append('file', $('#file_pc')[0].files[0]);
        console.log($('#file_pc')[0].files[0])
        $.ajax({
            url: "<?php echo url('Estate/uploadQiniuPc'); ?>",
            type: "post",
            dataType: 'json',
            data: formData,
            cache: false,
            contentType:false,
            processData: false,
            success: function (pc) {
                console.log(pc)
                layer.close(loading)
                if (pc.errcode == 1) {
                    layer.msg('上传成功', {icon: 1, time: 2000}, function () {
                        // alert('上传成功');
                        $('#pc_url').val(pc.data)
                        $('#pc_url_file').append('value',pc.data)
                        console.log($('#pc_url').val())
                    });
                } else {
                    console.log(pc)
                    layer.msg(result.message, {icon: 2});
                }
            },
            error: function () {
                layer.close(loading)
                layer.msg("上传出错了");
            }
        });
    });


</script>
<script type="text/javascript">
    $('#submitForm').on('click', function(e){
        // 阻止默认表单提交
        e.preventDefault();
        var myfile = $('#myFile')[0].files[0];
        //获取文件后缀
        var ext = myfile.name.split('.')[1];
        // 定义文件标识符
        var unique_tag = getFileIdentifier(myfile);
        console.log('当前文件的唯一标识:'+unique_tag);
        // 数据切片
        var chunks = fileSlice(myfile);
        // 发送分割数据段
        sendChunk(unique_tag, chunks,ext);
    })

    function getFileIdentifier(file){
        // 获取文件标识符 计算式方式: md5(文件大小+文件名字[包含后缀])
        return md5(file.size + file.name);
    }

    function fileSlice(file, chunkSize = 1024*1024*2){
        // 1.初始化数据
        var totalSize = file.size;
        var start = 0;
        var end = start + chunkSize;
        var chunks = [];
        // 2.使用bolb提供的slice方法切片
        while(start < totalSize){
            var chunk = file.slice(start, end);
            chunks.push(chunk);
            start = end;
            end += chunkSize;
        }
        // 3.返回切片组chunk[]
        return chunks;
    }

    function sendChunk(unique_tag, chunks,ext){
        // 逐个提交
        // 用于保证ajax发送完毕
        var task = [];
        chunks.forEach(function(chunk, index){
            var formData = new FormData();
            //唯一标识
            formData.append('unique_tag', unique_tag);
            //文件
            formData.append('file', chunk);
            //片的总数
            formData.append('total_num', chunks.length);
            //当前传输的片数
            formData.append('now_num', index+1);
            //文件的后缀
            formData.append('ext', ext);
            $.ajax({
                type: "POST",
                url: "<?php echo url('System/chunkUpload'); ?>",
                data: formData,
                contentType: false,
                processData: false,
                success: function(done){
                    // 移除已完成任务
                    task.pop();
                    if (task.length === 0) {
                        //已完成进行相关的逻辑处理
                        alert("切片上传文件完成,url为:"+done.data_no_aes.url);
                    }
                }
            })
            task.push('file Working');
        })
    }
</script>
<!--上传ios文件-->
<script>
    var subObject;
    var file;
    //定义上传配置  大于 4M 时可分块上传，小于 4M 时直传
    var config = {
        useCdnDomain: true,     //是否使用 cdn 加速域名
        region: qiniu.region.z0,    //上传域名区域 华东空间z0,华南z2，华北z1
        retryCount: 6,      //上传自动重试次数  默认3次
    };

    //定义putExtra上传限制
    var putExtra = {
        fname: "",  //文件原始文件名
        params: { 'x:flag': 'qiniu'},    //自定义变量
        // mimeType: ["pak"]     //指定所传的文件类型
    };

    var compareChunks = [];
    var observable;
    var subscription;

    function upFile(node) {
        var fileURL = "";
        try{
            file = null;
            if(node.files && node.files[0] ){
                file = node.files[0];
            }else if(node.files && node.files.item(0)) {
                file = node.files.item(0);
            }
        }catch(e){

        }
        creatFile(fileURL,file.name);
        return fileURL;
    }

    // 文件名
    function creatFile(fileURL,fileName){
        console.log(fileName)
        $("#iosTotalBarColor").css("width","0%");
    }

    // 上传操作
    function uploadIos() {
        // 设置next,error,complete对应的操作，分别处理相应的进度信息，错误信息，以及完成后的操作
        subObject = {
            next: ios_next,
            error: ios_error,
            complete: ios_complete
        };
        $.ajax({
            url:"<?php echo url('Scene/getToken'); ?>",
            success(result){
                $.token = result
            }
        })　//需要从后端获取
        //因为文件上传七牛云后文件名相同的会覆盖原，所以给文件重新命名防止覆盖原文件  file.name 是原文件名
        var date = new Date();
        var filename = date.getTime()+'_'+file.name
        //上传
        observable = qiniu.upload(file, filename, $.token, putExtra, config);
        // 调用sdk上传接口获得相应的observable，控制上传和暂停
        subscription = observable.subscribe(subObject);
    }

    // 暂停操作
    function filepause(){
        subscription.unsubscribe();
    }

    //分片上传返回response，标记上传进度
    var ios_next = function(response) {
        var chunks = response.chunks||[];
        var total = response.total;
        $(".ios_speed").text("进度：" + Math.floor(total.percent) + "% ");  //进度向下取整
        $("#iosTotalBarColor").css("width", total.percent + "%");
        compareChunks = chunks;
    };
    // 错误信息
    var ios_error = function(err) {
        alert('token失效，请重新上传');
    };
    // 上传完成
    var ios_complete = function(res) {
        // 返回文件信息
        layer.msg('上传成功', {icon: 1, time: 2000}, function () {
            // alert('上传成功');
            $('#ios_url').val('http://qiniu.dreamhouses.com.cn/'+res.key)
            $('#ios_url').append('value','http://qiniu.dreamhouses.com.cn/'+res.key)
            console.log($('#ios_url').val());
        });
        console.log('http://qiniu.dreamhouses.com.cn/'+res.key);

    };
</script>
<!--上传Android文件-->
<script>
    var subObject;
    var file;
    //定义上传配置  大于 4M 时可分块上传，小于 4M 时直传
    var config = {
        useCdnDomain: true,     //是否使用 cdn 加速域名
        region: qiniu.region.z0,    //上传域名区域 华东空间z0,华南z2，华北z1
        retryCount: 6,      //上传自动重试次数  默认3次
    };

    //定义putExtra上传限制
    var putExtra = {
        fname: "",  //文件原始文件名
        params: { 'x:flag': 'qiniu'},    //自定义变量
        // mimeType: ["pak"]     //指定所传的文件类型
    };

    var compareChunks = [];
    var observable;
    var subscription;

    function upFile(node) {
        var fileURL = "";
        try{
            file = null;
            if(node.files && node.files[0] ){
                file = node.files[0];
            }else if(node.files && node.files.item(0)) {
                file = node.files.item(0);
            }
        }catch(e){

        }
        creatFile(fileURL,file.name);
        return fileURL;
    }

    // 文件名
    function creatFile(fileURL,fileName){
        console.log(fileName)
        $("#AndroidTotalBarColor").css("width","0%");
    }

    // 上传操作
    function uploadAndroid() {
        // 设置next,error,complete对应的操作，分别处理相应的进度信息，错误信息，以及完成后的操作
        subObject = {
            next: Android_next,
            error: Android_error,
            complete: Android_complete
        };
        $.ajax({
            url:"<?php echo url('Scene/getToken'); ?>",
            success(result){
                $.token = result
            }
        })　//需要从后端获取
        //因为文件上传七牛云后文件名相同的会覆盖原，所以给文件重新命名防止覆盖原文件  file.name 是原文件名
        var date = new Date();
        var filename = date.getTime()+'_'+file.name
        //上传
        observable = qiniu.upload(file, filename, $.token, putExtra, config);
        console.log(observable)
        // 调用sdk上传接口获得相应的observable，控制上传和暂停
        subscription = observable.subscribe(subObject);
    }

    // 暂停操作
    function filepause(){
        subscription.unsubscribe();
    }

    //分片上传返回response，标记上传进度
    var Android_next = function(response) {
        var chunks = response.chunks||[];
        var total = response.total;
        $(".Android_speed").text("进度：" + Math.floor(total.percent) + "% ");  //进度向下取整
        $("#AndroidTotalBarColor").css("width", total.percent + "%");
        compareChunks = chunks;
    };
    // 错误信息
    var Android_error = function(err) {
        alert('token失效，请重新上传');
    };
    // 上传完成
    var Android_complete = function(res) {
        // 返回文件信息
        layer.msg('上传成功', {icon: 1, time: 2000}, function () {
            // alert('上传成功');
            $('#Android_url').val('http://qiniu.dreamhouses.com.cn/'+res.key)
            $('#Android_url').append('value','http://qiniu.dreamhouses.com.cn/'+res.key)
            console.log($('#Android_url').val());
        });
        console.log('http://qiniu.dreamhouses.com.cn/'+res.key);

    };
</script>
<!--上传pc文件-->
<script>
    var subObject;
    var file;
    //定义上传配置  大于 4M 时可分块上传，小于 4M 时直传
    var config = {
        useCdnDomain: true,     //是否使用 cdn 加速域名
        region: qiniu.region.z0,    //上传域名区域 华东空间z0,华南z2，华北z1
        retryCount: 6,      //上传自动重试次数  默认3次
    };

    //定义putExtra上传限制
    var putExtra = {
        fname: "",  //文件原始文件名
        params: { 'x:flag': 'qiniu'},    //自定义变量
        // mimeType: ["pak"]     //指定所传的文件类型
    };

    var compareChunks = [];
    var observable;
    var subscription;

    function upFile(node) {
        var fileURL = "";
        try{
            file = null;
            if(node.files && node.files[0] ){
                file = node.files[0];
            }else if(node.files && node.files.item(0)) {
                file = node.files.item(0);
            }
        }catch(e){

        }
        creatFile(fileURL,file.name);
        return fileURL;
    }

    // 文件名
    function creatFile(fileURL,fileName){
        console.log(fileName)
        $("#pcTotalBarColor").css("width","0%");
    }

    // 上传操作
    function uploadPc() {
        // 设置next,error,complete对应的操作，分别处理相应的进度信息，错误信息，以及完成后的操作
        subObject = {
            next: pc_next,
            error: pc_error,
            complete: pc_complete
        };
        $.ajax({
            url:"<?php echo url('Scene/getToken'); ?>",
            success(result){
                $.token = result
            }
        })　//需要从后端获取
        //因为文件上传七牛云后文件名相同的会覆盖原，所以给文件重新命名防止覆盖原文件  file.name 是原文件名
        var date = new Date();
        var filename = date.getTime()+'_'+file.name
        //上传
        observable = qiniu.upload(file, filename, $.token, putExtra, config);
        console.log(observable)
        // 调用sdk上传接口获得相应的observable，控制上传和暂停
        subscription = observable.subscribe(subObject);
    }

    // 暂停操作
    function filepause(){
        subscription.unsubscribe();
    }

    //分片上传返回response，标记上传进度
    var pc_next = function(response) {
        var chunks = response.chunks||[];
        var total = response.total;
        $(".pc_speed").text("进度：" + Math.floor(total.percent) + "% ");  //进度向下取整
        $("#pcTotalBarColor").css("width", total.percent + "%");
        compareChunks = chunks;
    };
    // 错误信息
    var pc_error = function(err) {
        alert('token失效，请重新上传');
    };
    // 上传完成
    var pc_complete = function(res) {
        // 返回文件信息
        layer.msg('上传成功', {icon: 1, time: 2000}, function () {
            // alert('上传成功');
            $('#pc_url').val('http://qiniu.dreamhouses.com.cn/'+res.key)
            $('#pc_url').append('value','http://qiniu.dreamhouses.com.cn/'+res.key)
            console.log($('#pc_url').val());
        });
        console.log('http://qiniu.dreamhouses.com.cn/'+res.key);

    };
</script>

</body>
</html>