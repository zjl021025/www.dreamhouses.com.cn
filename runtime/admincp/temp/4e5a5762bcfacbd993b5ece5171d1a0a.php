<?php /*a:4:{s:81:"F:\phpstudy_pro\WWW\www.dreamhouses.com.cn\app/admincp/dytpl/furniture/frock.html";i:1700470440;s:79:"F:\phpstudy_pro\WWW\www.dreamhouses.com.cn\app/admincp/dytpl/public/layout.html";i:1698049681;s:81:"F:\phpstudy_pro\WWW\www.dreamhouses.com.cn\app/admincp/dytpl/public/dyupload.html";i:1698049681;s:75:"F:\phpstudy_pro\WWW\www.dreamhouses.com.cn\app/admincp/dytpl/public/js.html";i:1698049681;}*/ ?>
<!doctype html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <!-- Apple devices fullscreen -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <!-- Apple devices fullscreen -->
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <link href="/public/static/css/main.css" rel="stylesheet" type="text/css">
    <link href="/public/static/css/list_optimization.css" rel="stylesheet" type="text/css">
    <link href="/public/static/css/page.css" rel="stylesheet" type="text/css">
    <link href="/public/static/font/css/font-awesome.min.css" rel="stylesheet" />
    <link href="/public/static/css/union.css" rel="stylesheet" />
    <link href="/public/js/layui/css/layui.css" rel="stylesheet" />
    <!--[if IE 7]>
    <link rel="stylesheet" href="/public/static/font/css/font-awesome-ie7.min.css">
    <![endif]-->
    <link href="/public/static/js/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
    <link href="/public/static/js/perfect-scrollbar.min.css" rel="stylesheet" type="text/css"/>
    <style type="text/css">
    html, body { overflow: visible;}
    .fine_layer{
		width: 100%;
		padding: 10px;
		box-sizing: border-box;
    }
	.fine_layer *{
		box-sizing: border-box;
    }
	.fine_layer textarea{
		width: 100%;
        height: 80px;
		border: 1px solid #e6e6e6;
        border-radius: 4px;
        padding: 5px 4px;
        resize: none!important;
    }
    .red{
        color: red !important;
    }
    .hDiv th div{
        text-align: center;
    }
    .bDiv td div{
        text-align: center;
    }
    </style>
    <script type="text/javascript" src="/public/static/js/jquery.js"></script>
    <script type="text/javascript" src="/public/static/js/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="/public/static/js/layer/layer.js"></script><!-- 弹窗js 参考文档 http://layer.layui.com/-->
    <script src="/public/js/layui/layui.js"></script>
    <script type="text/javascript" src="/public/static/js/admin.js"></script>
    <script type="text/javascript" src="/public/static/js/jquery.validation.min.js"></script>
    <script type="text/javascript" src="/public/static/js/common.js"></script>
    <script type="text/javascript" src="/public/static/js/perfect-scrollbar.min.js"></script>
    <script type="text/javascript" src="/public/static/js/jquery.mousewheel.js"></script>
    <script src="/public/js/myFormValidate.js"></script>
    <script src="/public/js/myAjax2.js"></script>
    <script src="/public/js/dyglobal.js"></script>
    <script type="text/javascript">
        function in_array(needle, haystack) {
            if(typeof needle == 'string' || typeof needle == 'number') {
                for(var i in haystack) {
                    if(haystack[i] == needle) {
                        return true;
                    }
                }
            }
            return false;
        }
        function checkEmtpy(obj,title) {
            var url = $(obj).attr('href')
            if(!url){
                if(!title){
                    title = '没有图片';
                }
                layer.msg(title)
                return false;
            }
            return true;
        }
        function checkPrice(obj){
            obj.value = obj.value.replace(/[^\d.]/g,"");  //清除“数字”和“.”以外的字符
            obj.value = obj.value.replace(/^\./g,"");  //验证第一个字符是数字而不是.
            obj.value = obj.value.replace(/\.{2,}/g,"."); //只保留第一个. 清除多余的.
            obj.value = obj.value.replace(".","$#$").replace(/\./g,"").replace("$#$",".");
            obj.value = obj.value.replace(/^(\d+)\.(\d\d).*$/,'$1.$2');
        }
        function checkNumber(obj){
            obj.value = obj.value.replace(/[^\d]/g,"");  //清除“数字”以外的字符
        }
    function is_show(obj) {
        var content = $(obj).data('name');
        layer.tips(content, obj, {
            tips: [1, "#4794ec"]
        });
    }
    function selectAll(name,obj){
        $('input[name*='+name+']').prop('checked', $(obj).checked);
    }
    //个人认证/企业认证/车辆认证-通过
    function auth_agree(obj)
    {
        var url = "<?php echo url('User/checkAuth'); ?>";
        var id = $(obj).data('id');
        var type = $(obj).data('type');
        layer.confirm('确认审核通过吗？',{title:'温馨提示',closeBtn:0,icon:3,btn: ['确定','取消'] }, function() {
            $.ajax({
                type : 'post',
                url : url,
                data : {act:'agree',id:id,type:type},
                dataType : 'json',
                success : function(data) {
                    layer.closeAll();
                    if (data.errcode == 1) {
                        layer.msg(data.message, {icon: 1, time: 2000},function(){
                            location.href = data.data.url;
                        });
                    } else {
                        layer.msg(data.message, {icon: 2,time: 2000});
                    }
                }
            })
        }, function(index){
            layer.close(index);
            return false;// 取消
        });
    }
    //-拒绝
    function auth_refuse(obj)
    {
        var url = "<?php echo url('User/checkAuth'); ?>";
        var id = $(obj).data('id');
        var type = $(obj).data('type');
        var html = `<div class="fine_layer">
            <textarea id="remark" maxlength="100" placeholder="请填写拒绝原因(100字内)"></textarea>
        </div>`;
		layer.open({
			type:1,
			title: '审核拒绝',
            content: html,
			area: ['400px', '210px'],
            closeBtn:0,
			btn:['确定', '取消'],
			yes:function(index){
				var content = $("#remark").val();
				/* if (content == "") {
					layer.msg('请填写拒绝原因', {icon: 2,time:2000});
					return false;
				} */
				$.ajax({
					type: 'POST',
					url: url,
					data : {id:id,remark:content,act:'refuse',type:type},
					dataType: 'json',
					success: function(data){
                        if (data.errcode == 1) {
                            layer.msg(data.message, {icon: 1, time: 2000},function(){
                                location.href = data.data.url;
                            });
                        } else {
                            layer.msg(data.message, {icon: 2,time: 2000});
                        }
					},
					error: function(){
						layer.alert("服务器繁忙, 请联系管理员!");
					}
				});
			}
		});
    }

    function delAll() {
        var ids = '';
        $('#flexigrid .trSelected').each(function(i,o) {
            ids += $(o).data('id')+',';
        })
        if (ids == '' || ids == '0,' || ids == '0') {
            layer.confirm('请选择删除项', {title:'温馨提示',closeBtn:0,icon:3});
            return;
        }
        layer.confirm('确认删除？',{title:'温馨提示',closeBtn:0,icon:3,btn: ['确定','取消'] }, function() {
            $.ajax({
                type : 'post',
                url : $('#flexigrid').data('url'),
                data : {act:'del',ids:ids},
                dataType : 'json',
                success : function(data) {
                    layer.closeAll();
                    if (data.errcode == 1) {
                        layer.msg('操作成功', {icon: 1});
                        $('#flexigrid .trSelected').each(function(i,o) {
                            $(o).remove();
                        })
                    } else {
                        layer.msg(data.message, {icon: 2,time: 2000});
                    }
                }
            })
        }, function(index){
            layer.close(index);
            return false;// 取消
        });
    }

    /**
     * 全选
     * @param obj
     */
    function checkAllSign(obj){
        $(obj).toggleClass('trSelected');
        if($(obj).hasClass('trSelected')){
            $('#flexigrid > table>tbody >tr').addClass('trSelected');
        }else{
            $('#flexigrid > table>tbody >tr').removeClass('trSelected');
        }
    }
    //判断是否全选
	function renderCheckAll(){
        var goodsLen = $('#flexigrid tr').length;
        var checkedLen = $('#flexigrid tr.trSelected').length;
        if(goodsLen == checkedLen){
          $('.check-all').addClass('trSelected')
        }else{
          $('.check-all').removeClass('trSelected')
        }
	}
    /**
     * 批量公共操作（删，改）
     * @returns {boolean}
     */
    function publicHandleAll(type){
        var ids = '';
        $('#flexigrid .trSelected').each(function(i,o){
            ids += $(o).data('id')+',';
        });
        if(ids == ''){
            layer.msg('至少选择一项', {icon: 2, time: 2000});
            return false;
        }
        publicHandle(ids,type); //调用删除函数
    }
    /**
     * 批量公共操作（删）新
     * @returns {boolean}
     */
    function publicUpdateAll(name,field_name){
        var ids = '';
        $('#flexigrid .trSelected').each(function(i,o){
            if(ids ==''){
                ids = $(o).data('id');
            }else{
                ids += ','+$(o).data('id');
            }
        });
        if(ids == ''){
            layer.msg('至少选择一项', {icon: 2, time: 2000});
            return false;
        }
        publicUpdate(ids,name,field_name); //调用删除函数
    }
    /**
     * 公共操作（删，改）
     * @param type
     * @returns {boolean}
     */
    function publicHandle(ids,handle_type){
        layer.confirm('确认当前操作？', {
            title:'温馨提示',closeBtn:0,icon:3,
                    btn: ['确定', '取消'] //按钮
                }, function () {
                    // 确定
                    $.ajax({
                        url: $('#flexigrid').data('url'),
                        type:'post',
                        data:{ids:ids,act:handle_type},
                        dataType:'JSON',
                        success: function (data) {
                            layer.closeAll();
                            if (data.errcode == 1){
                                layer.msg(data.message, {icon: 1, time: 2000},function(){
                                    location.href = data.dyurl;
                                });
                            }else{
                                layer.msg(data.message, {icon: 2, time: 2000});
                            }
                        }
                    });
                }, function (index) {
                    layer.close(index);
                }
        );
    }

        /**
         * 公共操作（删，改）新
         * @param type
         * @returns {boolean}
         */
        function publicUpdate(ids,name,field_name){
            layer.confirm('确认当前操作？', {
                title:'温馨提示',closeBtn:0,icon:3,
                        btn: ['确定', '取消'] //按钮
                    }, function () {
                        // 确定
                        $.ajax({
                            url: '/Admincp/System/deleteData',
                            type:'post',
                            data:{ids:ids,name:name,field_name:field_name},
                            dataType:'JSON',
                            success: function (data) {
                                layer.closeAll();
                                if (data.status == 1){
                                    layer.msg(data.msg, {icon: 1, time: 2000},function(){
                                        window.location.reload();
                                    });
                                }else{
                                    layer.msg(data.msg, {icon: 2, time: 2000});
                                }
                            }
                        });
                    }, function (index) {
                        layer.close(index);
                    }
            );
        }

        function delfuntion(obj) {
            // 删除按钮
            layer.confirm('确认刪除？', {
                title:'温馨提示',closeBtn:0,icon:3,
                btn: ['确定', '取消'] //按钮
            }, function () {
                $.ajax({
                    type: 'post',
                    url: '/Admincp/System/deleteData',
                    data : {ids:$(obj).attr('data-id'),name:$(obj).attr('data-name'),field_name:$(obj).attr('data-field_name')},
                    dataType: 'json',
                    success: function (data) {
                        layer.closeAll();
                        if (data.status == 1) {
                            $(obj).parent().parent().parent().remove();
                            layer.closeAll();
                        } else {
                            layer.alert(data, {icon: 2});  //alert('删除失败');
                        }
                    }
                })
            }, function () {
                layer.closeAll();
            });
        }
        function delfunc(obj,field='id') {
            var url = $(obj).data('url');
            // 删除按钮
            layer.confirm('确认刪除？', {
                title:'温馨提示',closeBtn:0,icon:3,
                btn: ['确定', '取消'] //按钮
            }, function () {
                var data = {}
                data[field] = $(obj).data('id')
                data['act'] = 'del'
                $.ajax({
                    type: 'post',
                    url: url,
                    data : data,
                    dataType: 'json',
                    success: function (data) {
                        layer.closeAll();
                        if (data.errcode == 1) {
                            layer.msg(data.message,{icon:1,time:2000},function(){
                                window.location.reload();
                            });
                        } else {
                            layer.msg(data.message, {icon: 2});  //alert('删除失败');
                        }
                    }
                })
            }, function () {
                layer.closeAll();
            });
        }
    </script>
</head>
<style>
    .fa-check-circle,.fa-ban{cursor:pointer}
</style>
<script src="https://unpkg.com/qiniu-js@2.5.4/dist/qiniu.min.js"></script>
<style type="text/css">
  .dysubmit{
    background-color: #148eff;
    border-color: #148eff;display:
          inline-block;
    text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.1);
    font: bold 14px/20px "microsoft yahei", arial;
    color:#FFFFFF;
    text-align: center;
    vertical-align: middle;
    display: inline-block;
    height: 36px;
    padding: 7px 19px;
    border-radius: 3px;
    cursor: pointer;
  }

  .btn-sm{
    background-color: #148eff;
    border-color: #148eff;
    color: #FFFFFF;
    text-align: center;
    vertical-align: middle;
    padding: 7px;
    border-radius: 3px;
    cursor: pointer;
  }
  .size,.material{
    margin: 10px;
  }
  .images{
    float: left;
  }
  .grid-square{
    position: relative;
  }
  .grid-square:hover .fun-line{
    height: 30px;
  }
  .fun-line{
    width: 62px;
    height: 0px;
    background-color: rgba(0,0,0,0.5);
    position: absolute;
    top: 0px;
    left: 0px;
    z-index: 9;
    padding: 0 9px;
    transition: height 0.3s;
    overflow: hidden;
  }
  .fun-line a{
    width: 17px;
    height: 15px;
    display: block;
    float: right;
    background-image: url("/public/plugins/webuploader/images/icons.png");
    background-position: -48px -25px;
    margin-top: 7px;
  }
  .fun-line a:hover{
    background-position: -48px -1px;
  }
</style>
<body style="background-color: #FFF; overflow: auto;">
<div id="toolTipLayer" style="position: absolute; z-index: 9999; display: none; visibility: visible; left: 95px; top: 573px;"></div>
<div id="append_parent"></div>
<div id="ajaxwaitid"></div>
<div class="page">
  <div class="fixed-bar">
    <div class="item-title"><a class="back" href="javascript:history.back();" title="返回列表"><i class="fa fa-arrow-circle-o-left"></i></a>
      <div class="subject">
        <h3>硬装管理 - <?php if($act == 'add'): ?>新增<?php else: ?>编辑<?php endif; ?>硬装管理</h3>
        <h5>硬装管理索引与管理</h5>
      </div>
    </div>
  </div>
  <form class="form-horizontal" id="ajaxform">
    <input type="hidden" name="id" value="<?php echo $info['id']; ?>">
    <input type="hidden" name="act" value="<?php echo $act; ?>">
    <input type="hidden" name="typeid" value="1">
    <div id="size" style="display: none">

    </div>
    <div id="material" style="display: none">

    </div>
    <div id="attr" style="display: none">

    </div>
    <div class="ncap-form-default tab_div_1">
      <dl class="row"><h3 class="updt-protit">基本信息</h3></dl>
      <dl class="row">
        <dt class="tit"><em>*</em><label>分类</label></dt>
        <dd class="opt">
          <div class="opt" style="float: left;">
            <select id="cate_id" class="select validate[required]" name="cate_id" style="width: 120px !important;height: 26px;">
              <option value="">选择一级分类</option>
              <?php if(is_array($category) || $category instanceof \think\Collection || $category instanceof \think\Paginator): if( count($category)==0 ) : echo "" ;else: foreach($category as $key=>$vo): ?>
                <option value="<?php echo $vo['id']; ?>" <?php if($info['cate_id'] == $vo['id']): ?>selected<?php endif; ?>><?php echo $vo['name']; ?></option>
              <?php endforeach; endif; else: echo "" ;endif; ?>
            </select>
          </div>
          <div class="opt" style="float: left;">
            <select id="two_cate_id" class="select validate[required]" name="two_cate_id" style="width: 120px !important;height: 26px;">
              <option value="">选择二级分类</option>
              <?php if(is_array($twocategory) || $twocategory instanceof \think\Collection || $twocategory instanceof \think\Paginator): if( count($twocategory)==0 ) : echo "" ;else: foreach($twocategory as $key=>$vo): ?>
                <option value="<?php echo $vo['id']; ?>" <?php if($info['two_cate_id'] == $vo['id']): ?>selected<?php endif; ?>><?php echo $vo['name']; ?></option>
              <?php endforeach; endif; else: echo "" ;endif; ?>
            </select>
          </div>
          <div class="opt" style="float: left;">
            <select id="three_cate_id" class="select validate[required]" name="three_cate_id" style="width: 120px !important;height: 26px;">
              <option value="">选择三级分类</option>
              <?php if(is_array($threecategory) || $threecategory instanceof \think\Collection || $threecategory instanceof \think\Paginator): if( count($threecategory)==0 ) : echo "" ;else: foreach($threecategory as $key=>$vo): ?>
                <option value="<?php echo $vo['id']; ?>" <?php if($info['three_cate_id'] == $vo['id']): ?>selected<?php endif; ?>><?php echo $vo['name']; ?></option>
              <?php endforeach; endif; else: echo "" ;endif; ?>
            </select>
          </div>
          <p class="notic"><span class="red"></span></p>
        </dd>
      </dl>
      <dl class="row">
        <dt class="tit">
          <label><em></em>品牌名称：</label>
        </dt>
        <dd class="opt">
          <input type="text" class="input-txt" id="brand_name" name="brand_name" value="<?php echo $info['brand_name']; ?>" />
          <span class="err" id="err_brand_name"></span>
          <p class="notic"></p>
        </dd>
      </dl>
      <dl class="row">
        <dt class="tit">
          <label><em>*</em>名称：</label>
        </dt>
        <dd class="opt">
          <input type="text" class="input-txt validate[required]" id="name" name="name" value="<?php echo $info['name']; ?>" />
          <span class="err" id="err_name"></span>
          <p class="notic"></p>
        </dd>
      </dl>
      <dl class="row">
        <dt class="tit">
          <label><em>*</em>文件名称：</label>
        </dt>
        <dd class="opt">
          <input type="text" class="input-txt validate[required]" id="filename" name="filename" value="<?php echo $info['filename']; ?>" />
          <span class="err" id="err_filename"></span>
          <p class="notic"></p>
        </dd>
      </dl>
      <dl class="row">
        <dt class="tit">
          <label><em>*</em>编号：</label>
        </dt>
        <dd class="opt">
          <input type="text" class="input-txt validate[required]" id="furniture_no" name="furniture_no" value="<?php echo $info['furniture_no']; ?>" />
          <span class="err" id="err_furniture_no"></span>
          <p class="notic"></p>
        </dd>
      </dl>
      <dl class="row">
        <dt class="tit">
          <label><em>*</em>价格：</label>
        </dt>
        <dd class="opt">
          <input type="text" class="input-txt validate[required]" id="price" name="price" value="<?php echo $info['price']; ?>" />
          <span class="err" id="err_price"></span>
          <p class="notic"></p>
        </dd>
      </dl>
<!--      <dl class="row">-->
<!--        <dt class="tit">-->
<!--          <label><em>*</em>封面：</label>-->
<!--        </dt>-->
<!--        <dd class="opt">-->
<!--          <div class="input-file-show">-->
<!--                        <span class="show">-->
<!--                                <a target="_blank" class="nyroModal img_a" rel="gal" href="<?php echo $info['image']; ?>">-->
<!--                                    <i class="fa fa-picture-o img_i"-->
<!--                                       onMouseOver="layer.tips('<img src=<?php echo $info['image']; ?> width=192>',this,{tips: [1, '#fff']});" onMouseOut="layer.closeAll();"></i>-->
<!--                                </a>-->
<!--                        </span>-->
<!--            <span class="type-file-box">-->
<!--                            <input type="text" id="image" name="image" value="<?php echo $info['image']; ?>" class="type-file-text">-->
<!--                            <input type="button" name="button" id="button1" value="选择上传..." class="type-file-button" onclick="dykjUpload('#dykj-image')">-->
<!--                            <input type="file" class="type-file-file" id="dykj-image" onchange="uploadImg('#dykj-image','#image','furniture','1');" size="30" accept="image/gif,image/jpeg,image/jpg,image/png" title="点击前方预览图可查看大图，点击按钮选择文件并提交表单后上传生效">-->
<!--                        </span>-->
<!--          </div>-->
<!--          <span class="err"></span>-->

<!--          <p class="notic">请上传图片格式文件，建议图片尺寸800*800像素</p>-->
<!--        </dd>-->
<!--      </dl>-->
      <dl class="row">
        <dt class="tit">
          <label><em>*</em>相册图</label>
        </dt>
        <dd class="opt">
          <div class="tab-pane">
            <table class="table table-bordered">
              <tbody>
              <tr>
                <td>
                  <div class="list images" id="images">
                    <?php if(is_array($info['images']) || $info['images'] instanceof \think\Collection || $info['images'] instanceof \think\Paginator): $i = 0; $__LIST__ = $info['images'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$image): $mod = ($i % 2 );++$i;?>
                      <div style="width:80px; text-align:center; margin: 5px; display:inline-block;" class="goods_xc grid-square">
                        <input type="hidden" value="<?php echo $image; ?>" name="images[]">
                        <img style="width:80px;height:80px;object-fit:cover" src="<?php echo $image; ?>">
                        <div class="fun-line"><a href="javascript:void(0)" onClick="ClearPicArr2(this,'<?php echo $image; ?>')"></a></div>
                      </div>
                    <?php endforeach; endif; else: echo "" ;endif; ?>
                  </div>
                  <div class="goods_xc" style="width:80px; text-align:center; margin: 5px; display:inline-block;">
                    <a href="javascript:void(0);" onClick="GetUploadify(10,'','furniture','images_call_back');"><img src="/public/images/add-button.jpg" width="80" height="80" /></a>
                  </div>
                </td>
              </tr>
              <tr>
                <td><p style="color:#AAA">请上传图片格式文件，建议图片尺寸800*800像素。</p></td>
              </tr>
              </tbody>
            </table>
          </div>
        </dd>
      </dl>
      <dl class="row">
        <dt class="tit">
          <label><em>*</em>ios_url：</label>
        </dt>
        <dd class="bot" style="margin-left: 240px;margin-top: -25px;">
          <div id="iosTotalBar" style="float:left;width:20%;height:20px;border:1px solid;border-radius:3px">
            <div id="iosTotalBarColor" style="width:0%;border:0;background:#00b7ee; color: #FFF;height:20px;"></div>
            <span class="ios_speed"></span>
          </div>
          <a id="ios_video" class="item" onclick="selectIos()">
            <i class="icon_emot_photo_video icon_video"></i><span class="layui-btn layui-btn-xs">选择资源</span>
          </a>
          <input type="file" name="file" value="" class="upload" id="ios-file" accept="application/x-zip-compressed"
                 style="display: none" onchange="upFile(this)">
          <br/>
          <br/>
          <a class="ios-item" onclick="uploadIos()">
            <i class="icon_emot_photo_video icon_video"></i><span class="layui-btn layui-btn-xs">上传</span>
          </a>
        </dd>
      </dl>
      <div style="margin-left: 240px">
        <input type="text" name="ios_url" id="ios_url" value="<?php echo $info['ios_url']; ?>">
      </div>
      <dl class="row">
        <dt class="tit">
          <label><em>*</em>Android_url：</label>
        </dt>
        <dd class="bot" style="margin-left: 240px;margin-top: -25px;">
          <div id="AndroidTotalBar" style="float:left;width:20%;height:20px;border:1px solid;border-radius:3px">
            <div id="AndroidTotalBarColor" style="width:0%;border:0;background:#00b7ee; color: #FFF;height:20px;"></div>
            <span class="Android_speed"></span>
          </div>
          <a id="Android_video" class="item" onclick="selectAndroid()">
            <i class="icon_emot_photo_video icon_video"></i><span class="layui-btn layui-btn-xs">选择资源</span>
          </a>
          <input type="file" name="file" value="" class="upload" id="Android-file" accept="application/x-zip-compressed"
                 style="display: none" onchange="upFile(this)">
          <br/>
          <br/>
          <a class="Android-item" onclick="uploadAndroid()">
            <i class="icon_emot_photo_video icon_video"></i><span class="layui-btn layui-btn-xs">上传</span>
          </a>
        </dd>
      </dl>
      <div style="margin-left: 240px">
        <input type="text" name="Android_url" id="Android_url" value="<?php echo $info['Android_url']; ?>">
      </div>
      <dl class="row">
        <dt class="tit">
          <label><em>*</em>pc_url：</label>
        </dt>
        <dd class="bot" style="margin-left: 240px;margin-top: -25px;">
          <div id="pcTotalBar" style="float:left;width:20%;height:20px;border:1px solid;border-radius:3px">
            <div id="pcTotalBarColor" style="width:0%;border:0;background:#00b7ee; color: #FFF;height:20px;"></div>
            <span class="pc_speed"></span>
          </div>
          <a id="pc_video" class="item" onclick="selectPc()">
            <i class="icon_emot_photo_video icon_video"></i><span class="layui-btn layui-btn-xs">选择资源</span>
          </a>
          <input type="file" name="file" value="" class="upload" id="pc-file" accept="application/x-zip-compressed"
                 style="display: none" onchange="upFile(this)">
          <br/>
          <br/>
          <a class="pc-item" onclick="uploadPc()">
            <i class="icon_emot_photo_video icon_video"></i><span class="layui-btn layui-btn-xs">上传</span>
          </a>
        </dd>
      </dl>
      <div style="margin-left: 240px">
        <input type="text" name="pc_url" id="pc_url" value="<?php echo $info['pc_url']; ?>">
      </div>
      <dl class="row">
        <dt class="tit">
          <label><em>*</em>尺寸：</label>
        </dt>
        <dd class="opt">
          <a href="javascript:;" class="btn btn-sm add_size">添加</a>
        </dd>
      </dl>
      <dl class="row" style="padding: 0">
        <dt class="tit">
          <label></label>
        </dt>
        <dd class="opt size_list">
          <?php if(!(empty($info['size']) || (($info['size'] instanceof \think\Collection || $info['size'] instanceof \think\Paginator ) && $info['size']->isEmpty()))): if(is_array($info['size']) || $info['size'] instanceof \think\Collection || $info['size'] instanceof \think\Paginator): if( count($info['size'])==0 ) : echo "" ;else: foreach($info['size'] as $key=>$vo): ?>
              <div class="size">
                长<input type="text" class="input-txt length" style="width: 80px!important;" value="<?php echo $vo['length']; ?>" />cm
                宽<input type="text" class="input-txt width" style="width: 80px!important;" value="<?php echo $vo['width']; ?>" />cm
                高<input type="text" class="input-txt height" style="width: 80px!important;" value="<?php echo $vo['height']; ?>" />cm
                <a href="javascript:;" class="btn btn-sm" onclick="del_size(this)">删除</a>
              </div>
            <?php endforeach; endif; else: echo "" ;endif; ?>
          <?php endif; ?>
        </dd>
      </dl>
      <dl class="row">
        <dt class="tit">
          <label><em>*</em>材料：</label>
        </dt>
        <dd class="opt">
          <a href="javascript:;" class="btn btn-sm add_material">添加</a>
        </dd>
      </dl>
      <dl class="row" style="padding: 0">
        <dt class="tit">
          <label></label>
        </dt>
        <dd class="opt material_list">
          <?php if(!(empty($info['material']) || (($info['material'] instanceof \think\Collection || $info['material'] instanceof \think\Paginator ) && $info['material']->isEmpty()))): if(is_array($info['material']) || $info['material'] instanceof \think\Collection || $info['material'] instanceof \think\Paginator): if( count($info['material'])==0 ) : echo "" ;else: foreach($info['material'] as $key=>$vo): ?>
              <div class="material">
                名称<input type="text" class="input-txt name" style="width: 80px!important;" value="<?php echo $vo['name']; ?>" />
                文件名称<input type="text" class="input-txt filename" style="width: 80px!important;" value="<?php echo $vo['filename']; ?>" />
                图片<div class="input-file-show">
                  <span class="show">
                      <a target="_blank" class="nyroModal img_a" rel="gal" href="<?php echo $vo['image']; ?>">
                          <i class="fa fa-picture-o img_i"
                             onMouseOver="layer.tips('<img src=<?php echo $vo['image']; ?> width=192>',this,{tips: [1, '#fff']});" onMouseOut="layer.closeAll();"></i>
                      </a>
                  </span>
                <span class="type-file-box">
                    <input type="text" value="<?php echo $vo['image']; ?>" class="type-file-text image img_b">
                      <input type="button" value="选择上传..." class="type-file-button" onclick="uploadImgBtn(this);">
                      <input type="file" class="type-file-file" onchange="uploadImg(this,'','furniture','1');" size="30" accept="image/gif,image/jpeg,image/jpg,image/png" title="点击前方预览图可查看大图，点击按钮选择文件并提交表单后上传生效">
                  </span>
              </div>
                <a href="javascript:;" class="btn btn-sm" onclick="del_material(this)">删除</a>
              </div>
            <?php endforeach; endif; else: echo "" ;endif; ?>
          <?php endif; ?>
        </dd>
      </dl>
      <dl class="row">
        <dt class="tit">
          <label><em></em>属性：</label>
        </dt>
        <dd class="opt">
          <a href="javascript:;" class="btn btn-sm add_attr">添加</a>
        </dd>
      </dl>
      <dl class="row" style="padding: 0">
        <dt class="tit">
          <label></label>
        </dt>
        <dd class="opt attr_list">
          <?php if(!(empty($info['attr']) || (($info['attr'] instanceof \think\Collection || $info['attr'] instanceof \think\Paginator ) && $info['attr']->isEmpty()))): if(is_array($info['attr']) || $info['attr'] instanceof \think\Collection || $info['attr'] instanceof \think\Paginator): if( count($info['attr'])==0 ) : echo "" ;else: foreach($info['attr'] as $key=>$vo): ?>
              <div class="attr">
                名称<input type="text" class="input-txt name" style="width: 80px!important;" value="<?php echo $vo['name']; ?>" />
                值<input type="text" class="input-txt value" style="width: 80px!important;" value="<?php echo $vo['value']; ?>" />
                <a href="javascript:;" class="btn btn-sm" onclick="del_attr(this)">删除</a>
              </div>
            <?php endforeach; endif; else: echo "" ;endif; ?>
          <?php endif; ?>
        </dd>
      </dl>
      <dl class="row">
        <dt class="tit">
          <label><em></em>排序：</label>
        </dt>
        <dd class="opt">
          <input type="text" class="input-txt" id="sort" name="sort" value="<?php echo $info['sort']; ?>" />
          <span class="err" id="err_sort"></span>
          <p class="notic"></p>
        </dd>
      </dl>
      <dl class="row">
        <dt class="tit">
          <label><em>*</em>显示：</label>
        </dt>
        <dd class="opt">
          <div class="onoff">
            <label for="article_show1" class="cb-enable <?php if($info['is_show'] == 1): ?>selected<?php endif; ?>">是</label>
            <label for="article_show0" class="cb-disable <?php if($info['is_show'] == 0): ?>selected<?php endif; ?>">否</label>
            <input id="article_show1" name="is_show" value="1" type="radio" <?php if($info['is_show'] == 1): ?> checked="checked"<?php endif; ?>>
            <input id="article_show0" name="is_show" value="0" type="radio" <?php if($info['is_show'] == 0): ?> checked="checked"<?php endif; ?>>
          </div>
          <p class="notic"></p>
        </dd>
      </dl>
    </div>
    <div class="ncap-form-default">
      <div class="bot">
        <input type="submit" id="submit" class="dysubmit" value="确认提交">
      </div>
    </div>
  </form>
</div>
<div id="goTop">
  <a href="JavaScript:void(0);" id="btntop"><i class="fa fa-angle-up"></i></a>
  <a href="JavaScript:void(0);" id="btnbottom"><i class="fa fa-angle-down"></i></a>
</div>
<script>
    /* 上传图片 */
    isupload = false;
    function dykjUpload(fileInput){
        if(isupload != false){
            layer.msg("其它文件正在上传，请稍后...");
        }else{
            $(fileInput).click();
        }
    }

    function uploadImg(fileInput,target,imgpath,dytypes){
        var loading = layer.msg('图片上传中，请稍等...', {
				icon: 16,offset: '300px',shade: 0.2
			});
        var filename = $(fileInput).val();
        if(filename != '' && filename != null){
            isupload = true;
            var pic = $(fileInput)[0].files[0];
            var dyformdata = new FormData();
            dyformdata.append('dypic', pic);
            dyformdata.append('imgpath', imgpath);
            $.ajax({
                url:"<?php echo url('Index/dyupload'); ?>",
                type:"post",
                dataType:'json',
                data: dyformdata,
                cache: false,
                contentType: false,
                processData: false,
                beforeSend:function(){
                    //图片上传中
                },
                success:function(data){
                    if(data.errcode == 1){
                        layer.msg(data.message,{icon:1,time:500});
                        if(target){
                            $(target).val(data.data);
                        }
                        if(dytypes == 1){
                            $(fileInput).parents('.input-file-show').find(".img_a").attr('href', data.data);
                            $(fileInput).parents('.input-file-show').find(".img_b").val(data.data);
                            $(fileInput).parents('.input-file-show').find(".img_i").attr('onmouseover', "layer.tips('<img src=" + data.data + " width=192>',this,{tips: [1, '#fff']});");
                        }
                    }else{
                        layer.msg(data.message,{icon:2});
                    }
                    layer.close(loading);
                },
                error:function (){
                    layer.msg("上传出错了");
                }
            });
            isupload = false;
        }
        isupload = false;
        layer.close(loading);
    }

    function uploadVideo(fileInput,target,imgpath,dytypes){
        var loading = layer.msg('视频上传中，请稍等...', {
				icon: 16,offset: '300px',shade: 0.2
			});
        var filename = $(fileInput).val();
        if(filename != '' && filename != null){
            isupload = true;
            var video = $(fileInput)[0].files[0];
            var dyformdata = new FormData();
            dyformdata.append('dyvideo', video);
            dyformdata.append('imgpath', imgpath);
            $.ajax({
                url:"<?php echo url('Index/dyuploadVideo'); ?>",
                type:"post",
                dataType:'json',
                data: dyformdata,
                cache: false,
                contentType: false,
                processData: false,
                beforeSend:function(){
                    //图片上传中
                },
                success:function(data){
                    if(data.errcode == 1){
                        layer.close(loading);
                        layer.msg(data.message,{icon:1,time:500});
                        $(target).val(data.data);
                        if(dytypes == 1){
                            if (data.data) {
                                $("#video-button").show();
                                $("#videotext").val(data.data);
                                $("#video_a").attr('href', data.data);
                            }
                        }
                    }else{
                        layer.msg(data.message,{icon:2});
                        layer.close(loading);
                    }
                },
                error:function (){
                    layer.msg("上传出错了");
                }
            });
            isupload = false;
        }
        isupload = false;
        //layer.close(loading);
    }
</script>
<link href="/public/static/js/validation/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/public/static/js/validation/jquery.validationEngine.js"></script>
<script type="text/javascript" src="/public/static/js/validation/jquery.validationEngine-zh_CN.js"></script>
<script src="/public/js/sortable.js"></script>
<script type="text/javascript">
  function ClearPicArr2(obj, path) {
    $(obj).parents(".grid-square").remove();
  }
  $('.add_attr').click(function () {
    var str = '<div class="attr">\n';
    str += '名称<input type="text" class="input-txt name" style="width: 80px!important;" value="" />\n';
    str += '值<input type="text" class="input-txt value" style="width: 80px!important;" value="" />\n';
    str += '        <a href="javascript:;" class="btn btn-sm" onclick="del_attr(this)">删除</a>\n';
    str += '</div>\n';
    $('.attr_list').append(str);
  })
  function del_attr(obj) {
    $(obj).parent('.attr').remove()
  }
  //上传图片回调事件
  function images_call_back(paths) {
    for (var i=0;i<paths.length ;i++ ){
      let src = paths[i];
      let last_div = `<div style="width:80px; text-align:center; margin: 5px; display:inline-block;" class="goods_xc grid-square">
            <input type="hidden" value="${src}" name="images[]">
            <img style="width:80px;height:80px;object-fit:cover" src="${src}" >
            <div class="fun-line">
              <a href="javascript:" data-src="${src}" onclick="ClearPicArr2(this,'${src}',0)"></a>
            </div>
          </div>`;
      $("#images").append(last_div);
    }
  }
  $('.add_size').click(function () {
    var str = '<div class="size">\n';
    str += '长<input type="text" class="input-txt length" style="width: 80px!important;" value="" />cm\n';
    str += '宽<input type="text" class="input-txt width" style="width: 80px!important;" value="" />cm\n';
    str += '高<input type="text" class="input-txt height" style="width: 80px!important;" value="" />cm\n';
    str += '        <a href="javascript:;" class="btn btn-sm" onclick="del_size(this)">删除</a>\n';
    str += '</div>\n';
    $('.size_list').append(str);
  })

  $('.add_material').click(function () {
    var str = '<div class="material">\n';
    str += '名称<input type="text" class="input-txt name" style="width: 80px!important;" value="" />\n';
    str += '文件名称<input type="text" class="input-txt filename" style="width: 80px!important;" value="" />\n';
    str += '图片<div class="input-file-show">\n';
    str += '<span class="show">\n';
    str += '<a target="_blank" class="nyroModal img_a" rel="gal" href="">\n';
    str += '<i class="fa fa-picture-o img_i"\n';
    str += ' onMouseOut="layer.closeAll();"></i>\n';
    str += '</a>\n';
    str += '</span>\n';
    str += '<span class="type-file-box">\n';
    str += '<input type="text" value="" class="type-file-text image img_b">\n';
    str += '<input type="button" value="选择上传..." class="type-file-button" onclick="uploadImgBtn(this);">\n';
    str += '<input type="file" class="type-file-file" onchange="uploadImg(this,\'\',\'furniture\',\'1\');" size="30" accept="image/gif,image/jpeg,image/jpg,image/png" title="点击前方预览图可查看大图，点击按钮选择文件并提交表单后上传生效">\n';
    str += '</span>\n';
    str += '</div>\n';
    str += '<a href="javascript:;" class="btn btn-sm" onclick="del_material(this)">删除</a>\n';
    str += '</div>\n';
    $('.material_list').append(str);
  })

  function del_size(obj) {
    $(obj).parent('.size').remove()
  }
  function uploadImgBtn(obj) {
    $(obj).parent('.type-file-box').find('.type-file-file').click()
  }
  function del_material(obj) {
    $(obj).parent('.material').remove()
  }
  function showloading(t) {
    if (t == true) {
      var loading = layer.msg('数据提交中...', {
        icon: 16,offset: '300px',shade: 0.2
      });
    } else {
      layer.closeAll(loading);
    }
  }

  $(function(){
    $('#ajaxform').validationEngine('attach', {
      promptPosition : "centerRight",
      autoPositionUpdate : true,
      showOneMessage: true,
      'custom_error_messages': {
        '#name': {
          'required': {
            'message': '* 请输入名称！'
          }
        },
        '#folder': {
          'required': {
            'message': '* 请输入目录！'
          }
        },
      },
      onValidationComplete: function(form, status){
        //表单校验成功并提交
        if(status == true){
          //尺寸组装
          $('#size').html('');
          var i = 0;
          $('.size_list .size').each(function () {
            var str = '<input type="hidden" name="size['+i+'][length]" value="'+$(this).find('.length').val()+'">';
            str += '<input type="hidden" name="size['+i+'][width]" value="'+$(this).find('.width').val()+'">';
            str += '<input type="hidden" name="size['+i+'][height]" value="'+$(this).find('.height').val()+'">';
            $('#size').append(str);
            i++
          })
          //材料组装
          $('#material').html('');
          var j = 0;
          $('.material_list .material').each(function () {
            var str = '<input type="hidden" name="material['+j+'][name]" value="'+$(this).find('.name').val()+'">';
            str += '<input type="hidden" name="material['+j+'][filename]" value="'+$(this).find('.filename').val()+'">';
            str += '<input type="hidden" name="material['+j+'][image]" value="'+$(this).find('.image').val()+'">';
            $('#material').append(str);
            j++
          })
          var k = 0;
          $('.attr_list .attr').each(function () {
            var str = '<input type="hidden" name="attr['+k+'][name]" value="'+$(this).find('.name').val()+'">';
            str += '<input type="hidden" name="attr['+k+'][value]" value="'+$(this).find('.value').val()+'">';
            $('#attr').append(str);
            k++
          })
          $('span.err').hide();
          $('#submit').attr('disabled', true);
          $.ajax({
            type: "POST",
            url: "<?php echo url('Furniture/frockHandle'); ?>",
            data: $("#ajaxform").serialize(),
            async:false,
            dataType: "json",
            error: function () {
              showloading(false);
              layer.alert("服务器繁忙, 请联系管理员!",{title:'温馨提示',closeBtn:0,icon:2});
              $('#submit').attr('disabled',false);
            },
            beforeSend:function() {
              showloading(true);
            },
            success: function (data) {
              showloading(false);
              if (data.errcode == 1) {
                layer.msg(data.message, {icon: 1,time: 1000}, function() {
                  location.href = "<?php echo url('Furniture/frockList'); ?>";
                });
              } else {
                $('#submit').attr('disabled',false);
                $('span.err').text('').show();
                if (data.data) {
                  $.each(data.data, function(index, item) {
                    $('#err_' + index).text(item).show();
                  });
                }
                layer.msg(data.message, {icon: 2,time: 1000});
              }
            }
          })
        }
      }
    });
  })

  $('#cate_id').change(function () {
    var id = $(this).val();
    $.ajax({
      type: "POST",
      url: "<?php echo url('Furniture/getTwoCate'); ?>",
      data: {'id':id},
      async:false,
      dataType: "json",
      error: function () {
        showloading(false);
        layer.alert("服务器繁忙, 请联系管理员!",{title:'温馨提示',closeBtn:0,icon:2});
        $('#submit').attr('disabled',false);
      },
      beforeSend:function() {
        showloading(true);
      },
      success: function (data) {
        showloading(false);
        if (data.errcode == 1) {
          // layer.msg(data.message, {icon: 1,time: 1000}, function() {
          //   location.href = "<?php echo url('Furniture/softList'); ?>";
          // });
          var html='<option value="">选择二级分类</option>';
          if(data.data){
            for (cate of data.data){
              html +='<option value="'+cate.id+'">'+cate.name+'</option>';
            }
          }
          $('#two_cate_id').html(html)
        } else {
          layer.msg(data.message, {icon: 2,time: 1000});
        }
      }
    })
  })
  $('#two_cate_id').change(function () {
    var id = $(this).val();
    $.ajax({
      type: "POST",
      url: "<?php echo url('Furniture/getThreeCate'); ?>",
      data: {'id':id},
      async:false,
      dataType: "json",
      error: function () {
        showloading(false);
        layer.alert("服务器繁忙, 请联系管理员!",{title:'温馨提示',closeBtn:0,icon:2});
        $('#submit').attr('disabled',false);
      },
      beforeSend:function() {
        showloading(true);
      },
      success: function (data) {
        showloading(false);
        if (data.errcode == 1) {
          // layer.msg(data.message, {icon: 1,time: 1000}, function() {
          //   location.href = "<?php echo url('Furniture/softList'); ?>";
          // });
          var html='<option value="">选择三级分类</option>';
          if(data.data){
            for (cate of data.data){
              html +='<option value="'+cate.id+'">'+cate.name+'</option>';
            }
          }
          $('#three_cate_id').html(html)
        } else {
          layer.msg(data.message, {icon: 2,time: 1000});
        }
      }
    })
  })

  $('#two_cate_id').change(function () {
    get_filename()
  })
  $('#three_cate_id').change(function () {
    get_filename()
  })
  $('#brand_name').change(function () {
    get_filename()
  })

  function get_filename() {
    var id = "<?php echo $info['id']; ?>"
    if(!id){
      var cate_id = $('#cate_id').val()
      var two_cate_id = $('#two_cate_id').val()
      var brand_name = $('#brand_name').val()
      if(!cate_id || !two_cate_id){
        return false;
      }
      $.ajax({
        type: "POST",
        url: "<?php echo url('Furniture/getFilename'); ?>",
        data: {cate_id:cate_id,two_cate_id:two_cate_id,brand_name:brand_name},
        async:false,
        dataType: "json",
        error: function () {
          showloading(false);
          layer.alert("服务器繁忙, 请联系管理员!",{title:'温馨提示',closeBtn:0,icon:2});
          $('#submit').attr('disabled',false);
        },
        beforeSend:function() {
          showloading(true);
        },
        success: function (data) {
          showloading(false);
          if (data.errcode == 1) {
            $('#filename').val(data.data.filename);
          }
        }
      })
    }
  }
  function selectIos(){
    console.log('ios')
    $('#ios-file').click()
  }
  function selectAndroid(){
    console.log('Android')
    $('#Android-file').click()
  }
  function selectPc(){
    console.log('pc')
    $('#pc-file').click()
  }

</script>

<!--上传ios文件-->
<script>
  var subObject;
  var file;
  //定义上传配置  大于 4M 时可分块上传，小于 4M 时直传
  var config = {
    useCdnDomain: true,     //是否使用 cdn 加速域名
    region: qiniu.region.z0,    //上传域名区域 华东空间z0,华南z2，华北z1
    retryCount: 6,      //上传自动重试次数  默认3次
  };

  //定义putExtra上传限制
  var putExtra = {
    fname: "",  //文件原始文件名
    params: { 'x:flag': 'qiniu'},    //自定义变量
    // mimeType: ["pak"]     //指定所传的文件类型
  };

  var compareChunks = [];
  var observable;
  var subscription;

  function upFile(node) {
    var fileURL = "";
    try{
      file = null;
      if(node.files && node.files[0] ){
        file = node.files[0];
      }else if(node.files && node.files.item(0)) {
        file = node.files.item(0);
      }
    }catch(e){

    }
    creatFile(fileURL,file.name);
    return fileURL;
  }

  // 文件名
  function creatFile(fileURL,fileName){
    console.log(fileName)
    $("#iosTotalBarColor").css("width","0%");
  }

  // 上传操作
  function uploadIos() {
    // 设置next,error,complete对应的操作，分别处理相应的进度信息，错误信息，以及完成后的操作
    subObject = {
      next: ios_next,
      error: ios_error,
      complete: ios_complete
    };
    $.ajax({
      url:"<?php echo url('Scene/getToken'); ?>",
      success(result){
        $.token = result
      }
    })　//需要从后端获取
    //因为文件上传七牛云后文件名相同的会覆盖原，所以给文件重新命名防止覆盖原文件  file.name 是原文件名
    var date = new Date();
    var filename = date.getTime()+'_'+file.name
    //上传
    observable = qiniu.upload(file, filename, $.token, putExtra, config);
    // 调用sdk上传接口获得相应的observable，控制上传和暂停
    subscription = observable.subscribe(subObject);
  }

  // 暂停操作
  function filepause(){
    subscription.unsubscribe();
  }

  //分片上传返回response，标记上传进度
  var ios_next = function(response) {
    var chunks = response.chunks||[];
    var total = response.total;
    $(".ios_speed").text("进度：" + Math.floor(total.percent) + "% ");  //进度向下取整
    $("#iosTotalBarColor").css("width", total.percent + "%");
    compareChunks = chunks;
  };
  // 错误信息
  var ios_error = function(err) {
    alert('token失效，请重新上传');
  };
  // 上传完成
  var ios_complete = function(res) {
    // 返回文件信息
    layer.msg('上传成功', {icon: 1, time: 2000}, function () {
      // alert('上传成功');
      $('#ios_url').val('http://qiniu.dreamhouses.com.cn/'+res.key)
      $('#ios_url').append('value','http://qiniu.dreamhouses.com.cn/'+res.key)
      console.log($('#ios_url').val());
    });
    console.log('http://qiniu.dreamhouses.com.cn/'+res.key);

  };
</script>
<!--上传Android文件-->
<script>
  var subObject;
  var file;
  //定义上传配置  大于 4M 时可分块上传，小于 4M 时直传
  var config = {
    useCdnDomain: true,     //是否使用 cdn 加速域名
    region: qiniu.region.z0,    //上传域名区域 华东空间z0,华南z2，华北z1
    retryCount: 6,      //上传自动重试次数  默认3次
  };

  //定义putExtra上传限制
  var putExtra = {
    fname: "",  //文件原始文件名
    params: { 'x:flag': 'qiniu'},    //自定义变量
    // mimeType: ["pak"]     //指定所传的文件类型
  };

  var compareChunks = [];
  var observable;
  var subscription;

  function upFile(node) {
    var fileURL = "";
    try{
      file = null;
      if(node.files && node.files[0] ){
        file = node.files[0];
      }else if(node.files && node.files.item(0)) {
        file = node.files.item(0);
      }
    }catch(e){

    }
    creatFile(fileURL,file.name);
    return fileURL;
  }

  // 文件名
  function creatFile(fileURL,fileName){
    console.log(fileName)
    $("#AndroidTotalBarColor").css("width","0%");
  }

  // 上传操作
  function uploadAndroid() {
    // 设置next,error,complete对应的操作，分别处理相应的进度信息，错误信息，以及完成后的操作
    subObject = {
      next: Android_next,
      error: Android_error,
      complete: Android_complete
    };
    $.ajax({
      url:"<?php echo url('Scene/getToken'); ?>",
      success(result){
        $.token = result
      }
    })　//需要从后端获取
    //因为文件上传七牛云后文件名相同的会覆盖原，所以给文件重新命名防止覆盖原文件  file.name 是原文件名
    var date = new Date();
    var filename = date.getTime()+'_'+file.name
    //上传
    observable = qiniu.upload(file, filename, $.token, putExtra, config);
    console.log(observable)
    // 调用sdk上传接口获得相应的observable，控制上传和暂停
    subscription = observable.subscribe(subObject);
  }

  // 暂停操作
  function filepause(){
    subscription.unsubscribe();
  }

  //分片上传返回response，标记上传进度
  var Android_next = function(response) {
    var chunks = response.chunks||[];
    var total = response.total;
    $(".Android_speed").text("进度：" + Math.floor(total.percent) + "% ");  //进度向下取整
    $("#AndroidTotalBarColor").css("width", total.percent + "%");
    compareChunks = chunks;
  };
  // 错误信息
  var Android_error = function(err) {
    alert('token失效，请重新上传');
  };
  // 上传完成
  var Android_complete = function(res) {
    // 返回文件信息
    layer.msg('上传成功', {icon: 1, time: 2000}, function () {
      // alert('上传成功');
      $('#Android_url').val('http://qiniu.dreamhouses.com.cn/'+res.key)
      $('#Android_url').append('value','http://qiniu.dreamhouses.com.cn/'+res.key)
      console.log($('#Android_url').val());
    });
    console.log('http://qiniu.dreamhouses.com.cn/'+res.key);

  };
</script>
<!--上传pc文件-->
<script>
  var subObject;
  var file;
  //定义上传配置  大于 4M 时可分块上传，小于 4M 时直传
  var config = {
    useCdnDomain: true,     //是否使用 cdn 加速域名
    region: qiniu.region.z0,    //上传域名区域 华东空间z0,华南z2，华北z1
    retryCount: 6,      //上传自动重试次数  默认3次
  };

  //定义putExtra上传限制
  var putExtra = {
    fname: "",  //文件原始文件名
    params: { 'x:flag': 'qiniu'},    //自定义变量
    // mimeType: ["pak"]     //指定所传的文件类型
  };

  var compareChunks = [];
  var observable;
  var subscription;

  function upFile(node) {
    var fileURL = "";
    try{
      file = null;
      if(node.files && node.files[0] ){
        file = node.files[0];
      }else if(node.files && node.files.item(0)) {
        file = node.files.item(0);
      }
    }catch(e){

    }
    creatFile(fileURL,file.name);
    return fileURL;
  }

  // 文件名
  function creatFile(fileURL,fileName){
    console.log(fileName)
    $("#pcTotalBarColor").css("width","0%");
  }

  // 上传操作
  function uploadPc() {
    // 设置next,error,complete对应的操作，分别处理相应的进度信息，错误信息，以及完成后的操作
    subObject = {
      next: pc_next,
      error: pc_error,
      complete: pc_complete
    };
    $.ajax({
      url:"<?php echo url('Scene/getToken'); ?>",
      success(result){
        $.token = result
      }
    })　//需要从后端获取
    //因为文件上传七牛云后文件名相同的会覆盖原，所以给文件重新命名防止覆盖原文件  file.name 是原文件名
    var date = new Date();
    var filename = date.getTime()+'_'+file.name
    //上传
    observable = qiniu.upload(file, filename, $.token, putExtra, config);
    console.log(observable)
    // 调用sdk上传接口获得相应的observable，控制上传和暂停
    subscription = observable.subscribe(subObject);
  }

  // 暂停操作
  function filepause(){
    subscription.unsubscribe();
  }

  //分片上传返回response，标记上传进度
  var pc_next = function(response) {
    var chunks = response.chunks||[];
    var total = response.total;
    $(".pc_speed").text("进度：" + Math.floor(total.percent) + "% ");  //进度向下取整
    $("#pcTotalBarColor").css("width", total.percent + "%");
    compareChunks = chunks;
  };
  // 错误信息
  var pc_error = function(err) {
    alert('token失效，请重新上传');
  };
  // 上传完成
  var pc_complete = function(res) {
    // 返回文件信息
    layer.msg('上传成功', {icon: 1, time: 2000}, function () {
      // alert('上传成功');
      $('#pc_url').val('http://qiniu.dreamhouses.com.cn/'+res.key)
      $('#pc_url').append('value','http://qiniu.dreamhouses.com.cn/'+res.key)
      console.log($('#pc_url').val());
    });
    console.log('http://qiniu.dreamhouses.com.cn/'+res.key);

  };
</script>
</body>
</html>