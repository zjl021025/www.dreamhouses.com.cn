<?php

return [
    // 默认磁盘
    'default' => env('filesystem.driver', 'local'),
    // 磁盘列表
    'disks'   => [
        'local'  => [
            'type' => 'local',
            'root' => app()->getRuntimePath() . 'upload',
        ],
        'public' => [
            // 磁盘类型
            'type'       => 'local',
            // 磁盘路径
            'root'       => app()->getRootPath() . 'public/public/upload',
            // 磁盘路径对应的外部URL路径
            'url'        => '/public/upload',
            // 可见性
            'visibility' => 'public/public',
        ],
        // 更多的磁盘配置信息
        'qiniu'=>[
            'type'=>'qiniu',
            'accessKey'=>'neRVrqfNv3IW7brRh2ZRMGhCRJjLO3PEg40jZ5jx',//你的accessKey
            'secretKey'=>'nIxZgFLfMU3l1jzIMDzNFG-MKB5wMmQgifX9rauj',//你的secretKey
            'bucket'=>'fanhuakeji',//你的存储空间名
            'domain'=>'qiniu.dreamhouses.com.cn' //你的七牛云加速域名
        ]
    ],
];
