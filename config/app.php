<?php
// +----------------------------------------------------------------------
// | 应用设置
// +----------------------------------------------------------------------


return [
    // 应用地址
    'app_host'         => env('app.host', ''),
    // 应用的命名空间
    'app_namespace'    => '',
    // 是否启用路由
    'with_route'       => true,
    // 默认应用
    'default_app'      => 'index',
    // 默认时区
    'default_timezone' => 'Asia/Shanghai',

    // 应用映射（自动多应用模式有效）
    'app_map'          => [],
    // 域名绑定（自动多应用模式有效）
    'domain_bind'      => [],
    // 禁止URL访问的应用列表（自动多应用模式有效）
    'deny_app_list'    => [],

    // 异常页面的模板文件
    'exception_tmpl'   => app()->getThinkPath() . 'tpl/think_exception.tpl',

    // 错误显示信息,非调试模式有效
    'error_message'    => '非常抱歉：您可能输入了错误的网址，或者该页面已经不存在了哦……',
    // 显示错误信息
    'show_error_msg'   => true,
    // 密码加密串
    'AUTH_CODE' => "DYSHOP",

    'redisstatus' =>[1,2,3,4,5,6,7,8],

    //选择自定义库
    'dysetcron' => ['__keyevent@3__:expired'],
    'dysetcron_select' => 3,

    //AES KEY
    'DY_AES_KEY' => "www.doing.net.cn",

    'API_SECRET_KEY' => "www.doing.net.cn", //API密钥
    'user_upload_limit_size' => 1024 * 1024 * 20,//上传图片大小限制
    'user_image_upload_limit_size' => 1024 * 1024 * 20,//上传图片大小限制
    'user_file_upload_limit_size' => 1024 * 1024 * 50,//上传图片大小限制
    'user_video_upload_limit_size' => 1024 * 1024 * 50,//上传视频大小限制
    'image_upload_limit_size' => 1024 * 1024 * 20,//上传图片大小限制
    'zip_upload_limit_size' => 1024 * 1024 * 200,//上传图片大小限制

    //短信使用场景
    'SEND_SCENE' => array(
        '1'=>array('用户注册','验证码${code}，用于注册新账号，如非本人操作，请及时检查账户安全!','register_sms_enable'),
        '2'=>array('登录','验证码${code}，用于验证码登录，如非本人操作，请及时检查账户安全!','login_sms_enable'),
        '3'=>array('手机号绑定','验证码${code}，用于绑定手机号码，如非本人操作，请及时检查账户安全!','bind_sms_enable'),
        '4'=>array('找回密码','验证码${code}，用于找回密码，如非本人操作，请及时检查账户安全!','forgetpwd_sms_enable'),
        '5'=>array('手机号绑定','验证码${code}，用于绑定手机号码，如非本人操作，请及时检查账户安全!','bind_sms_enable'),
        '6'=>array('验证手机号','验证码${code}，用于验证手机号，如非本人操作，请及时检查账户安全!','check_sms_enable'),
    ),
    'order_status' => [
        -999=>'全部',
        0=>'待确认',
        1=>'待测量/待维修',
        2=>'测量审核/维修审核',
        3=>'待发货',
        4=>'待安装',
        5=>'安装审核',
        6=>'已完成',
        7=>'已中止',
        8=>'已取消'
    ],
    'PAGESIZE'=>10,
    //协议
    'agreement'=>[
        1=>'用户协议',
        2=>'隐私政策',
//        3=>'关于我们',
        4=>'版本更新',
        5=>'注销账号',
    ],
    'erasable_type'=>['.jpg','.png','.gif','.jpeg'],
    //腾讯云即时通讯配置
    'Im' => [
        'appId' => '',//appId
        'key' => '',//密钥
        'admin' => '',//账号管理员
    ],
    //认证类型
    'designer_type'=>[
        1=>'独立设计师',
        2=>'机构设计师',
    ],
    //证件类型
    'cre_type'=>[
        1=>'身份证',
    ],
    'amap_web_key' => '9e8cbf4ae45671c2184a04286acc5793',//高德地图key-web端
    'ios_version'=>'1.0.0',
    'android_version'=>'1.0.0',
    'android_code'=>'100',
];
