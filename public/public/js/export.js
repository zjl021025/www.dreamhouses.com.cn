    /* 数据导出 */
    //url1 --请求计算要导出的数据/导出数据
    //url2 -- 请求下载文件地址
    //url2_name -- 存放下载地址的缓存名称
    //url3   //请求百分比的地址
    //all_num  要导出数据的总量的缓存名称
    // leading_num  已导出数据的总量的缓存名称
    function exportdata(url1, url2, url2_name, url3, all_num, leading_num) {
        var loading = layer.msg('正在统计数据，请耐心等待', {
				icon: 16,offset: '300px',shade: 0.2
			});
        let formData = $("#dyform").serialize();
        $.ajax({
            type: 'post',
            url: url1,//获取到导出的数据总量
            data: formData+"&type="+1,
            dataType: "json",
            success: function (data) {
                layer.close(loading);
                if (data.errcode == 1) {
                    var num = data.data.num;
                    if (num > 50000) {
                        var title = '总共有'+num+'条数据，导出时间较长。<br> 确定要继续吗？';
                    } else if (num<50000 && num > 0) {
                        var title = '总共有'+num+'条数据。<br> 确定要导出吗?';
                    } else {
                        layer.msg('暂无需要导出的内容',{icon: 6,time: 3000});
                        return false;
                    }
                    var second = layer.confirm(title, {
                            title:'温馨提示：',closeBtn:0,icon:3,
                            btn: ['确定', '取消'] //按钮
                    },function(){
                        layer.close(second);
                        layui.use('element', function () {
                            var $ = layui.jquery,
                            element = layui.element; //Tab的切换功能，切换事件监听等，需要依赖element模块
                            element.progress('demo',"0%");//设置初始进度条为0
                        });
                        var show = layer.open({
                            type:1,
                            title:'数据导出进度：',
                            skin: 'layui-layer-demo',
                            area:['420px','240px'],
                            shadeClose: true,
                            content:$("#show"),
                            closeBtn:0,
                            btn: ['停止导出'],
                            btn1: function(index, layero) {
                                clearInterval(h);
                                if ($("#process").val() == 100) {
                                    clearInterval(h1);
                                }
                                $("#process").val(0);
                                layer.close(show);
                            }
                        });
                        //请求导出
                        $.ajax({
                            type: 'post',
                            url: url1,//导出数据
                            data: formData,
                            dataType: "json",
                            success: function(d){
                            }
                        });
                        //请求导出百分比和导出完成后的下载地址
                        var h1;
                        var h2 = 0;
                        var h = setInterval(function(){
                           if ($("#process").val() == 100) {
                            clearInterval(h);
                            h1 = setInterval(function(){
                                //获取下载地址
                                $.ajax({
                                   type: 'post',
                                   url: url2,//获取下载地址
                                   data: {url:url2_name},//下载地址的缓存名称
                                   dataType: "json",
                                   success: function (data) {
                                        if (data.errcode == 1) {
                                            $("#process").val(0);
                                            clearInterval(h1);
                                            layer.close(show);
                                            $("#excel-title").html("正在导出，请耐心等待......");
                                            window.location.href = data.dyurl;
                                        } else {
                                            h2 = h2 + 1;
                                            if (h2 > 150) {
                                                //请求超过 150次
                                                clearInterval(h1);
                                                layer.close(show);
                                                layer.confirm("导出压缩包失败", {
                                                    title:'温馨提示：',closeBtn:0,icon:3,
                                                    btn: ['确定', '取消'] //按钮
                                                });
                                            }
                                        }
                                    },
                                    error: function () {
                                        clearInterval(h1);
                                    }
                               });
                            },3000);
                           }
                           get_process(url3, all_num, leading_num);
                        },200);
                    })
                }
            }
        });
    }
    function get_process(url3, all_num, leading_num){
        $.ajax({
            type: 'post',
            url: url3,
            data: {all_num:all_num,leading_num:leading_num},
            dataType: "json",
            success: function (data) {
                $("#process").val(data.data.num);
                layui.use('element', function () {
                    var element = layui.element; 
                    element.progress('demo',data.data.num+'%');     
                    if (data.data.num == 100) {
                        $("#excel-title").html("正在生成压缩包，请稍后......");
                    }
                });
            }
        });
    }

    //导入数据
    $('#excel_file').on('change', function() {
        var url = $("#import_data").data("import_data");
        var formData = new FormData();
        var g_id = $("#g_id").val();
        if (g_id > 0) {
            formData.append('goods_id', g_id);
        }
        formData.append('excel_file', this.files[0]);
        var loading = layer.msg('正在统计数据，请耐心等待', {
            icon: 16,offset: '300px',shade: 0.2
        });
        setTimeout(function(){
            layer.close(loading);
            import_process();
        },1500)
        $.ajax({
            type: "POST",
            dataType: "json",
            processData: false,
            contentType: false,
            url: url,
            data: formData,
            success: function (data) {
                if(data.errcode==1){
                    /*setTimeout(function(){
                        layer.msg(data.message, {icon: 1,time:1000},function(){
                            window.location.reload();
                        });
                    },2000)*/
                    return ;
                }else{
                    layer.close(loading);
                    layer.msg(data.message,{icon: 2,time: 1000},function(){
                        window.location.reload();
                    });
                }
            },
        });
    })
    //获取导入百分比
    function import_process()
    {
        layui.use('element', function () {
            var $ = layui.jquery,
            element = layui.element; //Tab的切换功能，切换事件监听等，需要依赖element模块
            element.progress('demo',"0%");//设置初始进度条为0
        });
        $("#excel-title").html("正在导入，请耐心等待......");
        var show = layer.open({
            type:1,
            title:'数据导入进度：',
            skin: 'layui-layer-demo',
            area:['420px','240px'],
            shadeClose: true,
            content:$("#show"),
            closeBtn:0,
            btn: ['停止导入'],
            btn1: function(index, layero) {
                clearInterval(h);
                if ($("#process").val() == 100) {
                    clearInterval(h1);
                }
                $("#process").val(0);
                layer.close(show);
            }
        });
        var h1;
        var url3 = $("#import_url").val();
        var all_num = $("#import_all_num").val();
        var leading_num = $("#import_leading_num").val();
        var h = setInterval(function(){
           if ($("#process").val() == 100) {
            layer.close(show);
            clearInterval(h);
               layer.msg('导入成功', {icon: 1,time:2000},function(){
                    var index = parent.layer.getFrameIndex(window.name); //获取窗口索引  
                    parent.layer.close(index);
                    window.location.reload();
               });
               return ;
           }
           $.ajax({
            type: 'post',
            url: url3,
            data: {all_num:all_num,leading_num:leading_num},
            dataType: "json",
            success: function (data) {
                $("#process").val(data.data.num);
                layui.use('element', function () {
                    var element = layui.element; 
                    element.progress('demo',data.data.num+'%');     
                    if (data.data.num == 100) {
                        $("#excel-title").html("导入成功");
                    }
                });
            }
        });
        },200);
    }