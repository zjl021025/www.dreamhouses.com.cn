/**
 * 获取省份
 */
function get_province(){
    var url = '/index.php?m=Admincp&c=Api&a=getRegion&level=1&parent_id=0';
    $.ajax({
        type : "GET",
        url  : url,
        error: function(request) {
            alert("服务器繁忙, 请联系管理员!");
            return;
        },
        success: function(v) {
            v = '<option value="0">选择省份</option>'+ v;
            $('#province').empty().html(v);
			layui.form.render('select');
        }
    });
}

/*
 * 上传图片 后台专用
 * @access  public
 * @null int 一次上传图片张图
 * @elementid string 上传成功后返回路径插入指定ID元素内
 * @path  string 指定上传保存文件夹,默认存在public/upload/temp/目录
 * @callback string  回调函数(单张图片返回保存路径字符串，多张则为路径数组 )
 */
function GetUploadify(num,elementid,path,callback,fileType='Images')
{
	var upurl ='/index.php?m=Admincp&c=Uploadify&a=upload&num='+num+'&input_name='+elementid+'&path='+path+'&func='+callback+'&fileType='+fileType;
    console.log(upurl);
	var title = '上传图片';
    if(fileType == 'Flash'){
        title = '上传视频';
    }else if(fileType == 'txt'){
        title = '上传文件';
    }
    layer.open({
        type: 2,
        title: title,
        shadeClose: true,
        shade: false,
        maxmin: false, //开启最大化最小化按钮
        area: ['60%', '430px'],
        content: upurl
     });
}

/**
 * 海报专用
 */
function GetUploadifyPoster(num,elementid,path,callback,fileType)
{
    var upurl ='/index.php?m=Admincp&c=Uploadify&a=poster_upload&num='+num+'&input='+elementid+'&path='+path+'&func='+callback+'&fileType='+fileType;
    var title = '上传图片';
    if(fileType == 'Flash'){
        title = '上传视频';
    }
    layer.open({
        type: 2,
        title: title,
        shadeClose: true,
        shade: false,
        maxmin: false, //开启最大化最小化按钮
        area: ['60%', '400px'],
        content: upurl
    });
}

/*
 * 删除组图input
 * @access   public
 * @val  string  删除的图片input
 */
function ClearPicArr(val){
	$("li[rel='"+ val +"']").remove();
	$.get(
		"{:U('Admincp/Uploadify/delupload')}",{action:"del", filename:val},function(){}
	);
}