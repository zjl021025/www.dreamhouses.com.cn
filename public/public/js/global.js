/**
 * 输入为空检查
 * @param name '#id' '.id'  (name模式直接写名称)
 * @param type 类型  0 默认是id或者class方式 1 name='X'模式
 */
function is_empty(name,type){
    if(type == 1){
        if($('input[name="'+name+'"]').val() == ''){
            return true;
        }
    }else{
        if($(name).val() == ''){
            return true;
        }
    }
    return false;
}

/**
 * 手机号码格式判断
 * @param tel
 * @returns {boolean}
 */
function checkMobile(tel) {
    //var reg = /(^1[3|4|5|7|8][0-9]{9}$)/;
    var reg = /^1[0-9]{10}$/;
    if (reg.test(tel)) {
        return true;
    }else{
        return false;
    };
}

/**
 * 固定电话号码判断
 * @param tel
 * @returns {boolean}
 */
function checkTelphone(tel){
    var reg = /^((0\d{2,3})-)(\d{7,8})(-(\d{3,}))?$/;
    if(reg.test(tel)){
        return true;
    }else{
        return false;
    }
}

/**
 * 固定电话号码判断
 * @param tel
 * @returns {boolean}
 */
function checkTelphones(tel){
    //判断座机格式的
    var re = /^(\d{3,4}\-)?\d{7,8}$/i;
    if (re.test(tel)){
        return true;
    }
    var reg = /^(([0-9]\d{2,3})-)(\d{7,8})(-(\d{3,}))?$/;
    if(reg.test(tel)){
        return true;
    }else{
        return false;
    }
}

function GetUrlParams(name)
{
     var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
     var r = window.location.search.substr(1).match(reg);
     if(r!=null)return  unescape(r[2]); return null;
}


function layConfirm(msg , callback){
	layer.confirm(msg, {
          title:'温馨提示',closeBtn:0,icon:3,
		  btn: ['确定','取消'] //按钮
		}, function(){
			callback();
			layer.closeAll();
		}, function(index){
			layer.close(index);
			return false;// 取消
		}
	);
}

function isMobile(){
	return "yes";
}

// 判断是否手机浏览器
function isMobileBrowser(){
    var sUserAgent = navigator.userAgent.toLowerCase();
    var bIsIpad = sUserAgent.match(/ipad/i) == "ipad";
    var bIsIphoneOs = sUserAgent.match(/iphone os/i) == "iphone os";
    var bIsMidp = sUserAgent.match(/midp/i) == "midp";
    var bIsUc7 = sUserAgent.match(/rv:1.2.3.4/i) == "rv:1.2.3.4";
    var bIsUc = sUserAgent.match(/ucweb/i) == "ucweb";
    var bIsAndroid = sUserAgent.match(/android/i) == "android";
    var bIsCE = sUserAgent.match(/windows ce/i) == "windows ce";
    var bIsWM = sUserAgent.match(/windows mobile/i) == "windows mobile";
    if (bIsIpad || bIsIphoneOs || bIsMidp || bIsUc7 || bIsUc || bIsAndroid || bIsCE || bIsWM ){
        return true;
    }else
	    return false;
}

function getCookieByName(name) {
    var start = document.cookie.indexOf(name + "=");
    var len = start + name.length + 1;
    if ((!start) && (name != document.cookie.substring(0, name.length))) {
        return null;
    }
    if (start == -1)
        return null;
    var end = document.cookie.indexOf(';', len);
    if (end == -1)
        end = document.cookie.length;
    return unescape(document.cookie.substring(len, end));
}
function showErrorMsg(msg){
    // layer.msg(msg, {icon: 2});
    layer.open({content: msg, time: 2});
}
//关闭页面
function CloseWebPage(){
    if (navigator.userAgent.indexOf("MSIE") > 0) {
        if (navigator.userAgent.indexOf("MSIE 6.0") > 0) {
            window.opener = null;
            window.close();
        } else {
            window.open('', '_top');
            window.top.close();
        }
    }
    else if (navigator.userAgent.indexOf("Firefox") > -1 || navigator.userAgent.indexOf("Chrome") > -1) {
       // window.location.href = 'about:blank';
        window.open('', '_top');
        window.top.close();
    } else {
        window.opener = null;
        window.open('', '_self', '');
        window.close();
    }
}
/**
 * 设置地区缓存
 * @param address
 */
function doCookieArea(address){
    $.ajax({
        type : "POST",
        url:"/index.php?m=Home&c=Api&a=doCookieArea",
        data: {address: address}
    });
}
//时间戳转换
function add0(m){return m<10?'0'+m:m }
function  formatDate(now)   {
    var time = new Date(now);
    var y = time.getFullYear();
    var m = time.getMonth()+1;
    var d = time.getDate();
    var h = time.getHours();
    var mm = time.getMinutes();
    var s = time.getSeconds();
    return y+'/'+add0(m)+'/'+add0(d)+' '+add0(h)+':'+add0(mm)+':'+add0(s);
}
function round(x, num){
    return Math.round(x * Math.pow(10, num)) / Math.pow(10, num) ;
}
// 校验组织机构代码
function orgcodevalidate(value) {
    if (value != "") {
        var part1 = value.substring(0, 8);
        var part2 = value.substring(value.length - 1, 1);
        var ws = [3, 7, 9, 10, 5, 8, 4, 2];
        var str = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        var reg = /^([0-9A-Z]){8}$/;
        if (!reg.test(part1)) {
            return true
        }
        var sum = 0;
        for (var i = 0; i < 8; i++) {
            sum += str.indexOf(part1.charAt(i)) * ws[i];
        }
        var C9 = 11 - (sum % 11);
        var YC9 = part2 + '';
        if (C9 == 11) {
            C9 = '0';
        } else if (C9 == 10) {
            C9 = 'X';
        } else {
            C9 = C9 + '';
        }
        return YC9 != C9;
    }
}

//根据key值返回spec_goods_price记录
function search_spec_goods_price(spec_key)
{
    var spec_goods_price_item = [];
    $.each(spec_goods_price, function (i, o) {
        if (o.key == spec_key) {
            spec_goods_price_item = o;
        }
    })
    return spec_goods_price_item;
}


//小时分钟秒的后尾补零
function checkTime(i){
    if (i<10){
        i = "0" + i;
    }
    return i;
}
//将时间戳转成字符串
function time_format(timestamp){
    var d = new Date(timestamp * 1000);    //根据时间戳生成的时间对象
    return (d.getFullYear()) + "-" + (d.getMonth() + 1) + "-" + (d.getDate()) + " " + (checkTime(d.getHours())) + ":" + (checkTime(d.getMinutes()));
}

//局部搜索条件验证表单提交
function clickSearch(obj,form){
    var html = "<input type='hidden' name='is_search'  value='1'>"
    $(obj).after(html);
    $('#'+form).submit();
}

$(document).ready(function() {
    //搜索状态显示优化，选中后字体颜色改变start
    $(".flexigrid .sDiv2 .select").css({"color": "#B8B8B8 !important"});
    $(".flexigrid .sDiv2 .select option").css({"color": "#333 !important"});
    $(".select").change(function () {
        var selItem = $(this).val();
        if (selItem == $(this).find('option:first').val()) {
            $(this).css("color", "#B8B8B8 !important");
        } else {
            $(this).css("color", "#2796FF !important");
        }
    });
    //搜索状态显示优化，选中后字体颜色改变end
})

/*
 * 上传图片 后台专用
 * @access  public
 * @null int 一次上传图片张图
 * @elementid string 上传成功后返回路径插入指定ID元素内
 * @path  string 指定上传保存文件夹,默认存在public/upload/temp/目录
 * @callback string  回调函数(单张图片返回保存路径字符串，多张则为路径数组 )
 */
function GetChunkUploadify(num,elementid,path,callback,fileType,title = '上传图片')
{
    layer.closeAll();
    var upurl ='/index.php?m=Admincp&c=Uploadify&a=chunkUpload&num='+num+'&input='+elementid+'&path='+path+'&func='+callback+'&fileType='+fileType;
    if(fileType == 'Flash'){
        title = '上传视频';
    }else if(fileType == 'Pdf'){
        title = '上传PDF';
    }
    layer.open({
        type: 2,
        title: title,
        shadeClose: true,
        shade: 0.1,
        maxmin: false, //开启最大化最小化按钮
        area: ['60%', '400px'],
        content: upurl
    });
}