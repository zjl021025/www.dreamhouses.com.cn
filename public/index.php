<?php

namespace think;

 
if (extension_loaded('zlib')){
    @ob_end_clean();
    ob_start('ob_gzhandler');
}
//防止iframe框架攻击
header('X-Frame-Options: SAMEORIGIN');
// 检测PHP环境
/* if(version_compare(PHP_VERSION,'7.1.0','<')){
    header("Content-type: text/html; charset=utf-8");  
    die('抱歉：PHP 版本必须 7.1以上 !');
} */

//header("Access-Control-Allow-Origin:*");
//header("Access-Control-Allow-Methods:GET, POST, OPTIONS");
//header("Access-Control-Allow-Headers:DNT,X-Mx-ReqToken,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type, Accept-Language, Origin, Accept-Encoding");


error_reporting(E_ERROR | E_WARNING | E_PARSE);//报告运行时错误
define('PLUGIN_PATH', dirname(__DIR__) . '/plugins/');
defined('UPLOAD_PATH') or define('UPLOAD_PATH','public/upload/'); // 编辑器图片上传路径
define('DYSHOP_CACHE_TIME',1); // DYshop 缓存时间  31104000
$http = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] && $_SERVER['HTTPS'] != 'off') ? 'https' : 'http';
define('SITE_URL',$http.'://'.$_SERVER['HTTP_HOST']); // 网站域名
define('HTTP',$http);
define('INSTALL_DATE',1463741583);
define('SERIALNUMBER','20210501hqkjlaowu');
// 定义时间
define('NOW_TIME',$_SERVER['REQUEST_TIME']);

// [ 应用入口文件 ]
require __DIR__ . '/../vendor/autoload.php';
// 定义应用目录
//define('APP_PATH', __DIR__ . '../app/');
//echo APP_PATH;
// 执行HTTP应用并响应
$http = (new App())->http;

$response = $http->run();

$response->send();

$http->end($response);
